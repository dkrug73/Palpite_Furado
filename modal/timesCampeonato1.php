<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
    include "../utils/funcoes.php";
    include "../classes.php";

    if (isset($_POST)) {
        $campeonato = new Campeonato();
        $mensagem = "Times gravados com sucesso.";

        $campeonatoId = $_POST['campeonatoId'];

        if (!$campeonato->ApagarTimesDoCampeonato($campeonatoId, $conexao)){
            $array  = array('mensagem' => "Erro ao apagar times do campeonato.", 'resultado' => false);  
            echo json_encode($array);
        }

        if (!ApagarJogosCampeonato($conexao,$campeonatoId)){
            $array  = array('mensagem' => "Erro ao apagar jogos do campeonato.", 'resultado' => false);  
            echo json_encode($array);
        }

        if (!$campeonato->ApagarTabelaCampeonato($conexao,$campeonatoId)){
            $array  = array('mensagem' => "Erro ao apagar jogos do campeonato.", 'resultado' => false);  
            echo json_encode($array);
        }

        $quantidadeTimesFormula = RetornaQuantidadeTimesFormula($conexao, $campeonatoId);
        $dadosCampeonato = RetornaDadosCampeonato($conexao, $campeonatoId);
        
        if(isset($_POST['timeId'])) {
            $timesA = $_POST['timeId'];	
            $quantidadeTimesA = count($timesA);

            if ($quantidadeTimesA == $quantidadeTimesFormula) {
                if (!$campeonato->GravarTimesCampeonato($campeonatoId, $timesA, "1", $conexao) ){
                    $array  = array('mensagem' => "Erro ao gravar os times do grupo A.", 'resultado' => false);  
                    echo json_encode($array);
                } 

                if (!$campeonato->GerarJogos($campeonatoId, $timesA, "1", $dadosCampeonato, $conexao)){
                    $array  = array('mensagem' => "Erro ao gravar os times do grupo A.", 'resultado' => false);  
                    echo json_encode($array);
                }
            }  
            else {
                $array  = array('mensagem' => "Informe corretamente a quantidade de times.", 'resultado' => false);  
                echo json_encode($array);
            } 
        }

        if(isset($_POST['timeId2'])) {
            $timesB = $_POST['timeId2'];	
            $quantidadeTimesB = count($timesB);

            if ($quantidadeTimesB == $quantidadeTimesFormula) {
                if (!$campeonato->GravarTimesCampeonato($campeonatoId, $timesB, "2", $conexao) ){
                    $array  = array('mensagem' => "Erro ao gravar os times do grupo B.", 'resultado' => false);  
                    echo json_encode($array);
                } 

                if (!$campeonato->GerarJogos($campeonatoId, $timesB, "2", $dadosCampeonato, $conexao)){
                    $array  = array('mensagem' => "Erro ao gravar os times do grupo B.", 'resultado' => false);  
                    echo json_encode($array);
                }
            }  
            else {
                $array  = array('mensagem' => "Informe corretamente a quantidade de times.", 'resultado' => false);  
                echo json_encode($array);
            } 
        }
    }    

    $array  = array('mensagem' => $mensagem, 'resultado' => true);  
    echo json_encode($array);
        
    mysqli_close($conexao);	
?>