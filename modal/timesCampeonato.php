<div class="modal-dialog modal-dialog-centered" role="document">
      <form action="paginas/configuracoes1.php" method="post"> 
            <div class="modal-content">
                  <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle" style="color: white;">TIMES DO CAMPEONATO</h1>                             
                  </div>            

                  <div class="modal-body">
                        <select class="select2" name="timeId[]" id="timeId[]" multiple="multiple" style="width: 100%;"> <?PHP
                              $sqlA = "
                                    SELECT 
                                          timeId, 
                                          times.nome as nomeTime                            
                                    FROM 
                                          times_campeonato INNER JOIN 
                                          times ON timeId = times.id 
                                    WHERE 
                                          times_campeonato.campeonatoId = '".$campeonato->Id."'
                                    ORDER BY 
                                          nomeTime";                       

                              $rsA=$conexao->query($sqlA);

                              $sql_time="
                                    SELECT 
                                          id, 
                                          nome                    
                                    FROM 
                                          times
                                    WHERE 
                                          id not in (SELECT timeId FROM times_campeonato WHERE campeonatoId = '".$campeonato->Id."')
                                    ORDER BY 
                                          nome;";                       

                              $rs_time=$conexao->query($sql_time);

                              while($idTimeA=mysqli_fetch_array($rsA)) {
                                    $itens_time = $itens_time."<option value='".$idTimeA['timeId']."' selected='selected'>".$idTimeA['nomeTime']."</option><br /> ";
                              }
                              
                              while($time=mysqli_fetch_array($rs_time))		
                              {	
                                    $itens_time = $itens_time."<option value='".$time['id']."'>".$time['nome']."</option><br /> ";   
                              }

                              print $itens_time; ?>       
                        </select> 
                  </div>  

                  <input type="hidden" name="campeonatoId" value="<?php print $campeonato->Id; ?>" >                
                  
                  <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Concluir</button>
                  </div>            
            </div>
      </form>
</div>
  



