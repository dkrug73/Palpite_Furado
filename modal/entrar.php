<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f9ddca;">
                <a href="#"><img src="imagens/logo2.png" style="width: 70%;"></a>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">  
                    <form method="post" action="paginas/validarAcesso.php" id="formLogin" style="padding-bottom: 10px;">
                        <div class="form-group">
                            <input type="text" class="form-control" id="campo_usuario" name="usuario" placeholder="Email ou Usuário" required>
                        </div>

                        <div class="form-group"> 
                            <input type="password" class="form-control" id="campo_senha" name="senha" placeholder="Senha" required>
                        </div>

                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="lembrete" value="SIM" > Lembrar senha
                            </label>
                        </div>

                        <button type="buttom" class="btn btn-primary" id="btn_login">Entrar</button>

                        <div class="row"> </div> 
                    </form> 
                    
                    <a href="cadastro.php">Cadastrar novo participante</a>

                    <?php   
                    if ($erro == 1){
                        echo '<font color="FF0000" style="margin-top: 10px;">Usuário ou senha inválidos.</font>';
                    } ?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModalSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f9ddca;">
                        <a href="#"><img src="imagens/logo2.png" style="width: 70%;"></a>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
            </div>
        
            <div class="modal-body">
                <form method="post" action="enviarSenha.php" id="enviar-email">
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Informe seu email">
                        <span id="erro-email" style="color: red;font-size: 14px;">  </span>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" id="enviar-senha">Enviar senha</button>
            </div>
        </div>
    </div>
</div>

<script>

$("#enviar-senha").click(function(){
    var email = document.getElementById("email").value;

    var salvar = true;
    document.getElementById("erro-email").innerHTML = "";

    if (email == "") {
        document.getElementById("erro-email").innerHTML = "Informe o email ou nome do usuário.";
        salvar = false;
    }

    if (salvar){
        $.ajax ({
            method: "post",
            url: "enviarSenha.php",
            data: { email: email },
            success: function(data) {
                if (data == 1) alert("Foi enviado para o email informado a nova senha.");
                else alert("Email não cadastrado.");
                $('#exampleModalSenha').modal('hide');
            },

            beforeSend: function(){
                var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Enviando...';
                $('#enviar-senha').html(loadingText);    
                $('#enviar-senha').attr('disabled', 'disabled');
            },

            complete: function(){
                $('#enviar-senha').html('Enviar senha');
                $('#enviar-senha').removeAttr('disabled');
            }
        });				
    }
});
</script>