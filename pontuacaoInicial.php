<?php 
	SESSION_START();

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$erro = '0';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }	
	
	$participanteId = "0";
    if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Cadastro da Pontuação Inicial</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/pontuacaoInicial.js"></script> 	
	<script src="componentes/js/utils.js"></script>
	<link rel="icon" type="image/png" href="imagens/favicon.png">	
</head>

<body>

	<?php include("componentes/menu.php"); ?>	

	<div class = "container">

		<div id="salvar-sucesso" style="display:none;">
			<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
				Pontuação inicial salva com sucesso!
			</div>           
		</div>

		<div id="salvar-erro" style="display:none;">
			<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
				Erro ao salvar pontuação inicial.
			</div> 
		</div>

		<div id="excluir-sucesso" style="display:none;">
			<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
				Pontuação inicial excluído com sucesso!
			</div>           
		</div>

		<div id="excluir-erro" style="display:none;">
			<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
				Erro ao excluir pontuação inicial.
			</div> 
		</div>

		<section class="titulo-pagina">
            <h3 class="tituloPagina">Pontuação inicial</h3>				
        </section>

		<section>	
			<form name="formulario-cadastro" id="formulario-cadastro" method="" action="" enctype="">

				<div class="form-group row">
					<label for="id" class="col-lg-2 col-md-2 col-form-label">ID</label>

					<div class="col-lg-10 col-md-10">
						<input type="text" class="form-control" id="id" name="id" value="" maxlength="100" readonly='yes'>
						<span id="erro-id" style="color: red;font-size: 14px;">  </span>
					</div>
				</div>

				<div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label" for="campeonato">Campeonato</label>
                
                    <div class="col-lg-10 col-md-10"> 
                        <select name="campeonatoId" id="campeonatoId" class="form-control" readonly='yes'>                        
                            <div id="comboCampeonato"></div>     
                        </select> 
                    </div>
                </div>

				<div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label" for="participante">Participante</label>
                
                    <div class="col-lg-10 col-md-10"> 
                        <select name="participanteId" id="participanteId" class="form-control" >                        
                            <div id="comboParticipante"></div>     
                        </select> 
						<span id="erro-participante" style="color: red;font-size: 14px;">  </span>
                    </div>					
                </div>

				<div class="form-group row">
                    <label for="rodada" class="col-lg-2 col-md-2 col-form-label">Rodada</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="rodada" name="rodada" value="" maxlength="2" >
                        <span id="erro-rodada" style="color: red;font-size: 14px;">  </span>
                 	</div>
                </div>				

				<div class="form-group row">
                    <label for="turno" class="col-lg-2 col-md-2 col-form-label">Turno</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="turno" name="turno" value="" maxlength="2" >
                        <span id="erro-turno" style="color: red;font-size: 14px;">  </span>
                 	</div>
                </div>

				<div class="form-group row">
                    <label for="pontos" class="col-lg-2 col-md-2 col-form-label">Pontos</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="pontos" name="pontos" value=""  >
                        <span id="erro-pontos" style="color: red;font-size: 14px;">  </span>
                 	</div>
                </div>				

				<div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="button" class="btn btn-primary mr-2" id="btn-salvar">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn-cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>

                    </div>
                </div>
			</form>

            <div id="resultado-tabela"></div>

		</section>
	</div>

	<!-- MODAL ENTRAR -->		
	<?php include("modal/entrar.php"); ?>
	
	<?php include("componentes/rodape.php"); ?>

	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>