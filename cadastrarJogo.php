<?PHP 
	SESSION_START();
	require_once('classes/ConexaoBancoDeDados.php');
	require_once('classes/Campeonato.php');

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$erro = '0';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }	

	$campeonato = new Campeonato($conexao);
	
	$_SESSION['campeonatoId'] = $campeonato->RetornaCampeonatoAtivo();
	
	$participanteId = "0";
    if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}	

?>

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Jogos</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" type="image/png" href="imagens/favicon.png">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/utils.js"></script>
	<script src="componentes/js/cadastrarJogo.js"></script>

	<script type="text/javascript" src="bootstrap/js/moment.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/pt-br.js"></script>
	<script type="text/javascript" src="bootstrap/js/tempusdominus-bootstrap-4.min.js"></script>
	<link rel="stylesheet" href="bootstrap/css/tempusdominus-bootstrap-4.min.css">	

	<script type="text/javascript">
		$(function () {
			$('#datetimepicker2').datetimepicker({
				locale: 'pt-br'
			});
		});

		$.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
			icons: {
				time: 'fa fa-clock',
				date: 'fa fa-calendar',
				up: 'fa fa-arrow-up',
				down: 'fa fa-arrow-down',
				previous: 'fa fa-chevron-left',
				next: 'fa fa-chevron-right',
				today: 'fa fa-calendar-check-o',
				clear: 'fa fa-trash',
				close: 'fa fa-times'
			}
		});

	</script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">
		
		<!-- MENU -->
		<?php include("componentes/menu.php"); ?>

		<div class = "container">

			<div id="salvar-sucesso" style="display:none;">
				<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
					Jogo salvo com sucesso!
				</div>           
			</div>

			<div id="salvar-erro" style="display:none;">
				<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
					Erro ao salvar jogo.
				</div> 
			</div>

			<div id="excluir-sucesso" style="display:none;">
				<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
					Jogo excluído com sucesso!
				</div>           
			</div>

			<div id="excluir-erro" style="display:none;">
				<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
					Erro ao excluir jogo.
				</div> 
			</div>

			<section class="titulo-pagina">
				<h3 class="tituloPagina">Cadastro do jogo</h3>				
			</section>
		
			<section>	
				<form name="formulario-cadastro" id="formulario-cadastro" method="" action="" 
						enctype="">

					<div class="form-group row">
						<label for="id" class="col-lg-2 col-md-2 col-form-label">ID</label>

						<div class="col-lg-10 col-md-10">
							<input type="text" class="form-control" id="id" name="id" value="" maxlength="100" readonly="yes" requiered>
							<span id="erro-id" style="color: red;font-size: 14px;">  </span>
						</div>
					</div>

					<div class="form-group row">
						<label for="inputPadrao" class="col-lg-2 col-md-2 col-form-label">Rodada</label>						
							<div class="col-lg-1 col-md-1">
								<input type="text" class="form-control" id="rodada" name="rodada" style="text-align: center;"
									value="" >
								<span id="erro-rodada" style="color: red;font-size: 14px;">  </span>
							</div>

							<span style="margin-top: 4px;margin-left: 15px;">Turno</span>

							<div class="col-lg-1 col-md-1">
								<input type="text" class="form-control" id="turno" name="turno" style="text-align: center;"
									value="" >
								<span id="erro-turno" style="color: red;font-size: 14px;">  </span>
							</div>
					</div>

					<div class="form-group row">
						<label for="id" class="col-lg-2 col-md-2 col-form-label">Data do jogo</label>

                        <div class="col-lg-3 col-md-3">                            
                           
						<div class="input-group date" id="datetimepicker2" data-target-input="nearest">
							<input id="dataJogo" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
							<div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>							
						</div>
						<span id="erro-dataJogo" style="color: red;font-size: 14px;">  </span>
						
						</div>
					</div>

					<div class="form-group row">
						<label for="inputPadrao" class="col-lg-2 col-md-2 col-form-label">Confronto</label>
						
							<div class="col-md" style="min-width: 240px;max-width: 240px;">                           
								<select  name="mandanteId" id="mandanteId" class="form-control col-md" >
									<?php include("paginas/combos/carregarComboTime.php") ?>                            
								</select>
								<span id="erro-mandanteId" style="color: red;font-size: 14px;">  </span>
							</div>

							<span style="margin-top: 4px;">x</span>

							<div class="col-md" style="min-width: 240px;max-width: 240px;">                           
								<select  name="visitanteId" id="visitanteId" class="form-control col-md" >
									<?php include("paginas/combos/carregarComboTime.php") ?>                            
								</select>
							</div>
					</div>

					<div class="form-group row">
						<label for="estadio" class="col-lg-2 col-md-2 col-form-label">Estádio</label>

						<div class="col-lg-10 col-md-10">
							<input type="text" class="form-control" id="estadio" name="estadio" value="" maxlength="100">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-2 col-md-2 col-form-label"></div>
						
						<div class="col-lg-10 col-md-10">
							<button type="button" class="btn btn-primary mr-2" id="btn_salvar">Salvar</button>

							<button type="reset" class="btn btn-default mr-2" id="btn_cancelar">Cancelar</button>

							<button type="button" class="btn btn-danger mr-2" id="btn_excluir">Excluir</button>
						</div>
					</div>
				</form>

				<div id="resultado-tabela"></div>

			</section>
		
		</div>

		<!-- MODAL ENTRAR -->		
		<?php include("modal/entrar.php"); ?>

		<!-- RODAPE -->		
		<?php include("componentes/rodape.php"); ?>
	</div>
	<!-- ./wrapper -->
	
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>