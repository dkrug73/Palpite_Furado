<?PHP
    SESSION_START();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Participante.php');
    require_once('../classes/Rodada.php');
    require_once('../classes/Jogo.php');
    require_once('../classes/Campeonato.php');
    require_once('../classes/Palpite.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

    $campeonatoId = $_SESSION['campeonatoId'];    
    $rodada = $_POST['rodada'];
    $total_registros = $_POST['total_registros'];
    
    $palpite = new Palpite($conexao);  
    $palpite->CampeonatoId = $campeonatoId;
    $palpite->Rodada = $rodada;

    if (!$palpite->GravarPalpitePadrao()) {
        echo 2;
    }
    else {    
        for ($i = 1; $i <= $total_registros; $i++) {
            $nomeJogo = "txtjogo".$i;
            $jogoId = $_POST[$nomeJogo];		
            $mandante = "txtmandante".$i;
            $visitante = "txtvisitante".$i;
            $placarMandante = $_POST[$mandante];
            $placarVisitante = $_POST[$visitante];

            if ($placarMandante != "" && $placarVisitante != "") {		
                $jogo = new Jogo($conexao);
                $jogo->Id = $jogoId;
                $jogo->PlacarMandante = $placarMandante;
                $jogo->PlacarVisitante = $placarVisitante;

                if (!$jogo->AtualizarJogo()) echo 3;	 
                
                if (!$palpite->AtualizarPalpites($jogo)) echo 4;                    

                unset($jogo);
            }
        }	

        echo 0;
    }
?>