<?php
    session_start();
    require_once('classes/FormulaJogos.php');

    if(!isset($_SESSION['nome_usuario_botao'])){
        header('Location: index.php?erro=1');
    }

    $formulaId = null;
    if (isset($_GET['formula-id'])){
        $formulaId = $_GET['formula-id'];
    }

    $erro=null;
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro da Fórmula dos Jogos</title>

    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->
    <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/jquery/jquery-3.3.1.min.js"></script> 
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/js/cadastroFormulaJogos.js"></script> 
    <script src="componentes/js/utils.js"></script> 

    <script>
    $(document).ready(function(){
        
        $.ajax({
            type: 'post',
            url: 'paginas/combos/carregarComboFormulas.php',
            success: function(data){
                $('#formulaId').html(data);
                document.getElementById("formulaId").value = <?php echo $formulaId ?>;
                document.getElementById('rodada').focus();
                CarregarTabelaFormulas(<?php echo $formulaId ?>);                
            }
        });
     
    });    
    
    </script>
</head>
<body>        
    <?php require 'componentes/menu.php'; ?>
    
    <div class="container corpo">  

        <?php require 'componentes/alerta.php'; ?>

        <section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro da Fórmula dos Jogos</h3>				
        </section>

        <section>
            <form id="formulario-formula-jogos" name="formulario-formula-jogos">
                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label">ID</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="formula-jogos-id" name="formula-jogos-id" 
                            placeholder="" readonly value="">
                    </div>
                </div>  

                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label" for="formula">Formula</label>
                
                    <div class="col-lg-10 col-md-10">
                        <select name="formulaId" id="formulaId" class="form-control"></select> 
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="rodada" class="col-lg-2 col-md-2 col-form-label">Rodada</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="rodada" name="rodada" value="" required autocomplete="off" maxlength="2">
                    </div>
                </div>            

                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label" for="confronto">Confronto</label>

                    <div class="col-md-1">
                        <input type="text" class="form-control" id="mandante" name="mandante" value="" required style="width: 50px;" maxlength="2" autocomplete="off">
                    </div>

                    <div class="col-md-1">
                        <label class="col-md-1 col-form-label" for="confronto">x</label>
                    </div>

                    <div class="col-md-1">
                        <input type="text" class="form-control" id="visitante" name="visitante" value="" required style="width: 50px;" maxlength="2" autocomplete="off">
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="submit" class="btn btn-primary mr-2">Salvar</button>

                        <button type="button" class="btn btn-default mr-2" id="btn-cancelar" >Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>                        

                    </div>
                </div>
                </div>
            </form>     
            <div class="container"> 
                <div id="resultado-tabela"></div>               
            </div>

        </section>
    </div>

    <?php require 'componentes/rodape.php'; ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="componentes/bootstrap/js/bootstrap.min.js"></script>

      
</body>
</html>