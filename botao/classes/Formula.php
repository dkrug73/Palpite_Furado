<?php
    class Formula{
        public $Id;
        public $Descricao;
        public $QuantidadeTimes;
        public $Conexao;

        public function setId($parametroId){
            $this->Id = $parametroId;
        }

        public function getId(){
            return $this->Id;
        }

        public function setDescricao($parametroDescricao){
            $this->Descricao = $parametroDescricao;
        }

        public function getDescricao(){
            return $this->Descricao;
        }

        public function setQuantidadeTimes($parametroQuantidade){
            $this->QuantidadeTimes = $parametroQuantidade;
        }

        public function getQuantidadeTimes(){
            return $this->QuantidadeTimes;
        }

        public function setConexao($parametroConexao){
            $this->Conexao = $parametroConexao;
        }

        function __construct($conexao){
            $this->Conexao = $conexao;
        }

        public function GravarFormula(){
            if(!empty($formula->Id)){
                $sql = "
                    UPDATE 
                        formulas
                    SET
                        descricao = '$this->Descricao',
                        quantidadeTimes = '$this->QuantidadeTimes'
                    WHERE
                        id = '$this->Id' ";
                        
                if (mysqli_query($this->Conexao, $sql)) return $this->Id;
                else return false;

            }           
            else{
                $sql = "
                    INSERT INTO formulas (descricao, quantidadeTimes)
                    VALUES ('$this->Descricao', '$this->QuantidadeTimes') ";

                if($this->Conexao->query($sql)) return $this->Conexao->insert_id;    
                else return false; 
            }
        }

        public function ObterDadosFormulas(){
            $sql = "
                SELECT
                    id,
                    descricao,
                    quantidadeTimes
                FROM
                    formulas ";

            return $this->Conexao->query($sql);
        }

        public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;
        }
    }
?>