<?php   
    class FormulaJogos{
        public $Id;
        public $FormulaId;
        public $Rodada;
        public $Mandante;
        public $Visitante;
        public $Conexao;

        public function setId($parametroId){
            $this->Id = $parametroId;
        }

        public function getId(){
            return $this->Id;
        }

        public function setFormulaId($parametroFormulaId){
            $this->FormulaId = $parametroFormulaId;
        }

        public function getFormulaId(){
            return $this->FormulaId;
        }

        public function setRodada($parametroRodada){
            $this->Rodada = $parametroRodada;
        }

        public function getRodada(){
            return $this->Rodada;
        }

        public function setMandante($parametroMandante){
            $this->Mandante = $parametroMandante;
        }

        public function getMandante(){
            return $this->Mandante;
        }

        public function setVisitante($parametroVisitante){
            $this->Visitante = $parametroVisitante;
        }

        public function getVisitante(){
            return $this->Visitante;
        }

        public function setConexao($parametroConexao){
            $this->Conexao = $parametroConexao;
        }

        function __construct($conexao){
            $this->Conexao = $conexao;
        }

        public function GravarFormulaJogo($formulaJogos){
            $sql = "";

            if(empty($formulaJogos->Id)){
                  $sql = "
                    INSERT INTO formulaJogos (formulaId, rodada, mandante, visitante)
                    VALUES ('$this->FormulaId', '$this->Rodada', '$this->Mandante', '$this->Visitante') ";
            }
            else{
                $sql = "
                    UPDATE 
                        formulaJogos
                    SET
                        formulaId = '$this->FormulaId',
                        rodada = '$this->Rodada',
                        mandante = '$this->Mandante',
                        visitante = '$this->Visitante'
                    WHERE
                        id = '$this->Id' ";
            }

            return mysqli_query($this->Conexao, $sql);
        }

        public function ObterDadosFormulaJogos(){
            $sql = "
                SELECT 
                    fj.id, 
                    fj.formulaId,
                    f.descricao, 
                    fj.rodada, 
                    fj.mandante, 
                    fj.visitante 
                FROM 
                    formulajogos fj                 
                INNER JOIN 
                    formulas f ON 
                        f.id = fj.formulaId 
                WHERE
                    fj.formulaId = '$this->FormulaId'";

        return $this->Conexao->query($sql);

        }

        public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;
        }
    }
?>