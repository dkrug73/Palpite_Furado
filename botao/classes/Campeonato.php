<?php
class Campeonato
{
    public $Id;
    public $Descricao;
    public $Local;
    public $Edicao;
    public $ClasseRanking;
    public $Data;
    public $Ativo;
    public $FormulaId;
    public $TipoFinal;
    public $DoisGrupos;
    public $DoisTurnos;
    public $EvitarConfrontos;
    public $Jogos;


    function setId($campeonatoId){
        $this->Id = $campeonatoId;
    }

    function getId(){
        return $this->Id;
    }

    function setDescricao($descricaoCampeonato){
        $this->Descricao = $descricaoCampeonato;
    }

    function getDescricao(){
        return $this->Descricao;
    }

    function setLocal($localCampeonato){
        $this->Local = $localCampeonato;
    }

    function getLocal(){
        return $this->Local;
    }

    function setEdicao($edicaoCampeonato){
        $this->Edicao = $edicaoCampeonato;
    }

    function getEdicao(){
        return $this->Edicao;
    }

    function setClasseRanking($classeRankingCampeonato){
        $this->ClasseRanking = $classeRankingCampeonato;
    }

    function getClasseRanking(){
        return $this->ClasseRanking;
    }

    function setData($dataCampeonato){
        $this->Data = $dataCampeonato;
    }

    function getData(){
        return $this->Data;
    }

    function setAtivo($campeonatoAtivo){
        $this->Ativo = $campeonatoAtivo;
    }

    function getAtivo(){
        return $this->Ativo;
    }

    function setFormulaId($parametroFormulaId){
        $this->FormulaId = $parametroFormulaId;
    }

    function getFormulaId(){
        return $this->FormulaId;
    }

    function setTipoFinal($parametroTipoFinal){
        $this->TipoFinal = $parametroTipoFinal;
    }

    function getTipoFinal(){
        return $this->TipoFinal;
    }

    function setDoisGrupos($parametroDoisGrupos){
        $this->DoisGrupos = $parametroDoisGrupos;
    }

    function getDoisGrupos(){
        return $this->DoisGrupos;
    }

    function setDoisTurnos($parametroDoisTurnos){
        $this->DoisTurnos = $parametroDoisTurnos;
    }

    function getDoisTurnos(){
        return $this->DoisTurnos;
    }

    function setEvitarConfrontos($parametroEvitarConfrontos){
        $this->EvitarConfrontos = $parametroEvitarConfrontos;
    }

    function getEvitarConfrontos(){
        return $this->EvitarConfrontos;
    }

    function setJogos($parametroJogos){
        $this->Jogos = $parametroJogos;
    }

    function getJogos(){
        return $this->Jogos;
    }

    public function setConexao($parametroConexao){
        $this->Conexao = $parametroConexao;
    }

    function __construct($conexao){
        $this->Conexao = $conexao;
    }
    
    public function GravarCampeonato(){
        if (!empty($this->Id)){
            $sql = "
                UPDATE 
                    campeonatos
                SET
                    descricao = '$this->Descricao',
                    local = '$this->Local', 
                    edicao = '$this->Edicao', 
                    classeRanking = '$this->ClasseRanking', 
                    data = '$this->Data', 
                    ativo = '$this->Ativo'
                WHERE
                    id = '$this->Id' ";

            if ($this->Conexao->query($sql)) return $this->Id;
        }
        else {
            $sql = "INSERT INTO campeonatos (descricao, local, edicao, classeRanking, data, ativo)
                VALUES ('$this->Descricao', '$this->Local', '$this->Edicao', '$this->ClasseRanking', '$this->Data', '$this->Ativo') ";
                
            if ($this->Conexao->query($sql)) return $this->Conexao->insert_id;    
            else return false;
        }        
    }

    public function AtualizarConfiguracoes(){
        $sql = "
            UPDATE 
                campeonatos
            SET
                formulaId = '$this->FormulaId',
                jogos = '$this->Jogos',
                tipoFinal = '$this->TipoFinal', 
                doisGrupos = '$this->DoisGrupos', 
                doisTurnos = '$this->DoisTurnos', 
                evitarConfrontos = '$this->EvitarConfrontos'
            WHERE
                id = '$this->Id' ";

        return $this->Conexao->query($sql);
    }

    public function InserirTimeCampeonato($campeonatoId, $timeId, $grupo){  
        $sql = "
            INSERT INTO timesCampeonato (campeonatoId, timeId, grupo)
            VALUES ('$campeonatoId', '$timeId', '$grupo') ";

        return mysqli_query($this->Conexao, $sql);
    }

    public function ApagarTimesCampeonato($id){
        $sql = "
            DELETE FROM timesCampeonato WHERE campeonatoId = '".$id."' ";
            
        $this->Conexao->query($sql);
    }

    public function ObterConfiguracoesCampeonato($id){
        $sql = "
            SELECT 
                doisGrupos,
                doisTurnos,
                tipoFinal
            FROM
                campeonatos
            WHERE 
                id = '$id' ";

        $rs = $this->Conexao->query($sql);

        $reg = mysqli_fetch_array($rs);

        return array($reg['doisGrupos'], $reg['doisTurnos'], $reg['tipoFinal']);
    }

    public function ObterDadoscampeonato(){
        $sql = "
            SELECT
                id,
                descricao,
                local,
                edicao,
                classeRanking,
                date_format(data, '%d/%m/%Y') AS data,
                CASE ativo 
                    WHEN 0 THEN '' 
                    WHEN 1 THEN 'sim' 
                END AS ativo,
                doisGrupos,
                doisTurnos,
                evitarConfrontos,
                formulaId,
                jogos,
                tipoFinal
            FROM
                campeonatos ";

        return $this->Conexao->query($sql);
    }

    public function RetornarCampeaoVice(){
        $sql = "
            SELECT
                ativo,
                (SELECT nome FROM times WHERE id = campeao) AS campeao,
                (SELECT nome FROM times WHERE id = vice) AS vice
            FROM
                campeonatos 
            WHERE
                id = '$this->Id'";

        $rs = $this->Conexao->query($sql);
        $reg=mysqli_fetch_array($rs);

        return array($reg['ativo'], $reg['campeao'], $reg['vice']);
    }

    public function GerarJogoscampeonato($grupoA, $grupoB){
        if(!$this->ApagarJogosCampeonato()) return false;        
     
        $sql = "
            SELECT 
                mandante, 
                visitante, 
                rodada 
            FROM 
                formulaJogos 
            WHERE 
                formulaId = '$this->FormulaId'
            ORDER BY
                rodada,
                id ";

        $rs=$this->Conexao->query($sql);

        $ordem = 1;
        while($reg=mysqli_fetch_array($rs)) {
            $mandante=$reg['mandante'];
            $visitante=$reg['visitante'];
            $rodada=$reg['rodada'];           

            $mandanteId = $grupoA[$mandante];
            $visitanteId = $grupoA[$visitante];

            if ($this->EvitarConfrontos && $this->TimesTecnicosIguais($mandanteId, $visitanteId)) continue;

            if ($this->DoisGrupos){
                $mandanteIdGrupoB = $grupoB[$mandante];
                $visitanteIdGrupoB = $grupoB[$visitante];

                if ($this->EvitarConfrontos && $this->TimesTecnicosIguais($mandanteIdGrupoB, $visitanteIdGrupoB)) continue;

                if(!$this->GravarJogo($mandanteId, $visitanteId, $rodada, 1, "A", $ordem)) return false; 
                if(!$this->GravarJogo($mandanteIdGrupoB, $visitanteIdGrupoB, $rodada, 1, "B", $ordem)) return false; 
                // grava jogos do grupo A e do primeiro turno
                // grava jogos do grupo B e do primeiro turno

                if ($this->DoisTurnos){        
                    if(!$this->GravarJogo($visitanteId, $mandanteId, $rodada, 2, "A", $ordem)) return false; 
                    if(!$this->GravarJogo($visitanteIdGrupoB, $mandanteIdGrupoB, $rodada, 2, "B", $ordem)) return false;   
                    // grava jogos do grupo A e do segundo turno
                    // grava jogos do grupo B e do segundo turno
                }
            }
            else{
                if(!$this->GravarJogo($mandanteId, $visitanteId, $rodada, 1, "A", $ordem)) return false; 
                // grava jogos do primeiro turno

                if ($this->DoisTurnos){
                    if(!$this->GravarJogo($visitanteId, $mandanteId, $rodada, 2, "A", $ordem)) return false;  
                    // grava jogos do segundo turno
                }
            }

            $ordem++;
        } 

        return true;
    }

    public function GerarTabelacampeonato($grupoA, $grupoB){
        if(!$this->ApagarTabelasCampeonato()) return false;

        if ($this->DoisGrupos){
            $mandanteIdGrupoB = $grupoB[$mandante];
            $visitanteIdGrupoB = $grupoB[$visitante];

            if(!$this->GravarTabela($grupoA, 1, "A")) return false;
            if(!$this->GravarTabela($grupoB, 1, "B")) return false;            
            // grava tabela do grupo A e do primeiro turno
            // grava tabela do grupo B e do primeiro turno

            if ($this->DoisTurnos){    
                if(!$this->GravarTabela($grupoA, 2, "A")) return false;
                if(!$this->GravarTabela($grupoB, 2, "B")) return false;
                // grava tabela do grupo A e do segundo turno
                // grava tabela do grupo B e do segundo turno
            }
        }
        else{
            if(!$this->GravarTabela($grupoA, 1, "A")) return false;        
            // grava tabela do primeiro turno

            if ($this->DoisTurnos){
                if(!$this->GravarTabela($grupoA, 2, "A")) return false;                
                // grava tabela do segundo turno
            }
        }
    }

    public function RetornaTimesGrupo($grupo) {
        $sql = "
            SELECT 
                timeId 
            FROM 
                timesCampeonato 
            WHERE 
                grupo = '".$grupo."' AND
                campeonatoId = '$this->Id'
            ORDER BY 
                id";
    
        $rs = $this->Conexao->query($sql);
        
        $array[] = null;
        while ($reg = mysqli_fetch_array($rs)) {
            $array[] = $reg['timeId'];
        }
                
        return $array;
    }

    private function TimesTecnicosIguais($mandanteId, $visitanteId) {
        $sql = "
        SELECT 
            tecnico as tecnicoMandante, 
            (SELECT tecnico FROM times WHERE id = '$visitanteId') AS tecnicoVisitante 
        FROM 
            times 
        WHERE 
            id = '$mandanteId' ";
        
        $rs = $this->Conexao->query($sql);    
        $reg=mysqli_fetch_array($rs);        
        return $reg['tecnicoMandante'] == $reg['tecnicoVisitante'];
    }

    private function GravarJogo($mandanteId, $visitanteId, $rodada, $turno, $grupo, $ordem) {                
        $sql = "INSERT INTO jogos
                      (campeonatoId, mandanteId, visitanteId, rodada, turno, grupo, ordem) VALUES 
                      ($this->Id, $mandanteId, $visitanteId, $rodada, $turno, '$grupo', $ordem)";
    
        return $this->Conexao->query($sql);
    }

    private function GravarTabela($equipes, $turno, $grupo) {
        foreach($equipes as $equipe) {
            if ($equipe <> null) { 
                $timeNome = $this->RetornaNomeTime($equipe);
        
                $sql = "INSERT INTO tabela (jogoId, campeonatoId, timeId, timeNome, pontoGanho, vitoria, empate, derrota, golFavor, golContra, turno, grupo) 
                    VALUES ('0', '$this->Id','$equipe', '$timeNome', 0, 0, 0, 0, 0, 0, '$turno', '$grupo')";
        
                if(!$this->Conexao->query($sql)) return false; 
            }
        }
    
        return true;
    }

    private function RetornaNomeTime($timeId) {
        $sql = "
            SELECT 
                nome
            FROM 
                times 
            WHERE 
                id = '$timeId' ";
        
        $rs =$this->Conexao->query($sql);    
        $reg=mysqli_fetch_array($rs);
        
        return $reg['nome'];
    }

    private function ApagarJogosCampeonato() {
        return $this->Conexao->query("DELETE FROM jogos WHERE campeonatoId = '$this->Id' ");
    }
    
    private function ApagarTabelasCampeonato() {
        return $this->Conexao->query("DELETE FROM tabela WHERE campeonatoId = '$this->Id' ");
    }

    public function CarregarGridTabela($grupo, $turno) {
        $sql="
            SELECT
                campeonatoId,
                timeId,
                timeNome,
                SUM(pontoGanho) AS pontoGanho,
                SUM(vitoria) AS vitoria,
                SUM(empate) AS empate,
                SUM(derrota) AS derrota,
                SUM(golFavor) AS golFavor,
                SUM(golContra) AS golContra,
                turno,
                grupo,
                (SUM(golFavor) - SUM(golContra)) AS saldo
            FROM 
                tabela 
            WHERE 
                campeonatoId = '$this->Id' ";

                if (isset($grupo)) $sql = $sql." AND grupo = '$grupo' ";
                if (isset($turno)) $sql = $sql." AND turno = '$turno' ";

                $sql = $sql."
            GROUP BY 
                timeId
            ORDER BY
                pontoGanho DESC,
                saldo DESC,
                vitoria DESC,
                golFavor DESC,
                timeNome ASC";
    
        return $this->Conexao->query($sql);	
    }

    public function ObterDadosGoleadoresPorTime(){
        $sql = "
            SELECT 
                jogadores.numero AS numero, 
                jogadores.nome AS jogadorNome, 
                SUM(jogoJogador.gols) AS gols, 
                times.nome AS timeNome 
            FROM 
                jogoJogador INNER JOIN 
                jogos ON 
                    jogoJogador.jogoId = jogos.id INNER JOIN 
                jogadores ON 
                    jogadores.id = jogoJogador.jogadorId INNER JOIN 
                times ON 
                    times.id = jogadores.timeId 
            WHERE 
                jogos.campeonatoId = '$this->Id' 
            GROUP BY 
                jogoJogador.jogadorId  
            ORDER BY 
                timeNome, 
                gols DESC, 
                jogadorNome ASC";

        return $this->Conexao->query($sql);
    }

    public function ObterDadosGoleadoresPorCampeonato(){
        $sql = "
            SELECT 
                jogadores.numero AS numero, 
                jogadores.nome AS jogadorNome, 
                SUM(jogoJogador.gols) AS gols, 
                times.nome AS timeNome 
            FROM 
                jogoJogador INNER JOIN 
                jogos ON 
                    jogoJogador.jogoId = jogos.id INNER JOIN 
                jogadores ON 
                    jogadores.id = jogoJogador.jogadorId INNER JOIN 
                times ON 
                    times.id = jogadores.timeId 
            WHERE 
                jogos.campeonatoId = '$this->Id' 
            GROUP BY 
                jogoJogador.jogadorId  
            ORDER BY                 
                gols DESC, 
                jogadorNome ASC,
                timeNome ";

        return $this->Conexao->query($sql);
    }

    public function ObterDadosRanking(){
        $sql = "
            SELECT
                timeNome AS timeNome,
                (SELECT nome FROM usuarios WHERE id = times.tecnico) AS tecnico,
                timeId,
                SUM(pontoGanho) AS pontos
                
            FROM 
                tabela INNER JOIN 
                campeonatos ON 
                    tabela.campeonatoId = campeonatos.id INNER JOIN
                times ON 
                    times.id = tabela.timeId
            WHERE
                tabela.turno <> 'G' AND
                campeonatos.classeRanking = (SELECT classeRanking FROM campeonatos WHERE id = '$this->Id') 
            GROUP BY	
                timeId
            ORDER BY
                pontos DESC,
                timeNome ASC";

        return $this->Conexao->query($sql);
    }

    public function ObterDadosGoleadoresPorRanking(){
        $sql = "
            SELECT 
                jogadores.numero AS numero, 
                jogadores.nome AS jogadorNome, 
                SUM(jogoJogador.gols) AS gols, 
                times.nome AS timeNome 
            FROM 
                jogoJogador INNER JOIN 
                jogos ON 
                    jogoJogador.jogoId = jogos.id INNER JOIN 
                jogadores ON 
                    jogadores.id = jogoJogador.jogadorId INNER JOIN 
                times ON 
                    times.id = jogadores.timeId INNER JOIN 
                campeonatos ON 
                    jogos.campeonatoId = campeonatos.id
            WHERE 
                campeonatos.classeRanking = (SELECT classeRanking FROM campeonatos WHERE id = '$this->Id') 
            GROUP BY 
                jogoJogador.jogadorId  
            ORDER BY                 
                gols DESC, 
                jogadorNome ASC,
                timeNome ";

        return $this->Conexao->query($sql);
    }

    public function FecharConexao(){
        mysqli_close($this->Conexao);
        $this->Conexao = null;
    }
}
?>