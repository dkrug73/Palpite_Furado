<?php
class Jogo{
    public $Id;
    public $CampeonatoId;
    public $MandanteId;
    public $MandanteNome;
    public $VisitanteId;
    public $VisitanteNome;
    public $PlacarMandante;
    public $PlacarVisitante;
    public $PenaltiMandante;
    public $PenaltiVisitante;
    public $Rodada;
    public $Turno;
    public $Confronto;
    public $Grupo;
    public $Ordem;

    public function setId($parametroId){
        $this->Id = $parametroId;
    }

    public function getId(){
        return $this->Id;
    }

    public function setCampeonatoId($parametroCampeonatoId){
        $this->CampeonatoId = $parametroCampeonatoId;
    }

    public function getCampeonatoId(){
        return $this->CampeonatoId;
    }

    public function setMandanteId($parametroMandanteId){
        $this->MandanteId = $parametroMandanteId;
    }

    public function getMandanteId(){
        return $this->MandanteId;
    }

    public function setMandanteNome($parametroMandanteNome){
        $this->MandanteNome = $parametroMandanteNome;
    }

    public function getMandanteNome(){
        return $this->MandanteNome;
    }

    public function setPlacarMandante($parametroPlacarMandante){
        $this->PlacarMandante = $parametroPlacarMandante;
    }

    public function getPlacarMandante(){
        return $this->PlacarMandante;
    }

    public function setVisitanteId($parametroVisitanteId){
        $this->VisitanteId = $parametroVisitanteId;
    }

    public function getVisitanteId(){
        return $this->VisitanteId;
    }

    public function setVisitanteNome($parametroVisitanteNome){
        $this->VisitanteNome = $parametroVisitanteNome;
    }

    public function getVisitanteNome(){
        return $this->VisitanteNome;
    }

    public function setPlacarVisitante($parametroPlacarVisitante){
        $this->PlacarVisitante = $parametroPlacarVisitante;
    }

    public function getPlacarVisitante(){
        return $this->PlacarVisitante;
    }

    public function setPenaltiMandante($parametroPenaltiMandante){
        $this->PenaltiMandante = $parametroPenaltiMandante;
    }

    public function getPenaltiMandante(){
        return $this->PenaltiMandante;
    }

    public function setPenaltiVisitante($parametroPenaltiVisitante){
        $this->PenaltiVisitante = $parametroPenaltiVisitante;
    }

    public function getPenaltiVisitante(){
        return $this->PenaltiVisitante;
    }

    public function setRodada($parametroRodada){
        $this->Rodada = $parametroRodada;
    }

    public function getRodada(){
        return $this->Rodada;
    }

    public function setTurno($parametroTurno){
        $this->Turno = $parametroTurno;
    }

    public function getTurno(){
        return $this->Turno;
    }

    public function setConfronto($parametroConfronto){
        $this->Confronto = $parametroConfronto;
    }

    public function getConfronto(){
        return $this->Confronto;
    }

    public function setGrupo($parametroGrupo){
        $this->Grupo = $parametroGrupo;
    }

    public function getGrupo(){
        return $this->Grupo;
    }

    public function setOrdem($parametroOrdem){
        $this->Ordem = $parametroOrdem;
    }

    public function getOrdem(){
        return $this->Ordem;
    }

    public function setConexao($parametroConexao){
        $this->Conexao = $parametroConexao;
    }

    function __construct($conexao){
        $this->Conexao = $conexao;
    }

    public function GravarJogo(){
        if(empty($this->Id)){        
            $sql = "
            INSERT INTO jogos (
                campeonatoId, mandanteId, visitanteId, rodada, turno, grupo, ordem) 
            VALUES (
                '$this->CampeonatoId', '$this->MandanteId', '$this->VisitanteId', '$this->Rodada', '$this->Turno', '$this->Grupo', '$this->Ordem') ";
        }
        else{
            $sql = "
                UPDATE 
                    jogos 
                SET 
                    campeonatoId = '$this->CampeonatoId',       
                    mandanteId = '$this->MandanteId',
                    visitanteId = '$this->VisitanteId',
                    rodada = '$this->Rodada',
                    turno = '$this->Turno',
                    grupo = '$this->Grupo',
                    ordem ='$this->Ordem'
                WHERE
                        id = '$this->Id' ";                
        }

        return mysqli_query($this->Conexao, $sql);
    }

    public function GravarJogoRetornaId(){
        $sql = "
            INSERT INTO jogos (
                campeonatoId, mandanteId, visitanteId, rodada, turno, grupo, ordem, placarMandante, placarVisitante) 
            VALUES (
                '$this->CampeonatoId', '$this->MandanteId', '$this->VisitanteId', '$this->Rodada', '$this->Turno', '$this->Grupo', 
                '$this->Ordem', $this->PlacarMandante, $this->PlacarVisitante) ";
        
        $this->Conexao->query($sql);

        return $this->Conexao->insert_id;  
    }

    public function Atualizarjogo(){
        if (isset($this->PenaltiMandante)){
            $sql = "
                UPDATE
                    jogos
                SET 
                    penaltiMandante = $this->PenaltiMandante, 
                    penaltiVisitante = $this->PenaltiVisitante    
                WHERE 
                    id = '$this->Id' ";
        }
        else{
            $sql = "
                UPDATE
                    jogos
                SET
                    placarMandante = $this->PlacarMandante,
                    placarVisitante = $this->PlacarVisitante                    
                WHERE 
                    id = '$this->Id' ";
        }

        return mysqli_query($this->Conexao, $sql);
    }

    public function ObterDadosJogos(){
        $sql = "
            SELECT 
                id,
                mandanteId, 
                visitanteId, 
                (SELECT nome FROM times WHERE id = mandanteId) AS mandanteNome, 
                (SELECT nome FROM times WHERE id = visitanteId) AS visitanteNome,
                (SELECT foto FROM times WHERE id = mandanteId) AS mandanteFoto, 
                (SELECT foto FROM times WHERE id = visitanteId) AS visitanteFoto,
                placarMandante, 
                placarVisitante, 
                penaltiMandante, 
                penaltiVisitante, 
                rodada, 
                turno, 
                grupo, 
                ordem
            FROM 
                jogos
            WHERE
                campeonatoId = '$this->CampeonatoId'
            ORDER BY  
                turno, 
                grupo,
                ordem, 
                id ";

        return $this->Conexao->query($sql);
    }

    public function ObterDadosJogosTurnoGrupo($grupo, $turno){
        $sql = "
            SELECT 
                id,
                mandanteId, 
                visitanteId, 
                (SELECT nome FROM times WHERE id = mandanteId) AS mandanteNome, 
                (SELECT nome FROM times WHERE id = visitanteId) AS visitanteNome,
                (SELECT foto FROM times WHERE id = mandanteId) AS mandanteFoto, 
                (SELECT foto FROM times WHERE id = visitanteId) AS visitanteFoto,
                placarMandante, 
                placarVisitante, 
                penaltiMandante, 
                penaltiVisitante, 
                rodada, 
                turno, 
                grupo, 
                ordem
            FROM 
                jogos
            WHERE
                campeonatoId = '$this->CampeonatoId' ";

            if (isset($grupo)) $sql = $sql." AND grupo = '$grupo' ";
            if (isset($turno)) $sql = $sql." AND turno = '$turno' ";

            $sql = $sql."ORDER BY  
                turno, 
                grupo,
                ordem, 
                id ";

        return $this->Conexao->query($sql);
    }

    public function GravarGolsJogador($jogadores){
        foreach ($jogadores as $jogador) {
            $jogadorId = $jogador['id'];
            $jogadorGols = $jogador['gols'];

            $sql = "INSERT INTO jogoJogador (jogoId, jogadorId, gols) VALUES
            ('$this->Id', '$jogadorId', '$jogadorGols') ";
            
            if (!$this->Conexao->query($sql)) return false;
        }

        return true;
    }

    public function AtualizarTabelasClassificacao($inclusao) {
        if ($inclusao){   
            $pontoGanhoMandante = 0;
            $vitoriaMandante = 0;
            $empateMandante = 0;
            $derrotaMandante = 0;   
            $pontoGanhoVisitante = 0;
            $vitoriaVisitante = 0;
            $empateVisitante = 0;
            $derrotaVisitante = 0;  

            $this->Turno = $this->RetornaTurnoJogo();
            $this->Grupo = $this->RetornaGrupoJogo();
        
            $golsMandante = $this->PlacarMandante;
            $golsVisitante = $this->PlacarVisitante;
        
            if ($this->PlacarMandante == $this->PlacarVisitante) {
                $pontoGanhoMandante = 1;
                $empateMandante = 1;
                $pontoGanhoVisitante = 1;
                $empateVisitante = 1;
            }
        
            else if ($this->PlacarMandante > $this->PlacarVisitante) {
                $pontoGanhoMandante = 3;
                $vitoriaMandante = 1;   
                $derrotaVisitante = 1;
            }
        
            else  {
                $derrotaMandante = 1;
                $pontoGanhoVisitante = 3;
                $vitoriaVisitante = 1;
            }
           
            $dadosMandante = array($this->MandanteId, $this->MandanteNome, $pontoGanhoMandante, 
                $vitoriaMandante, $empateMandante, $derrotaMandante, $golsMandante, $golsVisitante);
                
            $dadosVisitante = array($this->VisitanteId, $this->VisitanteNome, $pontoGanhoVisitante, 
                $vitoriaVisitante, $empateVisitante, $derrotaVisitante, $golsVisitante, $golsMandante);            
           
            if (!$this->SalvarTabela($dadosMandante, $this->Grupo, $this->Turno)) return false;
            if (!$this->SalvarTabela($dadosVisitante, $this->Grupo, $this->Turno)) return false;
        }
        else{
            // excluir jogo da tabela
        }
        
        return true;
    }

    public function AtualizarTabelasClassificacaoPenalti() {
        $pontoGanhoMandante = 0;
        $vitoriaMandante = 0;
        $empateMandante = 0;
        $derrotaMandante = 0;   
        $pontoGanhoVisitante = 0;
        $vitoriaVisitante = 0;
        $empateVisitante = 0;
        $derrotaVisitante = 0;  

        $this->Turno = $this->RetornaTurnoJogo();
        $this->Grupo = $this->RetornaGrupoJogo();
           
        $dadosMandante = array($this->MandanteId, $this->MandanteNome, $pontoGanhoMandante, 
            $vitoriaMandante, $empateMandante, $derrotaMandante, $this->PenaltiMandante, $this->PenaltiVisitante);
            
        $dadosVisitante = array($this->VisitanteId, $this->VisitanteNome, $pontoGanhoVisitante, 
            $vitoriaVisitante, $empateVisitante, $derrotaVisitante, $this->PenaltiVisitante, $this->PenaltiMandante);            
        
        if (!$this->SalvarTabela($dadosMandante, $this->Grupo, $this->Turno)) return false;
        if (!$this->SalvarTabela($dadosVisitante, $this->Grupo, $this->Turno)) return false;
               
        return true;
    }

    public function SalvarTabela($dadosTime, $grupo, $turno) {
        $sql = "
            INSERT INTO tabela (jogoId, campeonatoId, timeId, timeNome, pontoGanho, vitoria, empate, derrota, golFavor, golContra, turno, grupo) 
            VALUES ('$this->Id', '$this->CampeonatoId', '$dadosTime[0]', '$dadosTime[1]', '$dadosTime[2]', '$dadosTime[3]', 
            '$dadosTime[4]', '$dadosTime[5]', '$dadosTime[6]', '$dadosTime[7]', '$turno', '$grupo')";
    
        return $this->Conexao->query($sql);
    }

    public function RetornaTurnoJogo(){
        $sql = "
            SELECT
                turno
            FROM 
                jogos
            WHERE
                id = '$this->Id' ";

        $rs =$this->Conexao->query($sql);
    
        $reg = mysqli_fetch_array($rs);

        return $reg["turno"];
    }
    
    public function RetornaGrupoJogo(){
        $sql = "
            SELECT
                grupo
            FROM 
                jogos
            WHERE
                id = '$this->Id' ";

        $rs =$this->Conexao->query($sql);
    
        $reg = mysqli_fetch_array($rs);

        return $reg["grupo"];
    }

    public function RetornaDadosJogo() {
        $sql = "
            SELECT 
                campeonatoId,
                mandanteId,
                visitanteId,
                turno,
                grupo,
                placarMandante,
                placarVisitante,
                penaltiMandante,
                penaltiVisitante
            FROM 
                jogos 
            WHERE 			
                id =  '$this->Id' ";
    
        $rs =$this->Conexao->query($sql);
    
        $reg = mysqli_fetch_array($rs);
        
        $campeonatoId = $reg['campeonatoId'];
        $mandanteId = $reg['mandanteId'];
        $visitanteId = $reg['visitanteId'];	
        $turno = $reg['turno'];	
        $grupo = $reg['grupo'];	
        $placarMandante = $reg['placarMandante'];	
        $placarVisitante = $reg['placarVisitante'];	
        $penaltiMandante = $reg['penaltiMandante'];	
        $penaltiVisitante = $reg['penaltiVisitante'];	
        
        return array($campeonatoId, $mandanteId, $visitanteId, $turno, $grupo, $placarMandante, $placarVisitante, $penaltiMandante, $penaltiVisitante);
    }

    public function JogosTurnoEncerrados($turno){
        $sql = "
            SELECT 
                id 
            FROM 
                jogos 
            WHERE 
                campeonatoId = '$this->CampeonatoId' AND ";

        if ($turno > "2") $sql = $sql. " turno <= '$turno' AND ";

        $sql = $sql. "
                placarMandante IS NULL AND 
                placarVisitante IS NULL";

        $rs =$this->Conexao->query($sql);	

        return $rs->num_rows == 0;
    }

    public function AtualizarCampeonatoSeForUltimoJogo(){
        $turno = $this->RetornaTurnoJogo();

        if (!$this->JogosTurnoEncerrados($turno)) return true;

        $sqlCampeonato = "
            SELECT
                tipoFinal,
                doisGrupos,
                doisTurnos,
                evitarConfrontos,
                jogos
            FROM
                campeonatos
            WHERE
                id = '$this->CampeonatoId' ";  

        $rsCampeonato =$this->Conexao->query($sqlCampeonato);

        $reg = mysqli_fetch_array($rsCampeonato);
        $doisGrupos = $reg['doisGrupos'];
        $doisTurnos = $reg['doisGrupos'];
        $tipoFinal = $reg['tipoFinal'];
        $quantidadeJogos = $reg['jogos'];
        $evitarConfrontos = $reg['evitarConfrontos']; 

        if ($turno == 1 or $turno == 2){            

            if ($tipoFinal === "1"){ //Confrontos entre os 4 primeiros da Geral

            }

            elseif ($tipoFinal === "2"){ //Confrontos entre os 2 melhores de cada turno    
                $this->GerarSemiFinalDoisMelhoresCadaTurno($quantidadeJogos);
            }

            elseif ($tipoFinal === "3"){ //Confrontos entre os 2 melhores de cada grupo

            }

            elseif ($tipoFinal === "4"){ // Final entre os 2 melhores colocados da Geral                
                $this->GerarFinalDoisMelhoresDaGeral($quantidadeJogos);
            }

            elseif ($tipoFinal === "5"){ // Final entre os campeões de cada turno
                return $this->GerarFinalCampeaoDeCadaTurno($quantidadeJogos);
            }

            elseif ($tipoFinal === "6"){ // Final entre os campeões de cada grupo
                $this->GerarFinalCampeaoDeCadaGrupo($quantidadeJogos);
            }

            else{ // Sem final - campeão melhor time da geral
                $this->EncerrarCampeonato("primeirosColocados");
            }
        }
        
        else if ($turno == 3){
            $this->GerarFinalAposSemiFinal($quantidadeJogos);
        }

        else if ($turno == 4 AND $this->JogosEncerrados($quantidadeJogos)){
            $this->EncerrarCampeonato("resultadoJogos");
        }
    }

    private function JogosEncerrados($quantidadeJogos){
        $sql = "
            SELECT 
                * 
            FROM 
                jogos 
            WHERE 
                campeonatoId = '$this->CampeonatoId' AND
                turno = 4 ";

        $rs = $this->Conexao->query($sql);        
           
        $somaPlacarMandante = 0;
        $somaPlacarVisitante = 0;
        $somaPenaltiMandante = 0;
        $somaPenaltiVisitante = 0;
        while($reg = mysqli_fetch_array($rs)){   
            $placarMandante = $reg['placarMandante'];
            $placarVisitante = $reg['placarVisitante'];
            $penaltiMandante = $reg['penaltiMandante'];
            $penaltiVisitante = $reg['penaltiVisitante'];

            if ($placarMandante == null OR $placarVisitante == null) return false;

            $somaPlacarMandante += $placarMandante;
            $somaPlacarVisitante += $placarVisitante;
            $somaPenaltiMandante += $penaltiMandante;
            $somaPenaltiVisitante += $penaltiVisitante;
        }

        if ($somaPlacarMandante == $somaPlacarVisitante AND $somaPenaltiMandante == $somaPenaltiVisitante) return false;

        return true;
    }

    private function GerarSemiFinalDoisMelhoresCadaTurno($quantidadeJogos){
        $dadosTurnoUm = $this->CampeaoViceTurno("1");
        $dadosTurnoDois = $this->CampeaoViceTurno("2");

        $campeaoTurnoUm = $dadosTurnoUm[0];
        $viceTurnoUm = $dadosTurnoUm[1];
        $campeaoTurnoDois = $dadosTurnoDois[0];
        $viceTurnoDois = $dadosTurnoDois[1];    

        if ($campeaoTurnoUm === null or $viceTurnoUm === null or $campeaoTurnoDois === null or $viceTurnoDois === null) return false;   

        $this->setId(null);
        $this->setTurno(3);        
        $this->setRodada(1);               

        $this->setMandanteId($campeaoTurnoUm);
        $this->setVisitanteId($viceTurnoDois);   
        $this->setGrupo("C");   
        if ($campeaoTurnoUm == $viceTurnoDois) {
            $this->setPlacarMandante(1);
            $this->setPlacarVisitante(0);
            $this->setOrdem(2); 
            $jogoId = $this->GravarJogoRetornaId();
        
            $sql = "INSERT INTO tabela (jogoId, campeonatoId, timeId, timeNome, pontoGanho, vitoria, empate, derrota, golFavor, golContra, turno, grupo) 
                VALUES ('$jogoId', '$this->CampeonatoId','$this->getMandanteId', '$this->getMandanteNome', 3, 1, 0, 0, 1, 0, '$this->Turno', '$this->Grupo')";
    
            if(!$this->Conexao->query($sql)) return false; 
        }
        else{
            if (!$this->GravarJogo()) return false;

            if ($quantidadeJogos > 1){
                $this->setVisitanteId($campeaoTurnoUm);
                $this->setMandanteId($viceTurnoDois); 
                $this->setOrdem(1); 
                
                if (!$this->GravarJogo()) return false;
            }
        }

        $this->setMandanteId($campeaoTurnoDois);
        $this->setVisitanteId($viceTurnoUm);   
        $this->setGrupo("D");   
        if ($campeaoTurnoDois == $viceTurnoUm) {
            $this->setPlacarMandante(1);
            $this->setPlacarVisitante(0);
            $this->setOrdem(4); 
            $jogoId = $this->GravarJogoRetornaId();
        
            $sql = "INSERT INTO tabela (jogoId, campeonatoId, timeId, timeNome, pontoGanho, vitoria, empate, derrota, golFavor, golContra, turno, grupo) 
                VALUES ('$jogoId', '$this->CampeonatoId','$this->MandanteId', '$this->MandanteNome', 3, 1, 0, 0, 1, 0, '$this->Turno', '$this->Grupo')";
    
            if(!$this->Conexao->query($sql)) return false; 
        }
        else{
            if (!$this->GravarJogo()) return false;

            if ($quantidadeJogos > 1){
                $this->setVisitanteId($campeaoTurnoDois);
                $this->setMandanteId($viceTurnoUm); 
                $this->setOrdem(3); 

                if (!$this->GravarJogo()) return false;
            }
        }        

        return true;
    }

    private function GerarFinalDoisMelhoresDaGeral($quantidadeJogos){
        
        //1 jogo  
       // 2 jogos
      //  sem final
    }

    private function GerarFinalCampeaoDeCadaTurno($quantidadeJogos){
        $finalistaA = $this->CampeaoTurno("1");
        $finalistaB = $this->CampeaoTurno("2"); 

        if ($finalistaA === null or $finalistaB === null) return false;
        
        if ($finalistaA === $finalistaB) {
            return $this->EncerrarCampeonato("primeirosColocados");
        }
        
        $this->setId(null);
        $this->setMandanteId($finalistaA);
        $this->setVisitanteId($finalistaB);
        $this->setRodada(1);
        $this->setTurno(4);
        $this->setGrupo("A");
        $this->setOrdem(1);    

        if (!$this->GravarJogo()) return false;

        if ($quantidadeJogos == "2") {
            $this->setMandanteId($finalistaB);
            $this->setVisitanteId($finalistaA);
            $this->setOrdem(2);    

            if (!$this->GravarJogo()) return false;
        }

        return true;
    }

    private function GerarFinalAposSemiFinal($quantidadeJogos){
        $finalistaA = $this->RetornarFinalista("C");
        $finalistaB = $this->RetornarFinalista("D");

        if (!isset($finalistaA) or !isset($finalistaB)) return false;
        
        if ($finalistaA === null or $finalistaB === null) return false;
        
        if ($finalistaA === $finalistaB) {
            return $this->EncerrarCampeonato("primeirosColocados");
        }
        
        $this->setId(null);
        $this->setMandanteId($finalistaA);
        $this->setVisitanteId($finalistaB);
        $this->setRodada(1);
        $this->setTurno(4);
        $this->setGrupo("E");
        $this->setOrdem(1);    

        if (!$this->GravarJogo()) return false;

        if ($quantidadeJogos == "2") {
            $this->setMandanteId($finalistaB);
            $this->setVisitanteId($finalistaA);
            $this->setOrdem(2);    

            if (!$this->GravarJogo()) return false;
        }

        return true;
    }

    private function CampeaoTurno($turno) {
        $sql = "
            SELECT 
                timeId, 
                SUM(pontoGanho), 
                SUM(vitoria), 
                (SUM(golFavor) - SUM(golContra)) AS saldo, 
                SUM(golFavor) 
            FROM 
                tabela 
            WHERE 
                campeonatoId = '$this->CampeonatoId' AND
                turno = '$turno'
            GROUP BY
	            timeId
            ORDER BY
                pontoGanho DESC,
                vitoria DESC,
                saldo DESC,
                golFavor DESC LIMIT 1";
    
        $rs = $this->Conexao->query($sql);
    
        $reg = mysqli_fetch_array($rs);

        return $reg['timeId'];
    }

    public function CampeaoViceTurno($turno){
        $sql = "
            SELECT 
                timeId,
                SUM(pontoGanho) AS pontoGanho, 
                SUM(vitoria) AS vitoria,
                (SUM(golFavor) +  - SUM(golContra)) AS saldo,
                SUM(golFavor) AS golFavor
            FROM 
                tabela 
            WHERE 
                campeonatoId = '$this->CampeonatoId' AND
                turno = '$turno'
            GROUP BY
                timeId
            ORDER BY
                pontoGanho DESC,
                vitoria DESC,
                saldo DESC,
                golFavor DESC 
            LIMIT 2";

        $rs = $this->Conexao->query($sql);
        
        $times = array();
        while($reg = mysqli_fetch_array($rs)){   
            $times[] = $reg['timeId'];
        }
        
        return $times;
    }

    private function RetornarFinalista($grupo){
        $sql = "
            SELECT 
                timeId,
                SUM(pontoGanho) AS pontoGanho, 
                SUM(vitoria) AS vitoria,
                (SUM(golFavor) - SUM(golContra)) AS saldo,
                SUM(golFavor) AS golFavor
            FROM 
                tabela                 
            WHERE 
                campeonatoId = '$this->CampeonatoId' AND
                turno = 3 AND 
                (SELECT grupo FROM jogos WHERE id = jogoId) = '$grupo'                
            GROUP BY
                timeId
            ORDER BY
                pontoGanho DESC,
                vitoria DESC,
                saldo DESC,
                golFavor DESC 
            LIMIT 1";

        $rs =$this->Conexao->query($sql);

        $reg = mysqli_fetch_array($rs);

        return $reg["timeId"];
    }

    public function EncerrarCampeonato($tipoEncerramento){

        if ($tipoEncerramento === "primeirosColocados"){
            $sql = "
                SELECT 
                    timeId,
                    SUM(pontoGanho) AS pontoGanho, 
                    SUM(vitoria) AS vitoria,
                    (SUM(golFavor) - SUM(golContra)) AS saldo,
                    SUM(golFavor) AS golFavor
                FROM 
                    tabela 
                WHERE 
                    campeonatoId = '$this->CampeonatoId' 
                GROUP BY 
                    timeId
                ORDER BY 
                    pontoGanho DESC,
                    (golFavor - golContra) DESC,
                    vitoria DESC,
                    golFavor DESC ";

            $rs = $this->Conexao->query($sql);

            $timeIdAnterior = null;
            $pontoGanhoAnterior = null;
            $vitoriaAnterior = null;	
            $saldoAnterior = null;	
            $golFavorAnterior = null; 
            
            $campeaoId = null;
            $viceId = null;
            
            $count = 0;
            while($reg = mysqli_fetch_array($rs)){         
                $timeId = $reg['timeId'];
                $pontoGanho = $reg['pontoGanho'];
                $vitoria = $reg['vitoria'];	
                $saldo = $reg['saldo'];	
                $golFavor = $reg['golFavor'];
                
                if ($count == 1) {
                    if ($pontoGanho < $pontoGanhoAnterior) $campeaoId = $timeIdAnterior;
                    elseif ($vitoria < $vitoriaAnterior) $campeaoId = $timeIdAnterior;
                    elseif ($saldo < $saldoAnterior) $campeaoId = $timeIdAnterior;
                    elseif ($golFavor < $golFavorAnterior) $campeaoId = $timeIdAnterior;
                }

                elseif ($count == 2) {
                    if ($pontoGanho < $pontoGanhoAnterior) $viceId = $timeIdAnterior;
                    elseif ($vitoria < $vitoriaAnterior) $viceId = $timeIdAnterior;
                    elseif ($saldo < $saldoAnterior) $viceId = $timeIdAnterior;
                    elseif ($golFavor < $golFavorAnterior) $viceId = $timeIdAnterior;
                }

                $timeIdAnterior = $timeId;
                $pontoGanhoAnterior = $pontoGanho;
                $vitoriaAnterior = $vitoria;	
                $saldoAnterior = $saldo;	
                $golFavorAnterior = $golFavor;
                $count++;
            }
            
            if ($campeaoId == null or $viceId == null){
                return false;
            }
            else{
                $sql = "
                    UPDATE campeonatos SET campeao = '$campeaoId', vice = '$viceId', ativo = 0 WHERE id = '$this->CampeonatoId' ";

                    return $this->Conexao->query($sql);
            }
        }
        elseif ($tipoEncerramento === "mesmosTimes"){

        }

        elseif ($tipoEncerramento === "resultadoJogos"){

            $times = $this->CampeaoViceTurno(4);
            
            $sql = " UPDATE campeonatos SET campeao = '$times[0]', vice = '$times[1]', ativo = 0 WHERE id = '$this->CampeonatoId' ";

            if (!$this->Conexao->query($sql)) return false;
        }
        else{ // 

        }

        return true;
    }

    public function FecharConexao(){
        mysqli_close($this->Conexao);
        $this->Conexao = null;   
    }
}
?>