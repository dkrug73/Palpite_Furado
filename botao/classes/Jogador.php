<?php
    class Jogador{
        public $Id;
        public $TimeId;
        public $Numero;
        public $Nome;
        public $Conexao;

        public function setId($parametroId){
            $this->Id = $parametroId;
        }

        public function getId(){
            return $this->Id;
        }

        public function setTimeId($parametroTimeId){
            $this->TimeId = $parametroTimeId;
        }

        public function getTimeId(){
            return $this->TimeId;
        }

        public function setNumero($parametroNumero){
            $this->Numero = $parametroNumero;
        }

        public function getNumero(){
            return $this->Numero;
        }

        public function setNome($parametroNome){
            $this->Nome = $parametroNome;
        }

        public function getNome(){
            return $this->Nome;
        }

        public function setConexao($parametroConexao){
            $this->Conexao = $parametroConexao;
        }

        function __construct($conexao){
            $this->Conexao = $conexao;
        }

        public function GravarJogador($jogador){           
            $sql = "
                INSERT INTO jogadores (numero, nome, timeId)
                VALUES ('$jogador->Numero', '$jogador->Nome', '$jogador->TimeId') ";

            return mysqli_query($this->Conexao, $sql);
        }

        public function AtualizarJogador($jogador){           
            $sql = "
            UPDATE 
                jogadores 
            SET 
                numero = '$jogador->Numero', 
                nome = '$jogador->Nome'                    
            WHERE 
                id = '$jogador->Id'";          

            return mysqli_query($this->Conexao, $sql);
        }

        public function ObterDadosJogador(){
            $sql = "
                SELECT
                    j.id,
                    j.timeId,
                    CONCAT(t.nome, ' - ', u.nome) as nomeTime,
                    j.numero,
                    j.nome
                FROM
                    jogadores j INNER JOIN 
                    times t ON
                        j.timeId = t.id INNER JOIN
                    usuarios u ON
                        u.id = t.tecnico
                ORDER BY    
                    t.tecnico,
                    t.nome,
                    j.nome";

            return $this->Conexao->query($sql);
        }

        public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;
        }
    }

?>