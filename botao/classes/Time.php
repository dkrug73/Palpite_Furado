<?php
    class Time{
        public $Id;
        public $Nome;
        public $NomeOficial;
        public $Tecnico;
        public $Foto;
        public $Conexao;

        public function setId($parametroId){
            $this->Id = $parametroId;
        }

        public function getId(){
            return $this->Id;
        }

        public function setNome($parametroNome){
            $this->Nome = $parametroNome;
        }

        public function getNome(){
            return $this->Nome;
        }

        public function setNomeOficial($parametroNomeOficial){
            $this->NomeOficial = $parametroNomeOficial;
        }

        public function getNomeOficial(){
            return $this->NomeOficial;
        }

        public function setTecnico($parametroTecnico){
            $this->Tecnico = $parametroTecnico;
        }

        public function getTecnico(){
            return $this->Tecnico;
        }

        public function setFoto($parametroFoto){
            $this->Foto = $parametroFoto;
        }

        public function getFoto(){
            return $this->Foto;
        }

        public function setConexao($parametroConexao){
            $this->Conexao = $parametroConexao;
        }

        function __construct($conexao){
            $this->Conexao = $conexao;
        }

        public function GravarTime(){
            if(empty($this->Id)){        
                $sql = "
                INSERT INTO times (
                    nome, nomeOficial, tecnico, foto) 
                VALUES (
                    '$this->Nome', '$this->NomeOficial', '$this->Tecnico', '$this->Foto') ";
            }
            else{
                $sql = "
                    UPDATE 
                        times 
                    SET 
                        nome ='$this->Nome', 
                        nomeOficial = '$this->NomeOficial', ";       
                        if (isset($this->Foto)){
                            $sql = $sql."foto = '$this->Foto', ";
                        }                        
                        $sql = $sql."
                        tecnico = '$this->Tecnico' 
                        WHERE
                            id = '$this->Id' ";                
            }

            return mysqli_query($this->Conexao, $sql);
        }

        public function ObterDadosTimes(){
            $sql = "
            SELECT
                t.id,
                t.nome,
                t.nomeOficial,
                t.tecnico as tecnicoId,
                u.nome as tecnico,
                t.foto
            FROM
                times t INNER JOIN
                usuarios u ON
                t.tecnico = u.id ";

            return $this->Conexao->query($sql);
        }

        public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;   
        }
    }
?>