<?php   
    class Usuario {
        public $Id;
        public $Nome;
        public $NomeUsuario;
        public $Email;
        public $Senha;
        public $Administrador;
        public $Foto;
        private $Conexao;

        //getter and setter
        public function setId ($parametroId){
            $this->Id = $parametroId;
        }

        public function getId(){
            return $this->Id;
        }    
        
        public function setNome ($parametroNome){
            $this->Nome = $parametroNome;
        }

        public function getNome(){
            return $this->Nome;
        }    

        public function setNomeUsuario ($parametroNomeUsuario){
            $this->NomeUsuario = $parametroNomeUsuario;
        }

        public function getNomeUsuario(){
            return $this->NomeUsuario;
        }

        public function setEmail ($parametroEmail){
            $this->Email = $parametroEmail;
        }

        public function getEmail(){
            return $this->Email;
        }

        public function setSenha($parametroSenha){
            $this->Senha = $parametroSenha;
        }
        
        public function getSenha(){
            return $this->Senha;
        }

        public function setAdministrador ($parametroAdministrador){
            $this->Administrador = $parametroAdministrador;
        }        

        public function setFoto ($parametroFoto){
            $this->Foto = $parametroFoto;
        }

        public function setConexao ($parametroConexao){
            $this->Conexao = $parametroConexao;
        }

        function __construct($conexao){
            $this->Conexao = $conexao;
        }    

        public function ObterDadosUsuario(){          
            $sql = " 
                SELECT 
                    id, 
                    nomeUsuario, 
                    email,
                    administrador 
                FROM 
                    usuarios 
                WHERE 
                    nomeUsuario = '$this->NomeUsuario' AND 
                    senha = '$this->Senha' ";

            $rs = $this->Conexao->query($sql);

            return mysqli_fetch_array($rs);              
        }

        public function ObterUsuario($usuario){
            $sql ="
                SELECT 
                    nome, 
                    nomeUsuario, 
                    email, 
                    administrador, 
                    foto 
                FROM 
                    usuarios
                WHERE
                    id = '$usuario->Id' ";

            $rs = $this->Conexao->query($sql);

            $reg=mysqli_fetch_array($rs);
            
            $usuario->Nome = $reg['nome'];
            $usuario->NomeUsuario = $reg['nomeUsuario'];
            $usuario->Email = $reg['email'];
            $usuario->Administrador = $reg['administrador'];
            $usuario->Foto = $reg['foto'];

            return $usuario;    
        }

        public function GravarUsuario(){
            $sql = "SELECT id FROM usuarios WHERE nomeUsuario = '$this->NomeUsuario' or email = '$this->Email' ";
            $rs=$this->Conexao->query($sql);
            
            if ($rs->num_rows > 0) return 2;

            if(empty($this->Id)){        
                $sql = "
                INSERT INTO usuarios (
                    nome, nomeUsuario, email, senha, administrador, foto) 
                VALUES (
                    '$this->Nome', '$this->NomeUsuario', '$this->Email', '$this->Senha', '$this->Administrador', '$this->Foto') ";
            }
            else{
                $sql = "
                    UPDATE 
                        usuarios 
                    SET 
                        nome ='$this->Nome', 
                        nomeUsuario = '$this->NomeUsuario', 
                        email = '$this->Email', 
                        senha = '$this->Senha', ";                        
                if (isset($this->Foto)){
                    $sql = $sql."foto = '$this->Foto', ";
                }                        
                $sql = $sql."administrador = '$this->Administrador' ";
                
                $sql = $sql."WHERE
                        id = '$this->Id' ";                
            }

            return mysqli_query($this->Conexao, $sql);
        }

        public function ObterDadosTodosUsuarios(){
            $sql="
                SELECT
                    id,
                    nome,
                    nomeUsuario,
                    email,
                    if(administrador=1, 'Sim', 'Não') as administrador,
                    foto                
                FROM 
                    usuarios ";
            
            return $this->Conexao->query($sql);	
        }

        public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;   
        }

        public function ExcluirUsuario($id){
            $sql = "DELETE FROM usuarios WHERE id = '$id' ";

            return mysqli_query($this->Conexao, $sql);
        }
    }
?>