<?php
    session_start();
    require_once('classes/Jogo.php');

    

    $erro = null;
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Futebol de Botão</title>

      <link rel="icon" href="imagens/favicon.png">

      <!-- Bootstrap -->
      <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="componentes/css/estilo.css" rel="stylesheet">
      <script src="componentes/jquery/jquery-3.3.1.min.js"></script> 

      <!--<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>-->

      <script>
            // valicação dos campos obrigatórios LOGIN
            $(document).ready(function(){

                  $('#btn_login').click(function(){ 
                        var campo_preenchido = true;                        

                        if ($('#campo_usuario').val() == ''){
                              $('#campo_usuario').css({'border-color':'#A94442'});   
                              campo_preenchido = false;
                        }
                        else{
                              $('#campo_usuario').css({'border-color':'#CCC'});                  
                              campo_preenchido = true;           
                        }

                        if ($('#campo_senha').val() == ''){
                              $('#campo_senha').css({'border-color':'#A94442'});
                              campo_preenchido = false;
                        }
                        else{
                              $('#campo_senha').css({'border-color':'#CCC'});                  
                              campo_preenchido = true;    
                        }

                        return campo_preenchido;
                  });
            });
      </script>
</head>
<body>

      <?php require 'componentes/menu.php'; ?>




      <div class="container">            

            <div class="page-header">
                  <h1>Futebol de Botão</h1>

                  <button class="btn btn-danger">gravar</button>

            </div>
      </div>
      
      <?php require 'componentes/rodape.php'; ?>

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="componentes/bootstrap/js/bootstrap.min.js"></script>
      
</body>
</html>