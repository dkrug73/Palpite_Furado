$(document).ready(function(){
    
    $('#btn-cancelar').click(function(){
        LimparCampos(['formula-jogos-id','rodada','mandante','visitante']);
    });

    $('#resultado-tabela').on('click', 'tr', function() { 
        document.getElementById('formula-jogos-id').value = $(this).find("td:nth-child(1)").text();
        document.getElementById('rodada').value = $(this).find("td:nth-child(3)").text();
        document.getElementById('mandante').value = $(this).find("td:nth-child(4)").text();
        document.getElementById('visitante').value = $(this).find("td:nth-child(5)").text();
    }); 
    
    $('#btn-excluir').click(function(){
        var formulaJogosId = document.getElementById('formula-jogos-id').value;

        if(formulaJogosId != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: formulaJogosId, pagina: 'formulaJogos' },
                success: function(data){                   
                    CarregarTabelaFormulas(document.getElementById('formulaId').value); 
                    LimparCampos(['formula-jogos-id','rodada','mandante','visitante']);

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    }); 

    $('#formulario-formula-jogos').submit(function(){
        $.ajax({
            type: "post",
            url: "paginas/cadastroFormulaJogos1.php",
            data: $('#formulario-formula-jogos').serialize(),
            success: function(data){
                var classe = '#salvar-erro';
                if (data == 1){
                    classe = '#salvar-sucesso';
                }

                ExibirAviso(classe);
                
                LimparCampos(['formula-jogos-id','mandante','visitante']);
                CarregarTabelaFormulas(document.getElementById('formulaId').value);         
                $("input:text:eq(2):visible").focus();    
            }
        });
        return false;
    });  
    
    $('#formulaId').on('change', function() {
        CarregarTabelaFormulas(this.value);
    });   
});