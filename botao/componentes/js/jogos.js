$(document).ready(function(){
        var MANDANTE_ID = null;
        var MANDANTE_NOME  = null
        var MANDANTE_DISTINTIVO = null;
        var VISITANTE_ID = null;
        var VISITANTE_NOME = null;
        var VISITANTE_DISTINTIVO = null;
        var GOLEADORES_MANDANTE = [];
        var GOLEADORES_VISITANTE = [];
        var CAMPEONATO_ID = null;
        var JOGO_ID = null;
        var TURNO = null;

        if(localStorage.getItem('campeonatoId') !== null){
            CAMPEONATO_ID = localStorage.getItem('campeonatoId');
            CarregarTabelaJogos(0);   
            CarregarTabelaClassificacao();  
            VerifcarEncerramentoCampeonato();
        }

        $.ajax({
            type: 'post',
            url: 'paginas/combos/carregarComboCampeonatos.php',
            data: { campeonatoId: localStorage.getItem('campeonatoId') },
            success: function(data){
                $('#campeonatoId').html(data);
            }
        });              

        $("#formulario-jogo").submit(function(){
            $('#modalJogo').modal('hide'); 
            //$('#spinner').modal('show');           

            var aba = document.getElementsByClassName("tab-pane fade show active")[0].id;           

            var dados = {
                idMandante: MANDANTE_ID, 
                nomeMandante: MANDANTE_NOME,
                idVisitante: VISITANTE_ID, 
                nomeVisitante: VISITANTE_NOME,
                jogoId: JOGO_ID, 
                campeonatoId: CAMPEONATO_ID, 
                goleadoresMandante: GOLEADORES_MANDANTE, 
                goleadoresVisitante: GOLEADORES_VISITANTE, 
                placarMandante: document.getElementById("jogo-placar-mandante").innerHTML, 
                placarVisitante: document.getElementById("jogo-placar-visitante").innerHTML 
            };

            $.ajax({
                type: "post",
                url: "paginas/jogos1.php",
                data: dados,
                success:function(data){  

                    if (data == "visitante"){
                        alert("Efetue login com usuário do tipo Administrador para alterar resultado do jogo.");
                    }

                    var placarMandante = document.getElementById('jogo-placar-mandante').innerHTML;
                    var placarVisitante = document.getElementById('jogo-placar-visitante').innerHTML;
                    
                    CarregarTabelaJogos(aba);
                    CarregarTabelaClassificacao();                    
                    VerifcarEncerramentoCampeonato();                                       
					
					if (TURNO == 3 || TURNO == 4) {
						if (placarMandante == placarVisitante) {
                            document.getElementById('penalti-nome-mandante').innerHTML = MANDANTE_NOME;
                            document.getElementById('penalti-nome-visitante').innerHTML = VISITANTE_NOME;
                            document.getElementById('distintivo-penalti-mandante').src = MANDANTE_DISTINTIVO;
                            document.getElementById('distintivo-penalti-visitante').src = VISITANTE_DISTINTIVO;

							$('#modalPenaltis').modal('show');
						}
                    }  
                    
                    //$('#spinner').modal('hide');
                },
                /*
                beforeSend: function(){
                    $('#spinner').modal('show');
                },
    
                complete: function(){
                    $("#spinner").removeClass("in");
                    $(".modal-backdrop").remove();
                    $("#spinner").hide();
                }
                */

            });
            
            return false;
        });

        $("#btn-penaltis").click(function(){
            var aba = document.getElementsByClassName("tab-pane fade show active")[0].id; 
            
            var dados = { 
                penaltiMandante: document.getElementById('input-mandante').value,
                penaltiVisitante: document.getElementById('input-visitante').value,
                idMandante: MANDANTE_ID,
                idVisitante: VISITANTE_ID,
                nomeMandante: MANDANTE_NOME,
                nomeVisitante: VISITANTE_NOME,
                jogoId: JOGO_ID,
                campeonatoId: CAMPEONATO_ID
            };

            $.ajax({
                type: 'post',
                url: 'paginas/atualizarJogoPenalti.php',
                data: dados,
                success: function(data){
                    CarregarTabelaJogos(aba);
                    CarregarTabelaClassificacao();                    
                    VerifcarEncerramentoCampeonato();
                    $('#modalPenaltis').modal('hide'); 
                }
            });
        });
       
        $('#campeonatoId').on('change', function() {
            CAMPEONATO_ID = this.value;

            localStorage.setItem('campeonatoId',CAMPEONATO_ID); 
            CarregarTabelaJogos(0);   
            CarregarTabelaClassificacao();   
            VerifcarEncerramentoCampeonato();     
        });

        $('#tabela-jogos').on('click', 'tr', function() {  
            ZerarModalJogo();

            JOGO_ID = $(this).find("td:nth-child(1)").text();
            MANDANTE_ID = $(this).find("td:nth-child(9)").text();
            VISITANTE_ID = $(this).find("td:nth-child(10)").text();
            MANDANTE_NOME  = $(this).find("td:nth-child(2)").text();
            VISITANTE_NOME = $(this).find("td:nth-child(6)").text();
            TURNO = $(this).find("td:nth-child(11)").text();
            MANDANTE_DISTINTIVO = "imagens/times/" + $(this).find("td:nth-child(7)").text();
            VISITANTE_DISTINTIVO = "imagens/times/" + $(this).find("td:nth-child(8)").text();
            
            document.getElementById('nome-mandante').innerHTML = MANDANTE_NOME;
            document.getElementById('nome-visitante').innerHTML = VISITANTE_NOME;
            document.getElementById('distintivo-mandante').src = MANDANTE_DISTINTIVO;
            document.getElementById('distintivo-visitante').src = VISITANTE_DISTINTIVO;

            $.ajax({
                type: 'post',
                url: 'paginas/validarJogo.php',
                data: { campeonatoId: CAMPEONATO_ID, jogoId: JOGO_ID },
                success(data){                    
                    if (data == 1){                       
                        $('#modalConfirmar').modal("show");                       
                    }
                    else if (data == 2){
                        $('#aviso').modal('show');
                    }
                    else{
                        CarregarCombosMandanteVisitante();
                        $('#rodada').focus();                        
                        $('#modalJogo').modal('show');
                    }
                }
            });             
        }); 
        
        $('#btn-zerar').on('click', function() {           
            if (JOGO_ID > 0) {
                var aba = document.getElementsByClassName("tab-pane fade show active")[0].id;
                $.ajax(
                {
                    url: "paginas/apagarDadosJogo.php",
                    type: "post",
                    data: { jogoId: JOGO_ID },
                    success: function () { 
                        CarregarTabelaJogos(aba);
                        CarregarTabelaClassificacao();
                        CarregarCombosMandanteVisitante();
                        $('#rodada').focus();                        
                        $('#modalJogo').modal('show');
                    }
                });
               
                $(this).parents(".del_cat").animate({ opacity: "hide" }, "slow");
            }
            
            $('#modalConfirmar').modal("hide");           
        });

        $('#gravar-mandante').click(function(){   
            $('#modal-add-jogador').find('.modal-title').text('Novo jogador - ' + MANDANTE_NOME)
            document.getElementById("time-id").value = MANDANTE_ID;            
            $('#modal-add-jogador').modal('show');
        });
        
        $('#gravar-visitante').click(function(){   
            $('#modal-add-jogador').find('.modal-title').text('Novo jogador - ' + VISITANTE_NOME)
            document.getElementById("time-id").value = VISITANTE_ID;
            $('#modal-add-jogador').modal('show');
        });

        $("#btn-gravar-jogador").click(function(){
            var campo_vazio = false;
            if($('#numero-jogador').val() == ''){
                $('#numero-jogador').css({'border-color' : '#A94442'});
                campo_vazio = true;
            }else{
                $('#numero-jogador').css({'border-color' : '#CCC'});
            }

            if($('#nome-jogador').val() == ''){
                $('#nome-jogador').css({'border-color' : '#A94442'});
                campo_vazio = true;
            }else{
                $('#nome-jogador').css({'border-color' : '#CCC'});
            }

            if(campo_vazio) return false;

            if(document.getElementById('numero-jogador').value == "" || document.getElementById('nome-jogador').value == ""){
                alert('vazio');
                return false;
            }            

            $.ajax({
                type: 'post',
                url: 'paginas/gravarJogador.php',
                data: $("#formulario-jogador").serialize(),
                success: function(data){
                    if (data == "visitante"){
                        alert("Efetue login com usuário do tipo Administrador para alterar resultado do jogo.");
                    }
                    
                    CarregarCombosMandanteVisitante();
                    LimparCamposForm("#formulario-jogador");
                    $('#modal-add-jogador').modal('hide');
                }
            });        

        });

        $('#modal-add-jogador').on('show.bs.modal', function() {
            $('#numero-jogador').focus();  
        });

        $("#adicionar-mandante").click(function(){
            Adicionar(true);
        });

        $("#remover-mandante").click(function(){
            Remover(true);
        });
        
        $("#adicionar-visitante").click(function(){
            Adicionar(false);
        });

        $("#remover-visitante").click(function(){
            Remover(false);
        });
        
        function CarregarCombosMandanteVisitante(){
            $.ajax({
                type: 'post',
                url: 'paginas/combos/carregarComboJogadorTime.php',
                data: { timeId: MANDANTE_ID },
                success: function(data){
                    $('#select-mandante').html(data);
                }
            }); 

            $.ajax({
                type: 'post',
                url: 'paginas/combos/carregarComboJogadorTime.php',
                data: { timeId: VISITANTE_ID },
                success: function(data){
                    $('#select-visitante').html(data);
                }
            }); 
        };

        function Adicionar(mandante){
            var lista = "lista-visitante";
            var goleadores = GOLEADORES_VISITANTE;
            var selectTime = "select-visitante";
            var placarTime = "jogo-placar-visitante";

            if (mandante){
                lista = "lista-mandante";
                goleadores = GOLEADORES_MANDANTE;
                selectTime = "select-mandante";
                placarTime = "jogo-placar-mandante";
            }

            var jogadorId = document.getElementById(selectTime).value;
            var jogadorNome = document.getElementById(selectTime).options[document.getElementById(selectTime).selectedIndex].text
                
            var naoAchei = true;
            goleadores.forEach(function(item) {   
                if (item.id == jogadorId) {
                    item.gols += 1;
                    naoAchei = false;
                }            
            });

            if (naoAchei) {
                var item = {};
                item.id = jogadorId;
                item.nome = jogadorNome;
                item.gols = 1;
                
                goleadores.push(item);
            }
            
            document.getElementById(lista).value = null;
            var placar = 0;

            goleadores.forEach(function(item) {  
                document.getElementById(lista).value += item.nome + "   " + item.gols + " gols \r\n";
                placar += item.gols;
            });

            document.getElementById(placarTime).innerHTML = placar;
        };

        function Remover(mandante){
            var lista = "lista-visitante";
            var goleadores = GOLEADORES_VISITANTE;
            var selectTime = "select-visitante";
            var placarTime = "jogo-placar-visitante";

            if (mandante){
                lista = "lista-mandante";
                goleadores = GOLEADORES_MANDANTE;
                selectTime = "select-mandante";
                placarTime = "jogo-placar-mandante";
            }

            var jogadorId = document.getElementById(selectTime).value;            

            goleadores.forEach(function(item) {   
                if (item.id == jogadorId) {
                    if (item.gols > 0){
                        item.gols -= 1;
                    }
                } 
            }); 

            goleadores = goleadores.filter(x => x.gols !== 0);
            document.getElementById(lista).value = null;
            var placar = 0;

            goleadores.forEach(function(item) {  
                document.getElementById(lista).value += item.nome + "   " + item.gols + " gols \r\n";     
                placar += item.gols;                 
            });

            document.getElementById(placarTime).innerHTML = placar;
        };

        function CarregarTabelaJogos(aba){
            $.ajax({
                type: 'post',
                url: 'paginas/tabelas/carregarTabelaJogos.php',
                data: { campeonatoId: CAMPEONATO_ID, abaAtiva: aba },
                success: function(data){
                    $('#tabela-jogos').html(data);
                }
            });
        };

        function CarregarTabelaClassificacao(){
            $.ajax({
                type: "post",
                url: "paginas/tabelas/carregarTabelasClassificacao.php",
                data: { campeonatoId: CAMPEONATO_ID },
                success: function(data){
                    $('#tabela-classificacao').html(data);
                }
            });
        }

        function ZerarModalJogo(){
            document.getElementById("lista-mandante").value = null;
            document.getElementById("lista-visitante").value = null;
            document.getElementById("jogo-placar-mandante").innerHTML = 0;
            document.getElementById("jogo-placar-visitante").innerHTML = 0;
            MANDANTE_ID = null;
            MANDANTE_NOME  = null
            VISITANTE_ID = null;
            VISITANTE_NOME = null;
            JOGO_ID = null;
            GOLEADORES_MANDANTE = [];
            GOLEADORES_VISITANTE = [];
        }       

        function VerifcarEncerramentoCampeonato(){
            $.ajax({
                type: 'post',
                url: 'paginas/verificarEncerramentoCampeonato.php',
                data: { campeonatoId: CAMPEONATO_ID},
                success: function (data) {
                    $('#div-campeao-vice').html(data)
                }
            });
        }
    });   