$(document).ready(function(){  
    var campeonato_id = "";

    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboCampeonatos.php',
        success: function(data){
            $('#campeonatoId').html(data);
        }
    });        

    $('#formulario-jogos').submit(function(){            
        $.ajax({
            type: 'post',
            url: 'paginas/cadastroJogos1.php',
            data: $("#formulario-jogos").serialize(),
            success: function(data){
                CarregarTabelaJogos(campeonato_id); 
                LimparCamposForm('#formulario-jogos');
                document.getElementById('campeonatoId').value = campeonato_id;
                $('#mandante').focus().select();    
                
                var classe = '#salvar-erro';
                if (data == 1){
                    classe = '#salvar-sucesso';
                }

                ExibirAviso(classe);             
            }
        });     
        
        return false;
    });

    $('#campeonatoId').on('change', function() {
        campeonato_id = this.value;
        $.ajax({
            type: 'post',
            url: 'paginas/combos/carregarComboTimesGruposCampeonato.php',
            data: { campeonatoId: campeonato_id },
            success: function(data){
                $('#mandante').html(data);
                $('#visitante').html(data);
                $('#mandante').focus().select();

                CarregarTabelaJogos(campeonato_id); 
                
                $('#formulario-jogos').each (function(){
                    this.reset();
                });

                document.getElementById('campeonatoId').value = campeonato_id;
            }
        });
    });

    $('#resultado-tabela').on('click', 'tr', function() {            
        var id_jogo= $(this).find("td:nth-child(1)").text();
        var mandanteId = $(this).find("td:nth-child(9)").text();
        var visitanteId = $(this).find("td:nth-child(10)").text();
        var rodada = $(this).find("td:nth-child(5)").text();
        var turno = $(this).find("td:nth-child(6)").text();
        var grupo = $(this).find("td:nth-child(7)").text();
        var ordem = $(this).find("td:nth-child(8)").text();
        
        document.getElementById('jogo-id').value = id_jogo;
        document.getElementById('mandante').value = mandanteId;
        document.getElementById('visitante').value = visitanteId;
        document.getElementById('rodada').value = rodada;
        document.getElementById('turno').value = turno;
        document.getElementById('grupo').value = grupo;
        document.getElementById('ordem').value = ordem;

        $('#rodada').focus();
    });   

     $('#btn-excluir').click(function(){
        var jogo = document.getElementById('jogo-id').value;

        if(jogo != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: jogo, pagina: 'jogos' },
                success: function(data){                        
                    CarregarTabelaJogos(campeonato_id); 
                    
                    $('#formulario-jogos').each (function(){
                        this.reset();
                    });

                    document.getElementById('campeonatoId').value = campeonato_id;

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    }); 

    function CarregarTabelaJogos(id){
        $.ajax({
            url: 'paginas/tabelas/carregarTabelaCadastroJogos.php',
            method: 'post',
            data: { campeonatoId: id },
            success: function(data){
                $('#resultado-tabela').html(data);
            }
        });
    };        
});