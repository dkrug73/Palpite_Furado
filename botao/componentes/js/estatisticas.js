$(document).ready(function(){

    if(localStorage.getItem('campeonatoId') !== null){
        CarregarTabelasEstatisticas(localStorage.getItem('campeonatoId'));  
    }

    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboCampeonatos.php',
        data: { campeonatoId: localStorage.getItem('campeonatoId') },
        success: function(data){
            $('#campeonatoId').html(data);
        }
    });  

    $('#campeonatoId').on('change', function() {
        CarregarTabelasEstatisticas(this.value);                 
    });   

    function CarregarTabelasEstatisticas(campeonatoId){
        $.ajax({
            type: 'post',
            url: 'paginas/tabelas/carregarTabelasEstatisticas.php', 
            data: {campeonatoId: campeonatoId },
            success: function(data){
                $('#tabela-estatisticas').html(data);
            }
        });  
    };

});