CarregarTabela('carregarTabelaUsuarios.php');

$(document).ready(function(){
    $('#btn_excluir').click(function(){
        var usuario_id = document.getElementById('usuario-id').value;
        var caminho_foto = document.getElementById('foto-time').src;

        if(usuario_id == '') return false;

        $.ajax({
            url: 'paginas/excluirCadastro.php',
            method: 'post',
            data: { id: usuario_id, pagina: 'usuarios', caminho: caminho_foto },
            success: function(data){
                LimparCamposForm('#cadastro-usuario');
                CarregarTabela('carregarTabelaUsuarios.php');
                document.getElementById('foto-time').src = 'imagens/usuarios/sem_imagem.png';

                var classe = '#excluir-erro';
                if (data == 1){
                    classe = '#excluir-sucesso';
                }

                ExibirAviso(classe);   
            }
        });
    });        

    $('#resultado-tabela').on('click', 'tr', function () {
        var id_usuario= $(this).find("td:nth-child(1)").text();
        var nome = $(this).find("td:nth-child(2)").text();
        var nomeUsuario = $(this).find("td:nth-child(3)").text();
        var email = $(this).find("td:nth-child(4)").text();
        var administrador = $(this).find("td:nth-child(5)").text();
        var foto = $(this).find("td:nth-child(6) img").attr('src');

        if (administrador == 'Sim') document.getElementById('administrador').checked = true;
        else document.getElementById('administrador').checked = false;

        if(foto == '') foto = 'imagens/usuarios/sem_imagem.png';

        document.getElementById('usuario-id').value = id_usuario;
        document.getElementById('nome').value = nome;
        document.getElementById('nome-usuario').value = nomeUsuario;
        document.getElementById('email').value = email;
        document.getElementById('foto-time').src = foto;  
    });

    $('#btn_cancelar').click(function(){
        document.getElementById('foto-time').src = 'imagens/usuarios/sem_imagem.png';
    });
});     