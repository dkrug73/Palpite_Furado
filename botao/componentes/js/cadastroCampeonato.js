$(document).ready(function(){

    $('#gerar-jogos').hide();
    $('#configuracoes').hide();
    CarregarTabela('carregarTabelaCampeonatos.php'); 
    var comboGrupos = "";

    $('#gerar-jogos').show();

    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboFormulas.php',
        success: function(data){
            $('#formulaId').html(data);
        }
    });

    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboTimesGrupos.php',
        success: function(data){
            var resultado = data;
            $('#grupo-unico').html(data);
            comboGrupos = data;
        }
    });

    $('#formulario-campeonato').submit(function(){           
        $.ajax({
            type: "post",
            url: "paginas/cadastroCampeonato1.php",
            data: $("#formulario-campeonato").serialize(),
            success:function(data){  
                    
                classe = '#salvar-sucesso';
                if (data == '0'){
                    var classe = '#salvar-erro';
                    ExibirAviso(classe);  
                    return false;
                }

                ExibirAviso(classe);  

                CarregarTabela('carregarTabelaCampeonatos.php'); 
                
                document.getElementById('campeonato-id').value = data;
                
                $('#configuracoes').show();
            }
        });
        
        return false;
    });  

    $('#concluir-configuracoes').click(function(){
        var campeonatoId = document.getElementById('campeonato-id').value;

        $.ajax({
            type: "post",
            url: "paginas/cadastroCampeonatoConfiguracoes1.php",
            data: $("#formulario-configuracoes").serialize() + "&campeonato-id=" + document.getElementById('campeonato-id').value,
            success: function(data){
                $("[data-dismiss=modal]").trigger({ type: "click" });
            }
        });            
    });

     $('#resultado-tabela').on('click', 'tr', function() {            
        var id_campeonato= $(this).find("td:nth-child(1)").text();
        var descricao = $(this).find("td:nth-child(2)").text();
        var local = $(this).find("td:nth-child(3)").text();
        var edicao = $(this).find("td:nth-child(4)").text();
        var classeRanking = $(this).find("td:nth-child(5)").text();
        var data = $(this).find("td:nth-child(6)").text();
        var ativo = $(this).find("td:nth-child(7)").text();

        var doisGrupos = $(this).find("td:nth-child(8)").text();
        var doisTurnos = $(this).find("td:nth-child(9)").text();
        var evitarConfrontos = $(this).find("td:nth-child(10)").text();
        var formulaId = $(this).find("td:nth-child(11)").text();
        var jogos = $(this).find("td:nth-child(12)").text();
        var tipoFinal = $(this).find("td:nth-child(13)").text();
        
        if (ativo == 'sim') document.getElementById('ativo').checked = true;
        else document.getElementById('ativo').checked = false;

        data = data.split('/').reverse().join('-');
        
        document.getElementById('campeonato-id').value = id_campeonato;
        document.getElementById('descricao').value = descricao;
        document.getElementById('local').value = local;
        document.getElementById('edicao').value = edicao;
        document.getElementById('classe-ranking').value = classeRanking;
        document.getElementById('data-campeonato').value = data;

        document.getElementById('formulaId').value = formulaId;
        document.getElementById('tipo-final').value = tipoFinal;
        document.getElementById('dois-turnos').checked = doisTurnos;
        document.getElementById('dois-grupos').checked = doisGrupos;
        document.getElementById('evitar-confrontos').checked = evitarConfrontos;
        document.getElementById('radio-jogos-'+jogos).checked = true;
        
        $('#configuracoes').show();
    });   

    $('#btn-excluir').click(function(){
        var campeonato = document.getElementById('campeonato-id').value;

        if(campeonato != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: campeonato, pagina: 'campeonatos' },
                success: function(data){                        
                    CarregarTabela('carregarTabelaCampeonatos.php'); 
                    LimparCamposForm('#formulario-campeonato');

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    }); 

    $('#btn-cancelar').click(function(){
        $('#gerar-jogos').hide();
        $('#configuracoes').hide();
    });

    $('#dois-grupos').click (function(){
        document.getElementById('grupo-dois').disabled = !$(this).is (':checked');
        if($(this).is (':checked')) $('#grupo-dois').html(comboGrupos);
    });     
    
    $('#gerar-jogos').click(function(){
        var dados = {
            campeonatoId: document.getElementById('campeonato-id').value,
            formulaId: document.getElementById('formulaId').value,
            tipoFinal: document.getElementById('tipo-final').value,
            doisTurnos: document.getElementById('dois-turnos').checked,
            doisGrupos: document.getElementById('dois-grupos').checked,
            evitarConfrontos: document.getElementById('evitar-confrontos').checked,
            umJogo: document.getElementById('radio-jogos-1').checked,
            doisJogos: document.getElementById('radio-jogos-2').checked,
            semFinal: document.getElementById('radio-jogos-3').checked
        };

        $.ajax({
            type: 'post',
            url: 'paginas/gerarJogos.php',
            data: dados,
            success: function(data){
            },

            beforeSend: function(){
                var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Gravando...';
                $('#gerar-jogos').html(loadingText);    
                $('#gerar-jogos').attr('disabled', 'disabled');
            },

            complete: function(){
                $('#gerar-jogos').html('Gerar jogos');
                $('#gerar-jogos').removeAttr('disabled');
            }
        });
        
    });

});