$(document).ready(function(){
    CarregarTabela('carregarTabelaTimes.php');

    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboTecnico.php',
        success: function(data){
            $('#combo-tecnico').html(data);
        }
    });

    $('#btn-excluir').click(function(){ 
        var time_id = document.getElementById('time-id').value;
        var caminho_foto = document.getElementById('foto-time').src;

        if(time_id == '') return false;

        $.ajax({
            url: 'paginas/excluirCadastro.php',
            method: 'post',
            data: { id: time_id, pagina: 'times', caminho: caminho_foto },
            success: function(data){
                LimparCamposForm('#cadastro-time');
                CarregarTabela('carregarTabelaTimes.php');

                document.getElementById('foto-time').src = 'imagens/times/sem_imagem.png';

                var classe = '#excluir-erro';
                if (data == 1){
                    classe = '#excluir-sucesso';
                }

                ExibirAviso(classe);    
            }
        });
    });   

    $('#resultado-tabela').on('click', 'tr', function () {
        var id_usuario= $(this).find("td:nth-child(1)").text();
        var nome = $(this).find("td:nth-child(2)").text();
        var nomeOficial = $(this).find("td:nth-child(3)").text();
        var tecnico = $(this).find("td:nth-child(4)").text();
        var foto = $(this).find("td:nth-child(6) img").attr('src');

        if(foto == '') foto = 'imagens/times/sem_imagem.png';

        document.getElementById('time-id').value = id_usuario;
        document.getElementById('nome').value = nome;
        document.getElementById('nome-oficial').value = nomeOficial;
        document.getElementById('tecnicoId').value = tecnico;
        document.getElementById('foto-time').src = foto;  
    });

    $('#btn_cancelar').click(function(){
        document.getElementById('foto-time').src = 'imagens/times/sem_imagem.png';
    });

});    