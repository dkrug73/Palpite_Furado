$(document).ready(function(){
    CarregarTabela('carregarTabelaJogadores.php');

    $('#formulario').submit(function(){
        if(document.getElementById('jogador-id').value == ""){
            LimparCamposForm('#formulario');
            return false;
        }
        
        $.ajax({
            type:"post",
            url:"paginas/cadastroJogador1.php",
            data:$(this).serialize(),
            success:function(data){
                var classe = '#salvar-erro';
                if (data == 1){
                    classe = '#salvar-sucesso';
                    LimparCamposForm('#formulario');
                    CarregarTabela('carregarTabelaJogadores.php'); 
                }

                ExibirAviso(classe);
            }
        });
        return false;
    }); 
    
    $('#resultado-tabela').on('click', 'tr', function() { 
        document.getElementById('jogador-id').value = $(this).find("td:nth-child(1)").text();
        document.getElementById('jogador-time').value = $(this).find("td:nth-child(4)").text();
        document.getElementById('numero').value = $(this).find("td:nth-child(2)").text();
        document.getElementById('nome').value = $(this).find("td:nth-child(3)").text();
    }); 

    $('#btn-excluir').click(function(){
        var jogador = document.getElementById('jogador-id').value;

        if(jogador != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: jogador, pagina: 'jogadores' },
                success: function(data){                        
                    CarregarTabela('carregarTabelaJogadores.php'); 
                    LimparCamposForm('#formulario');

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    });  

});