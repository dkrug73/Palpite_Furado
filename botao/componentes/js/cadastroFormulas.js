$(document).ready(function(){
    $('#inserir-formula').hide();
    CarregarTabela('carregarTabelaFormulas.php');      
    
    $('#formulario-formula').submit(function(){
        $.ajax({
            type: 'post',
            url: 'paginas/cadastroFormula1.php',
            data: $('#formulario-formula').serialize(),
            success: function(data){
                var classe = '#salvar-erro';           
                if (data > 0){
                    classe = '#salvar-sucesso';
                    document.getElementById('formula-id').value = data;
                    $('#inserir-formula').show();
                    $('#inserir-formula').attr("href", "cadastroFormulaJogos.php?formula-id="+data);
                    CarregarTabela('carregarTabelaFormulas.php');     
                }

                ExibirAviso(classe);    
            }
        });

        return false;
    });
           
    $('#resultado-tabela').on('click', 'tr', function() {
        document.getElementById('formula-id').value = id_formula= $(this).find("td:nth-child(1)").text();
        document.getElementById('descricao').value = $(this).find("td:nth-child(2)").text();
        document.getElementById('quantidade').value = $(this).find("td:nth-child(3)").text();
        
        $('#inserir-formula').show();
        $('#inserir-formula').attr("href", "cadastroFormulaJogos.php?formula-id="+id_formula);
    });   

    $('#btn-excluir').click(function(){
        var formulaId = document.getElementById('formula-id').value;

        if(formulaId != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: formulaId, pagina: 'formulas' },
                success: function(data){
                    LimparCamposForm('#formulario-formula');
                    CarregarTabela('carregarTabelaFormulas.php');
                    $('#inserir-formula').hide();
                    
                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);     
                }
            });
        }
    });   
    
    $('#btn-cancelar').click(function(){
        $('#inserir-formula').hide();
    });
}); 



