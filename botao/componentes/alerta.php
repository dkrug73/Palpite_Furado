<?php  
    if (isset($erro)){  
        if($erro == '0'){ ?>
            <div class="alert alert-success" role="alert" id="alerta">
                Sucesso ao salvar cadastro!
            </div> <?php
        }
        elseif($erro == '1'){ ?>
            <div class="alert alert-danger" role="alert" id="alerta">
                Erro ao salvar cadastro!
            </div> <?php
        } 
        elseif($erro == '2'){ ?>
            <div class="alert alert-warning" role="alert" id="alerta">
                Email ou Nome de usuário já existe.
            </div> <?php
        } ?>    
            <script>
                setTimeout(function() {$('#alerta').fadeOut('slow');}, 3000);
            </script> <?php
    } 
?>   

<div id="salvar-sucesso" style="display:none;">
    <div class="alert alert-success" role="alert" id="alerta">
        Sucesso ao salvar cadastro!
    </div>           
</div>

<div id="salvar-erro" style="display:none;">
    <div class="alert alert-danger" role="alert" id="alerta">
        Erro ao salvar cadastro!
    </div> 
</div>

<div id="excluir-sucesso" style="display:none;">
    <div class="alert alert-success" role="alert" id="alerta">
        Sucesso ao excluir cadastro!
    </div>           
</div>

<div id="excluir-erro" style="display:none;">
    <div class="alert alert-danger" role="alert" id="alerta">
        Erro ao excluir cadastro!
    </div> 
</div>