<?php
function MoverFoto($diretorio, $files_foto, $post_foto){
    $resultado = "";

    if($files_foto['error'] == 0) {        
        //define o caminho 
        $destino = "../imagens/".$diretorio."/";

        // Pega extensão da imagem
        preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $files_foto["name"], $ext);		
            
        // Gera um nome único para a imagem
        $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

        //armazena na variavel o novo nome
        $arquivo = basename($nome_imagem);

        //concatena a variavel do caminho com o nome do arquivo 
        $destino = $destino . $arquivo; 

        //testa se a funçao de upload rodar
        if(move_uploaded_file($files_foto['tmp_name'], $destino)) {
            $resultado = $arquivo;
        }
    }

    return $resultado;
}

?>