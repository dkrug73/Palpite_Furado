DROP TABLE IF EXISTS usuarios;
CREATE TABLE usuarios (
    id int(11) not null PRIMARY KEY AUTO_INCREMENT,
    nome varchar(100) NOT null,
    nomeUsuario varchar(30) NOT null,
    email varchar(100) NOT null,
    senha varchar(32) NOT null,
    administrador char(1) NOT null,
    foto varchar(100)   
);

DROP TABLE IF EXISTS formulas;
CREATE TABLE formulas (
    id int(11) not null PRIMARY key AUTO_INCREMENT,
    descricao varchar(50) NOT null,
    quantidadeTimes int(3) NOT null    
);

DROP TABLE IF EXISTS formulaJogos;
CREATE TABLE formulaJogos(
	id int(11) NOT null PRIMARY KEY AUTO_INCREMENT,
    formulaId int(11) NOT null,
	rodada int(2) NOT null,
    mandante int(2) NOT null,
    visitante int(2) NOT null
);

DROP TABLE IF EXISTS times;
CREATE TABLE times(
  	id int(11) NOT null PRIMARY KEY AUTO_INCREMENT,
    nome varchar(30) NOT null,
    nomeOficial varchar(100) NOT null,
    tecnico int(11) NOT null,
    foto varchar(100) NOT null
);

DROP TABLE IF EXISTS jogadores;
CREATE TABLE jogadores(
  	id int(11) NOT null PRIMARY KEY AUTO_INCREMENT,
    timeId int(11) NOT null,
    numero int(2) NOT null,
    nome varchar(30) NOT null
);

DROP TABLE IF EXISTS jogos;
CREATE TABLE jogos (
    id int(11) NOT null  PRIMARY KEY AUTO_INCREMENT,
    campeonatoId int(11) NOT null,
    mandanteId int(11) NOT null,
    visitanteId int(11) NOT null,
    placarMandante int(2) null,
    placarVisitante int(2) null,
    penaltiMandante int(2) null,
    penaltiVisitante int(2) null,
    rodada int(2)  NOT null,
    turno int(1) NOT null,
    grupo char(1) null,
    ordem int(2) null
);


DROP TABLE IF EXISTS campeonatos;
CREATE TABLE campeonatos (
    id int(11) NOT null PRIMARY KEY AUTO_INCREMENT,
    descricao varchar(60) NOT null,
    edicao varchar(10) null,
    local varchar(30) NOT null,
    classeRanking varchar(20) NOT null,
    data  date NOT null,
    formulaId int(11) null,
    doisTurnos char(1) null,
    doisGrupos char(1) null,
    evitarConfrontos char(1) null,
    tipoFinal char(1) null,
    jogos char(1) null,
    campeao varchar(60) null,
    vice varchar(60) null,
    ativo int(1) null
);

DROP TABLE IF EXISTS timesCampeonato;
CREATE TABLE timesCampeonato (
    id int(11) NOT null PRIMARY KEY AUTO_INCREMENT,
    campeonatoId int(11) NOT null,
    timeId int(11) NOT null,
    grupo char(1) null
);

DROP TABLE IF EXISTS tabela;
CREATE TABLE tabela (
    id int(11) NOT null PRIMARY KEY AUTO_INCREMENT,
    campeonatoId int(11) NOT null,
    jogoId int(11) NOT null,
    timeId int(11) NOT null,
    timeNome varchar(60) NOT null,
    pontoGanho int(3) null,
    vitoria int(2) null,
    empate int(2) null,
    derrota int(2) null,
    golFavor int(3) null,
    golContra int(3) null,
    turno char(1) null,
    grupo char(1) null
);

DROP TABLE IF EXISTS jogoJogador;
CREATE TABLE jogoJogador (
  id int(11) NOT null PRIMARY KEY AUTO_INCREMENT,
  jogoId int(11) NOT null,
  jogadorId int(11) NOT null,
  gols int(3) null
);
