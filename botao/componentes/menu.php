<!-- MENU -->
<nav class="navbar fixed-top navbar-expand-lg bg-info fundo text-white navbar-dark sticky-top custom">
    <div class="container">
        <div class="navbar-header">    
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>

            <a class="navbar-brand" href="jogos.php"> <img src="imagens/logo2.png" id="logo"> </a>                                  
        </div>

        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <!--
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Início</a>
                </li>
                -->
                <li class="nav-item">
                    <a class="nav-link" href="jogos.php">Jogos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="estatisticas.php">Estatísticas</a>
                </li>

                <li class="nav-item dropdown" <?= !isset($_SESSION['administrador_usuario_botao']) ? 'style="display: none" ' : '' ?>>
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Cadastros</a>
                    
                    <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="cadastroCampeonato.php">Campeonatos</a></li>
                            <li><a class="dropdown-item" href="cadastroJogos.php">Jogos</a></li>
                            <li><a class="dropdown-item" href="cadastroJogador.php">Jogador</a></li>
                            <li><a class="dropdown-item" href="cadastroTime.php">Time</a></li>
                            <li><a class="dropdown-item" href="cadastroFormula.php">Fórmula</a></li>
                            <li><a class="dropdown-item" href="cadastroUsuario.php">Usuário</a></li>
                    </ul>
                </li>  
                
                <li class="divisor" role="separator"></li>                    
                <?php
                if (!isset($_SESSION['nome_usuario_botao'])){ ?>
                    <li class="nav-item dropdown">
                    
                        <a class="nav-link" href="#" id="entrar" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Entrar</a>
                        
                        <ul class="dropdown-menu <?= $erro == 1 ? 'show' : '' ?>" aria-labelledby="entrar" style="min-width: 15rem;">
                                <div class="col-md-12"> 
                                    <h4>Você possui uma conta?</h4>
                                     <br>

                                    <form method="post" action="paginas/validarAcesso.php" id="formLogin">
                                        <div class="form-group">
                                                <input type="text" class="form-control" id="campo_usuario" name="usuario" placeholder="Usuário" >
                                        </div>

                                        <div class="form-group"> 
                                                <input type="password" class="form-control" id="campo_senha" name="senha" placeholder="Senha" >
                                        </div>

                                        <button type="buttom" class="btn btn-primary" id="btn_login">Entrar</button>

                                        <br> <br>
                                        
                                    </form> <?php   

                                    if ($erro == 1){
                                        echo '<font color="FF0000">Usuário ou senha inválidos.</font>';
                                    } ?>
                        
                                </div>
                        </ul>
                    </li> <?php
                }
                else{
                    echo '<li class="nav-item">';
                        echo '<a class="nav-link" href="paginas/sair.php">Sair</a>';
                    echo '</li>';
                }
                ?>  
            </ul>
        </div>
    </div>
</nav> <!-- FIM MENU -->