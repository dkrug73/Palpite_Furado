<?php
    session_start();
    require_once('classes/Time.php');

    if(!isset($_SESSION['nome_usuario_botao'])){
        header('Location: index.php?erro=1');
    }

    $erro = null;
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro de Jogos</title>

    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->
    <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/jquery/jquery-3.3.1.min.js"></script> 
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/js/cadastroJogo.js"></script> 
    <script src="componentes/js/utils.js"></script>
</head>
<body>  
    <?php require 'componentes/menu.php'; ?>

    <div class="container corpo">  

        <?php require 'componentes/alerta.php'; ?>  
        <section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro de Jogos</h3>				
        </section>

        <section>
            <form id="formulario-jogos" name="formulario-jogos">
                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label">ID</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="jogo-id" name="jogo-id" 
                            placeholder="" readonly value="">
                    </div>
                </div>  

                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label" for="campeonato">Campeonato</label>
                
                    <div class="col-lg-10 col-md-10"> 
                        <select name="campeonatoId" id="campeonatoId" class="form-control">                        
                            <div id="comboCampeonato"></div>     
                        </select> 
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label" for="confronto">Confronto</label>
                    
                    <div class="col-lg-5 col-md-5">
                        <select name="mandante" id="mandante" name="mandante" class="form-control"></select>
                    </div> 

                    <div class="col-lg-5 col-md-5">
                        <select name="visitante" id="visitante" name="visitante" class="form-control"></select>
                    </div> 
                </div>

                <div class="form-group row">
                    <label for="rodada" class="col-lg-2 col-md-2 col-form-label">Rodada</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control somenteNumero" id="rodada" name="rodada" value="" required maxlength="2">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="turno" class="col-lg-2 col-md-2 col-form-label">Turno</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control somenteNumero" id="turno" name="turno" value="" required maxlength="1">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="grupo" class="col-lg-2 col-md-2 col-form-label">Grupo</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="grupo" name="grupo" value="" required  maxlength="1">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ordem" class="col-lg-2 col-md-2 col-form-label">Ordem</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control somenteNumero" id="ordem" name="ordem" value="" required maxlength="2">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="submit" class="btn btn-primary mr-2">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn-cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>

                    </div>
                </div>
            </form>

            <div class="container"> 
                <div id="resultado-tabela"></div> 
            </div>
        </section>
    </div>

    <?php require 'componentes/rodape.php'; ?>   

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
    <script src="componentes/bootstrap/js/bootstrap.min.js"></script>   
    
</body>
</html>