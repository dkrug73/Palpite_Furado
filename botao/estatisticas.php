<?php
    session_start();
    require_once('classes/Jogo.php');

    if(!isset($_SESSION['nome_usuario_botao'])){
        header('Location: index.php?erro=1');
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Estatísticas</title>

    <link rel="icon" href="imagens/favicon.png">

<!-- Bootstrap -->    
    <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/jquery/jquery-3.3.1.min.js"></script> 
    <script src="componentes/js/utils.js"></script>   
    <script src="componentes/js/estatisticas.js"></script>  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    
</head>
<body>
    <?php require 'componentes/menu.php'; ?>

    <div class="container-fluid">        
        <nav class="navbar navbar-expand-lg navbar-light bg-light pt-3 pb-3">
            <div class="container">
                <label class="col-md-2" for="campeonato">Campeonato</label>
            
                <div class="col-md">
                    <select name="campeonatoId" id="campeonatoId" class="form-control"><div id="comboCampeonato"></div></select> 
                </div>
            </div>
        </nav>
    </div>

    <div class="container-fluid estatisticas">
        <div id="tabela-estatisticas"></div>      
        
           
    </div>

    <?php require 'componentes/rodape.php'; ?>  

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="componentes/bootstrap/js/bootstrap.min.js"></script>
      
</body>
</html>