<?php
    session_start();
    require_once('classes/Time.php');

    if(!isset($_SESSION['nome_usuario_botao'])){
        header('Location: index.php?erro=1');
    }

    $erro = null;
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Futebol de Botão</title>
    <meta name="robots" content="noindex">
    
    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->
    <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/jquery/jquery-3.3.1.min.js"></script> 
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/js/cadastroCampeonato.js"></script>     
    <script src="componentes/js/gijgo.min.js" type="text/javascript"></script>
    <script src="componentes/js/utils.js"></script>    
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">      
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">

</head>

<body>
    <?php include "componentes/menu.php"; ?>

    <div class="container corpo">  

        <?php require 'componentes/alerta.php'; ?> 

        <section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro de Campeonato</h3>				
        </section>

        <section>
            <form id="formulario-campeonato" name="formulario-campeonato">
                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label">ID</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="campeonato-id" name="campeonato-id" placeholder=""
                            readonly value="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="descricao" class="col-lg-2 col-md-2 col-form-label">Descrição</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="descricao" name="descricao" value="" required maxlength="60">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="local" class="col-lg-2 col-md-2 col-form-label">Local</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" name="local" id="local" value="" required maxlength="30">
                    </div>
                </div>    

                <div class="form-group row">
                    <label for="edicao" class="col-lg-2 col-md-2 col-form-label">Edição</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="edicao" name="edicao" value="" required maxlength="10">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="classe-ranking" class="col-lg-2 col-md-2 col-form-label">Classe ranking</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" name="classe-ranking" id="classe-ranking" class="form-control" required maxlength="20">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="data-campeonato" class="col-lg-2 col-md-2 col-form-label">Data</label>

                    <div class="col-lg-3 col-md-3" >
                            <input type="date" class="form-control" id="data-campeonato" name="data-campeonato" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>

                    <div class="col-lg-3 col-md-3">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ativo" name="ativo">
                            <label class="custom-control-label" for="ativo">Campeonato ativo</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="submit" class="btn btn-primary mr-2">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn-cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>
                        
                        <button type="button" class="btn btn-info" id="configuracoes" data-toggle="modal" data-target="#modalConfiguracoes">Configurações</button>

                        <button type="button" class="btn btn-info" id="gerar-jogos">Gerar jogos</button>
                    </div>
                </div>
            </form>

            <div class="container"> 
                <div id="resultado-tabela"></div>               
            </div>
        </section>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalConfiguracoes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <form action="" id="formulario-configuracoes">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white" >
                        <h5 class="modal-title" id="exampleModalLabel">Configurações do campeonato</h5>
                        <button type="button" class="close branco" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="formula">Fórmula</label>
                            <select name="formulaId" id="formulaId" class="form-control"></select> 
                        </div>

                        <div class="form-group">
                            <label for="finais">Tipo de final</label>

                            <select name="tipo-final" id="tipo-final" class="form-control">
                                <option value="1">Confrontos entre os 4 primeiros da Geral</option>
                                <option value="2">Confrontos entre os 2 melhores de cada turno</option>
                                <option value="3">Confrontos entre os 2 melhores de cada grupo</option>
                                <option value="4">Final entre os 2 melhores colocados da Geral</option>
                                <option value="5">Final entre os campeões de cada turno</option>
                                <option value="6">Final entre os campeões de cada grupo</option>
                                <option value="7">Sem final - campeão melhor time da geral</option>
                            </select> 
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6 col-sm-4">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="dois-turnos" name="dois-turnos">
                                        <label class="custom-control-label" for="dois-turnos">2 turnos</label>
                                    </div>
                                </div>

                                <div class="col-6 col-sm-4">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="dois-grupos" name="dois-grupos">
                                        <label class="custom-control-label" for="dois-grupos">2 grupos</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="evitar-confrontos" name="evitar-confrontos">
                                <label class="custom-control-label" for="evitar-confrontos">Evitar confrontos entre times do mesmo participante</label>
                            </div>
                        </div>

                        <fieldset>             
                            <div class="row">                                        
                                <div class="form-check" style="padding-left: 0.7rem;">                                   
                                    <label class="containerRadio">1 jogo
                                        <input type="radio" checked="checked" name="radio-jogos" id="radio-jogos-1" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            
                                <div class="form-check" style="padding-left: 0.7rem;">                                   
                                    <label class="containerRadio">2 jogos
                                        <input type="radio" name="radio-jogos" id="radio-jogos-2" value="2">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            
                                <div class="form-check" style="padding-left: 0.7rem;">
                                    <label class="containerRadio">Sem final
                                        <input type="radio" name="radio-jogos" id="radio-jogos-3" value="3">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>                      
                        </fieldset>
                        
                        <fieldset class="grupos">
                            <legend class="legenda-grupo">Grupo único</legend>

                            <div class="form-group">      
                                <select class="form-control select2" multiple="multiple" style="width: 100%;" id="grupo-unico" name="grupo-unico[]">
                                    <div id="grupo-unico"></div>
                                </select>
                            </div>
                        </fieldset>

                        <fieldset class="grupos">
                            <legend class="legenda-grupo">Dois grupos</legend>
                            
                            <div class="form-group">                        
                                <select class="form-control select2" multiple="multiple" style="width: 100%;" id="grupo-dois" name="grupo-dois[]" disabled>
                                    <div id="dois-grupos"></div>
                                </select>
                            </div>
                        </fieldset>                                                         
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="btn-fechar-modal" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="concluir-configuracoes">Concluir</button>
                    </div>
                </div>
            </form> 
        </div>
    </div>
    <!-- Modal -->

    <?php require 'componentes/rodape.php'; ?>   

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
    <script src="componentes/bootstrap/js/bootstrap.min.js"></script>   
    

    <!-- Initialize Select2 Elements -->
    <script>
        $(function () {
            $(".select2").select2();
    });
    </script>

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
      
</body>
</html>