<?php
    session_start();
    require_once('classes/Jogo.php');

    if(!isset($_SESSION['nome_usuario_botao'])){
        header('Location: index.php?erro=1');
    }

    $erro = null;
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jogos</title>

    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->    
    <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/jquery/jquery-3.3.1.min.js"></script>    
    <script src="componentes/js/jogos.js"></script> 
    <script src="componentes/js/utils.js"></script>    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid blue;
  border-bottom: 16px solid blue;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
</head>
<body>
    
    <?php require 'componentes/menu.php'; ?>

    <div class="container">

    <div class="container-fluid jogos">        
        <nav class="navbar navbar-expand-lg navbar-light bg-light pt-3 pb-3">
            <div class="container">
                <label class="col-md-2" for="campeonato">Campeonato</label>
            
                <div class="col-md">
                    <select name="campeonatoId" id="campeonatoId" class="form-control"><div id="comboCampeonato"></div></select> 
                </div>
            </div>
        </nav>
    </div>
       
    <div id="div-campeao-vice"></div>     

    <div class="container corpo-jogos"> 
        <div class="row justify-content-md-center">
            <div class="col-xl-6">
                <div id="tabela-jogos"></div>  
            </div>

            <!--
            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalJogo"><i class="fa fa-edit"></i>jogo</button>

            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalPenaltis"><i class="fa fa-edit"></i>pênaltis</button>
            -->

            
            <div class="col-xl-6">  
                <div id="tabela-classificacao" style="margin-bottom: 70px;"></div>               
            </div>
        </div>
    </div>
</div>
    <?php require 'componentes/rodape.php'; ?>  

    
            
<div class="modal fade" id="spinner" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modalJogo" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="loader"></div>
    </div>
</div>

    <!-- Modal -->
    <div class="modal fade" id="modalConfirmar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Zerar placar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja zerar o placar deste jogo?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" id="btn-zerar">Zerar</button>
            </div>
            </div>
        </div>
    </div>


    <!-- Modal JOGO -->
    <div class="modal fade" id="modalJogo" tabindex="-1" role="dialog" aria-labelledby="modalJogoLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content" style="width: 603px;">
                <form id="formulario-jogo" name="formulario-jogo" style="width: 600px;">
                    <div class="modal-body bg-light" >
                        <div class="container-fluid" style="padding-left: 10px !important;">
                            <div class="row justify-content-center">
                                <div class="col-md-3" style="padding-left: 10px !important;"><img src="" id="distintivo-mandante" class="img-fluid"></div>
        
                                <div class="col-md-2 text-right" id="jogo-placar-mandante" style="font-size: 56px;">0</div>
        
                                <div class="col-1 text-secondary text-center" style="font-size: 50px; padding-left: 0px; padding-right: 0px;">x</div>
        
                                <div class="col-md-2 text-left" id="jogo-placar-visitante" style="font-size: 56px;">0</div>
        
                                <div class="col-md-3 "style="padding-left: 10px !important;"><img src="" id="distintivo-visitante" class="img-fluid"></div> 
                            </div>  
                            
                            <br>

                            <div class="row">
                                <div class="col-md-6 text-center"> <h4 class="font-weight-bold"> <label for="nome-mandante" id="nome-mandante"></label></h4>  </div>
                                <div class="col-md-6 ml-auto text-center"><h4 class="font-weight-bold"><label for="nome-visitante" id="nome-visitante"></label></h4></div>
                            </div>
                
                        </div>  
                    </div>

                    <div class="modal-body bg-info">      
                        <div class="row">
                            <div class="col-md-6 text-center">
                                
                                <div class="input-group input-group-sm">
                                    <div class="input-group-append">
                                        <button type="button" id="remover-mandante" class="btn btn-danger"><i class="fa fa-minus fa-1x"></i></button>
                                    </div>

                                    <select class="input-group custom-select" id="select-mandante" style="background: ghostwhite;"></select>
                                    
                                    <div class="input-group-append">
                                        <button type="button" id="adicionar-mandante" class="btn btn-success"><i class="fa fa-plus fa-1x"></i></button>
                                    </div>
                                </div>                              

                                <div class="form-group">
                                    <label for="lista-mandante">Goleadores</label>
                                    <textarea class="form-control" id="lista-mandante" rows="5" readonly></textarea>
                                </div>

                                <button class="btn btn-dark" type="button" id="gravar-mandante">Adicionar jogador</i></button>

                            </div>
                            <div class="col-md-6 text-center">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-append">
                                        <button type="button" id="remover-visitante" class="btn btn-danger"><i class="fa fa-minus fa-1x"></i></button>
                                    </div>
                                
                                    <select class="input-group custom-select" id="select-visitante" style="background: ghostwhite;"></select>
                                
                                    <div class="input-group-append">
                                        <button type="button" id="adicionar-visitante" class="btn btn-success"><i class="fa fa-plus fa-1x"></i></button>
                                    </div>
                                </div>   

                                <div class="form-group">
                                    <label for="lista-visitante">Goleadores</label>
                                    <textarea class="form-control" id="lista-visitante" rows="5" readonly></textarea>
                                </div>

                                <button class="btn btn-dark" type="button" id="gravar-visitante">Adicionar jogador</i></button>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary" >Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>    

    <div class="modal fade" id="modal-add-jogador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="formulario-jogador">
                        <div class="form-group">
                            <label for="time-id" class="col-form-label">Time ID:</label>
                            <input type="text" class="form-control" id="time-id" name="time-id" readonly>
                        </div>

                        <div class="form-group">
                            <label for="numero-jogador" class="col-form-label">Número:</label>
                            <input type="text" class="form-control" id="numero-jogador" name="numero-jogador" maxlength="2" required="required" autofocus>
                        </div>

                        <div class="form-group">
                            <label for="nome-jogador" class="col-form-label">Nome:</label>
                            <input type="text" class="form-control" id="nome-jogador" name="nome-jogador" maxlength="100" required=required>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-gravar-jogador">Gravar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal AVISO-->
    <div class="modal fade" id="aviso" tabindex="-1" role="dialog" aria-labelledby="avisoTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Jogos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
                <h5><i class="fa fa-info-circle text-primary"></i> Campeonato encerrado.</h5>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
            </div>
        </div>
    </div>

    <!-- Modal PENALTIS -->
    <div class="modal fade" id="modalPenaltis" tabindex="-1" role="dialog" aria-labelledby="modalPenaltisLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <div class="modal-body bg-light" >
                    <div class="container-fluid" style="padding-left: 10px !important;">
                        <div class="row justify-content-center">
                            <div class="col-md-3" style="padding-left: 10px !important;"><img src="" id="distintivo-penalti-mandante" class="img-fluid"></div>
    
                            <div class="col-md-2 text-right" name="quantidade-mandante" id="penalti-mandante" style="font-size: 56px;"></div>
    
                            <div class="col-1 text-secondary text-center" style="font-size: 50px; padding-left: 0px; padding-right: 0px;">x</div>
    
                            <div class="col-md-2 text-left" id="penalti-visitante" style="font-size: 56px;"></div>
    
                            <div class="col-md-3 "style="padding-left: 10px !important;"><img src="" id="distintivo-penalti-visitante" class="img-fluid"></div> 
                        </div>  
                        
                        <br>

                        <div class="row">
                            <div class="col-md-4 text-center"> <h4 class="font-weight-bold"><label for="penalti-nome-mandante" id="penalti-nome-mandante"></label></h4>  </div>
                           
                            <div class="col-md-4 ml-auto text-center"><h4 class="font-weight-bold"><label for="penalti-nome-visitante" id="penalti-nome-visitante"></label></h4></div>
                        </div>
               
                    </div>  
                </div>

                <div class="modal-body bg-dark">      
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <div class="input-group" id="spinner">
                                <span class="input-group-prepend">
                                    <button class="btn btn-danger btn-lg" id="botao-menos-mandante" data-action="decrement"><i class="fa fa-minus"></i></button>
                                </span>    

                                <input type="text" name="quantidade-mandante" id="input-mandante" class="form-control form-control-lg text-center font-weight-bold" value="0" min="0" max="99" disabled> 

                                <span class="input-group-prepend">
                                    <button class="btn btn-success btn-lg" id="botao-mais-mandante" data-action="increment"><i class="fa fa-plus"></i></button>
                                </span>
                            </div>      
                        </div>

                        <div class="col-md-6 text-center">
                            <div class="input-group" id="spinner">
                                <span class="input-group-prepend">
                                    <button class="btn btn-danger btn-lg" id="botao-menos-visitante" data-action="decrement"><i class="fa fa-minus"></i></button>
                                </span>                               

                                <input type="text" name="quantidade-visitante" id="input-visitante" class="form-control form-control-lg text-center font-weight-bold" value="0" min="0" max="99" disabled> 
                                
                                <span class="input-group-prepend">
                                    <button class="btn btn-success btn-lg" id="botao-mais-visitante" data-action="increment"><i class="fa fa-plus"></i></button>
                                </span>
                            </div>
                        </div>       
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" id="btn-penaltis">Salvar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="componentes/bootstrap/js/bootstrap.min.js"></script>

    <script>    
    
    jQuery(document).ready(function(){
        $('#spinner button').on('click', function(){
            let input = $(this).closest('#spinner').find('input[name=quantidade-mandante]');

            if($(this).data('action') === 'increment') {
                if(input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                    input.val(parseInt(input.val(), 10) + 1);
                }
            } else if($(this).data('action') === 'decrement') {
                if(input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                    input.val(parseInt(input.val(), 10) - 1);
                }
            }
        });

        $('#spinner button').on('click', function(){
            let input = $(this).closest('#spinner').find('input[name=quantidade-visitante]');

            if($(this).data('action') === 'increment') {
                if(input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                    input.val(parseInt(input.val(), 10) + 1);
                }
            } else if($(this).data('action') === 'decrement') {
                if(input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                    input.val(parseInt(input.val(), 10) - 1);
                }
            }
        });
    });
    
    </script>
</body>
</html>