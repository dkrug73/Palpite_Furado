<?php
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Campeonato.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $campeonato = new Campeonato($conexao); 

    if(isset($_POST['campeonatoId'])){
        $campeonato->setId($_POST['campeonatoId']);
    }
    
    $dadosCampeonato = $campeonato->RetornarCampeaoVice();
    
    $campeonato->FecharConexao();

    if ($dadosCampeonato['0'] === "0") { ?>
        <div class="container bg-warning mb-4 pt-3">   
            <div class="row justify-content-center">    
                <div class="col-4">
                    <label for="">Campeão: <h2 id="campeao"><?php echo $dadosCampeonato['1']; ?></h2></label>         
                </div>

                <div class="col-4">   
                    <label for="">Vice-campeão: <h2 id="vice"><?php echo $dadosCampeonato['2']; ?></h2></label>          
                </div>
            </div>      
        </div>  <?php
    } ?>