<?php
    session_start();

    unset($_SESSION['id_usuario_botao']);
    unset($_SESSION['nome_usuario_botao']);
    unset($_SESSION['email_usuario_botao']);
    unset($_SESSION['administrador_usuario_botao']);

    header('Location: ../index.php');
?>