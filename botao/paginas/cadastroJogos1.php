<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Jogo.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogo = new Jogo($conexao); 

    if (isset($_POST['jogo-id'])){
        $jogo->setId($_POST['jogo-id']);
    }

    $jogo->setCampeonatoId($_POST['campeonatoId']);
    $jogo->setMandanteId($_POST['mandante']);
    $jogo->setVisitanteId($_POST['visitante']);
    $jogo->setRodada($_POST['rodada']);
    $jogo->setTurno($_POST['turno']);
    $jogo->setGrupo($_POST['grupo']);
    $jogo->setOrdem($_POST['ordem']);
    
    $gravar = $jogo->Gravarjogo();
    $jogo->FecharConexao();

    echo $gravar;
?>