<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Jogador.php');

    if (!isset($_SESSION['administrador_usuario_botao'])) {
        echo "visitante";
        return true;
    }
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogador = new Jogador($conexao); 
    
    $jogador->setTimeId($_POST['time-id']);
    $jogador->setNome($_POST['nome-jogador']);
    $jogador->setNumero($_POST['numero-jogador']);
    
    $gravar = $jogador->Gravarjogador($jogador);
    $jogador->FecharConexao();

    echo $gravar;
?>