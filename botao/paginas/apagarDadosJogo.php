<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Jogo.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogo = new Jogo($conexao); 

    $jogo->Id = $_POST['jogoId'];	

    $dadosJogo = $jogo->RetornaDadosJogo(); 

    $jogo->CampeonatoId = $dadosJogo['0'];
    $jogo->MandanteId = $dadosJogo['1'];
    $jogo->VisitanteId = $dadosJogo['2'];
    $jogo->Turno = $dadosJogo['3'];
    $jogo->Grupo = $dadosJogo['4'];    
    $jogo->PlacarMandante = $dadosJogo['5'];
    $jogo->PlacarVisitante = $dadosJogo['6'];
    $jogo->PenaltiMandante = $dadosJogo['7'];
    $jogo->PenaltiVisitante = $dadosJogo['8'];

    $sql = "
        DELETE FROM 
			tabela 
		WHERE 
			jogoId = '$jogo->Id' ";

    if(!$conexao->query($sql)) echo false;
    
	$sql = "
        DELETE FROM 
			jogojogador 
		WHERE 
			jogoId = '$jogo->Id' ";

	if(!$conexao->query($sql)) echo false;

	$sql = "
        UPDATE 
			jogos 
		SET 
			placarMandante = null, 
			placarVisitante = null, 
			penaltiMandante = null, 
			penaltiVisitante = null 
		WHERE 
			id = '$jogo->Id' ";

	if(!$conexao->query($sql)) echo false;

    $jogo->FecharConexao();    

    echo true;
?>