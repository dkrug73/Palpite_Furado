<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Time.php');
    require_once('../componentes/funcoes.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $time = new Time($conexao); 

    $time->setNome($_POST['nome']);
    $time->setNomeOficial($_POST['nome']);
    $time->setNomeOficial($_POST['nome-oficial']);
    $time->setTecnico($_POST['tecnicoId']);
    
    if(isset($_POST['time-id'])){        
        $time->setId($_POST['time-id']);
    }

	if (!empty($_FILES['foto-time']['name'])) {
        $time->setFoto(MoverFoto("times", $_FILES['foto-time'], $_POST['foto-time']));
    }

    $gravar = $time->GravarTime($time);
    $time->FecharConexao();

    if($gravar == '1'){        
        header('Location: ../cadastroTime.php?erro=0');
    }  
    elseif ($gravar == '0'){
        header('Location: ../cadastroTime.php?erro=1');
    } 
    else(
        header("Location: ../cadastroTime.php?erro=$gravar ")
    )      
?>