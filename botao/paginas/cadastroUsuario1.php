<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Usuario.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $usuario = new Usuario($conexao); 

    $usuario->setNome($_POST['nome']);
    $usuario->setNomeUsuario($_POST['nome-usuario']);
    $usuario->setSenha(md5($_POST['senha']));
    $usuario->setEmail($_POST['email']);
    
    if(isset($_POST['usuario-id'])){        
        $usuario->setId($_POST['usuario-id']);
    }

    $usuario->setAdministrador(0);
    if(isset($_POST['administrador'])){
        $usuario->setAdministrador(1);
    }   

	if (isset($_FILES['foto-time'])) {

        $foto = $_FILES['foto-time'];

        if($foto['error'] == 0) {        
            //define o caminho 
            $destino = "../imagens/usuarios/";

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);		
                
            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            //armazena na variavel o novo nome
            $arquivo = basename($nome_imagem);

            //concatena a variavel do caminho com o nome do arquivo 
            $destino = $destino . $arquivo; 

            //testa se a funçao de upload rodar
            if(move_uploaded_file($foto['tmp_name'], $destino)) {
                $usuario->setFoto($arquivo);
            }
        }
        else{
            if(isset($_POST['foto-usuario'])){
                $foto = explode('/', $_POST['foto-usuario']);
                $usuario->setFoto($foto[2]);
            }
        }
    }

    $gravar = $usuario->GravarUsuario($usuario);
    $usuario->FecharConexao();

    if($gravar == '1'){        
        header('Location: ../cadastroUsuario.php?erro=0');
    }  
    elseif ($gravar == '0'){
        header('Location: ../cadastroUsuario.php?erro=1');
    } 
    else(
        header("Location: ../cadastroUsuario.php?erro=$gravar ")
    )      
?>