<?php
    require_once('../../classes/Formula.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $formula = new formula($conexao); 
    $rs = $formula->ObterDadosFormulas();
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-formula">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Descrição</th>
            <th>Quantidade de times</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){
            $formula->Id = $reg['id'];
            $formula->Descricao = $reg['descricao'];
            $formula->QuantidadeTimes = $reg['quantidadeTimes']; ?>
         

            <tr style='cursor: pointer;'>
                <td><?php echo $formula->Id; ?></td>
                <td><?php echo $formula->Descricao ?></td>
                <td><?php echo $formula->QuantidadeTimes ?></td>
            </tr> <?php
        } 

        $formula->FecharConexao();  ?>

    </tbody>
</table>
<br />