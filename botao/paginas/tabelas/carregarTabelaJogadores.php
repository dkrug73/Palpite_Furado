<?php
    require_once('../../classes/Jogador.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogador = new Jogador($conexao); 
    $rs = $jogador->ObterDadosjogador();
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-jogador">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Número</th>
            <th>Nome</th>
            <th>Time/técnico</th>
            <th>Gols</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){
            $jogador->Id = $reg['id'];
            $jogador->Numero = $reg['numero'];
            $jogador->Nome = $reg['nome']; 
            $nomeTime = $reg['nomeTime'];
            //$gols = $reg['gols'];?>
         

            <tr style='cursor: pointer;'>
                <td><?php echo $jogador->Id; ?></td>
                <td><?php echo $jogador->Numero ?></td>
                <td><?php echo $jogador->Nome ?></td>
                <td><?php echo $nomeTime ?></td>
                <td>0</td>
                <!--<td><?php //echo $gols ?></td>-->
            </tr> <?php
        } 

        $jogador->FecharConexao();  ?>

    </tbody>
</table>
<br />