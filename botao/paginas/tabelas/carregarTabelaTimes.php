<?php
    require_once('../../classes/Time.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $time = new Time($conexao); 
    $rs = $time->ObterDadosTimes();
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-time">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Nome Oficial</th>
            <th>Téc. Id</th>
            <th>Técnico</th>
            <th>Foto</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){
            $time->Id = $reg['id'];
            $time->Nome = $reg['nome'];
            $time->NomeOficial = $reg['nomeOficial']; 
            $time->Tecnico = $reg['tecnico'];
            $tecnicoId = $reg['tecnicoId'];
            $time->Foto = $reg['foto'];
            $foto = $reg['foto'];                            
            if(!empty($foto)){
                $foto = "imagens/times/".$foto;
            } ?> 

            <tr style='cursor: pointer;'>
                <td><?php echo $time->Id; ?></td>
                <td><?php echo $time->Nome ?></td>
                <td><?php echo $time->NomeOficial ?></td>
                <td><?php echo $tecnicoId ?></td>
                <td><?php echo $time->Tecnico ?></td>
                <td><img src = "<?php print $foto; ?>" width = "25"></td>
            </tr> <?php
        } 

        $time->FecharConexao();  ?>

    </tbody>
</table>
<br />