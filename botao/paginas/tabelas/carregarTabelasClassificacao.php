<?php
    require_once('../../classes/Campeonato.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    //$campeonatoId = $_POST['campeonatoId'];

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $campeonato = new Campeonato($conexao); 
    $campeonato->Id = $_POST['campeonatoId'];

    $configuracoes = $campeonato->ObterConfiguracoesCampeonato($campeonato->Id);

    $doisGrupos = $configuracoes[0];
    $doisTurnos = $configuracoes[1];

    if ($doisGrupos){
        $aba1 = "GRUPO A";
        $aba2 = "GRUPO B";

        $rsTabela1 = $campeonato->CarregarGridTabela("A", null);
        $rsTabela2 = $campeonato->CarregarGridTabela("B", null);
        $rsTabela3 = $campeonato->CarregarGridTabela("G", null);
    }
    else{
        if ($doisTurnos){
            $rsTabela1 = $campeonato->CarregarGridTabela("A", "1");
            $rsTabela2 = $campeonato->CarregarGridTabela("A", "2");
            $rsTabela3 = $campeonato->CarregarGridTabela(null, null);

            

            CarregarTabela("CLASSIFICAÇÃO DO 1º TURNO", $rsTabela1);
            CarregarTabela("CLASSIFICAÇÃO DO 2º TURNO", $rsTabela2);
            CarregarTabela("CLASSIFICAÇÃO GERAL", $rsTabela3);
        }
        else{
            $rsTabela1 = $campeonato->CarregarGridTabela("A", null);
        }
    }    


    function CarregarTabela($titulo, $rsTabela){ ?>

        <div class="card" style="margin-bottom: 30px;">
            <div style="font-size: 25px;text-align: center;" class="card text-white bg-success ">
                <?PHP echo $titulo ?>
            </div>           

            <table class="table table-responsive-xl table-sm">
                <thead class="bg-light">
                    <tr>
                        <th>P</th>
                        <th>Time</th>
                        <th class="text-center" style="width:40px;">PG</th>
                        <th class="text-center" style="width:40px;">V</th>
                        <th class="text-center" style="width:40px;">E</th>
                        <th class="text-center" style="width:40px;">D</th>
                        <th class="text-center" style="width:40px;">GP</th>
                        <th class="text-center" style="width:40px;">GC</th>
                        <th class="text-center" style="width:40px;">SG</th>
                        <th class="text-center" style="width:40px;">AP</th>
                    </tr> 
                </thead>                           
                                                                      
                <tbody><?php  
                    $posicao = 1;                                   
                    while($reg=mysqli_fetch_array($rsTabela)){ 
                        $divisor = $reg['pontoGanho'] * 100;
                        $dividendo = ($reg['vitoria'] + $reg['derrota'] + $reg['empate']) * 3;
                        
                        $aproveitamento = 0;

                        if ($dividendo > 0 And $divisor > 0) {
                                $aproveitamento = ($divisor / $dividendo) ;
                                $aproveitamento = round($aproveitamento, 0)."%"; // Onde 2 são as casas decimais 
                        }  
                        else {
                                $aproveitamento = $aproveitamento."%";
                        }
                        
                        ?>      
                    <tr>                               
                        <th><?php echo $posicao ?>º</th>                  
                        <td> <?php print $reg['timeNome']; ?></td>
                        <td class="text-center"> <?php echo $reg['pontoGanho']; ?></td>
                        <td class="text-center"> <?php echo $reg['vitoria']; ?></td>
                        <td class="text-center"> <?php echo $reg['empate']; ?></td>
                        <td class="text-center"> <?php echo $reg['derrota']; ?></td>
                        <td class="text-center"> <?php echo $reg['golFavor']; ?></td>
                        <td class="text-center"> <?php echo $reg['golContra']; ?></td> <?php 

                        if ($reg['saldo'] >= 0) {  ?>
                            <td class="text-center"><?PHP print $reg['saldo']; ?></td> <?php                        
                        }
                        else { ?>
                            <td style="color:red;"><?PHP print $reg['saldo']; ?></td> <?php                       
                        }?> 

                        <td class="text-center"> <?php echo $aproveitamento; ?></td>
                    </tr>  <?php
                        $posicao++;
                    }  ?>	                            
                                                                      
                       
                </tbody>
            </table>
        </div>
          <?php
    } ?>
