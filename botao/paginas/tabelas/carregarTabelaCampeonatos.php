<?php
    require_once('../../classes/Campeonato.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $campeonato = new Campeonato($conexao); 
    $rs = $campeonato->ObterDadoscampeonato();
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-campeonato">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Descrição</th>
            <th>Local</th>
            <th>Edição</th>
            <th>Ranking</th>
            <th>Data</th>
            <th>Ativo</th>
            <th style="display: none;">Grupos</th>
            <th style="display: none;">Turnos</th>
            <th style="display: none;">Evitar Confrontos</th>
            <th style="display: none;">Formula ID</th>
            <th style="display: none;">Jogos</th>
            <th style="display: none;">Tipo Final</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td style="width: 45px;"><?php echo $reg['id']; ?></td>
                <td><?php echo $reg['descricao']; ?></td>
                <td style="width: 180px;"><?php echo $reg['local']; ?></td>
                <td style="width: 60px;"><?php echo $reg['edicao']; ?></td>
                <td style="width: 70px;"><?php echo $reg['classeRanking']; ?></td>
                <td style="width: 90px;"><?php echo $reg['data']; ?></td>
                <td style="width: 90px;"><?php echo $reg['ativo']; ?></td>
                <td style="width: 90px; display: none;"><?php echo $reg['doisGrupos']; ?></td>
                <td style="width: 90px; display: none;"><?php echo $reg['doisTurnos']; ?></td>
                <td style="width: 90px; display: none;"><?php echo $reg['evitarConfrontos']; ?></td>
                <td style="width: 90px; display: none;"><?php echo $reg['formulaId']; ?></td>
                <td style="width: 90px; display: none;"><?php echo $reg['jogos']; ?></td>
                <td style="width: 90px; display: none;"><?php echo $reg['tipoFinal']; ?></td>
            </tr> <?php
        } 

        $campeonato->FecharConexao();  ?>

    </tbody>
</table>
<br />