<?php
    require_once('../../classes/FormulaJogos.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  
    
    $formulaJogos = new FormulaJogos($conexao); 
    $formulaJogos->setFormulaId($_POST['id']);
    $rs = $formulaJogos->ObterDadosFormulaJogos();
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-formula">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Formula</th>            
            <th>Rodada</th>
            <th>Mandante</th>
            <th>Visitante</th>
            <th style="display: none;">FormulaId</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td><?php echo $reg['id']; ?></td>
                <td><?php echo $reg['descricao'] ?></td>
                <td><?php echo $reg['rodada'] ?></td>                
                <td><?php echo $Mandante = $reg['mandante'] ?></td>
                <td><?php echo $reg['visitante'] ?></td>
                <td style="display: none;"><?php echo $reg['formulaId'] ?></td>
            </tr> <?php
        } 

        $formulaJogos->FecharConexao();  ?>
    </tbody>
</table>
<br />
<br />
<br />