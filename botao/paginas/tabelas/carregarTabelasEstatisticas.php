<?php
    require_once('../../classes/Campeonato.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $campeonato = new Campeonato($conexao); 
    $campeonato->setId($_POST['campeonatoId']);

    $rsPorTime = $campeonato->ObterDadosGoleadoresPorTime();
    $rsPorCampeonato = $campeonato->ObterDadosGoleadoresPorCampeonato();
    $rsRanking = $campeonato->ObterDadosRanking();
    $rsGoleadoresPorRanking = $campeonato->ObterDadosGoleadoresPorRanking();
?>

<div class="row justify-content-md-center">  
    <!--TABELA GOLEADORES POR TIME  -->
    <div class="col col-lg-4 pt-4">        
        <div class="p3 mb-2 bg-info text-white text-center">
            <h3>Goleadores por time</h3>
            <div> </div>
        </div>
            
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col" class="border-0 text-center" style="width: 50px;">Nº</th>
                    <th scope="col" class="border-0">Nome</th>
                    <th scope="col" class="border-0 text-center">Gols</th>
                </tr>
            </thead>
            <tbody> <?php
                $timeAnterior = null;                                   
                while($reg=mysqli_fetch_array($rsPorTime)){ 
                    if($reg['timeNome'] != $timeAnterior){ ?>
                        <tr>
                            <td colspan="3" class="text-uppercase text-center text-danger font-weight-bold"><?php echo $reg['timeNome'] ?></td>
                        </tr> <?php
                    } ?>  

                <tr>  
                    <td class="text-center"> <?php echo $reg['numero']; ?></td>
                    <td> <?php echo $reg['jogadorNome']; ?></td>
                    <td class="text-center"> <?php echo $reg['gols']; ?></td>
                </tr>  <?php
                    $timeAnterior = $reg['timeNome'];
                }  ?>	                       
            </tbody>
        </table> 
    </div> <!-- FINAL DA COLUNA GOLEADORES POR TIME -->

    <!-- COLUNA GOLEADORES POR CAMPEONATO -->
    <div class="col-md-auto col-lg-4 pt-4">
        <div class="p3 mb-2 bg-info text-white text-center">
            <h3>Goleadores por campeonato</h3>
            <div> </div>
        </div>                
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col" class="border-0" style="width: 50px;">Nº</th>
                    <th scope="col" class="border-0">Nome</th>
                    <th scope="col" class="border-0">Gols</th>
                    <th scope="col" class="border-0">Time</th>
                </tr> 
            </thead> 

            <tbody> <?php                              
                while($reg=mysqli_fetch_array($rsPorCampeonato)){ ?>
                <tr>  
                    <td class="text-center"> <?php echo $reg['numero']; ?></td>
                    <td> <?php echo $reg['jogadorNome']; ?></td>
                    <td class="text-center"> <?php echo $reg['gols']; ?></td>
                    <td> <?php echo $reg['timeNome']; ?></td>
                </tr>  <?php
                }  ?>	
            </tbody>
        </table>
    </div>  <!-- FINAL DA COLUNA GOLEADORES POR CAMPEONATO -->

    <!-- COLUNA RANKING -->
    <div class="col col-lg-4 pt-4">   
        <div>
            <div class="p3 mb-2 bg-info text-white text-center">
                <h3>Ranking</h3>
            <div> </div>
        </div>

        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col" class="border-0" style="width: 25px;">P</th>
                    <th scope="col" class="border-0">Time</th>
                    <th scope="col" class="border-0">Técnico</th>
                    <th scope="col" class="border-0">Pontos</th>
                </tr> 
            </thead>     
                        
            <tbody>   <?php
                $posicao = 0;                              
                while($reg=mysqli_fetch_array($rsRanking)){ 
                    $posicao++;?>
                
                <tr>  
                    <td class="text-center"> <?php echo $posicao; ?>º</td>
                    <td> <?php echo $reg['timeNome']; ?></td>
                    <td> <?php echo $reg['tecnico']; ?></td>
                    <td class="text-center"> <?php echo $reg['pontos']; ?></td>                   
                </tr>  <?php
                }  ?>	                   
            </tbody>
        </table>
    </div><!-- FINAL DA COLUNA RANKING -->

    <!-- COLUNA GOLEADORES DO RANKING -->
    <div>
        <div class="p3 mb-2 mt-5 bg-info text-white text-center">
            <h3>Goleadores do ranking</h3>
            <div></div>
        </div>

        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col" class="border-0" style="width: 50px;">Nº</th>
                    <th scope="col" class="border-0">Nome</th>
                    <th scope="col" class="border-0">Gols</th>
                    <th scope="col" class="border-0">Time</th>
                </tr> 
            </thead>   
                        
            <tbody> <?php                             
                while($reg=mysqli_fetch_array($rsGoleadoresPorRanking)){ ?>          
                    <tr>                                     
                        <td class="text-center"> <?php echo $reg['numero']; ?></td>
                        <td> <?php echo $reg['jogadorNome']; ?></td>
                        <td> <?php echo $reg['gols']; ?></td>
                        <td> <?php echo $reg['timeNome']; ?></td>                            
                    </tr> <?php
                }  ?>		                            
            </tbody>
        </table>
    </div> <!-- FINAL DA COLUNA GOLEADORES DO RANKING -->
</div>

             