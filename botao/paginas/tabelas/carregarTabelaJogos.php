<?php
    require_once('../../classes/Jogo.php');
    require_once('../../classes/Campeonato.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogo = new Jogo($conexao); 
    $campeonato = new Campeonato($conexao); 

    $abaAtiva = $_POST['abaAtiva'];

    if ($abaAtiva == "0") $abaAtiva = "pills-home";    
    
    if(isset($_POST['campeonatoId'])){
        $configuracaoCampeonato = $campeonato->ObterConfiguracoesCampeonato($_POST['campeonatoId']);

        $doisGrupos = $configuracaoCampeonato[0];
        $doisTurnos = $configuracaoCampeonato[1];
        $tipoFinal = $configuracaoCampeonato[2];

        $jogo->CampeonatoId = $_POST['campeonatoId'];
        
        $rs=$rs2=$jogo->ObterDadosJogos();
    }

    $aba1 = "JOGOS";
    $aba2 = "";
    $aba3 = "";
    $aba4 = "";
    
    if ($doisGrupos){
        $aba1 = "GRUPO A";
        $aba2 = "GRUPO B";

        $rsAba1 = $jogo->ObterDadosJogosTurnoGrupo("A", null);
        $rsAba2 = $jogo->ObterDadosJogosTurnoGrupo("B", null);
    }
    else{
        if ($doisTurnos){
            $aba1 = "1º TURNO";
            $aba2 = "2º TURNO";

            $rsAba1 = $jogo->ObterDadosJogosTurnoGrupo(null, "1");
            $rsAba2 = $jogo->ObterDadosJogosTurnoGrupo(null, "2");
        }
        else{
            $rsAba1 = $jogo->ObterDadosJogosTurnoGrupo(null, null);
        }
    }    
    
    $rsSemiFinal = $jogo->ObterDadosJogosTurnoGrupo(null, "3"); 

    if ($rsSemiFinal->num_rows > 0) {
        $aba3 = "SEMI-FINAL";
    }

    $rsFinal = $jogo->ObterDadosJogosTurnoGrupo(null, "4"); 

    if ($rsFinal->num_rows > 0) {
        $aba4 = "FINAL";
    }

    ?>


<ul class="nav nav-pills" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link  <?= $abaAtiva == "pills-home" ? 'active' : '' ?>" id="pills-home-tab" data-toggle="pill" 
            href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><?php echo $aba1; ?></a>
    </li>
    
    <li class="nav-item">
        <a class="nav-link  <?= $abaAtiva == "pills-profile" ? 'active' : '' ?>" id="pills-profile-tab" data-toggle="pill" 
            href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><?php echo $aba2; ?></a>
    </li> <?php 
    
    if ($aba3 != "") { ?>
        <li class="nav-item">
            <a class="nav-link  <?= $abaAtiva == "pills-semi" ? 'active' : '' ?>"id="pills-semi-tab" data-toggle="pill" 
                href="#pills-semi" role="tab" aria-controls="pills-semi" aria-selected="false"><?php echo $aba3; ?></a>
        </li> <?php
    } 
    
    if ($aba4 != "") { ?>
        <li class="nav-item">
            <a class="nav-link  <?= $abaAtiva == "pills-final" ? 'active' : '' ?>"id="pills-final-tab" data-toggle="pill" 
                href="#pills-final" role="tab" aria-controls="pills-final" aria-selected="false"><?php echo $aba4; ?></a>
        </li><?php
    } ?>
</ul>

<div class="card" style="margin-bottom: 30px; margin-top: 15px;">
    <div class="tab-content" id="pills-tabContent">

        <div class="tab-pane fade  <?= $abaAtiva == "pills-home" ? 'show active' : '' ?>" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">  
            <table class="table table-borderless table-hover table-responsive-xl">
                <tbody> <?php  
                    $rodadaAnterior = "";
                    $turnoAnterior = "";
                    while($reg=mysqli_fetch_array($rsAba1)){                        
                        $rodada = $reg['rodada'];
                        $turno = $reg['turno'];

                        $descricaoRodada = $reg['rodada']."ª RODADA";

                        if ($doisTurnos && $doisGrupos) $descricaoRodada = $reg['turno']."º TURNO";

                        if ($rodadaAnterior != $rodada || $turnoAnterior != $turno){
                            echo "<tr><td class='text-center' colspan='5' style='color: red;'>".$descricaoRodada."</td></tr>";
                            $rodadaAnterior = $rodada;
                            $turnoAnterior = $turno;
                        } ?>                        

                    <tr style='cursor: pointer;'>
                        <td class="text-right" style="display: none"><?php echo $reg['id']; ?></td>
                        <td class="text-right"><?php echo $reg['mandanteNome']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['mandanteFoto']; ?>" style="width: 30px;">  </td>
                        <td class="text-center"><?php echo $reg['placarMandante']; ?>
                            <span class="penalti"><?php echo $reg['penaltiMandante']; ?></span> x 
                            <span class="penalti"><?php echo $reg['penaltiVisitante'];?></span><?php echo $reg['placarVisitante']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['visitanteFoto']; ?>" style="width: 30px;">  </td>
                        <td><?php echo $reg['visitanteNome']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteId']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteId']; ?></td>
                    </tr> <?php                    
                    } ?> 			
                </tbody>
            </table>
        </div>  
  
        <div class="tab-pane fade  <?= $abaAtiva == "pills-profile" ? 'show active' : '' ?>" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab"> 
            <table class="table table-borderless table-hover">
                <tbody>     
                <?php  
                    $rodadaAnterior = "";
                    $turnoAnterior = "";
                    while($reg=mysqli_fetch_array($rsAba2)){                                
                        $rodada = $reg['rodada'];
                        $turno = $reg['turno'];

                        $descricaoRodada = $reg['rodada']."ª RODADA";

                        if ($doisTurnos && $doisGrupos) $descricaoRodada = $reg['turno']."º TURNO";

                        if ($rodadaAnterior != $rodada || $turnoAnterior != $turno){
                            echo "<tr><td class='text-center' colspan='5' style='color: red;'>".$descricaoRodada."</td></tr>";
                            $rodadaAnterior = $rodada;
                            $turnoAnterior = $turno;
                        } ?>                        

                    <tr style='cursor: pointer;'>
                        <td class="text-right" style="display: none"><?php echo $reg['id']; ?></td>
                        <td class="text-right"><?php echo $reg['mandanteNome']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['mandanteFoto']; ?>" style="width: 30px;">  </td>
                        <td class="text-center"><?php echo $reg['placarMandante']; ?>
                            <span class="penalti"><?php echo $reg['penaltiMandante']; ?></span> x 
                            <span class="penalti"><?php echo $reg['penaltiVisitante'];?></span><?php echo $reg['placarVisitante']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['visitanteFoto']; ?>" style="width: 30px;">  </td>
                        <td><?php echo $reg['visitanteNome']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteId']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteId']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['turno']; ?></td>
                    </tr> <?php                              
                    } ?> 			
                </tbody>
            </table>
        </div>

        <div class="tab-pane fade  <?= $abaAtiva == "pills-semi" ? 'show active' : '' ?>" id="pills-semi" role="tabpanel" aria-labelledby="pills-semi-tab"> 
            <table class="table table-borderless table-hover">
                <tbody>     
                <?php  
                    while($reg=mysqli_fetch_array($rsSemiFinal)){  ?>
                    <tr style='cursor: pointer;'>
                        <td class="text-right" style="display: none"><?php echo $reg['id']; ?></td>
                        <td class="text-right"><?php echo $reg['mandanteNome']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['mandanteFoto']; ?>" style="width: 30px;">  </td>
                        <td class="text-center"><?php echo $reg['placarMandante']; ?>
                            <span class="penalti"><?php echo $reg['penaltiMandante']; ?></span> x 
                            <span class="penalti"><?php echo $reg['penaltiVisitante'];?></span><?php echo $reg['placarVisitante']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['visitanteFoto']; ?>" style="width: 30px;">  </td>
                        <td><?php echo $reg['visitanteNome']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteId']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteId']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['turno']; ?></td>
                    </tr> <?php                              
                    } ?> 			
                </tbody>
            </table>
        </div>

        <div class="tab-pane fade  <?= $abaAtiva == "pills-final" ? 'show active' : '' ?>" id="pills-final" role="tabpanel" aria-labelledby="pills-final-tab"> 
            <table class="table table-borderless table-hover">
                <tbody>     
                <?php  
                    while($reg=mysqli_fetch_array($rsFinal)){  ?>
                    <tr style='cursor: pointer;'>
                        <td class="text-right" style="display: none"><?php echo $reg['id']; ?></td>
                        <td class="text-right"><?php echo $reg['mandanteNome']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['mandanteFoto']; ?>" style="width: 30px;">  </td>
                        <td class="text-center"><?php echo $reg['placarMandante']; ?>
                            <span class="penalti"><?php echo $reg['penaltiMandante']; ?></span> x 
                            <span class="penalti"><?php echo $reg['penaltiVisitante'];?></span><?php echo $reg['placarVisitante']; ?></td>
                        <td class="text-center"> <img src="imagens/times/<?php echo $reg['visitanteFoto']; ?>" style="width: 30px;">  </td>
                        <td><?php echo $reg['visitanteNome']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteFoto']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['mandanteId']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['visitanteId']; ?></td>
                        <td class="text-right" style="display: none"><?php echo $reg['turno']; ?></td>
                    </tr> <?php                              
                    } ?> 			
                </tbody>
            </table>
        </div>
        
    </div>

</div>

<script>
    $(document).ready(function(){
        $(".nav-pills li a").click(function(){
            $(this).tab('show');
        });

        $('.nav-pills li a').on('shown.bs.tab', function(event){
            var x = $(event.target).text();         // active tab
            var y = $(event.relatedTarget).text();  // previous tab
            $(".act span").text(x);
        });
        
        function SelectTab(tabindex)
        {
            $('.nav-pills li a').removeClass('active');
            $('.nav-pills li a').eq(tabindex).addClass('active'); 
            //tabindex start at 0 
        }

    });
</script>

















 <br>
        <br>

