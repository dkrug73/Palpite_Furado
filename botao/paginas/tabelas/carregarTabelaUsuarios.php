<?php
    require_once('../../classes/Usuario.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $usuario = new Usuario($conexao); 
    $rs = $usuario->ObterDadosTodosUsuarios();

?>

    <table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-usuario">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>                                    
            <th>Nome</th>
            <th>Nome de Usuário</th>
            <th>Email</th>
            <th>Administrador</th>
            <th>Foto</th>
        </tr>
    </thead>

    <tbody>  <?PHP                
        while($reg=mysqli_fetch_array($rs)) 
        {
            $id = $reg["id"];
            $nome = $reg["nome"];
            $nomeUsuario = $reg["nomeUsuario"];
            $email = $reg["email"];
            $administrador = $reg["administrador"];
            $foto = $reg['foto'];                            
            if(!empty($foto)){
                $foto = "imagens/usuarios/".$foto;
            } ?>                            
                                        
        <tr style='cursor: pointer;'> 
            <td><?PHP print $id; ?></td>
            <td><?PHP print $nome; ?></td>
            <td><?PHP print $nomeUsuario; ?></td>
            <td><?PHP print $email; ?></td>
            <td><?PHP print $administrador; ?></td>
            <td><img src = "<?php print $foto; ?>" width = "25"></td>
        </tr> <?PHP 
        }  
        
        $usuario->FecharConexao(); ?>
    </tbody>
</table>
<br />















