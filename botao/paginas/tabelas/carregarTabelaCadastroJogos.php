<?php
    require_once('../../classes/Jogo.php');
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogo = new jogo($conexao); 
    
    if(isset($_POST['campeonatoId'])){
        $jogo->CampeonatoId = $_POST['campeonatoId'];
    }

    $rs = $jogo->ObterDadosJogos();
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-jogo">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th class="text-right">Mandante</th>
            <th class="text-center">Placar</th>
            <th class="text-left">Visitante</th>
            <th>Rodada</th>
            <th>Turno</th>
            <th>Grupo</th>
            <th>Ordem</th>
            <th style="display: none;">MandanteId</th>
            <th style="display: none;">VisitanteId</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){
            $id = $reg['id'];
            $mandanteId = $reg['mandanteId'];
            $visitanteId = $reg['visitanteId']; 
            $placarMandante = $reg['placarMandante'];
            $placarVisitante = $reg['placarVisitante']; 
            $penaltiMandante = $reg['penaltiMandante'];
            $penaltiVisitante = $reg['penaltiVisitante']; 
            $mandanteNome = $reg['mandanteNome']; 
            $visitanteNome = $reg['visitanteNome']; 
            $rodada = $reg['rodada']; 
            $turno = $reg['turno'];
            $grupo = $reg['grupo'];
            $ordem = $reg['ordem']; ?>
           
            <tr style='cursor: pointer;'>
                <td><?php echo $id; ?></td>
                <td class="text-right"><?php echo $mandanteNome ?></td>
                <td class="text-center"><?php echo $placarMandante ?><span class="penalti"><?php echo $penaltiMandante ?></span> 
                    x <span class="penalti"><?php echo $penaltiVisitante ?></span><?php echo $placarVisitante ?></td>
                <td class="text-left"><?php echo $visitanteNome ?></td>
                <td><?php echo $rodada ?></td>
                <td><?php echo $turno ?></td>
                <td><?php echo $grupo ?></td>
                <td><?php echo $ordem ?></td>
                <td style="display: none;"><?php echo $mandanteId ?></td>
                <td style="display: none;"><?php echo $visitanteId ?></td>
            </tr> <?php
        } 

        $jogo->FecharConexao();  ?>

    </tbody>
</table>
<br />