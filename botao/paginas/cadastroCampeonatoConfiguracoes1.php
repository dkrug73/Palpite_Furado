<?php
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Campeonato.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $campeonato = new Campeonato($conexao); 

    if(isset($_POST['campeonato-id'])){
        $campeonato->setId($_POST['campeonato-id']);
    }

    if(isset($_POST['formulaId'])){
        $campeonato->setFormulaId($_POST['formulaId']);
    }

    if(isset($_POST['tipo-final'])){
        $campeonato->setTipoFinal($_POST['tipo-final']);
    }

    if(isset($_POST['dois-grupos'])){
        $campeonato->setDoisGrupos(1);
    }

    if(isset($_POST['dois-turnos'])){
        $campeonato->setDoisTurnos(1);
    }

    if(isset($_POST['evitar-confrontos'])){
        $campeonato->setEvitarConfrontos(1);
    }

    if(isset($_POST['radio-jogos'])){
        $campeonato->setJogos($_POST['radio-jogos']);
    }
    
    if($campeonato->AtualizarConfiguracoes()){
        $campeonato->ApagarTimesCampeonato($campeonato->Id);

        if(isset($_POST['grupo-unico'])){
            $times = $_POST['grupo-unico'];

            foreach ((array)$times as $timeId) {
                if (!$campeonato->InserirTimeCampeonato($campeonato->Id, $timeId, "A")) return false;
            }
        }

        if(isset($_POST['grupo-dois'])){
            $times = $_POST['grupo-dois'];

            foreach ((array)$times as $timeId) {
                if (!$campeonato->InserirTimeCampeonato($campeonato->Id, $timeId, "B")) return false;
            }
        }
    }
        
    $campeonato->FecharConexao();    

    echo true;
?>