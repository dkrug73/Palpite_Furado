<?php
    require_once('../../classes/ConexaoBancoDeDados.php');
    require_once('../../classes/Formula.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  

    $sql="select id,descricao from formulas";

    $rs=$conexao->query($sql);

    $select = "";
    
    while($reg=mysqli_fetch_array($rs))	
    {	
        $select = $select."<option value='".$reg['id']."'>".$reg['descricao']."</option> ";
    } 

    echo $select;

    mysqli_close($conexao);
?>