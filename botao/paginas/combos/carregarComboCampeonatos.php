<?php
    require_once('../../classes/ConexaoBancoDeDados.php');
    require_once('../../classes/Jogo.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();
    
    $campeonatoId = null;
    if(isset($_POST['campeonatoId'])){
        $campeonatoId = $_POST['campeonatoId'];
    }
   
    $sql="
        SELECT 
            id, 
            concat(descricao, ' - ', edicao, 'ª edição') AS descricao 
        FROM 
            campeonatos 
        ORDER BY 
            ativo DESC";

    $rs=$conexao->query($sql);
    
    $select = "<option value='0'>Selecione um campeonato</option>";

    while($reg=mysqli_fetch_array($rs))	
    {	
        
        if ($campeonatoId == $reg['id']) {
            $select = $select."<option value='".$reg['id']."'  selected='selected'>".$reg['descricao']."</option> ";
        }
        else {
            $select = $select."<option value='".$reg['id']."'>".$reg['descricao']."</option> ";
        }
       

        //$select = $select."<option value='".$reg['id']."'>".$reg['descricao']."</option> ";
    } 
   
    echo $select;

    mysqli_close($conexao);
?>