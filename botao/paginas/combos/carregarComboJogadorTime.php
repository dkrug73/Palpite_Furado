<?php
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  
    
    $timeId = $_POST['timeId'];

    $sql="
        SELECT 
            id, 
            CONCAT(numero, ' - ', nome) as descricao, 
            (
                SELECT 
                    SUM(gols) 
                FROM 
                    jogoJogador 
                WHERE 
                    jogoJogador.jogadorId = jogadores.id
            ) AS gols 
        FROM 
            jogadores 
        WHERE 
            timeId = '$timeId' 
        ORDER BY 
            gols DESC, 
            numero";

    $rs=$conexao->query($sql);
    
    $select = "";

    while($reg=mysqli_fetch_array($rs))	
    {	
        $select = $select."<option value='".$reg['id']."'>".$reg['descricao']."</option> ";
    } 

    echo $select;

    mysqli_close($conexao);
?>