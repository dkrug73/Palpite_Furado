<?php
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    

    $sql="SELECT t.id, CONCAT(t.nome, ' - ', u.nome) as descricao FROM times t INNER JOIN usuarios u ON t.tecnico = u.id ORDER BY u.nome ";

    $rs=$conexao->query($sql);
    
    $select = "";

    while($reg=mysqli_fetch_array($rs))	
    {	
        $select = $select."<option value='".$reg['id']."'>".$reg['descricao']."</option> ";
    } 

    echo $select;

    mysqli_close($conexao);
?>