<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Jogador.php');
    require_once('../componentes/funcoes.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogador = new Jogador($conexao); 

    $jogador->setId($_POST['jogador-id']);
    $jogador->setNome($_POST['nome']);
    $jogador->setNumero($_POST['numero']);
    
    $gravar = $jogador->AtualizarJogador($jogador);
    $jogador->FecharConexao();

    echo $gravar;
?>