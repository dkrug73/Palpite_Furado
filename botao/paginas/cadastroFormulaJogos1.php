<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/FormulaJogos.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $formulaJogos = new FormulaJogos($conexao); 

    if (isset($_POST['formula-jogos-id'])){ 
        $formulaJogos->setId($_POST['formula-jogos-id']);
    }

    $formulaJogos->setFormulaId($_POST['formulaId']);
    $formulaJogos->setRodada($_POST['rodada']);
    $formulaJogos->setMandante($_POST['mandante']);
    $formulaJogos->setvisitante($_POST['visitante']);

    $gravar = $formulaJogos->GravarFormulaJogo($formulaJogos);
    $formulaJogos->FecharConexao();

    echo $gravar;
    ?>