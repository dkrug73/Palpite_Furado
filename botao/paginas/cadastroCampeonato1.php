<?php
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Campeonato.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $campeonato = new Campeonato($conexao); 

    if(isset($_POST['campeonato-id'])){
        $campeonato->setId($_POST['campeonato-id']);
    }

    $campeonato->setDescricao($_POST['descricao']);
    $campeonato->setLocal($_POST['local']);
    $campeonato->setEdicao($_POST['edicao']);
    $campeonato->setClasseRanking($_POST['classe-ranking']);
    $campeonato->setData($_POST['data-campeonato']);

    $campeonato->setAtivo(0);
    if (isset($_POST['ativo'])){
        $campeonato->setAtivo(1);
    }
    
    $gravar = $campeonato->Gravarcampeonato();
    $campeonato->FecharConexao();

    echo $gravar;
?>