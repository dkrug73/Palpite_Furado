<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Campeonato.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $campeonato = new Campeonato($conexao); 

    if(isset($_POST['campeonatoId'])){
        $campeonato->setId($_POST['campeonatoId']);
    }

    if(isset($_POST['formulaId'])){
        $campeonato->setFormulaId($_POST['formulaId']);
    }

    if(isset($_POST['tipoFinal'])){
        $campeonato->setTipoFinal($_POST['tipoFinal']);
    }

    $campeonato->setDoisTurnos(false);
    if($_POST['doisTurnos'] == "true"){
        $campeonato->setDoisTurnos(true);
    }
    
    $campeonato->setDoisGrupos(false);
    if($_POST['doisGrupos'] == "true"){  
        $campeonato->setDoisGrupos(true);
    }

    $campeonato->setEvitarConfrontos(false);
    if($_POST['evitarConfrontos'] == "true") {
        $campeonato->setEvitarConfrontos(true);
    } 

    $campeonato->setJogos(3);
    if($_POST['umJogo'] == "true") {
        $campeonato->setJogos(1);
    }
    
    if($_POST['doisJogos'] == "true") {
        $campeonato->setJogos(2);
    }

    $grupoA = $campeonato->RetornaTimesGrupo("A");
        
    $grupoB = null;
    if ($campeonato->DoisGrupos) {
        $grupoB = $campeonato->RetornaTimesGrupo("B");
    }

    $gerarJogos = false;
    if ($campeonato->GerarJogoscampeonato($grupoA, $grupoB)) {
        $gerarJogos = $campeonato->GerarTabelacampeonato($grupoA, $grupoB);
    }

    $campeonato->FecharConexao();    

    echo $gerarJogos;
?>