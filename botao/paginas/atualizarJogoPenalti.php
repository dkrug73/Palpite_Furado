<?php
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Jogo.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogo = new Jogo($conexao);

    $jogo->setId($_POST['jogoId']);
    $jogo->setCampeonatoId($_POST['campeonatoId']);

    $jogo->setMandanteId($_POST['idMandante']);
    $jogo->setMandanteNome($_POST['nomeMandante']);
    $jogo->setVisitanteId($_POST['idVisitante']);
    $jogo->setVisitanteNome($_POST['nomeVisitante']);

    if (isset($_POST['penaltiMandante'])){
        $jogo->setPenaltiMandante($_POST['penaltiMandante']);
    }

    if (isset($_POST['penaltiVisitante'])){
        $jogo->setPenaltiVisitante($_POST['penaltiVisitante']);
    }

    if (!$jogo->Atualizarjogo()) echo false;

    if (!$jogo->AtualizarTabelasClassificacaoPenalti()) echo false;

    if (!$jogo->AtualizarCampeonatoSeForUltimoJogo()) echo false;

    $jogo->FecharConexao();

    echo true;
?>