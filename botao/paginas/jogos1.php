<?php
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Jogo.php');

    if (isset($_SESSION['administrador_usuario_botao'])) {
        if ($_SESSION['administrador_usuario_botao']) echo "administrador";
        else echo "visitante";
        return true;
    }
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $jogo = new Jogo($conexao);

    $jogo->setId($_POST['jogoId']);
    $jogo->setCampeonatoId($_POST['campeonatoId']);

    $jogo->setMandanteId($_POST['idMandante']);
    $jogo->setMandanteNome($_POST['nomeMandante']);
    $jogo->setVisitanteId($_POST['idVisitante']);
    $jogo->setVisitanteNome($_POST['nomeVisitante']);

    if(isset($_POST['placarMandante'])){
        $jogo->setPlacarMandante($_POST['placarMandante']);
    }

    if(isset($_POST['placarVisitante'])){
        $jogo->setPlacarVisitante($_POST['placarVisitante']);
    }

    if (isset($_POST['penaltiMandante'])){
        $jogo->setPenaltiMandante($_POST['penaltiMandante']);
    }

    if (isset($_POST['penaltiVisitante'])){
        $jogo->setPenaltiVisitante($_POST['penaltiVisitante']);
    }

    if(isset($_POST['goleadoresMandante'])){
        if (!$jogo->GravarGolsJogador($_POST['goleadoresMandante'])) echo false;
    }

    if(isset($_POST['goleadoresVistante'])){
        if (!$jogo->GravarGolsJogador($_POST['goleadoresVisitante'])) echo false;
    }

    if (!$jogo->Atualizarjogo()) echo false;

    if (!$jogo->AtualizarTabelasClassificacao(true)) echo false;

    if (!$jogo->AtualizarCampeonatoSeForUltimoJogo()) echo false;

    $jogo->FecharConexao();

    echo true;
?>