<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Usuario.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();
    
    $usuario = new Usuario($conexao); 

	$usuario->setNomeUsuario($_POST['usuario']);
	$usuario->setSenha(md5($_POST['senha']));

    $dados_usuario = $usuario->ObterDadosUsuario($conexao);

   if (isset($dados_usuario)){
        $_SESSION['id_usuario_botao'] = $dados_usuario['id'];
        $_SESSION['nome_usuario_botao'] = $dados_usuario['nomeUsuario'];
        $_SESSION['email_usuario_botao'] = $dados_usuario['email'];        

        if ($dados_usuario['administrador'] == "1"){
            $_SESSION['administrador_usuario_botao'] = true;
        }        

        header('Location: ../jogos.php');
   }
   else{
        header('Location: ../index.php?erro=1');
   }   

   
?>