<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Formula.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();    
    
    $formula = new Formula($conexao); 

    if(isset($_POST['formula-id'])){
        $formula->setId($_POST['formula-id']);
    }

    $formula->setDescricao($_POST['descricao']);
    $formula->setQuantidadeTimes($_POST['quantidade']);

    $gravar = $formula->GravarFormula();
    $formula->FecharConexao();    

    echo $gravar;
?>