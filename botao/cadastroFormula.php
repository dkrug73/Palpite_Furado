<?php
    session_start();
    require_once('classes/Formula.php');

    if(!isset($_SESSION['nome_usuario_botao'])){
        header('Location: index.php?erro=1');
    }

    $erro = null;
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro de Fórmula</title>

    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->
    <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/jquery/jquery-3.3.1.min.js"></script> 
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/js/cadastroFormulas.js"></script> 
    <script src="componentes/js/utils.js"></script> 

</head>
<body>
    
    <?php require 'componentes/menu.php'; ?>

    <div class="container corpo">    

        <?php require 'componentes/alerta.php'; ?>   
         
        <section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro de Fórmula</h3>				
        </section>
    
        <section>
            <form method="post" id="formulario-formula" enctype="multipart/form-data">
                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label">ID</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="formula-id" name="formula-id" 
                            placeholder="" readonly value="">
                    </div>
                </div>  
                
                <div class="form-group row">
                    <label for="descricao" class="col-lg-2 col-md-2 col-form-label">Descrição</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="descricao" name="descricao" value="" required maxlength="100">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="quantidade" class="col-lg-2 col-md-2 col-form-label">Quantidade de times</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="quantidade" name="quantidade" value="" required maxlength="3">
                    </div>
                </div>       

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="submit" class="btn btn-primary mr-2">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn-cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>

                        <a href="" id="inserir-formula">
							<i class="fa fa-edit" style="margin: 0 2px 0 15px;"></i> Inserir fórmula</a>

                    </div>
                </div>
            </form>            

            <div class="container"> 
                <div id="resultado-tabela"></div>               
            </div>
        </section>
    </div>
    
    <?php require 'componentes/rodape.php'; ?>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
    <script src="componentes/bootstrap/js/bootstrap.min.js"></script>      
</body>
</html>