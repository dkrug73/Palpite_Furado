<?php
    session_start();
    require_once('classes/Formula.php');

    if(!isset($_SESSION['nome_usuario_botao'])){
        header('Location: index.php?erro=1');
    }

    $erro = null;
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro de Time</title>

    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->
    <link href="componentes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/jquery/jquery-3.3.1.min.js"></script> 
    <link href="componentes/css/estilo.css" rel="stylesheet">
    <script src="componentes/js/cadastroTimes.js"></script> 
    <script src="componentes/js/utils.js"></script>
</head>
<body>    
    
    <?php require 'componentes/menu.php'; ?>

    <div class="container corpo">  

        <?php require 'componentes/alerta.php'; ?>   

        <section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro de Time</h3>				
        </section>
    
        <section>
            <form method="post" action="paginas/cadastroTime1.php" id="cadastro-time" enctype="multipart/form-data">
                <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label">ID</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="time-id" name="time-id" placeholder=""
                            readonly value="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nome" class="col-lg-2 col-md-2 col-form-label">Nome</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="nome" name="nome" value="" required maxlength="30">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nome-oficial" class="col-lg-2 col-md-2 col-form-label">Nome oficial</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="nome-oficial" name="nome-oficial" value="" required maxlength="100">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tecnico" class="col-lg-2 col-md-2 col-form-label">Técnico</label>
                    <div class="col-md">
                        <div id="combo-tecnico"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tecnico" class="col-lg-2 col-md-2 col-form-label">Distintivo</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="foto" name="foto-time" onchange="readURL(this);">
                            <label class="custom-file-label" for="foto" aria-describedby="inputGroupFileAddon02">Escolha o arquivo</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <img src="imagens/times/sem_imagem.png" class="figure-img img-fluid rounded" id="foto-time" name="foto-exibida" width="100" height="100">
                    </div>
                </div>                             

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="submit" class="btn btn-primary mr-2">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn_cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>

                    </div>
                </div>
            </form>
    
            <div class="container"> 
                <div id="resultado-tabela"></div> 
            </div>
        </section>
    </div>

    <?php require 'componentes/rodape.php'; ?>   

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
    <script src="componentes/bootstrap/js/bootstrap.min.js"></script>   
      
</body>
</html>