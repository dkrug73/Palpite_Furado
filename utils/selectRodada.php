<style>	
.tabela-navegacao {
    --border-bottom: 1px solid silver;
    --border-top: 1px solid #ddd;
    color: #00a65a;
    font-family: ProximaNova;
    font-weight: bold;
    font-size: 30px;
    position: relative;
    text-transform: uppercase;
    margin-bottom: 30px;
}

.tabela-rodada {
  display: flex;
  width: 370px;
}
.tabela-rodada > div {
  flex: 1; /*grow*/
}

.botao-esquerda {
    background-color: transparent;
    border: none;
}

.jogob{
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
    max-width:100%;
    margin: 0px 0px 15px 0px;
    text-align: center;
    align-items: center;
    font-size: 25px;
    max-width: 880px;
    margin-top: 10px;

margin-bottom: 10px;
}
.jogob > .jogo-itemb:nth-child(1){
    width:60px;
    text-align: left;
    --background-color: red;
}

.jogob> .jogo-itemb:nth-child(2){
    width:160px;
    text-align: center;
    --background-color: red;
}

.jogob > .jogo-itemb:nth-child(3){
    width:60px;
    text-align: right;
    --background-color: red;
}

.jogob > .jogo-itemb:nth-child(4){
    width:60px;
    text-align: right;
    --background-color: red;
}

.jogob > .jogo-itemb:nth-child(5){
    width:60px;
    text-align: right;
    --background-color: red;
}

</style>

<script type="text/javascript">	
	function AdicionaRodada()	
	{
	  var valorRodada = parseInt(document.getElementById('rodada-jogo').innerHTML) + 1;

	  if (valorRodada < 39){
	  	document.getElementById('rodada-jogo').innerHTML =  valorRodada +'ª Rodada';
		getValor(valorRodada, '<?php print $campeonato->Id; ?>');
	  }
	}

	function SubtraiRodada()	
	{
	  var valorRodada = parseInt(document.getElementById('rodada-jogo').innerHTML) - 1;

	  if (valorRodada > 0){
		document.getElementById('rodada-jogo').innerHTML =  valorRodada + 'ª Rodada';
		getValor(valorRodada, '<?php print $campeonato->Id; ?>');
	  }	  
	}

</script>


	<div class="tabela-navegacao">
		<div class="tabela-rodada">

			<div class="jogob">			
				<div class="jogo-itemb">
					<button class="botao-esquerda" type="button" onclick="SubtraiRodada()" >
						<span class="fa fa-angle-left"></span></button>				  
				</div>	

				<div class="jogo-itemb">	
  					<div type="text" id="rodada-jogo" ><?php print $rodadaAtual; ?>ª Rodada</div>  

				</div>	

				<div class="jogo-itemb">  
					<button class="botao-esquerda" type="button" onclick="AdicionaRodada()" >
						<span class="fa fa-angle-right"></span></button>

				</div>

				<div class="jogo-itemb"></div>					
	
				<div class="jogo-item"></div>
			</div>
		</div>
	</div>


<!--

<div class="jogo">			
	<div class="jogo-item"></div>	

	<div class="jogo-item"></div>					
	
	<div class="jogo-item"></div>

	<div class="jogo-item">
	<button class="botao-esquerda" type="button" onclick="SubtraiRodada()" >
  				<span class="fa fa-angle-left"></span></button>
	</div>

	<div class="jogo-item">
	<div type="text" id="rodada-jogo" ><?php print $rodadaAtual; ?>ª Rodada</div>  
  
	</div>
	
	<div class="jogo-item">
	<button class="botao-esquerda" type="button" onclick="AdicionaRodada()" >
				<span class="fa fa-angle-right"></span></button>
	</div>

	<div class="jogo-item"></div>

	<div class="jogo-item"></div>

	<div class="jogo-item"></div>

	<div class="jogo-item"></div>
	
	
</div>
-->

<!--
<div class="row grid-ranking">
	<div class="col-xs-12">
		<div class="input-group input-group-sm" style="width: 190px; font-size: 18px; margin: auto;">
			<div class="select-ranking"> 
				<select class="form-control" name="txtRodada" id="txtRodada" onChange="getValor(this.value, <?php print $campeonato->Id; ?>,0);" 
					style="font-size: 1.0em; padding: 0 10px; ">

														
					<?php /*					
					$sql="SELECT DISTINCT rodada FROM jogos WHERE campeonatoId = '".$campeonato->Id."' ORDER BY rodada DESC";
					
					$rs=$conexao->query($sql);
					
					while($reg=mysqli_fetch_array($rs)) {
						if ($rodada == $reg['rodada']) {
							$itens = $itens."<option value='".$reg['rodada'].
								"' selected='selected'>".$reg['rodada']."º Rodada"."</option><br />";											
						}
						else {
							$itens = $itens."<option value='".$reg['rodada']."'>".
							$reg['rodada']."º Rodada"."</option><br />";
						}									
					}
					print $itens;	*/								
					?>   

					      
				</select>	
			</div>
		</div>
	</div>
</div>
-->