<?php	
	SESSION_START();
	
	/**	
	* Funcao que valida um usuario e senha
	* @param string $usuario - O usuario a ser validado
	* @param string $senha - A senha a ser validada
	* @return bool - Se o usuario foi validado ou nao (true/false)
	*/
	function validaUsuario($usuario, $senha) {
		include ("../conexao/dbConexao.php");	
		global $_SG;
		$cS = ($_SG['caseSensitive']) ? 'BINARY' : '';
		// Usa a funcao addslashes para escapar as aspas
		$nusuario = addslashes($usuario);
		$nsenha = addslashes($senha);
		
		$tabela = $_SG['tabela'];			
		// Monta uma consulta SQL (query) para procurar um usuario
		$sql = "SELECT * FROM ".$_SG['tabela']." WHERE (".$cS." email = '".$nusuario."' AND ".$cS." senha = '".$nsenha."') LIMIT 1";
		$query=$conexao->query($sql);		
		
		$resultado="";
		while($resultado=mysqli_fetch_array($query))
		{			
			// armazena os dados do usuario nas variaveis de sessao
			$_SESSION['id_part'] = $resultado['id'];
			$_SESSION['nome_part'] = $resultado['nome'];
			$_SESSION['email'] = $resultado['email'];
			$_SESSION['senha'] = $resultado['senha'];
			$_SESSION['padraoMandante'] = $resultado['padraoMandante'];
			$_SESSION['padraoVisitante'] = $resultado['padraoVisitante'];
			$_SESSION['receberemail'] = $resultado['receberemail'];
			$_SESSION['foto'] = $resultado['foto'];
			$_SESSION['nivel'] = $resultado['nivel'];
			
			$_SESSION['acao'] = "alt";
			
			// Verifica a opcao se sempre validar o login
			if ($_SG['validaSempre'] == true) {
				// Definimos dois valores na sess�o com os dados do login
				$_SESSION['usuarioLogin'] = $usuario;
				$_SESSION['usuarioSenha'] = $senha;
			}
			return true;			
		}
		
		if (empty($resultado)) {
			// Nenhum registro foi encontrado => o usuario e invalido
			return false;
		} 	

		mysqli_close($conexao);
	}
	
	
	/**
	* Fun��o que protege uma p�gina
	*/
	function protegePagina() {
		global $_SG;
		if (!isset($_SESSION['id_part']) OR !isset($_SESSION['nome_part'])) {
			// N�o h� usu�rio logado, manda pra p�gina de login
			expulsaVisitante();
		} else if (!isset($_SESSION['id_part']) OR !isset($_SESSION['nome_part'])) {
			// H� usu�rio logado, verifica se precisa validar o login novamente
			if ($_SG['validaSempre'] == true) {
				// Verifica se os dados salvos na sess�o batem com os dados do banco de dados
				if (!validaUsuario($_SESSION['usuarioLogin'], $_SESSION['usuarioSenha'])) {
				// Os dados n�o batem, manda pra tela de login				
				expulsaVisitante();
				}
			}
		}
	}
	/**
	* Fun��o para expulsar um visitante
	*/
	function expulsaVisitante() {
		global $_SG;
		// Remove as vari�veis da sess�o (caso elas existam)
		unset($_SESSION['id_part'], $_SESSION['nome_part'], $_SESSION['usuarioLogin'], $_SESSION['usuarioSenha']);
		// Manda pra tela de login
		header("Location: ".$_SG['paginaLogin']);
	}
	
	// Libera os recursos usados pela conex�o atual
	
?>
