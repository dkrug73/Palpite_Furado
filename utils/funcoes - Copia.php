<?php

function LembrarSenha($conexao) {
	$email = "";
	$senha = "";
	$lembrete = "";	
	
	if (isset($_COOKIE['CookieEmail'])) {
		$email = $_COOKIE['CookieEmail'];
		$senha = $_COOKIE['CookieSenha'];
	}

	$dataAtual = time();

	$lembrete = (isset($_COOKIE['CookieLembrete'])) ? $_COOKIE['CookieLembrete'] : '';
	
	global $_SG;
	$cS = ($_SG['caseSensitive']) ? 'BINARY' : '';
	// Usa a função addslashes para escapar as aspas
	$nusuario = addslashes($email);
	$nsenha = addslashes($senha);	

	

    if (!empty($email) && !empty($senha)):
		$sql = "SELECT 
					* 
				FROM 
					participantes 
				WHERE 
					".$cS." email = '".$nusuario."' AND 
					".$cS." senha = '".$nsenha."' LIMIT 1 ";

        $rs=$conexao->query($sql);

		while($resultado=mysqli_fetch_array($rs)) {       
			$_SESSION['participanteId'] = $resultado['id'];
			$_SESSION['participanteNome'] = $resultado['nome'];
			$_SESSION['email'] = $resultado['email'];
			$_SESSION['senha'] = $resultado['senha'];
			$_SESSION['padraoMandante'] = $resultado['padraoMandante'];
			$_SESSION['padraoVisitante'] = $resultado['padraoVisitante'];
			$_SESSION['receberemail'] = $resultado['receberemail'];
			$_SESSION['foto'] = $resultado['foto'];
			$_SESSION['nivel'] = $resultado['nivel'];			
			$_SESSION['acao'] = "alt";			
			$_SESSION['logado'] = TRUE;
        }

	endif;		

	$checked = ($lembrete == 'SIM') ? 'checked' : '';

    /*	
	$sql_campeonato = "SELECT id FROM cadcampeonato WHERE ativo = 1 ";	
	$rs_campeonato=$conexao->query($sql_campeonato);
	
	$campeonatoId = 0;	
	while($row=mysqli_fetch_array($rs_campeonato))
	{
		$campeonatoId = $row['id'];		
	}
	
	$_SESSION['campeonatoId'] = $campeonatoId;
	
	//$_SESSION['turno'] = RetornaTurnoBaseadoRodadaAtual($conexao, $campeonatoId);
	
    
	$sql="SELECT * FROM campeonato WHERE id = '".$campeonatoId."' "; 
	$rs=$conexao->query($sql);
	
	while($reg=mysqli_fetch_array($rs))
	{
		$_SESSION['campeonatoNome'] = $reg['nome'];
		$_SESSION['campeonatoAno'] = $reg['ano'];
		$_SESSION['campeonatoSerie'] = $reg['serie'];
		$_SESSION['campeonatoBonus'] = $reg['bonus'];
		$_SESSION['campeonatoEmpate'] = $reg['empate'];
		$_SESSION['campeonatoVencedor'] = $reg['vencedor'];
		$_SESSION['campeonatoPlacar'] = $reg['placar'];
		$_SESSION['campeonatoImagem'] = $reg['imagem'];
	}
    */
}

// retorna o turno atual com base no campeonato e na rodada selecionada
function RetornaTurnoAtual($conexao, $campeonatoId, $rodada) {
	$sql = "SELECT DISTINCT 
				turno 
			FROM 
				jogos 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				rodada = '".$rodada."' ";	

	$rs=$conexao->query($sql);	
	$reg=mysqli_fetch_array($rs);
	
	return $reg['turno'];
}

// retorna a última rodada com pontos gravados
function RetornaUltimaRodadaPontos($conexao, $campeonatoId, $turno) {
	$sql = "SELECT DISTINCT 
				rodada 
			FROM 
				rodada 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				turno = '".$turno."' 
			ORDER BY 
				rodada DESC LIMIT 1 ";	
	
	$rs=$conexao->query($sql);	
	$reg=mysqli_fetch_array($rs);

	return $reg['rodada'];
}

function ParticipanteDesativar($conexao, $participanteId) {
	$sql = "SELECT 
				desativar 
			FROM 
				participantes 
			WHERE 
				id = ".$participanteId."";

	$rs=$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);

	return $reg["desativar"];		
}

// retorna oa última rodada com pontos gravados
function RetornaUltimaRodadaPontosCampeonato($conexao, $campeonatoId) {
	$rodada = "1";
	
	$sql = "SELECT DISTINCT 
				rodada 
			FROM 
				rodada 
			WHERE 
				campeonatoId = '".$campeonatoId."' 
			ORDER BY 
				rodada DESC LIMIT 1 ";	
	
	$rs=$conexao->query($sql);	
	$reg=mysqli_fetch_array($rs);

	if ($reg['rodada'] != "") $rodada = $reg['rodada'];

	return $rodada;
}

// função para retornar a rodada atual com base no campeonato
function RetornaRodadaAtual($conexao, $campeonatoId){	
	date_default_timezone_set("America/Sao_Paulo");
	$sql = "SELECT 
				DATEDIFF( NOW(), data) AS ultimaRodada,
				data, 
				rodada 
			FROM 
				jogos 
			WHERE 
				campeonatoId = '".$campeonatoId."' 
			ORDER BY  ultimaRodada DESC ";	
		
	$rs=$conexao->query($sql);

	$rodada = 1;
	while($row=mysqli_fetch_array($rs))
	{	
		$data_rodada=($row['ultimaRodada']);
		$rodada = $row['rodada'];

		if($data_rodada < 1)
		{
			return $row['rodada'];	
		}		
	}
	
	return $rodada;
}

// retorna TRUE se os palpites ainda não foram gravados	
function PalpitesAindaNaoGravados($conexao, $campeonatoId, $rodada, $participanteId){
	$sql = "SELECT 
				id 
			FROM 
				palpites 
			WHERE 
				participanteId = '".$participanteId."' AND 
				campeonatoId = '".$campeonatoId."' 
				AND rodada = '".$rodada."' ";
	
	$rs=$conexao->query($sql);
	$registros = $rs->num_rows;
	
	if ($registros == 0) return true;
	else return false;	
}

// função para validar a data e hora do jogo -->formato datatime (AAAA-mm-dd h:m:s)
function ValidaData($conexao, $data) {		
	date_default_timezone_set("America/Sao_Paulo");
	$dataAtual = time(); // formato timestamp
	$dataJogo = strtotime($data) - 3600; // Gera o timestamp de $data_mysql
	
	if($dataJogo >= $dataAtual) {	
		return true;
	}
	else {
		return false;
	}
}

// função para validar a data e hora do jogo  e bloquear a rodada-->formato datatime (AAAA-mm-dd h:m:s)
function ValidaRodada($conexao, $rodada, $campeonatoId) {		
	date_default_timezone_set("America/Sao_Paulo");
	
	$dataInicioRodada = RetornaDataInicioRodada($conexao,$rodada,$campeonatoId);
					
	$data_atual = time(); // formato timestamp
	
	// irá bloquear os palpites 2 horas antes do inicio da rodada
	$data_jogo = strtotime($dataInicioRodada) - 7200; // Gera o timestamp de $data_mysql
			
	if($data_jogo >= $data_atual) {	
		return true;
	}
	else {
		return false;
	}
}

function RetornaDataInicioRodada($conexao, $rodada, $campeonatoId){
	$sql = "SELECT 
				data 
			FROM 
				jogos 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND	
				rodada = '".$rodada."' AND
				serie = 'A'
			ORDER BY 
				data ASC LIMIT 1";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	return $reg['data'];			
}

function RetornaDataHoraInicioRodada($conexao, $rodada, $campeonatoId){
	$sql = "SELECT 
				DATE_FORMAT( data , '%d/%c/%Y - %H:%i:%s' ) AS data 
			FROM 
				jogos 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND	
				rodada = '".$rodada."' 
			ORDER BY 
				data ASC LIMIT 1";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	return $reg['data'];			
}

function RetornaDadosPalpite($conexao, $jogoId, $participanteId) {
	$sql="SELECT 
				palpiteMandante, 
				palpiteVisitante, 
				pontosRodada,
				naMosca 
			FROM 
				palpites 
			WHERE 
				jogoId = '".$jogoId."' AND 
				participanteId = '".$participanteId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
	$palpiteMandante = $reg['palpiteMandante'];
	$palpiteVisitante = $reg['palpiteVisitante'];
	$pontosRodada = $reg['pontosRodada'];
	$naMosca = $reg['naMosca'];	
	
    return array($palpiteMandante, $palpiteVisitante, $pontosRodada, $naMosca);
}

// retorna o turno atual com base no campeonato e na rodada selecionada
function RetornaTurnoBaseadoRodada($conexao, $campeonatoId, $rodada) {
	$sql = "SELECT DISTINCT 
				turno AS turno 
			FROM 
				jogos 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				rodada = '".$rodada."' ";		
	
	$rs=$conexao->query($sql);
	
	$reg = mysqli_fetch_array($rs);

	return $reg['turno'];
}

 function AlterarPalpite($conexao, $palpiteId, $palpiteMandante, $palpiteVisitante, $rodada) {
	 $dataAtual = date('Y-m-d H:i:s');
	 $sql="	UPDATE 
	 			palpites 
		 	SET 
			 	palpiteMandante='$palpiteMandante',
				palpiteVisitante='$palpiteVisitante',
				rodada='$rodada',
				dataPalpite='$dataAtual' 
			WHERE 
				id = '".$palpiteId."' ";	

      return $conexao->query($sql);
 }

function InserirPalpites($conexao, $jogoId, $participanteId, $palpiteMandante, $palpiteVisitante, $rodada, $campeonatoId){
	$dataAtual = date('Y-m-d H:i:s');

	$sqlJogo = "SELECT 
					nomeMandante,
					nomeVisitante,
					data,
					mandanteId,
					visitanteId,
					estadio,
					mandanteAbreviado,
					visitanteAbreviado
				FROM 
					jogos
				WHERE 
					id = '".$jogoId."' ";					

	$rsJogo=$conexao->query($sqlJogo);
	$regJogo=mysqli_fetch_array($rsJogo);

	$nomeMandante = $regJogo['nomeMandante'];
	$nomeVisitante = $regJogo['nomeVisitante'];
	$fotoMandante = $regJogo['mandanteId'].".png";
	$fotoVisitante = $regJogo['visitanteId'].".png";
	$estadio = $regJogo['estadio'];
	$dataJogo = $regJogo['data'];
	$mandanteAbreviado = $regJogo['mandanteAbreviado'];
	$visitanteAbreviado = $regJogo['visitanteAbreviado'];
                        
	$sql = "
		INSERT INTO palpites (
			jogoId,
			campeonatoId,
			participanteId,
			palpiteMandante,
			palpiteVisitante,
			rodada,
			dataJogo,
			nomeMandante,
			nomeVisitante,
			fotoMandante,
			fotoVisitante,
			mandanteAbreviado,
			visitanteAbreviado,
			estadio,
			dataPalpite) 
		VALUES(
			'$jogoId',
			'$campeonatoId',
			'$participanteId',
			'$palpiteMandante',
			'$palpiteVisitante',
			'$rodada',
			'$dataJogo',
			'$nomeMandante',
			'$nomeVisitante',
			'$fotoMandante',
			'$fotoVisitante',
			'$mandanteAbreviado',
			'$visitanteAbreviado',
			'$estadio',
			'$dataAtual') ";
                    
	return $conexao->query($sql);
}

// verifica se o participante consta na tabela participantes_campeonato
function verificaParticipanteCampeonato($conexao, $participanteId, $campeonatoId) {
	$sql = "SELECT 
			id 
		  FROM 
			participantes_campeonato  
		  WHERE 
			campeonatoId = '".$campeonatoId."' AND 
			participanteId = '".$participanteId."' ";
			
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	$naoParticipa = true;

	if ($reg['id'] != "") {
		$naoParticipa = false;
	}	
	return $naoParticipa;
}

function RetornaPalpiteId($conexao, $participanteId, $jogoId) {
	$sql = "SELECT 
				id 
			FROM 
				palpites 
			WHERE 
				participanteId = '".$participanteId."' AND 
				jogoId = '".$jogoId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	return $reg['id'];	
}

function RetornaNomeParticipante($conexao, $participanteId){	
	$sql = "SELECT 
				nome 
			FROM 
				participantes 
			WHERE 
				id = '".$participanteId."' ";
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	return $reg['nome'];	 
}

function RetornaFotoParticipante($conexao, $participanteId){	
	$sql = "SELECT 
				foto 
			FROM 
				participantes 
			WHERE 
				id = '".$participanteId."' ";
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	return $reg['foto'];	 
}

function RetornaParticipanteAnteriorPontuacaoInicial($conexao, $pontuacaoInicialId) {
	$sql = "SELECT 
				participanteId 
			FROM 
				pontuacao_inicial 
			WHERE 
				id = '".$pontuacaoInicialId."' ";
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	return $reg['participanteId'];	

}

function ValidaEmailCadastro($conexao, $email) {
	$sql = "SELECT 
				email 
			FROM 
				participantes 
			WHERE 
				email = '".$email."' ";

	$rs=$conexao->query($sql);

	$totalRegistros = $rs->num_rows;
		
	if ($totalRegistros == 1) {
		return false;
	}
	else {
		return true;
	}
}   

function InsereAtualizaFoto($conexao, $sql, $foto, $caminho_imagem, $id){	
	$nome_imagem="";

	// Se a foto estiver sido selecionada
	if (!empty($foto["name"]))
	{
		//$foto = $_FILES['foto'];		
		// Largura máxima em pixels
		$largura = 1500;
		// Altura máxima em pixels
		$altura = 1125;
		// Tamanho máximo do arquivo em bytes
		$tamanho = 1000000;

		$error="";
 
		// Verifica se o arquivo é uma imagem
		if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"])){
		   $error[1] = "Isso não é uma imagem.";
		} 
	
		// Pega as dimensões da imagem
		$dimensoes = getimagesize($foto["tmp_name"]);
	
		// Verifica se a largura da imagem é maior que a largura permitida
		if($dimensoes[0] > $largura) {
			$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
		}
 
		// Verifica se a altura da imagem é maior que a altura permitida
		if($dimensoes[1] > $altura) {
			$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
		}
		
		// Verifica se o tamanho da imagem é maior que o tamanho permitido
		if($foto["size"] > $tamanho) {
			$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
		}
		// Se não houver nenhum error_log
		$resultado = false;
		if ($error == "") {	
			// Pega extensão da imagem
			preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);		
			
			// Gera um nome único para a imagem
			//$nome_imagem = md5(uniqid(time())) . "." . $ext[1];

			$nome_imagem = $id. "." .$ext[1];
 
			// Caminho de onde ficará a imagem
			$caminho_imagem = $caminho_imagem . $nome_imagem;
			
			// Faz o upload da imagem para seu respectivo caminho
			move_uploaded_file($foto["tmp_name"], $caminho_imagem);

			$sql=$sql." '$nome_imagem' WHERE id = '" . $id . "' ";	
			$resultado = $conexao->query($sql);
		}
		
			return	$resultado;
	} 
}
	
function AtualizaVariaveisSessao($conexao, $id){
	unset(	
		$_SESSION['participanteId'],
		$_SESSION['participanteNome'],
		$_SESSION['foto'],
		$_SESSION['nivel']
	);
		
	$participanteId="";
	$participanteNome="";
	$foto="";	
	$nivel="";
	
	if ($id > 0) {
		$participanteId=$id;
		
		$sql = "SELECT 
					* 
				FROM 
					participantes 
				WHERE 
					id = '".$id."' ";	

		$rs=$conexao->query($sql);
		
		while($reg=mysqli_fetch_array($rs))
		{
			$participanteNome=$reg['nome'];
			$foto=$reg['foto'];
			$nivel=$reg['nivel'];
		}

		$_SESSION['participanteId']=$participanteId;
		$_SESSION['participanteNome']=$participanteNome;
		$_SESSION['foto']=$foto;	
		$_SESSION['nivel']=$nivel;
	}
}  

function RetornaPalpitePadrao($conexao, $participanteId){
	$sql = "SELECT 
				padraoMandante,
				padraoVisitante 
			FROM 
				participantes 
			WHERE 
				id = '".$participanteId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs); 
	
	$padraoMandante = $reg['padraoMandante']; 
	$padraoVisitante = $reg['padraoVisitante'];	

	return array($padraoMandante, $padraoVisitante);
}  

function RetornaEmailParticipangte($conexao, $participanteId){
	$sql = "SELECT 
				email 
			FROM 
				participantes 
			WHERE 
				id = '".$participanteId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs); 

	return $reg['email'];
} 

function PodeExcluirPontuacao($conexao, $pontuacaoId) {
	$sql = "SELECT 
				id 
			FROM 
				campeonatos 
			WHERE 
				pontuacaoId = '".$pontuacaoId."' ";
	
	$rs=$conexao->query($sql);
	$registros = $rs->num_rows;
	
	if ($registros == 0) return true;
	else return false;
}  

function RetornaNomeCampeonato($conexao, $campeonatoId) {
	$sql = "SELECT 
				descricao, 
				ano 
			FROM 
				campeonatos 
			WHERE 
				id = '".$campeonatoId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs); 

	return $reg['descricao']." - ".$reg['ano'];
} 

function retornaDadosTime($conexao, $timeId) {
	$sql="SELECT 
				nome, 
				nomeAbreviado, 
				estadio 
			FROM 
				times 
			WHERE 
				id = '".$timeId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
	$nome = $reg['nome'];
	$nomeAbreviado = $reg['nomeAbreviado'];
	$estadio = $reg['estadio'];	
	
    return array($nome, $nomeAbreviado, $estadio);
}

function RetornaPalpitePadraoParticipante($conexao, $participanteId){
	$sql = "SELECT 
				padraoMandante, 
				padraoVisitante 
			FROM 
				participantes 
			WHERE 
				id = '".$participanteId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs); 
	
	$padraoMandante = $reg['padraoMandante']; 
	$padraoVisitante = $reg['padraoVisitante'];

	return array($padraoMandante, $padraoVisitante);
}

function TemPlacarMandante($conexao, $jogoId) {
	$sql = "SELECT 
				placarMandante 
			FROM 
				jogos 
			WHERE 
				id = '".$jogoId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	$placarMandante = $reg['placarMandante'];
	
	if ($placarMandante != "") return true;
	else return false;
}

function retornaPontuacaoCampeonato($conexao, $campeonatoId) {
	$sql="SELECT 
				pontuacao.bonus,
				pontuacao.placar,
				pontuacao.empate,
				pontuacao.vencedor
			FROM 
				campeonatos JOIN pontuacao 
					on pontuacaoId = pontuacao.id 
			WHERE campeonatos.id = '".$campeonatoId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
	$bonus = $reg['bonus'];
	$placar = $reg['placar'];
	$empate = $reg['empate'];	
	$vencedor = $reg['vencedor'];
	
    return array($bonus, $placar, $empate, $vencedor);
}

function PodeGravarPalpitePadrao($conexao, $campeonatoId, $rodada, $participanteId) {
	$sql = "SELECT 
				rodada 
			FROM 
				pontuacao_inicial 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				participanteId = '".$participanteId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);

	$rodadaPontuacaoInicial = $reg['rodada'];

	if ($rodada > $rodadaPontuacaoInicial) return true;
	else return false;
}

function GravarPalpitePadrao($conexao, $campeonatoId, $rodada) {
	$sqlJogod = "SELECT 
					id 
				 FROM 
				 	jogos 
				 WHERE 
				 	rodada = '".$rodada."' AND 
					campeonatoId = '".$campeonatoId."' ";	

	$rsJogos=$conexao->query($sqlJogod);	

	while ($regJogos=mysqli_fetch_array($rsJogos)) {
		$jogoId = $regJogos['id'];

		$sql = "SELECT 
					participanteId 
				FROM 
					participantes_campeonato 
				WHERE 
					campeonatoId = '".$campeonatoId."' AND 
					participanteId not in (SELECT participanteId FROM palpites WHERE campeonatoId = '".$campeonatoId."' AND 
					jogoId = '".$jogoId."') ";

		$rs=$conexao->query($sql);	

		while ($reg=mysqli_fetch_array($rs)) {	
			$participanteId = $reg['participanteId'];

			if (PodeGravarPalpitePadrao($conexao, $campeonatoId, $rodada, $participanteId)) {				
				$palpitePadrao = RetornaPalpitePadrao($conexao, $participanteId);
				$palpiteMandante = $palpitePadrao[0];
				$palpiteVisitante = $palpitePadrao[1];

				if (($palpiteMandante != "" or $palpiteMandante != null) and 
					($palpiteVisitante != "" or $palpiteVisitante != null)) {

					if (!InserirPalpites($conexao, $jogoId, $participanteId, $palpiteMandante, $palpiteVisitante, $rodada, $campeonatoId)) return false;
				}				
			}
		}
	}

	return true;
}

function RetornaDadosPalpitesJogo($conexao, $jogoId, $participanteId) {	
	$sql = "SELECT 
				id, 
				palpiteMandante, 
				palpiteVisitante 
			FROM 
				palpites 
			WHERE 
				participanteId = '".$participanteId."' AND 
				jogoId = '".$jogoId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
    return array($reg['id'], $reg['palpiteMandante'], $reg['palpiteVisitante']);
}

function RetornaPontuacaoRodada($conexao, $participanteId, $campeonatoId, $rodada, $turno) {
	$sql = "SELECT 
				totalPontos, 
				totalNaMosca,
				posicaoRodada
			FROM 
				rodada 
			WHERE 
				rodada = '".$rodada."' AND 
				campeonatoId = '".$campeonatoId."' AND 
				participanteId = '".$participanteId."' AND 
				turno = '".$turno."' ";

	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
    return array($reg['totalPontos'], $reg['totalNaMosca'], $reg['posicaoRodada']);
}			

function AtualizaPalpiteParticipante($conexao, $jogoId, $placarMandante, $placarVisitante, $campeonatoId, $rodada) {	
	/*
	$pontuacaoCampeonato = retornaPontuacaoCampeonato($conexao, $campeonatoId);
	$bonus = $pontuacaoCampeonato[0];
	$placar = $pontuacaoCampeonato[1];
	$empate = $pontuacaoCampeonato[2];
	$vencedor = $pontuacaoCampeonato[3];
	*/

	$acertoVitoriaMandante = 10;
	$acertoVitoriaVisitante = 15;
	$acertoEmpate = 15;

	$placarMandante_1 = 3;
	$placarMandante_2 = 3;
	$placarMandante_0 = 6;
	$placarMandante_3 = 6;

	$placarVisitante_0 = 3;
	$placarVisitante_1 = 3;
	$placarVisitante_2 = 6;
	$placarVisitante_3 = 6;	

	$sql = "SELECT 
				id, 
				palpiteMandante, 
				palpiteVisitante 
			FROM 
				palpites 
			WHERE 
				jogoId = '".$jogoId."' ";
	
	$rsParticipante=$conexao->query($sql);
	
	$retorno = false;
	while ($regParticipante=mysqli_fetch_array($rsParticipante)) 
	{
		$palpiteId = $regParticipante['id'];
		$palpiteMandante = $regParticipante['palpiteMandante'];
		$palpiteVisitante = $regParticipante['palpiteVisitante'];	

		// calcula pontuação da rodada
		$qtPontos = 0;
		$naMosca = 0;		
		$vlBonusNaMosca = 0;	// acertos em cheio no placar
		$vlEmpate = 0;			// acertos do empate, sem acertar os gols
		$vlVencedor = 0;		// acertos do vencedor do jogo
		$vlAcertoPlacar = 0;	// acertos de um dos placares
		$acertoPlacarMandante = 0;
		$acertoPlacarVisitante = 0;
				
		if ($palpiteMandante == "" || $palpiteVisitante == "") {			
			$sql_palpite = "UPDATE palpites SET 
								pontosRodada='0', 
								naMosca='0' 
							WHERE 
								id = '".$palpiteId."' ";
		}
		else {				
			// acerto do empate
			if ($palpiteMandante == $palpiteVisitante && $placarMandante == $placarVisitante) {
				$qtPontos = $qtPontos + $acertoEmpate;	
				$naMosca = $naMosca + 1;
			}
				
			// acerto do vencedor
			else if ($palpiteMandante > $palpiteVisitante && $placarMandante > $placarVisitante) {
				$qtPontos = $qtPontos + $acertoVitoriaMandante;	
				$naMosca = $naMosca + 1;	
			}

			// acerto do perdedor
			else if ($palpiteMandante < $palpiteVisitante && $placarMandante < $placarVisitante) {
				$qtPontos = $qtPontos + $acertoVitoriaVisitante;	
				$naMosca = $naMosca + 1;	
			}

			// acerto do placar mandante
			if ($palpiteMandante == $placarMandante) {
				if ($placarMandante == 1) {
					$acertoPlacarMandante = $placarMandante_1;
				}

				else if ($placarMandante == 2) {
					$acertoPlacarMandante = $placarMandante_2;
				}

				else if ($placarMandante == 0) {
					$acertoPlacarMandante = $placarMandante_0;
				}

				else {
					$acertoPlacarMandante = $placarMandante_3;
				}

				$qtPontos = $qtPontos + $acertoPlacarMandante;
				$naMosca = $naMosca + 1;
			}
			
			// acerto do placar visitante
			if ($palpiteVisitante == $placarVisitante) {
				if ($placarVisitante == 0) {
					$acertoPlacarVisitante = $placarVisitante_0;
				}

				else if ($placarVisitante == 1) {
					$acertoPlacarVisitante = $placarVisitante_1;
				}

				else if ($placarVisitante == 2) {
					$acertoPlacarVisitante = $placarVisitante_2;
				}

				else {
					$acertoPlacarVisitante = $placarVisitante_3;
				}

				$qtPontos = $qtPontos + $acertoPlacarVisitante;
				$naMosca = $naMosca + 1;
			}

			$acertoPlacar = 0;
			if ($naMosca == 3) {
				$acertoPlacar = 1;

				$bonus = VerificaBonus($conexao, $rodada, $palpiteMandante, $palpiteVisitante, $jogoId);	
			}
			
			$sql_palpite = "UPDATE palpites SET 
								pontosRodada = '$qtPontos', 
								naMosca = '$acertoPlacar',
								placarMandante = '$placarMandante',
								placarVisitante = '$placarVisitante'
							WHERE 
								id = '".$palpiteId."' ";									
			
		}

		if (!$conexao->query($sql_palpite)){
			return false;		
		}	
	}
	
	return true;
}

function VerificaBonus($conexao, $rodada, $palpiteMandante, $palpiteVisitante, $jogoId) {
	$sql = "SELECT 
			count(*) AS jogos
		  FROM 
			palpites 
		  WHERE 
		  	rodada = '".$rodada."' AND 
			palpiteMandante = '".$palpiteMandante."' AND 
			palpiteVisitante = '".$palpiteVisitante."' AND 
			jogoid = '".$jogoId."'";

	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
    	$quantidadePalpites = $reg['jogos'];

	$sqlParticipantes = "SELECT DISTINCT participanteId FROM palpites WHERE rodada = '".$rodada."' ";

	$rs1=$conexao->query($sqlParticipantes);

	$registros = $rs1->num_rows;

	$valor = ($quantidadePalpites * 100) / $registros;

	$bonus = 0;
	if ($valor < 20) $bonus = 8;
	else if ($valor >= 20 AND $valor < 40) $bonus = 6;
	else if ($valor >= 40 AND $valor < 70) $bonus = 4;

	return $bonus;
}

function RetornaPontuacaoInicial($conexao, $participanteId, $campeonatoId, $turno, $rodada){
	$sql = "SELECT 
				pontos 
			FROM 
				pontuacao_inicial 
			WHERE 
				participanteId = '".$participanteId."' AND 
				campeonatoId = '".$campeonatoId."' AND 
				turno like '".$turno."' AND 
				rodada like '".$rodada."' ";
	
	$rs =$conexao->query($sql);

	$pontuacaoInicial = 0;
	while ($reg=mysqli_fetch_array($rs)) {
		$pontuacaoInicial = $pontuacaoInicial + $reg['pontos'];
	}
	
	return $pontuacaoInicial;
}

function RetornaPosicaoRodadaAnterior($conexao, $participanteId, $campeonatoId, $rodada, $turno) {
	$sql = "SELECT  
				posicaoRodada 
			FROM 
				rodada 
			WHERE 
				rodada = '".$rodada."' AND 
				campeonatoId = '".$campeonatoId."' AND 
				participanteId = '".$participanteId."' AND 
				turno = '".$turno."' ";

	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
    return $reg['posicaoRodada'];
}

function AtualizarPosicoesRodada($conexao, $campeonatoId, $rodada, $turno) {
	$sql = "SELECT
				id,
				participanteId
			FROM 
				rodada 
			WHERE
				rodada = '".$rodada."' AND 
				campeonatoId = '".$campeonatoId."' AND 
				turno = '".$turno."'
			ORDER BY 
				totalPontos desc, 
				totalNaMosca desc, 
				nomeParticipante asc ";
			
	$rs=$conexao->query($sql);		
	
	$posicao = 1;
	while ($reg=mysqli_fetch_array($rs)) {
		$id = $reg['id'];
		$participanteId = $reg['participanteId'];

		$rodadaAnterior = $rodada;

		if (($turno == 1 or $turno == 3) and $rodada > 1) {
			$rodadaAnterior = $rodada - 1;
		} 
		else if ($turno == 2) {
			$rodadaInicial = 20;
			
			if($rodada > 20){
				$rodadaAnterior = $rodada - 1;				
			}
		}

		$posicaoRodadaAnterior = RetornaPosicaoRodadaAnterior($conexao, $participanteId, $campeonatoId, $rodadaAnterior, $turno);

		if ($posicaoRodadaAnterior == "" or $posicaoRodadaAnterior == null){
			$posicaoRodadaAnterior = $posicao;
		}
 
		$variacao = $posicao - $posicaoRodadaAnterior;

		$sqlAtualizar = "UPDATE 
							rodada 
						 SET 
						 	posicaoRodada = $posicao, 
							posicaoRodadaAnterior = $posicaoRodadaAnterior
						 WHERE 
						 	id = '".$id."' ";

		$conexao->query($sqlAtualizar);
		$posicao = $posicao + 1;
	}
}

function GravaPontuacaoRodada($conexao, $campeonatoId, $participanteId, $nomeParticipante, $fotoParticipante,
				$rodada, $turno, $pontosRodada, $naMosca, $totalPontos, $totalNaMosca, $posicaoRodada, $posicaoRodadaAnterior) {

	$sql = "SELECT id FROM rodada WHERE participanteId = '".$participanteId."' AND rodada = '".$rodada."' 
		AND turno = '".$turno."' AND campeonatoId = '".$campeonatoId."' ";			
	
	$rs=$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	$id_rodada = $reg['id'];
	
	$sql_rodada = "";
	if ($id_rodada != "") {
		$sql_rodada = "UPDATE rodada SET
							turno = '$turno',
							rodada = '$rodada',
							pontosRodada = '$pontosRodada',
							naMosca = '$naMosca',
							posicaoRodada = '$posicaoRodada',
							posicaoRodadaAnterior = '$posicaoRodadaAnterior',
							totalPontos = '$totalPontos',
							totalNaMosca = '$totalNaMosca' 								
					   WHERE id = '".$id_rodada."' ";
	}
	else {		
		$sql_rodada = "INSERT INTO rodada 
							(participanteId,nomeParticipante,fotoParticipante,campeonatoId,turno,
								rodada,pontosRodada,naMosca,posicaoRodada,
								posicaoRodadaAnterior,totalPontos,totalNaMosca)
						VALUES
							('$participanteId','$nomeParticipante','$fotoParticipante','$campeonatoId','$turno',
								'$rodada','$pontosRodada','$naMosca','$posicaoRodada',
								'$posicaoRodadaAnterior','$totalPontos','$totalNaMosca') ";
	}
	
	$conexao->query($sql_rodada);	
}

function RetornaRodadaPontosGeral($conexao, $campeonatoId){	
	$sql = "SELECT 
				MAX(rodada) AS rodada 
			FROM 
				palpites 
			WHERE 
				campeonatoId = '".$campeonatoId."' 	AND 
				pontosRodada > 0 
			ORDER BY 
				rodada DESC limit 1";
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	return $reg['rodada'];	 
}

function RetornaRodadaPontosPalpites($conexao, $turno, $campeonatoId){
	$sql = "SELECT 
				MAX(rodada) AS rodada 
			FROM 
				palpites 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				(SELECT turno FROM jogos WHERE id = jogoId ) = '".$turno."' AND 
				pontosRodada > 0 
			ORDER BY 
				rodada DESC limit 1";	
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	return $reg['rodada'];	 
}

function ApagarRodada($conexao, $campeonatoId, $rodada, $turno) {
	$sql = "DELETE FROM 
				rodada 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND nome
				rodada = '".$rodada."' AND
				turno = '".$turno."' ";

	$conexao->query($sql);
}

function ApagarParticipanteRodada($conexao, $participanteId, $rodada, $campeonatoId) {
	$sql = "DELETE FROM 
				rodada 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				rodada = '".$rodada."' AND
				participanteId = '".$participanteId."' ";

	$conexao->query($sql);
}

function GravaPontuacaoPosicaoRodadaParticipante($conexao, $rodadaAtual, $campeonatoId) {	
	$totalPontos = 0;
	$rodadaAnterior = 1;
	$turno = RetornaTurnoBaseadoRodada($conexao, $campeonatoId, $rodadaAtual);			
	$maximoPalpitesPontos = RetornaRodadaPontosPalpites($conexao, $turno, $campeonatoId);
		
	for ($rodada = $rodadaAtual; $rodada <= $maximoPalpitesPontos; $rodada++) {
		$posicaoRodada = 0;			
		$rodadaInicial = 1;

		ApagarRodada($conexao, $campeonatoId, $rodada, $turno);

		if ($turno == 1 and $rodada > 1){
			$rodadaAnterior = $rodada - 1;
		} else if ($turno == 2) {
			$rodadaInicial = 20;
			
			if($rodada > 20){
				$rodadaAnterior = $rodada - 1;				
			}
		}

		$sql = "SELECT
					participanteId,
					participantes.nome as nomeParticipante,
					participantes.foto,
					SUM(pontosRodada) AS pontos, 
					SUM(naMosca) AS naMosca,
					rodada						
				FROM 
					palpites INNER JOIN 
					participantes ON participanteId = participantes.id
				WHERE 
					campeonatoId = '".$campeonatoId."' AND 
					rodada = '".$rodada."' AND
					participanteId IN (SELECT id FROM participantes WHERE desativar is null)
				GROUP BY 
					participanteId 
				ORDER BY 
					rodada DESC,
					pontos DESC,
                   		naMosca DESC,
					nomeParticipante ASC ";
						
		$rs=$conexao->query($sql);		
	
		while ($reg=mysqli_fetch_array($rs)) {
			$participanteId = $reg['participanteId'];
			$pontosRodada = $reg['pontos'];
			$naMosca = $reg['naMosca'];		
			$nomeParticipante = $reg['nomeParticipante'];
			$fotoParticipante = $reg['foto'];
			$posicaoRodada = $posicaoRodada + 1; 
			
			$pontosRodadaAnterior = 0;	
			$naMoscaRodadaAnterior = 0;				
			$posicaoRodadaAnterior = $posicaoRodada;	

			if ($rodada != "1" and $rodada != "20") {
				$dadosRodadaAnterior = RetornaPontuacaoRodada($conexao, $participanteId, $campeonatoId, $rodadaAnterior, $turno);			
				$pontosRodadaAnterior = $dadosRodadaAnterior['0'];	
				$naMoscaRodadaAnterior = $dadosRodadaAnterior['1'];				
				$posicaoRodadaAnterior = $dadosRodadaAnterior['2'];	
			}

			$pontuacaoInicial = RetornaPontuacaoInicial($conexao, $participanteId, $campeonatoId, $turno, $rodada);
			$totalPontos = $pontosRodadaAnterior + $pontosRodada + $pontuacaoInicial;
			$totalNaMosca = $naMoscaRodadaAnterior + $naMosca;		
			
			if ($rodada == $rodadaInicial) {
				$totalPontos = $pontosRodada;
				$totalNaMosca = $naMosca;			
			}	
			
			GravaPontuacaoRodada($conexao, $campeonatoId, $participanteId, $nomeParticipante, $fotoParticipante,
				$rodada, $turno, $pontosRodada, $naMosca, $totalPontos, $totalNaMosca, $posicaoRodada, $posicaoRodadaAnterior);			
		}	

		AtualizarPosicoesRodada($conexao, $campeonatoId, $rodada, $turno);
	}	
	return true;
}

function GravaPontuacaoPosicaoGeralParticipante($conexao, $rodadaAtual, $campeonatoId) {	
	$turno = 3;
	$totalPontos = 0;	
	
	$maximoPalpitesPontos = RetornaRodadaPontosGeral($conexao, $campeonatoId);
			
	for ($rodada = $rodadaAtual; $rodada <= $maximoPalpitesPontos; $rodada++) {
		$posicaoRodada = 0;	
		$rodadaAnterior = 1;
		$rodadaInicial = 1;

		if ($rodada > 1) {
			$rodadaAnterior = $rodada - 1;
		}

		ApagarRodada($conexao, $campeonatoId, $rodada, $turno);
		
		$sql = "SELECT
					participanteId,
					participantes.nome as nomeParticipante,
					participantes.foto,
					SUM(pontosRodada) AS pontos, 
					SUM(naMosca) AS naMosca,
					rodada						
				FROM 
					palpites INNER JOIN
					participantes ON participanteId = participantes.id
				WHERE 
					campeonatoId = '".$campeonatoId."' AND 
					rodada = '".$rodada."' 
				GROUP BY 
					participanteId 
				ORDER BY 
					rodada DESC,
					nomeParticipante ASC ";
				
		$rs=$conexao->query($sql);
	
		while ($reg=mysqli_fetch_array($rs)) 
		{
			$participanteId = $reg['participanteId'];
			$nomeParticipante = $reg['nomeParticipante'];
			$fotoParticipante = $reg['foto'];
			$pontosRodada = $reg['pontos'];
			$naMosca = $reg['naMosca'];		

			$dadosRodadaAnterior = RetornaPontuacaoRodada($conexao, $participanteId, $campeonatoId, $rodadaAnterior, $turno);
			$pontosRodadaAnterior = $dadosRodadaAnterior['0'];	
			$naMoscaRodadaAnterior = $dadosRodadaAnterior['1'];	

			$posicaoRodada = $posicaoRodada + 1; 	
			$posicaoRodadaAnterior = $dadosRodadaAnterior['2'];	

			$pontuacaoInicial = RetornaPontuacaoInicial($conexao, $participanteId, $campeonatoId, '%', $rodadaAtual);
			$totalPontos = $pontosRodadaAnterior + $pontosRodada + $pontuacaoInicial;
			$totalNaMosca = $naMoscaRodadaAnterior + $naMosca;		
			
			if ($rodada == $rodadaInicial) {
				$totalPontos = $pontosRodada;
				$totalNaMosca = $naMosca;			
			}			
			
			GravaPontuacaoRodada($conexao, $campeonatoId, $participanteId, $nomeParticipante, $fotoParticipante, $rodada,
				$turno, $pontosRodada, $naMosca, $totalPontos, $totalNaMosca, $posicaoRodada, $posicaoRodadaAnterior);
		}		

		AtualizarPosicoesRodada($conexao, $campeonatoId, $rodada, $turno);
	}
	
	return true;
}

function ValidaRodadaInter($conexao, $rodada, $campeonatoId, $jogoId) {
	$sql = "SELECT 
			nomeMandante,
			nomeVisitante,
			data
		FROM 
			jogos 
		WHERE 
			id = '".$jogoId."' ";	
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);

	if($reg['mandanteId'] == 'Internacional' or $reg['visitanteId'] == 'Internacional') {
		return ValidaData($conexao, $reg['data']);
	}
	else {
		return ValidaRodada($conexao, $rodada, $campeonatoId);
	}
}

?>