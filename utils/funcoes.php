<?php

function LembrarSenha($conexao) {
	$email = "";
	$senha = "";
	$lembrete = "";	
	
	if (isset($_COOKIE['CookieEmail'])) {
		$email = $_COOKIE['CookieEmail'];
		$senha = $_COOKIE['CookieSenha'];
	}

	$dataAtual = time();

	$lembrete = (isset($_COOKIE['CookieLembrete'])) ? $_COOKIE['CookieLembrete'] : '';
	
	global $_SG;
	$cS = ($_SG['caseSensitive']) ? 'BINARY' : '';
	// Usa a função addslashes para escapar as aspas
	$nusuario = addslashes($email);
	$nsenha = addslashes($senha);	


    	if (!empty($email) && !empty($senha)):
		$sql = "SELECT 
					* 
				FROM 
					participantes 
				WHERE 
					".$cS." email = '".$nusuario."' AND 
					".$cS." senha = '".$nsenha."' LIMIT 1 ";

		$rs=$conexao->query($sql);

		while($resultado=mysqli_fetch_array($rs)) {       
			$_SESSION['participanteId'] = $resultado['id'];
			$_SESSION['participanteNome'] = $resultado['nome'];
			$_SESSION['email'] = $resultado['email'];
			$_SESSION['senha'] = $resultado['senha'];
			$_SESSION['padraoMandante'] = $resultado['padraoMandante'];
			$_SESSION['padraoVisitante'] = $resultado['padraoVisitante'];
			$_SESSION['receberemail'] = $resultado['receberemail'];
			$_SESSION['foto'] = $resultado['foto'];
			$_SESSION['nivel'] = $resultado['nivel'];			
			$_SESSION['acao'] = "alt";			
			$_SESSION['logado'] = TRUE;
		}

	endif;		

	$checked = ($lembrete == 'SIM') ? 'checked' : '';
}

// retorna o turno atual com base no campeonato e na rodada selecionada







// função para retornar a rodada atual com base no campeonato


// retorna TRUE se os palpites ainda não foram gravados	
function PalpitesAindaNaoGravados($conexao, $campeonatoId, $rodada, $participanteId){
	$sql = "SELECT 
				id 
			FROM 
				palpites 
			WHERE 
				participanteId = '".$participanteId."' AND 
				campeonatoId = '".$campeonatoId."' 
				AND rodada = '".$rodada."' ";
	
	$rs=$conexao->query($sql);
	$registros = $rs->num_rows;
	
	if ($registros == 0) return true;
	else return false;	
}

// função para validar a data e hora do jogo -->formato datatime (AAAA-mm-dd h:m:s)
function ValidaData($conexao, $data) {		
	date_default_timezone_set("America/Sao_Paulo");
	$dataAtual = time(); // formato timestamp
	$dataJogo = strtotime($data) - 1800; // Gera o timestamp de $data_mysql
	
	if($dataJogo >= $dataAtual) {	
		return true;
	}
	else {
		return false;
	}
}

/*
// função para validar a data e hora do jogo  e bloquear a rodada-->formato datatime (AAAA-mm-dd h:m:s)
function ValidaRodada($conexao, $rodada, $campeonatoId) {		
	date_default_timezone_set("America/Sao_Paulo");
	
	$dataInicioRodada = RetornaDataInicioRodada($conexao,$rodada,$campeonatoId);
					
	$data_atual = time(); // formato timestamp
	
	// irá bloquear os palpites 2 horas antes do inicio da rodada
	$data_jogo = strtotime($dataInicioRodada) - 1800; // Gera o timestamp de $data_mysql
			
	if($data_jogo >= $data_atual) {	
		return true;
	}
	else {
		return false;
	}
}
*/






/**/
function RetornaDataPrimeiroJodoDaRodada($conexao, $rodada, $campeonatoId){
	$sql = "SELECT 
				data 
			FROM 
				jogos 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND	
				rodada = '".$rodada."' 
			ORDER BY 
				data ASC LIMIT 1";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	return $reg['data'];			
}





// retorna o turno atual com base no campeonato e na rodada selecionada
function RetornaTurnoBaseadoRodada($conexao, $campeonatoId, $rodada) {
	$sql = "SELECT DISTINCT 
				turno AS turno 
			FROM 
				jogos 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				rodada = '".$rodada."' ";		
	
	$rs=$conexao->query($sql);
	
	$reg = mysqli_fetch_array($rs);

	return $reg['turno'];
}

 



/*
// verifica se o participante consta na tabela participantes_campeonato
function verificaParticipanteCampeonato($conexao, $participanteId, $campeonatoId) {
	$sql = "SELECT 
			id 
		  FROM 
			participantes_campeonato  
		  WHERE 
			campeonatoId = '".$campeonatoId."' AND 
			participanteId = '".$participanteId."' ";
			
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	$naoParticipa = true;

	if ($reg['id'] != "") {
		$naoParticipa = false;
	}	
	return $naoParticipa;
}


function RetornaPalpiteId($conexao, $participanteId, $jogoId) {
	$sql = "SELECT 
				id 
			FROM 
				palpites 
			WHERE 
				participanteId = '".$participanteId."' AND 
				jogoId = '".$jogoId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	return $reg['id'];	
}
*/


function RetornaParticipanteAnteriorPontuacaoInicial($conexao, $pontuacaoInicialId) {
	$sql = "SELECT 
				participanteId 
			FROM 
				pontuacao_inicial 
			WHERE 
				id = '".$pontuacaoInicialId."' ";
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	return $reg['participanteId'];	

}   

function InsereAtualizaFoto($conexao, $sql, $foto, $caminho_imagem, $id){	
	$nome_imagem="";

	// Se a foto estiver sido selecionada
	if (!empty($foto["name"]))
	{
		//$foto = $_FILES['foto'];		
		// Largura máxima em pixels
		$largura = 1500;
		// Altura máxima em pixels
		$altura = 1125;
		// Tamanho máximo do arquivo em bytes
		$tamanho = 1000000;

		$error="";
 
		// Verifica se o arquivo é uma imagem
		if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"])){
		   $error[1] = "Isso não é uma imagem.";
		} 
	
		// Pega as dimensões da imagem
		$dimensoes = getimagesize($foto["tmp_name"]);
	
		// Verifica se a largura da imagem é maior que a largura permitida
		if($dimensoes[0] > $largura) {
			$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
		}
 
		// Verifica se a altura da imagem é maior que a altura permitida
		if($dimensoes[1] > $altura) {
			$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
		}
		
		// Verifica se o tamanho da imagem é maior que o tamanho permitido
		if($foto["size"] > $tamanho) {
			$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
		}
		// Se não houver nenhum error_log
		$resultado = false;
		if ($error == "") {	
			// Pega extensão da imagem
			preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);		
			
			// Gera um nome único para a imagem
			//$nome_imagem = md5(uniqid(time())) . "." . $ext[1];

			$nome_imagem = $id. "." .$ext[1];
 
			// Caminho de onde ficará a imagem
			$caminho_imagem = $caminho_imagem . $nome_imagem;
			
			// Faz o upload da imagem para seu respectivo caminho
			move_uploaded_file($foto["tmp_name"], $caminho_imagem);

			$sql=$sql." '$nome_imagem' WHERE id = '" . $id . "' ";				
		}
		else{
			$sql=$sql." 'sem_imagem.png' WHERE id = '" . $id . "' ";
		}		
	} 
	else{
		$sql=$sql." 'sem_imagem.png' WHERE id = '" . $id . "' ";
	}

	return $conexao->query($sql);
}
	
function AtualizaVariaveisSessao($conexao, $id){
	unset(	
		$_SESSION['participanteId'],
		$_SESSION['participanteNome'],
		$_SESSION['foto'],
		$_SESSION['nivel']
	);
		
	$participanteId="";
	$participanteNome="";
	$foto="";	
	$nivel="";
	
	if ($id > 0) {
		$participanteId=$id;
		
		$sql = "SELECT 
					* 
				FROM 
					participantes 
				WHERE 
					id = '".$id."' ";	

		$rs=$conexao->query($sql);
		
		while($reg=mysqli_fetch_array($rs))
		{
			$participanteNome=$reg['nome'];
			$foto=$reg['foto'];
			$nivel=$reg['nivel'];
		}

		$_SESSION['participanteId']=$participanteId;
		$_SESSION['participanteNome']=$participanteNome;
		$_SESSION['foto']=$foto;	
		$_SESSION['nivel']=$nivel;
	}
}  

  

function RetornaEmailParticipangte($conexao, $participanteId){
	$sql = "SELECT 
				email 
			FROM 
				participantes 
			WHERE 
				id = '".$participanteId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs); 

	return $reg['email'];
} 

function PodeExcluirPontuacao($conexao, $pontuacaoId) {
	$sql = "SELECT 
				id 
			FROM 
				campeonatos 
			WHERE 
				pontuacaoId = '".$pontuacaoId."' ";
	
	$rs=$conexao->query($sql);
	$registros = $rs->num_rows;
	
	if ($registros == 0) return true;
	else return false;
}  

function RetornaNomeCampeonato($conexao, $campeonatoId) {
	$sql = "SELECT 
				descricao, 
				ano 
			FROM 
				campeonatos 
			WHERE 
				id = '".$campeonatoId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs); 

	return $reg['descricao']." - ".$reg['ano'];
} 

function retornaDadosTime($conexao, $timeId) {
	$sql="SELECT 
				nome, 
				nomeAbreviado, 
				estadio 
			FROM 
				times 
			WHERE 
				id = '".$timeId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
	$nome = $reg['nome'];
	$nomeAbreviado = $reg['nomeAbreviado'];
	$estadio = $reg['estadio'];	
	
    return array($nome, $nomeAbreviado, $estadio);
}

function RetornaPalpitePadraoParticipante($conexao, $participanteId){
	$sql = "SELECT 
				padraoMandante, 
				padraoVisitante 
			FROM 
				participantes 
			WHERE 
				id = '".$participanteId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs); 
	
	$padraoMandante = $reg['padraoMandante']; 
	$padraoVisitante = $reg['padraoVisitante'];

	return array($padraoMandante, $padraoVisitante);
}

function TemPlacarMandante($conexao, $jogoId) {
	$sql = "SELECT 
				placarMandante 
			FROM 
				jogos 
			WHERE 
				id = '".$jogoId."' ";
	
	$rs=$conexao->query($sql);
	$reg = mysqli_fetch_array($rs);
	
	$placarMandante = $reg['placarMandante'];
	
	if ($placarMandante != "") return true;
	else return false;
}

function retornaPontuacaoCampeonato($conexao, $campeonatoId) {
	$sql="SELECT 
				pontuacao.bonus,
				pontuacao.placar,
				pontuacao.empate,
				pontuacao.vencedor
			FROM 
				campeonatos JOIN pontuacao 
					on pontuacaoId = pontuacao.id 
			WHERE campeonatos.id = '".$campeonatoId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
	$bonus = $reg['bonus'];
	$placar = $reg['placar'];
	$empate = $reg['empate'];	
	$vencedor = $reg['vencedor'];
	
    return array($bonus, $placar, $empate, $vencedor);
}



function RetornaDadosPalpitesJogo($conexao, $jogoId, $participanteId) {	
	$sql = "SELECT 
				id, 
				palpiteMandante, 
				palpiteVisitante 
			FROM 
				palpites 
			WHERE 
				participanteId = '".$participanteId."' AND 
				jogoId = '".$jogoId."' ";
	
	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
    return array($reg['id'], $reg['palpiteMandante'], $reg['palpiteVisitante']);
}

function RetornaPontuacaoRodada($conexao, $participanteId, $campeonatoId, $rodada, $turno) {
	$sql = "SELECT 
				totalPontos, 
				totalNaMosca,
				posicaoRodada
			FROM 
				rodada 
			WHERE 
				rodada = '".$rodada."' AND 
				campeonatoId = '".$campeonatoId."' AND 
				participanteId = '".$participanteId."' AND 
				turno = '".$turno."' ";

	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
    return array($reg['totalPontos'], $reg['totalNaMosca'], $reg['posicaoRodada']);
}			


function RetornaPosicaoRodadaAnterior($conexao, $participanteId, $campeonatoId, $rodada, $turno) {
	$sql = "SELECT  
				posicaoRodada 
			FROM 
				rodada 
			WHERE 
				rodada = '".$rodada."' AND 
				campeonatoId = '".$campeonatoId."' AND 
				participanteId = '".$participanteId."' AND 
				turno = '".$turno."' ";

	$rs=$conexao->query($sql);		
	$reg = mysqli_fetch_array($rs);
	
    return $reg['posicaoRodada'];
}

function AtualizarPosicoesRodada($conexao, $campeonatoId, $rodada, $turno) {
	$sql = "SELECT
				id,
				participanteId
			FROM 
				rodada 
			WHERE
				rodada = '".$rodada."' AND 
				campeonatoId = '".$campeonatoId."' AND 
				turno = '".$turno."'
			ORDER BY 
				totalPontos desc, 
				totalNaMosca desc, 
				nomeParticipante asc ";
			
	$rs=$conexao->query($sql);		
	
	$posicao = 1;
	while ($reg=mysqli_fetch_array($rs)) {
		$id = $reg['id'];
		$participanteId = $reg['participanteId'];

		$rodadaAnterior = $rodada;

		if (($turno == 1 or $turno == 3) and $rodada > 1) {
			$rodadaAnterior = $rodada - 1;
		} 
		else if ($turno == 2) {
			$rodadaInicial = 20;
			
			if($rodada > 20){
				$rodadaAnterior = $rodada - 1;				
			}
		}

		$posicaoRodadaAnterior = RetornaPosicaoRodadaAnterior($conexao, $participanteId, $campeonatoId, $rodadaAnterior, $turno);

		if ($posicaoRodadaAnterior == "" or $posicaoRodadaAnterior == null){
			$posicaoRodadaAnterior = $posicao;
		}
 
		$variacao = $posicao - $posicaoRodadaAnterior;

		$sqlAtualizar = "UPDATE 
							rodada 
						 SET 
						 	posicaoRodada = $posicao, 
							posicaoRodadaAnterior = $posicaoRodadaAnterior
						 WHERE 
						 	id = '".$id."' ";

		$conexao->query($sqlAtualizar);
		$posicao = $posicao + 1;
	}
}



function RetornaRodadaPontosGeral($conexao, $campeonatoId){	
	$sql = "SELECT 
				MAX(rodada) AS rodada 
			FROM 
				palpites 
			WHERE 
				campeonatoId = '".$campeonatoId."' 	AND 
				pontosRodada > 0 
			ORDER BY 
				rodada DESC limit 1";
	
	$rs =$conexao->query($sql);
	$reg=mysqli_fetch_array($rs);
	
	return $reg['rodada'];	 
}



function ApagarRodada($conexao, $campeonatoId, $rodada, $turno) {
	$sql = "DELETE FROM 
				rodada 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				rodada = '".$rodada."' AND
				turno = '".$turno."' ";

	$conexao->query($sql);
}

function ApagarParticipanteRodada($conexao, $participanteId, $rodada, $campeonatoId) {
	$sql = "DELETE FROM 
				rodada 
			WHERE 
				campeonatoId = '".$campeonatoId."' AND 
				rodada = '".$rodada."' AND
				participanteId = '".$participanteId."' ";

	$conexao->query($sql);
}



function GravaPontuacaoPosicaoGeralParticipante($conexao, $rodadaAtual, $campeonatoId) {	
	$turno = 3;
	$totalPontos = 0;	
	
	$maximoPalpitesPontos = RetornaRodadaPontosGeral($conexao, $campeonatoId);
			
	for ($rodada = $rodadaAtual; $rodada <= $maximoPalpitesPontos; $rodada++) {
		$posicaoRodada = 0;	
		$rodadaAnterior = 1;
		$rodadaInicial = 1;

		if ($rodada > 1) {
			$rodadaAnterior = $rodada - 1;
		}

		ApagarRodada($conexao, $campeonatoId, $rodada, $turno);
		
		$sql = "SELECT
					participanteId,
					participantes.nome as nomeParticipante,
					participantes.foto,
					SUM(pontosRodada) AS pontos, 
					SUM(naMosca) AS naMosca,
					rodada						
				FROM 
					palpites INNER JOIN
					participantes ON participanteId = participantes.id
				WHERE 
					campeonatoId = '".$campeonatoId."' AND 
					rodada = '".$rodada."' 
				GROUP BY 
					participanteId 
				ORDER BY 
					rodada DESC,
					nomeParticipante ASC ";
				
		$rs=$conexao->query($sql);
	
		while ($reg=mysqli_fetch_array($rs)) 
		{
			$participanteId = $reg['participanteId'];
			$nomeParticipante = $reg['nomeParticipante'];
			$fotoParticipante = $reg['foto'];
			$pontosRodada = $reg['pontos'];
			$naMosca = $reg['naMosca'];		

			$dadosRodadaAnterior = RetornaPontuacaoRodada($conexao, $participanteId, $campeonatoId, $rodadaAnterior, $turno);
			$pontosRodadaAnterior = $dadosRodadaAnterior['0'];	
			$naMoscaRodadaAnterior = $dadosRodadaAnterior['1'];	

			$posicaoRodada = $posicaoRodada + 1; 	
			$posicaoRodadaAnterior = $dadosRodadaAnterior['2'];	

			$pontuacaoInicial = RetornaPontuacaoInicial($conexao, $participanteId, $campeonatoId, '%', $rodadaAtual);
			$totalPontos = $pontosRodadaAnterior + $pontosRodada + $pontuacaoInicial;
			$totalNaMosca = $naMoscaRodadaAnterior + $naMosca;		
			
			if ($rodada == $rodadaInicial) {
				$totalPontos = $pontosRodada;
				$totalNaMosca = $naMosca;			
			}			
			
			GravaPontuacaoRodada($conexao, $campeonatoId, $participanteId, $nomeParticipante, $fotoParticipante, $rodada,
				$turno, $pontosRodada, $naMosca, $totalPontos, $totalNaMosca, $posicaoRodada, $posicaoRodadaAnterior);
		}		

		AtualizarPosicoesRodada($conexao, $campeonatoId, $rodada, $turno);
	}
	
	return true;
}


?>