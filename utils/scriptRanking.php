<?php
	/* Regras para exibição do aviso:
		- palpites para rodada ainda não gravados
		- tempo hábil para gravar palpites			
	

	$rodadaAtual =  RetornaRodadaAtual($conexao,$campeonato->Id);
	$palpitesNaoGravados = PalpitesAindaNaoGravados($conexao, $campeonato->Id, $rodadaAtual, $participanteId);

	$possoGravarPalpites = ValidaRodada($conexao, $rodadaAtual, $campeonato->Id);

	if($palpitesNaoGravados and $possoGravarPalpites){		
		$dataFinalPalpites = RetornaDataInicioRodada($conexao,$rodadaAtual,$campeonato->Id);		
		// formato retornado: 2016-06-25 19:00:00	
		
		$data = substr($dataFinalPalpites, 0, 10);
		$hora = substr($dataFinalPalpites, 11, 8);	

		$data = explode("-", $data);
		$horaInteira = explode(":", $hora);
		
		$ano = $data[0];
		$mes = $data[1];
		$dia = $data[2];
		$hora = $horaInteira[0];
		$minuto = $horaInteira[1];	
		
		$dataLimite = mktime($hora - 2, $minuto, 0, $mes, $dia, $ano);
		$dataLimite=date("Y-m-d H:i:s", $dataLimite);
		
		$data = substr($dataLimite, 0, 10);
		$hora = substr($dataLimite, 11, 8);	

		$data = explode("-", $data);
		$horaInteira = explode(":", $hora);
		
		$ano = $data[0];
		$mes = $data[1];
		$dia = $data[2];
		$hora = $horaInteira[0];
		$minuto = $horaInteira[1];
		
		// $dataLimite=date("Y-m-d H:i:s", $dataLimite);				
		// print "<script type='text/javascript'> alert('$dataLimite'); </script>";
		?>

		<div class="alert alert-warning alert-dismissable" style="text-align: -moz-center;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>			
				
			<div class="caixa-aviso" style="color:#ff0033; font-size:16px; background:#ffffff url() no-repeat center;
				border:1px solid #444444; max-height:185px; max-width:500px; padding:8px; 
				-moz-border-radius:7px;-khtml-border-radius:7px;-webkit-border-radius:7px;border-radius:7px;margin: auto;">

				<script type="text/javascript">
					document.write("<span id='pageinval35' style='font-family:arial, helvetica, sans-serif; font-weight:bold;';></span>");
					
					function countdown_load99(){						
						var the_event="Início da rodada: <?php print $dia.'/'.$mes.'/'.$ano.' - '.$hora.':'.$minuto ?>";
						var on_event="PALPITES ENCERRADOS PARA RODADA ATUAL"; //Mensagem no dia do evento
						var event="EVENTO";  //Nome do evento
						var yr="<?php print $ano ?>"; 
						var mo="<?php print $mes ?>";
						var da="<?php print $dia ?>";
						var hr="<?php print $hora ?>";
						var min="<?php print $minuto ?>";
						var sec=0;
						var month='';
						var month=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");var bottom_event="";var now_d=new Date();
						var now_year=now_d.getYear();if (now_year < 1000)now_year+=1900;var now_month=now_d.getMonth();
						var now_day=now_d.getDate();
						var now_hour=now_d.getHours();
						var now_min=now_d.getMinutes();
						var now_sec=now_d.getSeconds();
						var now_val=month[now_month]+" "+now_day+", "+now_year+" "+now_hour+":"+now_min+":"+now_sec;
						event_val=month[mo-1]+" "+da+", "+yr+" "+hr+":"+min+":"+sec;
						difference=Date.parse(event_val)-Date.parse(now_val);
						differenceday=Math.floor(difference/(60*60*1000*24)*1);
						differencehour=Math.floor((difference%(60*60*1000*24))/(60*60*1000)*1);
						differencemin=Math.floor(((difference%(60*60*1000*24))%(60*60*1000))/(60*1000)*1);
						differencesec=Math.floor((((difference%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1);

						if(document.getElementById('pageinval35')){
							if(differenceday<=0&&differencehour<=0&&differencemin<=0&&differencesec<=1&&now_day==da){
								document.getElementById('pageinval35').innerHTML=on_event;
								}
							else if (differenceday<=-1){
								document.getElementById('pageinval35').innerHTML="Evento : "+on_event+" ... já passou";
							}else {
								var mensagem=" ";
								if(differenceday>0){
									var dia=" dia, "
									if(differenceday>1){
										dia=" dias, "
									}
									mensagem=mensagem+differenceday+dia;
								} 
								
								if(differencehour>0){
									var hora=" hora, "
									if(differencehour>1){
										hora=" horas, "
									}
									mensagem=mensagem+differencehour+hora;
								}
								
								if(differencemin>0){
									var minuto=" minuto, "
									if(differencemin>1){
										minuto=" minutos, "
									}
									mensagem=mensagem+differencemin+minuto;
								}
								
								if(differencesec>0){
									var segundo=" segundo. "
									if(differencesec>1){
										segundo=" segundos. "
									}
									mensagem=mensagem+differencesec+segundo;
								}
								
								mensagem=mensagem+bottom_event;
								document.getElementById('pageinval35').innerHTML="<center><font size='4'>"+the_event+"</font><br/><br/> "+mensagem;
							}
						}
					setTimeout("countdown_load99()",1000);}countdown_load99();
				</script>
			</div>				
		</div> <?php
		
	}
		*/
	?>