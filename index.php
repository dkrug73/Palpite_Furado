<?php 
	SESSION_START();
	require_once('classes/ConexaoBancoDeDados.php');
	require_once('classes/Campeonato.php');

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$erro = '0';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }	

	$campeonato = new Campeonato($conexao);
	
	$_SESSION['campeonatoId'] = $campeonato->RetornaCampeonatoAtivo();

	$campeonato->Id = $_SESSION['campeonatoId'];
	$rodadaAtual = $campeonato->RetornaRodadaAtual();
	$turno = $campeonato->RetornaTurnoDaRodada($rodadaAtual);
	$descricaoTurno = "PRIMEIRO";
	if ($turno == 2) $descricaoTurno = "SEGUNDO";
	
	$participanteId = "0";
    if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
    }
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Ranking</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="description" content="No Bolão Palpite Furado tu dá teus palpites para os jogos do Campeonato Brasileiro e se 
		diverte competindo com os pitacos dos amigos e demais participantes do bolão. Para participar basta preencher o cadastro 
		e postar seus palpites furados.">
	<meta name="keywords" content="Palpite, palpites, pitaco, pitacos, campeonato brasileiro, brasileirão, bolão, bolão de futebol, bolão do campeonato brasileiro,
		bolão do brasileirão">
	<meta name="author" content="Daniel Krug">

	<link rel="icon" type="image/png" href="imagens/favicon.png">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/utils.js"></script>
	
	<script type="text/JavaScript">	
	
	$(document).ready(function(){	  

		atualizarTabelaTurnoRodadaGeral("rodada", "tabela-ranking-rodada");
		atualizarTabelaRodada();

		setInterval(function() { 
			atualizarTabelaTurnoRodadaGeral("rodada", "tabela-ranking-rodada");
			atualizarTabelaRodada();
		}, 60000);
		
		function atualizarTabelaTurnoRodadaGeral(tabela, tabelaId){
			var campeonatoId = '<?php echo $_SESSION['campeonatoId']; ?>' ;
			var participanteId = '<?php echo $participanteId; ?>';

			$.ajax({
				method: 'post',
				url: 'paginas/tabelas/carregarRankingTurno.php',
				dataType: 'json',
				data: { campeonatoId: campeonatoId, rodada: tabela },
				success: function(dados) {

					document.getElementById(tabelaId).innerHTML = null
					
					for(var i=0;dados.length>i;i++){
						var turno =  <?php echo $turno ?>;
						var participanteId = dados[i]['participanteId'];
						var nome_usuario = titleize(dados[i]['nomeParticipante']);
						var fotoParticipante = dados[i]['fotoParticipante'];
						var pontos = dados[i]['totalPontos'];
						var totalNaMosca = dados[i]['totalNaMosca'];
						var imagem = "src='imagens/fotos/sem_imagem.png' ";
						var pontuacaoInicial = dados[i]['pontuacaoInicial'];

						var pontos = dados[i]['totalPontos'] ;
						var totalNaMosca = dados[i]['totalNaMosca'];

						if (pontos == null){
							pontos = 0;
						}
						
						//pontos = parseInt(pontos) + parseInt(pontuacaoInicial);

						if (totalNaMosca == null){
							totalNaMosca = 0;
						}			

						if (fotoParticipante != "") imagem = "src=imagens/fotos/"+ fotoParticipante;

						var coluna = "<td  style='text-align:center; '>"+ (i+1) + "</td>";

						if (i == 0)
						{
							coluna = "<td  style='text-align:center; background-color: cornflowerblue;color: black;'>"+ (i+1) + "</td>";
						}

						if (i > 0 && i < 4)
						{
							coluna = "<td  style='text-align:center; background-color: lightblue;color: black;'>"+ (i+1) + "</td>";
						}
						else if (i >= 4 && i < 7)
						{
							coluna = "<td  style='text-align:center; background-color: bisque;color: black;'>"+ (i+1) + "</td>";
						}
						else if(i > dados.length-5)
						{
							coluna = "<td  style='text-align:center; background-color: red;color: black;'>"+ (i+1) + "</td>";
						}

						if (participanteId == '<?php echo $participanteId; ?>') {	
							$("#" + tabelaId).append(" <tr style='color:red; cursor:pointer;' onClick=AbrirPalpites("+participanteId+");>" +
								coluna +
								"<td>" + nome_usuario + "</td>" +
								"<td>" + pontos + "</td>" +
								"<td>"+ totalNaMosca + "</td>" +
								"<td> <img style='width: 22px;height: 22px;' " + imagem +" ></td>" +
								"</tr>"
							);
						}
						else {
							$("#" + tabelaId).append(" <tr style='color:black; cursor:pointer;' onClick=AbrirPalpites("+participanteId+");>" +
								coluna +
								"<td>" + nome_usuario + "</td>" +
								"<td>" + pontos + "</td>" +
								"<td>"+ totalNaMosca + "</td>" +
								"<td> <img style='width: 22px;height: 22px;' " + imagem +" ></td>" +
								"</tr>"
							);
						}
					}                
				}
			});		
		}

		function atualizarTabelaRodada(){
			var campeonatoId = '<?php echo $_SESSION['campeonatoId']; ?>' ;
			var participanteId = '<?php echo $participanteId; ?>';

			$.ajax({
				method: 'post',
				url: 'paginas/tabelas/carregarRankingRodada.php',
				dataType: 'json',
				data: { campeonatoId: campeonatoId },
				success: function(dados) {

					document.getElementById("tabela-rodada").innerHTML = null
					
					for(var i=0;dados.length>i;i++){
						var participanteId = dados[i]['participanteId'];
						var nome_usuario = titleize(dados[i]['nomeParticipante']);
						var fotoParticipante = dados[i]['fotoParticipante'];
						var pontos = dados[i]['totalPontos'];
						var naMosca = dados[i]['totalNaMosca'];
						var imagem = "src='imagens/fotos/sem_imagem.png' ";

						if (pontos == null){
							pontos = 0;
						}

						if (naMosca == null){
							naMosca = 0;
						}
						
						var coluna = "<td  style='text-align:center; '>"+ (i+1) + "</td>";

						if (i == 0)
						{
							coluna = "<td  style='text-align:center; background-color: cornflowerblue;color: black;'>"+ (i+1) + "</td>";
						}

						if (i > 0 && i < 4)
						{
							coluna = "<td  style='text-align:center; background-color: lightblue;color: black;'>"+ (i+1) + "</td>";
						}
						else if (i >= 4 && i < 7)
						{
							coluna = "<td  style='text-align:center; background-color: bisque;color: black;'>"+ (i+1) + "</td>";
						}
						else if(i > dados.length-5)
						{
							coluna = "<td  style='text-align:center; background-color: red;color: black;'>"+ (i+1) + "</td>";
						}		

						if (fotoParticipante != "") imagem = "src=imagens/fotos/"+ fotoParticipante;

						if (participanteId == '<?php echo $participanteId; ?>') {	
							$("#tabela-rodada").append(" <tr style='color:red;cursor:pointer;' onClick=AbrirPalpites("+participanteId+");>" +
								coluna +
								"<td>" + nome_usuario + "</td>" +
								"<td>" + pontos + "</td>" +
								"<td>"+ naMosca + "</td>" +
								"<td> <img style='width: 22px;height: 22px;' " + imagem +" ></td>" +
								"</tr>"
							);
						}
						else {
							$("#tabela-rodada").append(" <tr style='color:black;cursor:pointer;' onClick=AbrirPalpites("+participanteId+");>" +
								coluna +
								"<td>" + nome_usuario + "</td>" +
								"<td>" + pontos + "</td>" +
								"<td>"+ naMosca + "</td>" +
								"<td> <img style='width: 22px;height: 22px;' " + imagem +" ></td>" +
								"</tr>"
							);
						}
					}                
				}
			});		
		}	
	});

	</script>

</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">	
		
		<!-- MENU -->
		<?php include("componentes/menu.php"); ?>	
		
		<!-- TABELA RANKING TURNO -->
		<div class="container mt-4 mb-5" style="font-size: 1em;">

			<div class="card" style="margin-bottom: 30px;">
				<div style="font-size: 25px;text-align: center;" class="card text-white bg-success ">
					<?php echo $descricaoTurno ?> TURNO
				</div> 

				<div class = "table-responsive">	       

					<table class="table  table-sm table-striped table-borderless no-padding">
						<thead class="bg-light">
							<tr>
								<th style="text-align:center;">P</th>
								<th>Nome do participante</th>
								<th>Ptos</th>
								<th>Mosca</th>
								<th>Foto</th>
							</tr> 
						</thead>  

						<tbody id="tabela-ranking-rodada"> 
						</tbody>				
					</table>  
				</div>   	
			</div>
		</div>

		<!-- TABELA RODADA -->
		<div class="container mt-4 mb-5" style="font-size: 1em;">

			<div class="card" style="margin-bottom: 30px;">
				<div style="font-size: 25px;text-align: center;" class="card text-white bg-success ">
					PONTUADORES DA RODADA
				</div> 

				<div class = "table-responsive">	       

					<table class="table  table-sm table-striped table-borderless no-padding">
						<thead class="bg-light">
							<tr>
								<th style="text-align:center;">P</th>
								<th>Nome do participante</th>
								<th>Ptos</th>
								<th>Mosca</th>
								<th>Foto</th>
							</tr> 
						</thead>  

						<tbody id="tabela-rodada"> 
						</tbody>				
					</table> 
				</div>   	
			</div>
		</div>

		<!-- MODAL ENTRAR -->		
		<?php include("modal/entrar.php"); ?>

		<!-- RODAPE -->		
		<?php include("componentes/rodape.php"); ?>
	</div>	
	
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>