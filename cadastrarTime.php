<?php 
	SESSION_START();

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$erro = '0';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }	
	
	$participanteId = "0";
    if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cadastro de time</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/time.js"></script> 	
	<script src="componentes/js/utils.js"></script>
	<link rel="icon" type="image/png" href="imagens/favicon.png">	
</head>

<body>

	<?php include("componentes/menu.php"); ?>	

	<div class = "container">

		<div id="salvar-sucesso" style="display:none;">
			<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
				Time salva com sucesso!
			</div>           
		</div>

		<div id="salvar-erro" style="display:none;">
			<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
				Erro ao salvar time.
			</div> 
		</div>

		<div id="excluir-sucesso" style="display:none;">
			<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
				Time excluído com sucesso!
			</div>           
		</div>

		<div id="excluir-erro" style="display:none;">
			<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
				Erro ao excluir time.
			</div> 
		</div>

		<section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro de time</h3>				
        </section>

		<section>	
			<form name="formulario-cadastro" id="formulario-cadastro" method="" action="" enctype="">

				<div class="form-group row">
					<label for="id" class="col-lg-2 col-md-2 col-form-label">ID</label>

					<div class="col-lg-10 col-md-10">
						<input type="text" class="form-control" id="id" name="id" value="" maxlength="100" readonly='yes'>
						<span id="erro-id" style="color: red;font-size: 14px;">  </span>
					</div>
				</div>

				<div class="form-group row">
                    <label for="nome" class="col-lg-2 col-md-2 col-form-label">Nome do time</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="nome" name="nome" value="" maxlength="100">
                        <span id="erro-nome" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div>

				<div class="form-group row">
                    <label for="nome-abreviado" class="col-lg-2 col-md-2 col-form-label">Nome abreviado</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="nome-abreviado" name="nome-abreviado" value="" maxlength="3" style="font-variant-caps: all-petite-caps;">
                        <span id="erro-nome-abreviado" style="color: red;font-size: 14px;">  </span>
                 	</div>
                </div>	

				<div class="form-group row">
                    <label for="cidade" class="col-lg-2 col-md-2 col-form-label">Cidade</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="cidade" name="cidade" value="" maxlength="100">
                        <span id="erro-cidade" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div>

				<div class="form-group row">
                    <label for="estadio" class="col-lg-2 col-md-2 col-form-label">Estádio</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="estadio" name="estadio" value="" maxlength="100">
                        <span id="erro-estadio" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div>

				<div class="form-group row">
                    <label for="tecnico" class="col-lg-2 col-md-2 col-form-label">Distintivo</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="foto" name="foto-time" onchange="readURL(this);">
                            <label class="custom-file-label" for="foto" aria-describedby="inputGroupFileAddon02">Escolha o arquivo</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <img src="imagens/times/sem_imagem.png" class="figure-img img-fluid rounded" id="foto-time" name="foto-exibida" width="100" height="100">
                    </div>
                </div>     	
				
				<div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="button" class="btn btn-primary mr-2" id="btn-salvar">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn-cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>

                    </div>
                </div>
			</form>

            <div id="resultado-tabela"></div>

		</section>
	</div>

	<!-- MODAL ENTRAR -->		
	<?php include("modal/entrar.php"); ?>
	
	<?php include("componentes/rodape.php"); ?>

	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>