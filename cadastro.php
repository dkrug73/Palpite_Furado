<?php 
	SESSION_START();
	require_once('classes/Participante.php');
	require_once('classes/ConexaoBancoDeDados.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql();  

	$participante = new Participante($conexao);

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$erro = null;
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
	}	   

	if(isset ($_SESSION['participanteId']) == true) {
		$participante->Id = $_SESSION['participanteId'];

		$participante->CarregarDadosParticipante();
    }	
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Cadastro</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/participante.js"></script> 
	<script src="componentes/js/utils.js"></script>	
	
	<link rel="icon" type="image/png" href="imagens/favicon.png">	

	<style>	
        .my-error-class {
            color:red;
        }
	</style>

    <script>

        $(document).ready(function(){
    
            var participanteId = '<?php echo ($participante->Id); ?>';
            
            if (participanteId != ""){
                document.getElementById('nome').value = '<?php echo ($participante->Nome); ?>';
                document.getElementById('nome-usuario').value = '<?php echo ($participante->NomeUsuario); ?>';
                document.getElementById('email').value = '<?php echo ($participante->Email); ?>';
                document.getElementById('mandante').value = '<?php echo ($participante->PadraoMandante); ?>';
                document.getElementById('visitante').value = '<?php echo ($participante->PadraoVisitante); ?>';
                document.getElementById('regulamento').checked = '<?php echo ($participante->AceitouTermos); ?>' == true;
                document.getElementById('receberEmail').checked = '<?php echo ($participante->ReceberEmail); ?>' == true;
                document.getElementById('foto-time').src = 'imagens/fotos/<?php echo ($participante->Foto);?>';
            }
       });

    </script>
</head>

<body>

	<?php include("componentes/menu.php"); ?>	

	<div class = "container">

        <?php  
        if (isset($erro)){  
            if($erro == '0'){ ?>
                <div class="alert alert-success mt-3" role="alert" id="alerta">
                    Sucesso ao salvar cadastro!
                </div> <?php
            }
            elseif($erro == '4'){ ?>
                <div class="alert alert-danger mt-3" role="alert" id="alerta">
                    Erro ao salvar cadastro!
                </div> <?php
            } 
            elseif($erro == '5'){ ?>
                <div class="alert alert-warning mt-3" role="alert" id="alerta">
                    Email ou Nome de usuário já existe.
                </div> <?php
            }?>     
            
            <script>
                setTimeout(function() {$('#alerta').fadeOut('slow');}, 3000);
            </script> <?php
        } ?>  

		<section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro do participante</h3>				
        </section>
		
		<section>	
			<form name="formulario-cadastro" id="formulario-cadastro" method="" action="" 
					enctype="">

				<div class="form-group row">
                    <label for="nome" class="col-lg-2 col-md-2 col-form-label">Nome</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="nome" name="nome" value="" maxlength="20">
                        <span id="erro-nome" style="color: red;font-size: 14px;">  </span>
                 </div>
                </div>

				<div class="form-group row">
                    <label for="nome-usuario" class="col-lg-2 col-md-2 col-form-label">Nome usuário</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="nome-usuario" name="nomeUsuario" value="" maxlength="20" >
						<span id="erro-usuario" style="color: red;font-size: 14px;">  </span>
					</div>
                </div>

				<div class="form-group row">
                    <label for="email" class="col-lg-2 col-md-2 col-form-label">Email</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="email" name="email" value="" maxlength="100">
                        <span id="erro-email" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="senha" class="col-lg-2 col-md-2 col-form-label">Senha</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="password" class="form-control" id="senha" name="senha" value="" maxlength="32" autocomplete="new-password">
                        <span id="erro-senha" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contraSenha" class="col-lg-2 col-md-2 col-form-label">Confirmar senha</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="password" class="form-control" id="contraSenha" name="contraSenha" value="" maxlength="32">
                        <span id="erro-contraSenha" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div>

				<div class="form-group row">
					<label for="inputPadrao" class="col-lg-2 col-md-2 col-form-label">Palpite Padrão</label>
					
						<div class="col-md" style="min-width: 80px;max-width: 80px;">
							<input type="text" class="form-control" id="mandante" name="mandante" style="text-align: center;"
								value="" >
                            <span id="erro-mandante" style="color: red;font-size: 14px;">  </span>
						</div>

						<span style="margin-top: 4px;">x</span>

						<div class="col-md" style="min-width: 80px;max-width: 80px;">
							<input type="text" class="form-control" id="visitante" name="visitante" style="text-align: center;"
								value="" >
                            <span id="erro-visitante" style="color: red;font-size: 14px;">  </span>
						</div>

                        <span id="erro-padrao" style="color: red;font-size: 14px;">  </span>
				</div>

                <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>

                    <div class="col-lg-5 col-md-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="receberEmail" name="receberEmail">
                            <label class="custom-control-label" for="receberEmail">Receber email de lembrete para palpites não informados</label>
                        </div>
                    </div>
                </div>  		

				 <div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>

                    <div class="col-lg-5 col-md-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="regulamento" name="regulamento">
                            <label class="custom-control-label" for="regulamento">Eu li e concordo com o <a href="../regulamento.php">Regulamento</a>.</label>
                        </div>                        
                        
                        <span id="erro-regulamento" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div> 

				<div class="form-group row">
                    <label for="tecnico" class="col-lg-2 col-md-2 col-form-label">Foto</label>
                    
                    <div class="col-lg-10 col-md-10">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="foto" name="foto-time" onchange="readURL(this);">
                            <label class="custom-file-label" for="foto" aria-describedby="inputGroupFileAddon02">Escolha o arquivo</label>
                        </div>
                    </div>
                </div>				

				<div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                       <img src="imagens/fotos/sem_imagem.png" class="figure-img img-fluid rounded" id="foto-time" width="100" height="100">

                       <input type="hidden" name="foto-usuario" id="foto-usuario" value=""/>
                    </div>
                </div>  

				<div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="submit" class="btn btn-primary mr-2" id="btn_salvar">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn_cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn_excluir">Excluir</button>

                    </div>
                </div>
			</form>
            <div id="resultado"></div>
		</section>
		
	</div>

    <!-- MODAL ENTRAR -->		
    <?php include("modal/entrar.php"); ?>
        
	<?php include("componentes/rodape.php"); ?>

    <script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>