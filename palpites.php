<?php 
	SESSION_START();
	require_once('classes/Participante.php');
	require_once('classes/Campeonato.php');
	require_once('classes/Jogo.php');
	require_once('classes/ConexaoBancoDeDados.php');

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];
	
	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$participante = new Participante($conexao);
   
	$participanteId = '0';	
	if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}
	
	$erro = '';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
    
	$campeonato = new Campeonato($conexao);
	
    if (!isset($_SESSION['campeonatoId'])){
		$_SESSION['campeonatoId'] = $campeonato->RetornaCampeonatoAtivo();
	}

	$campeonato->Id = $_SESSION['campeonatoId'];	

    $rodadaAtual = $campeonato->RetornaRodadaAtual();
	$dataHora = $campeonato->RetornaDataHoraInicioRodada($rodadaAtual);   

	$rodada = $rodadaAtual;

	$jogo = new Jogo($conexao);
	$maiorRodada = $jogo->RetornaUltimaRodada($campeonato->Id);

	$participanteId = "0";
    if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}

?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Palpites</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="description" content="No Bolão Palpite Furado tu dá teus palpites para os jogos do Campeonato Brasileiro e se 
		diverte competindo com os pitacos dos amigos e demais participantes do bolão. Para participar basta preencher o cadastro 
		e postar seus palpites furados.">
	<meta name="keywords" content="Palpite, palpites, pitaco, pitacos, campeonato brasileiro, brasileirão, bolão, bolão de futebol, bolão do campeonato brasileiro,
		bolão do brasileirão">
	<meta name="author" content="Daniel Krug">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<link rel="stylesheet" href="bootstrap/css/css/palpites.css">	
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 

	<link rel="icon" type="image/png" href="imagens/favicon.png">

    <script type="text/JavaScript">
		
        window.onload = function(){
            getValor('<?php echo $rodada ?>', '<?php echo $campeonato->Id ?>');		
		}
		
		$(document).ready(function(){

			$('#formulario-palpites').submit(function(){    				
				$.ajax({
					type: 'post',
					url: 'paginas/palpites1.php',
					data: $('#formulario-palpites').serialize(),
					success: function(data){
						$('html, body').animate({scrollTop:0}, 'slow');
						
						var classe = '#salvar-erro';
						if (data == 1){
							classe = '#salvar-sucesso';
						}

						ExibirAviso(classe);
					}
				});

				return false;
			});
		});

		function ExibirAviso(classe){     
			$(classe).show(); 
			setTimeout(function() {$(classe).fadeOut('slow');}, 5000);
		} 
        
        function getValor(valor, id_campeonato){	
            setTimeout(		
                function(){	
                    next = valor;
                    $("#recebeValor").load(
                        "paginas/tabelas/carregarPalpites.php",{
                            rodada:valor,
							campeonatoId:id_campeonato,
							participanteId:"<?php echo $participanteId ?>"
                        }
                    )
                }
            );				
        }
        
        //next = "<?php echo $rodada; ?>";  
        
             
    </script>
</head>

<body>
	<div class="wrapper">

		<!-- MENU -->
		<?php include("componentes/menu.php"); ?>	

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">    
			<section class="content">

				<?php require('componentes/alerta.php'); ?>   

                <?php 
                if ($participanteId == "0") {  ?>
                    <div class="alert alert-warning alert-dismissible text-center m-3">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-warning"></i> IMPORTANTE!</h4>
                        Para informar seus palpites no bolão é necessário fazer login.
                    </div> <?php 
				/*}
				else {                
					if (!empty($tipoAviso)) { ?>
						<?php include ("componentes/mensagem.php"); ?> <?php   
					}  
					              
					else { ?>
						<div class="aviso-palpite"> 
							<div > Início: <?php print $dataHora ?> </div>    
							<?php include "utils/scriptPalpites.php"; ?>	
						</div> <?php
					} 
					*/
				}?>                                

				<div class="container mt-4 mb-5" style="font-size: 1em;">
					<div class="form-group" style="text-align: -moz-center; text-align: -webkit-center;">
						<?php include("utils/selectRodada.php") ?>
					</div>

					<form id="formulario-palpites" name="formulario-palpites">
						<div name="recebeValor" id="recebeValor"></div>	
					</form>							
				</div>
            </section>
        </div> <!-- /.content-wrapper -->

		<!-- MODAL ENTRAR -->		
		<?php include("modal/entrar.php"); ?>
		
		<!-- RODAPE -->		
		<?php include("componentes/rodape.php"); ?>

	</div> <!-- /wrapper -->
	

	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>