<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AdminLTE 2 | Blank Page</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<link rel="icon" type="image/png" href="imagens/favicon.png">
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">
		
		<!-- CABEÇALHO -->
		<header class="main-header">
			<a href="index.html" class="logo">
				<span class="logo-mini"><img src="imagens/favicon.png"></span>
				<span class="logo-lg">PALPITE<b> FURADO</b></span>
			</a>
			
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li><a href="#" data-toggle="modal" data-target="#myModal4"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Entrar</a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal5"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Cadastrar</a></li>
						<li><a href="paginas.sair.php" data-toggle="modal" data-target="paginas.sair.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Sair</a></li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- =============================================== -->

        <!-- MENU -->
		<aside class="main-sidebar">
			<section class="sidebar">
				<div class="user-panel">
					<div class="pull-left image">
						<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Alexander Pierce</p>
					</div>
				</div>

				<ul class="sidebar-menu">
					<li class="header">MENU DE NAVEGAÇÃO</li>

					<li><a href="index.html"><i class="fa fa-book"></i> <span> Ranking</span></a></li>

					<li><a href="palpites.html"><i class="fa fa-book"></i> <span> Palpites</span></a></li>

					<li><a href="palpitesAlheios.html"><i class="fa fa-book"></i> <span> Palpites Alheios</span></a></li>

					<li><a href="Tabela.html"><i class="fa fa-book"></i> <span> Tabela</span></a></li>

					<li class="header">UTILIDADES</li>

					<li><a href="cadastro.html"><i class="fa fa-book"></i> <span> Cadastro</span></a></li>

					<li><a href="contato.html"><i class="fa fa-book"></i> <span> Contato</span></a></li>

					<li><a href="regulamento.html"><i class="fa fa-book"></i> <span> Regulamento</span></a></li>

					<li class="header">ÁREA PRIVADA</li>


					<li class="treeview">
						<a href="#">
							<i class="fa fa-dashboard"></i> <span> Manutenção</span>
							<span class="pull-right-container">
             					<i class="fa fa-angle-left pull-right"></i>
            				</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="../../index.html"><i class="fa fa-circle-o"></i> Cadastrar jogos</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Gravar resultado</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Pontuação inicial</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Desativar participante</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Recalcular tabela</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Enviar email</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="#">
							<i class="fa fa-dashboard"></i> <span> Cadastro</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="../../index.html"><i class="fa fa-circle-o"></i> Time</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Campeonato</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Times/Campeonato</a></li>
							<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Participante/Campeonato</a></li>
						</ul>
					</li>
				</ul>
			</section>
		</aside>
		<!-- =============================================== -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Blank page
					<small>it all starts here</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Examples</a></li>
					<li class="active">Blank page</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Title</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						Start creating your amazing application!
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						Footer
					</div>
					<!-- /.box-footer-->
				</div>
				<!-- /.box -->

			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 3.1
			</div>
			<strong>Copyright &copy; 2014-2016 <a href="https://www.facebook.com/dkrug73">Daniel Krug</a>.</strong> Todos direitos reservados.
		</footer>

	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
</body>

</html>