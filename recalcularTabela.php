<?PHP 
	SESSION_START();
	require_once('classes/ConexaoBancoDeDados.php');
	require_once('classes/Campeonato.php');
	require_once('classes/Rodada.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql(); 
    
    $rodada = new Rodada($conexao);
    $rodada->CampeonatoId = $_SESSION['campeonatoId'];

    $maximoPalpitesPontos = $rodada->RetornaRodadaMaximoPontos();
            
    for ($contadorRodada = 1; $contadorRodada <= $maximoPalpitesPontos; $contadorRodada++) {     
          if (!$rodada->AtualizarTabelaRodada($contadorRodada)) return false;
          
          if (!$rodada->AtualizarTabelaRodadaGeral($contadorRodada)) return false;
    }	
    
	$rodada->FecharConexao();  
	
	header('Location: /index.php');
	

?>
