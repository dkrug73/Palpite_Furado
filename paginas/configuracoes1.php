<?php
    SESSION_START();
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";
	include "../classes.php";

	$campeonato = new Campeonato();
	$campeonato->Id = $_POST['campeonatoId'];

	$mensagem = "Times adicionados com sucesso!";
	$tipoAviso = "sucesso";
	$resultado = false;	

	$campeonato->ApagarTimesDoCampeonato($conexao, $campeonato->Id);
	
	if(isset($_POST['timeId'])) {
		if (!$campeonato->InserirTimesNoCampeonato($conexao, $campeonato->Id, $_POST['timeId'])){
			$mensagem="Houve um erro ao adicionar times ao campeonato.";
			$tipoAviso = "erro";
		}	
	}	

	print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastrarCampeonato.php?msg=$mensagem&tipoAviso=$tipoAviso'>";
	
	mysqli_close($conexao);	
?>