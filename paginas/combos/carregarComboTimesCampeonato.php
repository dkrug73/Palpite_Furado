<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();
       
    $sql="
        SELECT  
            id AS id, 
            nome AS nome 
        FROM 
            times 
        ORDER BY 
            nome";

    $rs=$conexao->query($sql);

    $select = null;
    while($reg=mysqli_fetch_array($rs))	
    {	                
        $select = $select."<option value='".$reg['id']."'>".$reg['nome']."</option> ";       
    } 
   
    echo $select;

    mysqli_close($conexao);
?>