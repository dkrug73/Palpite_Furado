<?php
    require_once('classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();
    
    $campeonatoId = $campeonato->RetornaCampeonatoAtivo();  

    $timeId = null;
    if(isset($_POST['mandanteId'])){
        $timeId = $_POST['mandanteId'];
    }
    elseif(isset($_POST['visitanteId'])){       
        $timeId = $_POST['visitanteId'];
    }
   
    $sql="
        SELECT DISTINCT 
            times.id, 
            times.nome 
        FROM 
            times_campeonato INNER JOIN 
            times ON 
                times_campeonato.timeId = times.id 
        WHERE 
            times_campeonato.campeonatoId = $campeonatoId 
        ORDER BY times.nome ";

    $rs=$conexao->query($sql);

    $select = null;
    while($reg=mysqli_fetch_array($rs))	
    {	        
        if ($timeId == $reg['id']) {
            $select = $select."<option value='".$reg['id']."'  selected='selected'>".$reg['nome']."</option> ";
        }
        else {
            $select = $select."<option value='".$reg['id']."'>".$reg['nome']."</option> ";
        }
    } 
   
    echo $select;

    mysqli_close($conexao);
?>