<?php
    require_once('../../classes/ConexaoBancoDeDados.php');
    require_once('../../classes/Campeonato.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();
    
    $campeonatoId = $_SESSION['campeonatoId'];
    if(isset($_POST['campeonatoId'])){
        $campeonatoId = $_POST['campeonatoId'];
    }
   
    $sql="SELECT id, descricao, ano FROM campeonatos ORDER BY ativo DESC, descricao, ano";

    $rs=$conexao->query($sql);

    $select = null;
    while($reg=mysqli_fetch_array($rs))	
    {	
        $descricao = $reg['descricao']." - ".$reg['ano'];
        
        if ($campeonatoId == $reg['id']) {
            $select = $select."<option value='".$reg['id']."'  selected='selected'>".$descricao."</option> ";
        }
        else {
            $select = $select."<option value='".$reg['id']."'>".$descricao."</option> ";
        }
    } 
   
    echo $select;

    mysqli_close($conexao);
?>