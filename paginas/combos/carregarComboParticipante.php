<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();
    
    $campeonatoId = $_SESSION['campeonatoId'];
   
    $sql="
        SELECT 
            participantes.id AS id, 
            participantes.nome AS nome 
        FROM 
            participantes INNER JOIN 
            participantes_campeonato ON 
                participantes.id = participantes_campeonato.participanteId AND 
                campeonatoId = $campeonatoId 
        ORDER BY 
            participantes.nome ";

    $rs=$conexao->query($sql);
    
    $select = "<option value='0'>Selecione um participante</option>";

    while($reg=mysqli_fetch_array($rs))	
    {	
        $nome = mb_strtolower($reg['nome'], 'UTF-8');
	    $nome = ucwords($nome);
        
        if ($campeonatoId == $reg['id']) {
            $select = $select."<option value='".$reg['id']."'  selected='selected'>".$nome."</option> ";
        }
        else {
            $select = $select."<option value='".$reg['id']."'>".$nome."</option> ";
        }
    } 
   
    echo $select;

    mysqli_close($conexao);
?>