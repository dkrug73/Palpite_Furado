<?php
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql(); 
   
    $sql="
        SELECT 
            id, 
            nome 
        FROM 
            participantes
        ORDER BY 
            nome";

    $rs=$conexao->query($sql);

    $select = null;
    while($reg=mysqli_fetch_array($rs))	
    {
        $nome = mb_strtolower($reg['nome'], 'UTF-8');
        $nome = ucwords($nome);

        $select = $select."<option value='".$reg['id']."'>".$nome."</option> ";
    } 
   
    echo $select;

    mysqli_close($conexao);
?>