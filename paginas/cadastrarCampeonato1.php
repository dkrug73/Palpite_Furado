﻿<?PHP
	require_once('../classes/ConexaoBancoDeDados.php');
	require_once('../classes/Campeonato.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
   	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$ativo = 0;
	if (isset($_POST['ativo'])) $ativo = 1;	

	$campeonato = new Campeonato($conexao);
	$campeonato->Id = $_POST['id'];
	$campeonato->Descricao = $_POST['descricao'];
	$campeonato->Ano = $_POST['ano'];
	$campeonato->Serie = strtoupper($_POST['serie']);
	$campeonato->Ativo = $ativo;	

	$gravar = $campeonato->GravarCampeonato();
	
	$campeonato->FecharConexao(); 

	echo $gravar;
?>