<?PHP
   SESSION_START();
   require_once('../classes/Jogo.php');
   require_once('../classes/Time.php');
   require_once('../classes/ConexaoBancoDeDados.php');

   $conexaoBancoDeDados = new ConexaoBancoDeDados();
   $conexao = $conexaoBancoDeDados->ConectarMySql(); 

   $jogo = new Jogo($conexao);

   if(isset($_POST['id'])){
		$jogo->Id = $_POST['id'];
	}

	$jogo->CampeonatoId = $_SESSION['campeonatoId'];
	$jogo->Turno = $_POST['turno'];
	$jogo->Rodada = $_POST['rodada'];

	$dataJogo = $_POST['dataJogo'];
	$jogo->Data = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$dataJogo)));

	$jogo->MandanteId = $_POST['mandanteId'];
	$jogo->VisitanteId = $_POST['visitanteId'];

	$time = new Time($conexao);

	$time->CarregarDadosTime($jogo->MandanteId);
	$jogo->NomeMandante = $time->Nome;
	$jogo->MandanteAbreviado = $time->NomeAbreviado;
	$jogo->Estadio = $time->Estadio;

	if(isset($_POST['estadio'])){
		$jogo->Estadio = $_POST['estadio'];
	}

	$time->CarregarDadosTime($jogo->VisitanteId);
	$jogo->NomeVisitante =  $time->Nome;
	$jogo->VisitanteAbreviado = $time->NomeAbreviado;

	$gravar = $jogo->Gravarjogo();

    $jogo->FecharConexao();    
    
    echo $gravar;
?>