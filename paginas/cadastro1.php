<?php
    SESSION_START();
	require_once('../classes/Participante.php');
	require_once('../classes/ConexaoBancoDeDados.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$participante = new Participante($conexao);

    if(isset ($_SESSION['participanteId']) == true) { 
        $participante->setId($_SESSION['participanteId']);
    }
  	
	$nome = mb_strtolower($_POST['nome'], 'UTF-8');
    $nome = trim(ucwords($nome));

	$participante->setNome($nome);
	$participante->setNomeUsuario(trim($_POST['nomeUsuario']));
    $participante->setSenha(md5($_POST['senha']));
    $participante->setSenha(md5($_POST['contraSenha']));
	$participante->setEmail(trim($_POST['email']));
	$participante->setPadraoMandante($_POST['mandante']);
    $participante->setPadraoVisitante($_POST['visitante']);

    if(isset($_POST['receberEmail'])) {
		$participante->setReceberEmail(1);
    }
    
    if(isset($_POST['regulamento'])) {
		$participante->setAceitouTermos(1);
    }

	if (isset($_FILES['foto-time'])) {
        $foto = $_FILES['foto-time'];

        if($foto['error'] == 0) {        
            //define o caminho 
            $destino = "../imagens/fotos/";

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);		
                
            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            //armazena na variavel o novo nome
            $arquivo = basename($nome_imagem);

            //concatena a variavel do caminho com o nome do arquivo 
            $destino = $destino . $arquivo; 

            //testa se a funçao de upload rodar
            if(move_uploaded_file($foto['tmp_name'], $destino)) {
                $participante->setFoto($arquivo);
            }
		}	
        else{
            if(isset($_POST['foto-usuario']) or ($_POST['foto-usuario'] <> "")){
                $foto = explode('/', $_POST['foto-usuario']);
                if (sizeof($foto) > 1) $participante->setFoto($foto[2]);
            }
		}	           
    }	   

    $gravar = $participante->GravarParticipante($participante);

    $participante->FecharConexao();    
    
    echo $gravar;
	?>