<?PHP
	SESSION_START();
	require_once('../classes/Time.php');
	require_once('../classes/ConexaoBancoDeDados.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$time = new Time($conexao);

	$time->Id = $_POST['id'];

	$nome = mb_strtolower($_POST['nome'], 'UTF-8');
	$nome = ucwords($nome);
	$time->Nome = $nome;

	$time->NomeAbreviado = strtoupper($_POST['nome-abreviado']);

	$cidade = mb_strtolower($_POST['cidade'], 'UTF-8');
	$cidade = ucwords($cidade);
	$time->Cidade = $cidade;	

	$time->Estadio = strtoupper($_POST['estadio']);

	if($_FILES['foto-time']['size'] > 0){
		$time->Foto = $_FILES['foto-time'];			
	}

	$gravar = $time->GravarTime();
	
	$time->FecharConexao(); 

	echo $gravar;
?>