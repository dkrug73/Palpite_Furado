<?php
    session_start();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Participante.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();
    
    $participante = new Participante($conexao); 

    $email = addslashes($_POST['usuario']);
    $senha = addslashes(md5($_POST['senha']));

    $participante->ObterDadosParticipante($email, $senha);

    if (isset($participante->Id)){
        $_SESSION['participanteId'] = $participante->Id;
        $_SESSION['participanteNome'] = $participante->Nome;
        $_SESSION['nomeUsuario'] = $participante->NomeUsuario;
        $_SESSION['email'] = $participante->Email;
        $_SESSION['senha'] = $participante->senha;
        $_SESSION['padraoMandante'] = $participante->PadraoMandante;
        $_SESSION['padraoVisitante'] = $participante->PadraoVisitante;
        $_SESSION['receberemail'] = $participante->ReceberEmail;
        $_SESSION['foto'] = $participante->Foto;
        $_SESSION['nivel'] = $participante->Nivel;
        $_SESSION['logado'] = true;  

        $lembrete = (isset($_POST['lembrete'])) ? $_POST['lembrete'] : ''; 

        if($lembrete == "SIM") {
            $cookie_name = "CookieLembrete";
            $cookie_value = 'SIM';
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias

            $cookie_name = "CookieEmail";
            $cookie_value = $email;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias

            $cookie_name = "CookieSenha";
            $cookie_value = $senha;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias

            $cookie_name = "CookieSenha";
            $cookie_value = $senha;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias
        }
        else {			
            $cookie_name = "CookieLembrete";
            $cookie_value = 'SIM';
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias

            $cookie_name = "CookieEmail";
            $cookie_value = $email;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias

            $cookie_name = "CookieSenha";
            $cookie_value = $senha;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias

            $cookie_name = "CookieSenha";
            $cookie_value = $senha;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias
        }

            header('Location: ../index.php');
    }
    else{
        
            //print "<script type='text/javascript'> alert('Email ou Senha inválido!'); </script>";

            header('Location: ../index.php?erro=1');
    }   
?>