﻿<?PHP
	SESSION_START();	
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	

	$acao = $_POST['acao'];	

    $id = null;
	if(isset($_POST['id'])){
		$id = $_POST['id'];
	}
		
	$campeonatoId = $_SESSION['campeonatoId'];
	$participanteId = $_POST['participante'];

	$mensagem="";
	$retorno=false;

    if ($acao == "inc") {  // INCLUSÃO/ALTERAÇÃO		
		if ($id != null) { // se não for vazio, é uma alteração
			$mensagem = "Alteração do Participante efetuada com sucesso.";			
		
			$sql = "UPDATE participantes_campeonato SET participanteId = '$participanteId' ";
			$sql = $sql . " WHERE id = '" . $id . "' ";	
			
			$retorno=$conexao->query($sql);			
		}
		else {
			$mensagem = "Participante incluído com sucesso.";
		
			$sql="INSERT INTO participantes_campeonato (participanteId, campeonatoId) VALUES ('$participanteId', '$campeonatoId') ";
			$retorno=$conexao->query($sql);
			$id = $conexao->insert_id;
		}	
	}
	else if ($acao == "exc") { // EXCLUSÃO	
		$mensagem="Participante excluído com sucesso.";

		$sql = "DELETE FROM participantes_campeonato WHERE id = '" . $id . "' ";	
			
		$retorno=$conexao->query($sql);  			    
	} 

	if ($retorno) $tipoAviso = "sucesso";	
	else $tipoAviso = "erro";

	print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastrarParticipantesCampeonato.php?msg=$mensagem&tipoAviso=$tipoAviso '>";
	
	mysqli_close ($conexao);
?>

