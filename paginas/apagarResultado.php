<?PHP
	SESSION_START();
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../classes/Jogo.php');
    require_once('../classes/Palpite.php');
	
	$conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql(); 
    
    $jogoId = $_POST['jogoId'];

    $jogo = new Jogo($conexao);
    $jogo->Id = $jogoId;
    $jogo->PlacarMandante = null;
    $jogo->PlacarVisitante = null;    

    if (!$jogo->AtualizarJogo()){
       echo 1; 
    } 	
    
    $palpite = new Palpite($conexao); 
    $palpite->JogoId = $jogoId;

    if (!$palpite->ApagarPontosJogo()){
        echo 1;
    }
    
    else echo 0;
    
?>