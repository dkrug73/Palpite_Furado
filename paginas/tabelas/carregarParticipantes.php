<?php
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  
    
    $sql = "
        SELECT
            id, 
            nome, 
            nomeUsuario,
            email, 
            CASE 
                desativar 
            WHEN 0 
                THEN 'Ativo' 
            WHEN 1 
                THEN 'Desativado' 
            END as desativar
        FROM 
            participantes			
        ORDER BY 
            nome,
            nomeUsuario";
            
    $rs=$conexao->query($sql);
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-participante">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Nome usuário</th>
            <th>Email</th>
            <th>Ativo</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td><?php echo $reg['id']; ?></td>
                <td><?php echo $reg['nome']; ?></td>
                <td><?php echo $reg['nomeUsuario']; ?></td>
                <td><?php echo $reg['email']; ?></td>
                <td><?php echo $reg['desativar']; ?></td>
            </tr> <?php
        } 

        mysqli_close ($conexao);  ?>

    </tbody>
</table>
<br />