<?php
    require_once('../../classes/ConexaoBancoDeDados.php');
    require_once('../../classes/Campeonato.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 
	
    $campeonato = new Campeonato($conexao);
	$campeonato->Id = $_POST["campeonatoId"];
	$rodadaAtual = $campeonato->RetornaUltimaRodadaPalpites($campeonato->Id);
	
	if ($rodadaAtual == null){
		$rodadaAtual = "1";
	}

	//$turno = $campeonato->RetornaTurnoDaRodada($rodadaAtual);
	
	//$rodadaPontos = $campeonato->RetornaUltimaRodadaPalpites($campeonato->Id);

    $sql = "
		SELECT
			participantes_campeonato.participanteId,
			participantes.nome AS nomeParticipante,
			participantes.foto AS fotoParticipante,
			SUM(palpites.pontosRodada) + IF(pi.pontos IS NOT NULL, CONCAT(pi.pontos), 0) AS totalPontos, 
			SUM(palpites.naMosca) AS totalNaMosca
		FROM 
			participantes_campeonato LEFT JOIN
			palpites ON
				palpites.participanteId = participantes_campeonato.participanteId AND palpites.campeonatoId = $campeonato->Id LEFT JOIN 
			pontuacao_inicial pi
				ON pi.participanteId = participantes_campeonato.participanteId AND pi.campeonatoId = $campeonato->Id JOIN
			participantes ON
				participantes.id = participantes_campeonato.participanteId
		WHERE 
			participantes_campeonato.campeonatoId = $campeonato->Id AND
			palpites.rodada = $rodadaAtual    
		GROUP BY
			participantes_campeonato.participanteId
		ORDER BY 
			totalPontos DESC, 
			totalNaMosca DESC,
			nomeParticipante ASC";

    $rs=$conexao->query($sql);
    
    while($reg=mysqli_fetch_array($rs))
    {
        $vetor[] = array_map(null, $reg); 
    }  

    echo json_encode($vetor)
    ?>