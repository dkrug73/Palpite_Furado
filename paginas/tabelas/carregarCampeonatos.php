<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  
    
    $sql = "
        SELECT
            id, 
            descricao, 
            ano,
            serie,
            (SELECT CASE WHEN  ativo THEN 'sim' ELSE 'não' END) AS ativo
        FROM 
            campeonatos
        ORDER BY
            ativo DESC,
            descricao ASC ";
            
    $rs=$conexao->query($sql);
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-participante">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Descrição</th>
            <th>Ano</th>
            <th>Série</th>
            <th>Ativo</th>                     
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td><?php echo $reg['id']; ?></td>
                <td><?php echo $reg['descricao']; ?></td>
                <td><?php echo $reg['ano']; ?></td>
                <td><?php echo $reg['serie']; ?></td>
                <td><?php echo $reg['ativo']; ?></td>            
            </tr> <?php
        } 

        mysqli_close ($conexao);  ?>

    </tbody>
</table>
<br />