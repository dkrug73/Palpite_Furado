<?php
    SESSION_START();
	require_once('../../classes/Palpite.php');
	require_once('../../classes/ConexaoBancoDeDados.php');	
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 
	
	$palpite = new Palpite($conexao);
    $campeonatoId = $_POST["campeonatoId"];
	$participanteId = $_POST['txtParticipante'];

	$sql = "	
		SELECT 
			participanteId,
			jogoId,
			nomeMandante,
			mandanteAbreviado,
			nomeVisitante,
			visitanteAbreviado,
			fotoMandante,
			fotoVisitante,
			rodada,
			pontosRodada,
			palpiteMandante,
			palpiteVisitante,
			placarMandante,
			placarVisitante, 
			naMosca,
			padrao
		FROM 
			palpites
		WHERE
			participanteId = $participanteId AND
			campeonatoId = $campeonatoId
		ORDER BY
			rodada DESC,
			dataJogo";



	$rs=$conexao->query($sql);
?>

	<div class="container"> <?php			
		$rodadaAnterior = "";
		while ($reg=mysqli_fetch_array($rs)) {
			$rodada = $reg['rodada'];
			$nomeMandante = $reg['nomeMandante'];
			$nomeVisitante = $reg['nomeVisitante'];
			$mandanteAbreviado = $reg['mandanteAbreviado'];
			$visitanteAbreviado = $reg['visitanteAbreviado'];
			$fotoMandante = $reg['fotoMandante'];
			$fotoVisitante = $reg['fotoVisitante'];
			$placarMandante = $reg['placarMandante'];
			$placarVisitante = $reg['placarVisitante'];
			$palpiteMandante = $reg['palpiteMandante'];
			$palpiteVisitante = $reg['palpiteVisitante'];
			$pontosRodada= $reg['pontosRodada'];
			$naMosca = $reg['naMosca'];	
			$jogoId = $reg['jogoId'];
			$participanteId = $reg['participanteId'];

			/*
			if ($jogoId == 535 or $jogoId == 540){
				$palpiteMandante = null;
				$palpiteVisitante = null;
			}
			
			if ($palpite->ValidaRodada($jogoId))
			{
				continue;
			}	
			*/
			
			if ($pontosRodada == "") $pontosRodada = "-";							
										
			if ($rodada != $rodadaAnterior) { 
				$asterisco = "";

				$sqlPadrao = "SELECT count(*) as palpitepadrao FROM palpites WHERE participanteId = $participanteId and campeonatoId = $campeonatoId and padrao = 1 and rodada = $rodada " ;
				$rsPadrao=$conexao->query($sqlPadrao);
				$regPadrao = mysqli_fetch_array($rsPadrao); 

        		$palpitepadrao = $regPadrao['palpitepadrao']; 

				if ($palpitepadrao == 10) {
					$asterisco = "     (palpite padrão)";
				}?>
				
				<div class="rodada"><?PHP print $rodada."ª rodada".$asterisco;?></div> <?php
				$rodadaAnterior = $rodada;
			} ?>	

			<div class="jogo">                            

				<div class="jogo-item"><?php print $mandanteAbreviado; ?></div>

				<div class="jogo-item"><?php print $nomeMandante; ?></div>

				<div class="jogo-item">
					<img class="distintivo" src="imagens/times/<?php print $fotoMandante; ?>" /></div>

				<div class="jogo-item">
					<font color="black"><?php print $palpiteMandante; ?></font>
					<font size="1px"><sub style="font-size: 11px; color: red; "><?PHP print $placarMandante; ?></sub></font><font color="black" ><big> x </big></font>
					<font size="1px"><sub style="font-size: 11px; color: red;"><?PHP print $placarVisitante; ?></sub></font>
					<font color="black"><?php print $palpiteVisitante; ?></font></div>       

				<div class="jogo-item">
					<img class="distintivo" src="imagens/times/<?php print $fotoVisitante; ?>" /></div>

				<div class="jogo-item"><?php print $visitanteAbreviado; ?></div>

				<div class="jogo-item"><?php print $nomeVisitante; ?></div>

				<?php 
				if($naMosca) { ?>
					<div class="jogo-item" style="color: green; "><?php print $pontosRodada; ?></div>
				<?php
				} 
				else { ?>
					<div class="jogo-item" style="color: red; "><?php print $pontosRodada; ?></div>
				<?php
				} ?>
			</div> <?php 
		} ?>
		
	</div> <!-- /.box -->	

<?php
	// Libera os recursos usados pela conexão atual
	mysqli_close ($conexao);
?>