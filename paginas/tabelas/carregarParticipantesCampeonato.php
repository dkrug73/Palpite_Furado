<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $campeonatoId = $_SESSION['campeonatoId'];

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  
    
    $sql = "
        SELECT 
            participantes_campeonato.id,
            participantes.id AS participanteId,
            participantes.nome AS nome, 
            campeonatos.descricao AS campeonato,
            campeonatos.ano AS ano
        FROM 
            participantes_campeonato INNER JOIN
            participantes ON participantes_campeonato.participanteId = participantes.id INNER JOIN
            campeonatos ON participantes_campeonato.campeonatoId = campeonatos.id
        WHERE
            participantes_campeonato.campeonatoId = $campeonatoId
        ORDER BY 
            participantes.nome";
            
    $rs=$conexao->query($sql);
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-participante">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Participante</th>
            <th>Campeonato</th>
            <th>Ano</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td><?php echo $reg['id']; ?></td>
                <td id=<?php echo $reg['participanteId']; ?>><?php echo $reg['nome']; ?></td>
                <td><?php echo $reg['campeonato']; ?></td>
                <td><?php echo $reg['ano']; ?></td>
            </tr> <?php
        } 

        mysqli_close ($conexao);  ?>

    </tbody>
</table>
<br />