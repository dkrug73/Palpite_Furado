<?php
	SESSION_START();
	require_once('../../classes/Palpite.php');
	require_once('../../classes/Campeonato.php');
	require_once('../../classes/Rodada.php');
    require_once('../../classes/ConexaoBancoDeDados.php');
    
    $conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 
	
    $campeonato = new Campeonato($conexao);
	$campeonato->Id = $_POST["campeonatoId"];	

    $participanteId = "%";
    if ($_POST["participanteId"] > "0"){
        $participanteId = $_POST["participanteId"];
    }

	$rodada = $_POST['rodada'];

	$turno = $campeonato->RetornaTurnoDaRodada($rodada);	

	$sql = "
		SELECT 
			id, 
			mandanteId,
			nomeMandante,
			mandanteAbreviado,
			placarMandante, 
			visitanteId,
			nomeVisitante,
			visitanteAbreviado,
			placarVisitante,
			data
		FROM 
			jogos
		WHERE 
			rodada = '$rodada' AND
			turno = '$turno' AND
			campeonatoId = '$campeonato->Id'
		ORDER BY 
			serie,
			data";

	$rs=$conexao->query($sql);
	$total_registros = $rs->num_rows;
?>

<!-- tabela dos palpites -->

	<div class="container"> <?php

		$count = 0;
		while($reg=mysqli_fetch_array($rs))
		{						
			$jogoId = $reg['id'];
			$mandanteId = $reg['mandanteId'];
			$nomeMandante = $reg['nomeMandante'];
			$mandanteAbreviado = $reg['mandanteAbreviado'];
			$placarMandante = $reg['placarMandante'];
			$visitanteId = $reg['visitanteId'];
			$nomeVisitante = $reg['nomeVisitante'];	
			$visitanteAbreviado = $reg['visitanteAbreviado'];					
			$placarVisitante = $reg['placarVisitante'];
			$data = $reg['data'];
			$fotoMandante = $mandanteId.'.png';
			$fotoVisitante = $visitanteId.'.png';

			$palpite = new Palpite($conexao);
			$palpite->JogoId = $jogoId;
			
			if ($participanteId > 0){
				$palpite->ParticipanteId = $participanteId;

				$palpite->CarregarPalpites();
			}		

			$count = $count + 1;

			$campo = $palpite->ValidaRodadaJogo();	
			
			$dataJogo = date('d/m H:i', strtotime($data));			
								
			if ($placarMandante != "") $jogo = $placarMandante."<small style='color: #ff7070;'> x </small>".$placarVisitante;
			else $jogo = " x ";	
			?>

			<div class="jogo" style="margin: 0;">			
				<div class="jogo-item"></div>	
				<div class="jogo-item"></div>	
				<div class="jogo-item"></div>
				<div class="jogo-item"></div>
				<div class="jogo-item" style="min-width: 135px;">
					<div type="text" id="rodada-jogo" ><small style="color: #333;"><?php print $dataJogo; ?></small></div>   
				</div>	
				<div class="jogo-item"></div>
				<div class="jogo-item"></div>
				<div class="jogo-item"></div>
				<div class="jogo-item"></div>
				<div class="jogo-item"></div>	
			</div>
			
			<div class="jogo">			
				<div class="jogo-item"><?PHP print $mandanteAbreviado; ?></div>	

				<div class="jogo-item"><?PHP print $nomeMandante; ?></div>			
				
				<div class="jogo-item"><img class="distintivo" src="imagens/times/<?php print $fotoMandante; ?>"/></div>

				<div class="jogo-item">
					<input style="padding: 0 5px 0 0;" type="number" name="txtmandante<?php print $count; ?>" value="<?php print $palpite->PalpiteMandante; ?>" 
						class="form-control texto-palpite" maxlength="1" max=9 min="0" <?= (!$campo or $participanteId == "0") ? "disabled" : "" ?> >						
				</div> 
				
				<div class="jogo-item"><?php print $jogo; ?></div>
				
				<div class="jogo-item" >
					<input style="padding: 0 5px 0 0;" type="number" name="txtvisitante<?php print $count; ?>" value="<?php print $palpite->PalpiteVisitante; ?>"
					class="form-control texto-palpite"  maxlength="1" max=9 min="0" style="padding-right: 3px;" <?= (!$campo or $participanteId == "0") ? "disabled" : "" ?> >
				</div>			
								
				<div class="jogo-item" ><img class="distintivo" src="imagens/times/<?php print $fotoVisitante; ?>"/></div>

				<div class="jogo-item"><?PHP print $visitanteAbreviado; ?></div>	
				
				<div class="jogo-item"><?PHP print $nomeVisitante; ?></div>
			
				<div class="jogo-item" <?= ($palpite->NaMosca) ? "style='color:green;'" : "style='color:red;'" ?> ><?php print $palpite->PontosRodada;?></div>
				
			</div>
											
			<input type="hidden" name="txtjogo<?php print $count; ?>" value="<?php print $jogoId; ?>" />
			<input type="hidden" name="txtjogoData<?php print $count; ?>" value="<?php print $data; ?>" />	
			<input type="hidden" name="txtPlacarMandante<?php print $count; ?>" value="<?php print $placarMandante; ?>" />
			<input type="hidden" name="txtPlacarVisitante<?php print $count; ?>" value="<?php print $placarVisitante; ?>" />
			<hr>													
		<?PHP
		} ?>				
				
		<button type="submit" class="btn btn-danger btn-lg btn-block" <?= ($participanteId == "%" or $participanteId == "0") ? "disabled" : "" ?> >Gravar Palpites</button>
						
		<input type="hidden" name="rodada" value="<?php print $rodada; ?>" />
		<input type="hidden" name="total_registros" value="<?php print $total_registros; ?>" />
	</div>