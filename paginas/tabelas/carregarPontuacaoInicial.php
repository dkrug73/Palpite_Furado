<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  

    $campeonatoId = $_SESSION['campeonatoId'];
    
    $sql = "
        SELECT
            pontuacao_inicial.id,
            pontuacao_inicial.campeonatoId,
            pontuacao_inicial.participanteId,
            pontuacao_inicial.rodada,
            pontuacao_inicial.turno,
            pontuacao_inicial.pontos,
            participantes.nome
        FROM 
            pontuacao_inicial INNER JOIN
            participantes ON 
                pontuacao_inicial.participanteId = participantes.id 
        WHERE 
            campeonatoId = $campeonatoId ";
            
    $rs=$conexao->query($sql);
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-participante">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Participante</th>
            <th>Pontos</th>
            <th>Rodada</th>
            <th>Turno</th>    
            <th>Campeonato</th>								
                   
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td><?php echo $reg['id']; ?></td>
                <td id=<?php echo $reg['participanteId']; ?>><?php echo $reg['nome']; ?></td>
                <td><?php echo $reg['pontos']; ?></td>
                <td><?php echo $reg['rodada']; ?></td>
                <td><?php echo $reg['turno']; ?></td>         
                <td><?php echo $reg['campeonatoId']; ?></td>       
            </tr> <?php
        } 

        mysqli_close ($conexao);  ?>

    </tbody>
</table>
<br />