<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  
    
    $sql = "
        SELECT
            id, 
            nome, 
            cidade, 
            foto,
            nomeAbreviado,
            estadio
        FROM 
            times 
        ORDER BY
            nome ";
            
    $rs=$conexao->query($sql);
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-participante">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Abrev.</th>
            <th>Cidade</th>
            <th>Estádio</th>        
            <th>Foto</th>                     
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td><?php echo $reg['id']; ?></td>
                <td><?php echo $reg['nome']; ?></td>
                <td><?php echo $reg['nomeAbreviado']; ?></td>
                <td><?php echo $reg['cidade']; ?></td>
                <td><?php echo $reg['estadio']; ?></td>
                <td><img src = "<?php echo "imagens/times/".$reg['foto']; ?>" width = "25"></td>           
            </tr> <?php
        } 

        mysqli_close ($conexao);  ?>

    </tbody>
</table>
<br />