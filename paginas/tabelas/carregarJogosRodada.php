<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  

    $campeonatoId = $_SESSION['campeonatoId'];

    $sql = "
        SELECT 
            id, 
            campeonatoId,
            rodada, 
            turno,
            DATE_FORMAT(data,'%d/%m/%Y') AS dataJogo, 
            TIME_FORMAT(data, '%H:%i' ) AS horaJogo,
            mandanteId, 
            nomeMandante,
            visitanteId, 
            nomeVisitante,
            estadio 
        FROM 
            jogos 
        WHERE 
            campeonatoId = $campeonatoId AND
            placarMandante IS null 
        ORDER BY 
            rodada, 
            data ";
            
    $rs=$conexao->query($sql);
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-participante">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Rodada</th>
            <th>Turno</th>
            <th>Data</th>
            <th>Mandante</th>
            <th>Visitante</th>
            <th>Estádio</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){  
            $mandanteId = $reg['mandanteId'];
            $visitanteId = $reg['visitanteId']?>

            <tr style='cursor: pointer;'>
                <td id=<?php echo $reg['campeonatoId']; ?>><?php echo $reg['id']; ?></td>
                <td><?php echo $reg['rodada']; ?></td>
                <td><?php echo $reg['turno']; ?></td>
                <td><?php echo "{$reg['dataJogo']} {$reg['horaJogo']}"; ?></td>
                <td id=<?php echo $reg['mandanteId']; ?>><?php echo $reg['nomeMandante']; ?></td>
                <td id=<?php echo $reg['visitanteId']; ?>><?php echo $reg['nomeVisitante']; ?></td>
                <td><?php echo $reg['estadio']; ?></td>
            </tr> <?php
        } 

        mysqli_close ($conexao);  ?>

    </tbody>
</table>
<br />