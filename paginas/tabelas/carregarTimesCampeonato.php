<?php
    SESSION_START();
    require_once('../../classes/ConexaoBancoDeDados.php');

    $campeonatoId = $_SESSION['campeonatoId'];

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql();  
    
    $sql = "
        SELECT 
            times_campeonato.id,
            times.id AS timeId,
            times.nome AS time, 
            campeonatos.descricao AS campeonato,
            campeonatos.ano AS ano
        FROM 
            times_campeonato INNER JOIN
            campeonatos ON times_campeonato.campeonatoId = campeonatos.id INNER JOIN
            times ON times_campeonato.timeId = times.id
        WHERE
            times_campeonato.campeonatoId = $campeonatoId
        ORDER BY 
            nome";
            
    $rs=$conexao->query($sql);
?>

<table class="table table-responsive-xl table-striped table-hover table-sm" id="tabela-participante">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Time</th>
            <th>Campeonato</th>
            <th>Ano</th>
        </tr>
    </thead>

    <tbody> <?php
        while($reg=mysqli_fetch_array($rs)){ ?>
            <tr style='cursor: pointer;'>
                <td><?php echo $reg['id']; ?></td>
                <td id=<?php echo $reg['timeId']; ?>><?php echo $reg['time']; ?></td>
                <td><?php echo $reg['campeonato']; ?></td>
                <td><?php echo $reg['ano']; ?></td>
            </tr> <?php
        } 

        mysqli_close ($conexao);  ?>

    </tbody>
</table>
<br />