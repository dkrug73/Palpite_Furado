﻿<?PHP
	SESSION_START();
    require_once('../classes/ConexaoBancoDeDados.php');
	require_once('../classes/Participante.php');
	
	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 
    
	$participante = new Participante($conexao);  

	$participante->Id = $_POST['id'];

	$desativar = 1;
	if (isset($_POST['desativar'])) $desativar = 0;

	$participante->Desativar = $desativar;
	
	if (!$participante->DesativarCadastro()) echo 1;
	else echo 0;
?>

