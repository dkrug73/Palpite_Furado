﻿<?PHP
	SESSION_START();	
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	

	$acao = $_POST['acao'];	

	// Campos da tabela
    $id = null;
	if(isset($_POST['id'])){
		$id = $_POST['id'];
	}
    $descricao =  ucwords(strtolower($_POST['descricao'])); 
	$bonus =  $_POST['bonus']; 
	$empate = $_POST['empate'];
	$vencedor = $_POST['vencedor'];	
	$placar = $_POST['placar'];

	$mensagem="";
	$retorno=false;

    if ($acao == "inc") {  // INCLUSÃO/ALTERAÇÃO
		if ($id != null) { // se não for vazio, é uma alteração
			$mensagem = "Alteração da pontuação efetuada com sucesso.";
			$tipoAviso = "sucesso";
		
			$sql = "UPDATE pontuacao SET ";
			$sql = $sql . "descricao = '$descricao', ";
			$sql = $sql . "bonus = '$bonus', ";
			$sql = $sql . "empate = '$empate', ";
			$sql = $sql . "vencedor = '$vencedor', ";
			$sql = $sql . "placar = '$placar' ";
			$sql = $sql . " WHERE id = '" . $id . "' ";	
			
			$retorno=$conexao->query($sql);			
		}
		else {
			$mensagem = "Pontuação incluída com sucesso.";
			$tipoAviso = "sucesso";
		
			$sql="INSERT INTO pontuacao ";
			$sql=$sql."(descricao, bonus, empate, vencedor, placar) VALUES ('$descricao', '$bonus', '$empate', '$vencedor', '$placar') ";
			$retorno=$conexao->query($sql);
			$mensagem = $sql;
			$id = $conexao->insert_id;
		}

		if ($retorno) {
			$tipoAviso = "sucesso";	
			$mensagem="Pontuação incluída/alterada com sucesso.";	
		}
		else {
			$mensagem="Erro ao cadastrar/alterar a Pontuação.";
			$tipoAviso = "erro";
		}	
	}
	else if ($acao == "exc") { // EXCLUSÃO	
		if(PodeExcluirPontuacao($conexao, $id)) {
			$sql = "DELETE FROM pontuacao WHERE id = '" . $id . "' ";	
			$mensagem="Pontuação excluída com sucesso.";
			$retorno=$conexao->query($sql);   

			if ($retorno) {
				$tipoAviso = "sucesso";	
				$mensagem="Pontuação excluída com sucesso.";	
			}
			else {
				$mensagem="Erro ao excluir a Pontuação.";
				$tipoAviso = "erro";
			}	 
		}
		else {
			$mensagem="A pontuação está vinculada a um ou mais campeonatos.";
			$retorno=false;  
			$tipoAviso = "alerta";
		}		    
	} 

	print "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../cadastrarPontuacao.php?msg=$mensagem&tipoAviso=$tipoAviso '>";
	
	mysqli_close ($conexao);
?>