<?php
	SESSION_START();

	 $pagina = $_SESSION['pagina'];

	unset(
		$_SESSION['participanteId'],
		$_SESSION['participanteNome'],
		$_SESSION['email'],
		$_SESSION['senha'],
		$_SESSION['padraoMandante'],
		$_SESSION['padraoVisitante'],
		$_SESSION['receberemail'],
		$_SESSION['foto'],
		$_SESSION['nivel'],
        $_SESSION['rodada_selecionada'],
        $_SESSION['campeonatoId'],
		
		$_SESSION['acao'], 
		$_SESSION['logado']
	);

	//$cookie_name = "CookieLembrete";
	//$cookie_value = '';
	//setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias
	
	//setcookie('CookieLembrete');
	//setcookie('CookieEmail');
	//setcookie('CookieSenha'); 	

	unset($_COOKIE['CookieLembrete']);
	unset($_COOKIE['CookieEmail']);
	unset($_COOKIE['CookieSenha']);
	setcookie('CookieLembrete', null, -1, '/');
	setcookie('CookieEmail', null, -1, '/');
	setcookie('CookieSenha', null, -1, '/');
	
	$_SESSION['clicouSair'] = TRUE;
	
	echo "<script>window.location = '$pagina'</script>";

?>

