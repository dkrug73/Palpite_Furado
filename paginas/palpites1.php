<?php
    SESSION_START();
    require_once('../classes/Participante.php');
    require_once('../classes/Rodada.php');
    require_once('../classes/Jogo.php');
    require_once('../classes/Campeonato.php');
    require_once('../classes/Palpite.php');
    require_once('../classes/ConexaoBancoDeDados.php');
    require_once('../utils/funcoes.php');

    $conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

    $mensagem = "Palpites gravados com êxito!";
	$tipoAviso = "sucesso";
    $resultado = true;    
	
    $participanteId = $_SESSION['participanteId'];
	$campeonatoId = $_SESSION['campeonatoId'];
    $rodada = $_POST['rodada'];
    $totalRegistros = $_POST['total_registros'];

    $participante = new Participante($conexao);
    $participante->Id = $participanteId;
    
    $turno = $participante->RetornaTurnoDaRodada($campeonatoId, $rodada);

    // verifica se o participante está participando do campeonato.
    // se não estiver, irá adicionar
    $participante->ValidarParticipante($campeonatoId, $rodada);
			
    for ($i = 1; $i <= $totalRegistros; $i++) {
        $jogoNome = "txtjogo".$i;
        $jogoId = $_POST[$jogoNome];

        $mandanteNome = "txtmandante".$i;
        $visitanteNome = "txtvisitante".$i;
        $mandantePlacar = "txtPlacarMandante".$i;
        $visitantePlacar = "txtPlacarVisitante".$i;

        if (isset($_POST[$mandanteNome]) and isset($_POST[$visitanteNome])){
            $palpiteMandante = $_POST[$mandanteNome];
            $palpiteVisitante = $_POST[$visitanteNome];
            $placarMandante = $_POST[$mandantePlacar];
            $placarVisitante = $_POST[$visitantePlacar];

            if($palpiteMandante == "" or $palpiteVisitante == "") continue;
            if($placarMandante <> "" or $placarVisitante <> "") continue;

            $palpite = new Palpite($conexao);                         

            $palpite->JogoId = $jogoId;
            $palpite->ParticipanteId = $participanteId;
            $palpite->PalpiteMandante = $palpiteMandante;
            $palpite->PalpiteVisitante = $palpiteVisitante;
            $palpite->Rodada = $rodada;
            $palpite->Turno = $turno;
            $palpite->CampeonatoId = $campeonatoId;
            $palpite->Padrao = '0';            

            if (!$palpite->GravarPalpite($palpite)) {
                echo false;
                return false;
            }

            unset($palpite);
        }
    }
   
    $palpitePadrao = new Palpite($conexao);
    $palpitePadrao->ParticipanteId = $participanteId;
    $palpitePadrao->CampeonatoId = $campeonatoId;
    $palpitePadrao->Rodada = $rodada;
    $palpitePadrao->GravarPadraoParaPalpitesIguais();  
    
    $palpitePadrao->FecharConexao();

    echo true;
?>