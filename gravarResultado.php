<?php 
	SESSION_START();
	require_once('classes/ConexaoBancoDeDados.php');
	require_once('classes/Campeonato.php');

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$erro = '0';
	if (isset($_GET['erro'])){
		$erro = $_GET['erro'];
	}	

	$campeonato = new Campeonato($conexao);
	
	$_SESSION['campeonatoId'] = $campeonato->RetornaCampeonatoAtivo();
	
	$participanteId = "0";
	if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}

	$campeonato->Id = $_SESSION['campeonatoId'];	

    $rodadaAtual = $campeonato->RetornaRodadaAtual();
    $dataHora = $campeonato->RetornaDataHoraInicioRodada($rodadaAtual);    
	$rodada = $rodadaAtual;
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Gravar resultado</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<meta name="author" content="Daniel Krug">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<link rel="stylesheet" href="bootstrap/css/css/palpites.css">	
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<link rel="icon" type="image/png" href="imagens/favicon.png">

    <script type="text/JavaScript">
        window.onload = function(){
            getValor("<?php print $rodada ?>", "<?php print $campeonato->Id ?>");		
        }

		$(document).ready(function(){
			$('#formulario-jogos').submit(function(){    				
				$.ajax({
					type: 'post',
					url: 'paginas/gravarResultado1.php',
					data: $('#formulario-jogos').serialize(),
					success: function(data){
						$('html, body').animate({scrollTop:0}, 'slow');
						
						var classe = '#salvar-erro';
						if (data == 0){
							classe = '#salvar-sucesso';
						}

						$(classe).show(); 

						setTimeout(function() {$(classe).fadeOut('slow');}, 5000);					
					}
				});

				return false;
				});
			});
        
		function getValor(valor, id_campeonato){	
			$("#recebeValor").load(
				"paginas/tabelas/carregarGravarResultados.php",{
					txtRodada: valor,
					campeonatoId: id_campeonato
				}
			)		
		}

		function ApagarResultado(jogoId){			
			$.ajax({
				type: 'POST',
				url: 'paginas/apagarResultado.php',
				data: { "jogoId" : jogoId },
				success: function(){
					getValor("<?php print $rodada ?>", "<?php print $campeonato->Id ?>");
					
				}
			});
		}
    </script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- MENU -->
		<?php include("componentes/menu.php"); ?>	

		<div class="content-wrapper">    
			<section class="content">

				<div id="salvar-sucesso" style="display:none;">
					<div class="alert alert-success text-center" role="alert" id="alerta">
						Sucesso ao gravar resultados!
					</div>           
				</div>

				<div id="salvar-erro" style="display:none;">
					<div class="alert alert-danger text-center" role="alert" id="alerta">
						Erro ao gravar resultados.
					</div> 
				</div>

				<div class="container mt-4 mb-5" style="font-size: 1em;">
					<div class="form-group" style="text-align: -moz-center; text-align: -webkit-center;">
						<?php include("utils/selectRodada.php") ?>
					</div>

					<form id="formulario-jogos" name="formulario-jogos">
						<div name="recebeValor" id="recebeValor"></div>	
					</form>							
				</div>
			</section>  
        </div>

		<!-- RODAPE -->		
		<?php include("componentes/rodape.php"); ?>
	</div>
	
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>