<?php

class Participante{
      public $Id;
      public $Nome;
      public $Email;
      public $Senha;
      public $ReceberEmail;
      public $PadraoMandante;
      public $PadraoVisitante;
      public $Foto;
      public $Nivel;
      public $AceitouTermos;

      public function GravarParticipante($participante, $conexao){       
            $sql = "
                  INSERT INTO participantes (
                        nome,
                        email,
                        senha,
                        receberemail,
                        padraoMandante,
                        padraoVisitante,
                        aceitouTermos) 
                  VALUES (
                        '$participante->Nome', 
                        '$participante->Email', 
                        '$participante->Senha', 
                        '$participante->ReceberEmail', 
                        $participante->PadraoMandante, 
                        $participante->PadraoVisitante,
                        '1') ";
          
            $conexao->query($sql);
            $participante->Id = $conexao->insert_id;  

            return $participante->Id;		
      }

      public function ExcluirParticipante($participante, $conexao){

            $sql = "
                  DELETE FROM 
                        participantes 
                  WHERE 
                        id = '" . $participante->Id . "' ";	
		
            $resultado = $conexao->query($sql);	

            if ($resultado){
                  if ($participante->Foto != "sem-imagem.png" AND $participante->Foto != null) {
                        $caminhoFoto="../imagens/fotos/".$participante->Foto;
                        
                        $participante->ExcluirArquivoFoto($conexao, $caminhoFoto);         
                  }                
			
			$sql	= "DELETE FROM palpites WHERE participanteId = '".$participante->Id."' ";
			$conexao->query($sql);
			
			$sql	= "DELETE FROM participantes_campeonato WHERE participanteId = '".$participante->Id."' ";
			$conexao->query($sql);
			
			$sql	= "DELETE FROM pontuacao_inicial WHERE participanteId = '".$participante->Id."' ";
                  $conexao->query($sql);
                  
                  $participante->LimparVariaveisDeSessao();

                  return true;
            }
      }     

      public function AlterarParticipante($participante, $conexao){
            $sql = "
                  UPDATE 
                        participantes 
                  SET 
                        nome = '$participante->Nome', 
                        senha = '$participante->Senha', 
                        padraoMandante = '$participante->PadraoMandante',
                        padraoVisitante = '$participante->PadraoVisitante',
                        receberemail = '$participante->ReceberEmail'
                  WHERE 
                        id = '".$participante->Id."' "; 

            return $conexao->query($sql);
      }

      public function ValidarParticipante($conexao, $participanteId, $campeonatoId, $rodadaPalpite){
            $sql = "
                  SELECT 
                        id 
                  FROM 
                        participantes_campeonato  
                  WHERE 
                        campeonatoId = '".$campeonatoId."' AND 
                        participanteId = '".$participanteId."' ";

            $rs=$conexao->query($sql);
            $reg = mysqli_fetch_array($rs);

            if ($reg['id'] == "" && $participanteId != "") {
			$sql_insere = "INSERT INTO participantes_campeonato (participanteId, campeonatoId) VALUES ('$participanteId','$campeonatoId') ";	
                  $conexao->query($sql_insere);

                  $participante = new Participante();
                  $dadosParticipante = $participante->RetornaParticipante($conexao, $participanteId);
                  $turno = RetornaTurnoBaseadoRodada($conexao, $campeonatoId, $rodadaPalpite);
                  
                  $rodada = new Rodada();
                  $rodada->ParticipanteId = $dadosParticipante->Id;
                  $rodada->NomeParticipante = $dadosParticipante->Nome;
                  $rodada->FotoParticipante = $dadosParticipante->Foto;
                  $rodada->CampeonatoId = $campeonatoId;
                  $rodada->Turno = $turno;
                  $rodada->Rodada = $rodadaPalpite;
                  $rodada->PontosRodada = '0';
                  $rodada->NaMosca = '0';
                  $rodada->TotalPontos = '0';
                  $rodada->TotalNaMosca = '0';
                  $rodada->PosicaoRodada = '0';

                  $rodada->id = $rodada->IncluirRodada($conexao, $rodada);	
            }
      }

      public function RetornaParticipante($conexao, $participanteId){
            $participante = new Participante();

            $sql = "
                  SELECT 
                        * 
                  FROM 
                        participantes 
                  WHERE 
                        id = '".$participanteId."' ";
                  
                  $rs =$conexao->query($sql);
                  $reg=mysqli_fetch_array($rs);

                  $participante->Id = $reg['id'];
                  $participante->Nome = $reg['nome'];
                  $participante->Email = $reg['email'];
                  $participante->Senha = $reg['senha'];
                  $participante->ReceberEmail = $reg['receberemail'];
                  $participante->PadraoMandante = $reg['padraoMandante'];
                  $participante->PadraoVisitante = $reg['padraoVisitante'];
                  $participante->Foto = $reg['foto'];

                  return $participante;
      }

      public function RetornaPalpitePadrao($conexao, $participante){
            $sql = "SELECT 
                              padraoMandante,
                              padraoVisitante 
                        FROM 
                              participantes 
                        WHERE 
                              id = '".$participante->Id."' ";
            
            $rs=$conexao->query($sql);
            $reg = mysqli_fetch_array($rs); 
            
            $participante->PadraoMandante = $reg['padraoMandante']; 
            $participante->PadraoVisitante = $reg['padraoVisitante'];	
      
            return ($participante);
      }

      public function CarregarDadosParticipante($conexao, $participante) {
		$sql="
                  SELECT 
                        * 
                  FROM 
                        participantes 
                  WHERE 
                        id='" . $participante->Id . "' ";	

		$rs=$conexao->query($sql);
		$reg=mysqli_fetch_array($rs);

		$participante->Nome = $reg['nome'];
		$participante->Email = $reg['email'];
            $participante->Senha = $reg['senha'];
            $participante->Foto = $reg['foto'];
		$participante->ReceberEmail = $reg['receberemail'];
		$participante->PadraoMandante = $reg['padraoMandante'];
		$participante->PadraoVisitante = $reg['padraoVisitante'];
		$participante->AceitouTermos = $reg['aceitouTermos'];
            
            return $participante;
      }

      public function ValidarEmailCadastro($conexao, $email) {
            $sql = "
                  SELECT 
				email 
			FROM 
				participantes 
			WHERE 
				email = '".$email."' ";

            $rs=$conexao->query($sql);

            $totalRegistros = $rs->num_rows;

            return $totalRegistros == 0;
      }

      public function ExcluirArquivoFoto($conexao, $caminhoFoto) {
		//$caminhoFoto="../imagens/fotos/".$foto;		
		
            //if($fotoParticipante != ""){
                  unlink($caminhoFoto);
           // }
      }

      public function RetornaFoto($conexao, $id){
            $sql="
                  SELECT 
                        foto 
                  FROM 
                        participantes 
                  WHERE 
                        id='" . $id . "' ";	

		$rs=$conexao->query($sql);
		$reg=mysqli_fetch_array($rs);
            
            return $reg['foto'];
      }

      public function LimparVariaveisDeSessao(){
           unset(	
                  $_SESSION['participanteId'],
                  $_SESSION['participanteNome'],
                  $_SESSION['foto'],
                  $_SESSION['nivel']
            ); 
      }

      public function AtualizaVariaveisSessao($conexao, $participante) {
            
            $participante->LimparVariaveisDeSessao();

            $participante->Foto = $participante->RetornaFoto($conexao, $participante->Id);
      
            $_SESSION['participanteId']=$participante->Id;
            $_SESSION['participanteNome']=$participante->Nome;
            $_SESSION['foto']=$participante->Foto;
            $_SESSION['nivel']=$participante->Nivel;
      }

      public function GravarSemImagem($conexao, $id){
            $sql="
                  UPDATE 
                        participantes 
                  SET 
                        foto = 'sem_imagem.png' 
                  WHERE 
                        id = '".$id."' ";

            return $conexao->query($sql);
      }
}

class Rodada {
      public $Id;
      public $ParticipanteId;
      public $NomeParticipante;
      public $FotoParticipante;
      public $CampeonatoId;
      public $Turno;
      public $Rodada;
      public $PontosRodada;
      public $NaMosca;
      public $TotalPontos;
      public $TotalNaMosca;
      public $PosicaoRodada;
      public $PosicaoRodadaAnterior;

      public function IncluirRodada($conexao, $rodada){
            $sql = "
                  INSERT INTO rodada (
                        participanteId, 
                        nomeParticipante, 
                        fotoParticipante, 
                        campeonatoId, 
                        turno,
                        rodada, 
                        pontosRodada, 
                        naMosca, 
                        totalPontos, 
                        totalNaMosca, 
                        posicaoRodada)
                  VALUES (
                        '$rodada->ParticipanteId',
                        '$rodada->NomeParticipante',
                        '$rodada->FotoParticipante',
                        '$rodada->CampeonatoId',
                        '$rodada->Turno',
                        '$rodada->Rodada',
                        '$rodada->PontosRodada',
                        '$rodada->NaMosca', 
                        '$rodada->TotalPontos',
                        '$rodada->TotalNaMosca', 
                        '$rodada->PosicaoRodada') ";

            $conexao->query($sql);

            return $conexao->insert_id; 
      } 

      public function GravarPontuacaoRodada($conexao, $objRodada, $tabela) {            
            $sql = "
                  SELECT 
                        id 
                  FROM 
                        ".$tabela." 
                  WHERE 
                        participanteId = '".$objRodada->ParticipanteId."' AND 
                        rodada = '".$objRodada->Rodada."' AND 
                        turno = '".$objRodada->Turno."' AND 
                        campeonatoId = '".$objRodada->CampeonatoId."' ";               
            
            $rs=$conexao->query($sql);
            $reg=mysqli_fetch_array($rs);
            
            $rodadaId = $reg['id'];

            if (empty($rodadaId)) {
                  return $objRodada->InserirRodada($conexao, $objRodada, $tabela);
            }
            else {
                  $objRodada->Id = $rodadaId;
                  return $objRodada->AlterarRodada($conexao, $objRodada, $tabela);
            }
      }
      
      private function InserirRodada($conexao, $objRodada, $tabela){
            $sql = "
                  INSERT INTO ".$tabela." (
                        participanteid,
                        nomeparticipante,
                        fotoparticipante,
                        campeonatoid,
                        turno,
                        rodada,
                        pontosrodada,
                        namosca,
                        posicaorodada,
                        posicaorodadaanterior,
                        totalpontos,
                        totalnamosca)
                  VALUES (
                        '$objRodada->ParticipanteId',
                        '$objRodada->NomeParticipante',
                        '$objRodada->FotoParticipante',
                        '$objRodada->CampeonatoId',
                        '$objRodada->Turno',
                        '$objRodada->Rodada',
                        '$objRodada->PontosRodada',
                        '$objRodada->NaMosca',
                        '$objRodada->PosicaoRodada',
                        '$objRodada->PosicaoRodadaAnterior',
                        '$objRodada->TotalPontos',
                        '$objRodada->TotalNaMosca') "; 

            return $conexao->query($sql);	  
      }

      private function AlterarRodada($conexao, $objRodada, $tabela){
            $sql = "
                  UPDATE ".$tabela." SET
                        turno = '$objRodada->Turno',
                        rodada = '$objRodada->Rodada',
                        pontosRodada = '$objRodada->PontosRodada',
                        naMosca = '$objRodada->NaMosca',
                        posicaoRodada = '$objRodada->PosicaoRodada',
                        posicaoRodadaAnterior = '$objRodada->PosicaoRodadaAnterior',
                        totalPontos = '$objRodada->TotalPontos',
                        totalNaMosca = '$objRodada->TotalNaMosca' 								
                  WHERE 
                        id = '".$objRodada->Id."' "; 
                       
            return $conexao->query($sql);	                
      }

      public function GravaPontuacaoPosicaoRodadaParticipante($conexao, $objRodada) { 

            $maximoPalpitesPontos = $objRodada->RetornaRodadaMaximoPontos($conexao, $objRodada->CampeonatoId);
             
            for ($contadorRodada = $objRodada->Rodada; $contadorRodada <= $maximoPalpitesPontos; $contadorRodada++) {   

                  $resultado = $objRodada->AtualizarTabelaRodada($conexao, $objRodada, $contadorRodada);     
                  
                  if (!$resultado) {
                        return false; 
                  } 
                  
                  $resultado = $objRodada->AtualizarTabelaRodadaGeral($conexao, $objRodada, $contadorRodada);              
                  
                  if (!$resultado) {
                        return false; 
                  } 
            }	

            return true;
      }
      
      private function AtualizarTabelaRodada($conexao, $objRodada, $rodada) {
            $posicaoRodada = 0;			
            $rodadaInicial = 1;

            $sql = "
                  SELECT
                        participanteId,
                        participantes.nome as nomeParticipante,
                        participantes.foto,
                        SUM(pontosRodada) AS pontos, 
                        SUM(naMosca) AS naMosca,
                        rodada,
                        turno,
                        palpites.padrao,
                        (
                              SELECT 
                                    SUM(pp.pontosRodada) 
                              FROM 
                                    palpites pp 
                              WHERE 
                                    pp.participanteId = palpites.participanteId AND 
                                    pp.campeonatoId = palpites.campeonatoId AND 
                                    pp.turno = palpites.turno AND 
                                    pp.rodada <= palpites.rodada
                        ) as totalPontos,
                        (
                              SELECT 
                                    SUM(pp.naMosca) 
                              FROM 
                                    palpites pp 
                              WHERE 
                                    pp.participanteId = palpites.participanteId AND 
                                    pp.campeonatoId = palpites.campeonatoId AND 
                                    pp.turno = palpites.turno AND 
                                    pp.rodada <= palpites.rodada
                        ) as totalNaMosca						
                  FROM 
                        palpites INNER JOIN 
                        participantes ON participanteId = participantes.id
                  WHERE 
                        campeonatoId = '".$objRodada->CampeonatoId."' AND 
                        rodada = '".$rodada."' AND
                        participanteId IN (SELECT id FROM participantes WHERE desativar is null OR desativar = 0)
                  GROUP BY 
                        participanteId 
                  ORDER BY 
                        rodada DESC,
                        totalPontos DESC,
                        totalNaMosca DESC,
                        nomeParticipante ASC ";
                                         
            $rs=$conexao->query($sql);	
      
            while ($reg=mysqli_fetch_array($rs)) {
                  $posicaoRodada += 1;
                  $rodadaParticipante = new Rodada();

                  $padrao =  $reg['padrao'];

                  $rodadaParticipante->ParticipanteId = $reg['participanteId'];
                  $rodadaParticipante->NomeParticipante = $reg['nomeParticipante'];
                  $rodadaParticipante->FotoParticipante = $reg['foto'];
                  $rodadaParticipante->Turno = $reg['turno'];
                  
                  $rodadaParticipante->PontosRodada = 0;
                  $pontos =  $reg['pontos'];

                  if (!empty($pontos)) {

                        if ($padrao AND $pontos % 5 != 0) {
                              $pontos = floor($pontos / 5);
                              $pontos = $pontos * 5;
                        }

                        $rodadaParticipante->PontosRodada = $pontos;
                  }
                 
                  $rodadaParticipante->NaMosca = 0;
                  if (!empty($reg['naMosca'])) {
                        $rodadaParticipante->NaMosca = $reg['naMosca'];
                  }

                  $rodadaParticipante->CampeonatoId = $objRodada->CampeonatoId;
                  $rodadaParticipante->Rodada = $rodada;
                  $rodadaParticipante->PosicaoRodada = $posicaoRodada;    

                  $tabela = "rodada";

                  $rodadaParticipante = $rodadaParticipante->AtualizarDadosRodada($conexao, $rodadaParticipante, $tabela);

                  $resultado = $rodadaParticipante->GravarPontuacaoRodada($conexao, $rodadaParticipante, $tabela);

                  if (!$resultado) {
                        return false; 
                  }                 
                        
                  unset($rodadaParticipante);
            }	

            return true;
      }      

      private function AtualizarTabelaRodadaGeral($conexao, $objRodada, $rodada) {
            $rodadaInicial = 1;

            $sql = "
                  SELECT
                        participanteId,
                        nomeParticipante,
                        fotoParticipante,
                        turno,
                        naMosca,
                        pontosRodada,
                        naMosca,
                        totalPontos,
                        totalNaMosca						
                  FROM 
                        rodada
                  WHERE 
                        campeonatoId = '".$objRodada->CampeonatoId."' AND 
                        rodada = '".$rodada."' AND
                        participanteId IN (SELECT id FROM participantes WHERE desativar is null OR desativar = 0)
                  ORDER BY 
                        pontosRodada desc, 
                        naMosca desc, 
                        totalNaMosca,
                        totalPontos,
                        nomeParticipante asc";

            $rs=$conexao->query($sql);	
            
            $somarPosicao = true;
            $posicaoRodada = 1;
            $countPosicao = 0;
            $posicao = 0;
            $pontosAnterior = 0;
            $naMoscaAnterior = 0;
            while ($reg=mysqli_fetch_array($rs)) {
                  $rodadaParticipante = new Rodada();

                  $rodadaParticipante->ParticipanteId = $reg['participanteId'];
                  $rodadaParticipante->NomeParticipante = $reg['nomeParticipante'];
                  $rodadaParticipante->FotoParticipante = $reg['fotoParticipante'];
                  $rodadaParticipante->Turno = $reg['turno'];   
                  
                  $rodadaParticipante->CampeonatoId = $objRodada->CampeonatoId;  
                  $rodadaParticipante->Rodada = $rodada;  
                  $rodadaParticipante->NaMosca = $reg['naMosca'];  
                  $rodadaParticipante->PontosRodada = $reg['pontosRodada']; 
                  $rodadaParticipante->TotalNaMosca = $reg['totalNaMosca'];; 

                  if ($rodadaParticipante->PontosRodada == $pontosAnterior AND $rodadaParticipante->NaMosca == $naMoscaAnterior){
                        $countPosicao++;
                        $somarPosicao = false;
                  }
                  else{
                        $somarPosicao = true;
                        $posicao++;                        
                  }  

                  if ($somarPosicao){
                        $posicao = $posicao + $countPosicao;
                        $countPosicao = 0;
                  }
                  
                  $rodadaParticipante->PosicaoRodada = $posicaoRodada;
                  $posicaoRodada++;

                  $pontos = 0;
                  switch ($posicao) {                  
                        case 1:
                              $pontos = 10;
                            break;
      
                        case 2:
                              $pontos = 8;
                            break;
      
                        case 3:
                              $pontos = 6;                       
                              break;
      
                        case 4:
                              $pontos = 5;
                              break;
      
                        case 5:
                              $pontos = 4;
                              break;
      
                        case 6:
                              $pontos = 2;
                              break;
      
                        case 7:
                              $pontos = 1;
                              break;
                  }                  
  
                  $pontosAnterior = $reg['pontosRodada']; 
                  $naMoscaAnterior = $reg['naMosca'];
      
                  $rodadaParticipante->PontosRodada = $pontos;
      
                  $dadosRodadaAnterior = $rodadaParticipante::RetornaPontuacaoRodadaAnterior($conexao, $rodadaParticipante, "rodadaGeral");
                  $pontosRodadaAnterior = $dadosRodadaAnterior['0'];	
                  $naMoscaRodadaAnterior = $dadosRodadaAnterior['1'];				
                  $posicaoRodadaAnterior = $dadosRodadaAnterior['2'];                 
      
                  if(empty($posicaoRodadaAnterior)) {
                        $posicaoRodadaAnterior = $rodadaParticipante->PosicaoRodada;
                  }
      
                  $rodadaParticipante->TotalPontos = $pontosRodadaAnterior + $pontos;
                  $rodadaParticipante->PosicaoRodadaAnterior = $posicaoRodadaAnterior;
      
                  $resultado = $rodadaParticipante->GravarPontuacaoRodada($conexao, $rodadaParticipante, "rodadaGeral");

                  if(!$resultado){
                        return false;
                  } 
                  
                  unset($rodadaParticipante);
            }

            if(!$objRodada->AtualizarPosicoesTabelaGeral($conexao, $rodada)) {
                  return false;
            }

            return true;
      }

      private function RetornaRodadaMaximoPontos($conexao, $campeonatoId){
            $sql = "SELECT 
                              MAX(rodada) AS rodada 
                        FROM 
                              palpites 
                        WHERE 
                              campeonatoId = '".$campeonatoId."' AND 
                              (SELECT turno FROM jogos WHERE id = jogoId ) = turno AND 
                              pontosRodada > 0 
                        ORDER BY 
                              rodada DESC limit 1";	
            
            $rs =$conexao->query($sql);
            $reg=mysqli_fetch_array($rs);

            if (empty($reg['rodada'])){
                  return 1;
            }
            else{
                  return $reg['rodada'];
            } 
      }

      private function ApagarRodada($conexao, $campeonatoId, $rodada, $tabela){
            $sql = "
                  DELETE FROM 
                        ".$tabela." 
                  WHERE 
                        campeonatoId = '".$campeonatoId."' AND 
                        rodada = '".$rodada."' ";

            $conexao->query($sql);
      }     

      private function AtualizarPosicoesTabelaGeral($conexao, $rodada) {
           $sql = "
                  SELECT
                        id
                  FROM
                        rodadaGeral
                  WHERE
                        rodada = '".$rodada."'
                  ORDER BY
                        rodada, 
                        totalPontos DESC, 
                        totalNaMosca DESC ";     

            $rs=$conexao->query($sql);

            $count = 0;
            while ($reg=mysqli_fetch_array($rs)) {
                  $count += 1;                  
                  $id = $reg['id'];	

                  $sqlRodada = "UPDATE rodadaGeral SET posicaoRodada = '".$count."' WHERE id = '".$id."' ";

                  if (!$conexao->query($sqlRodada)) {
                        return false;
                  }  
            }

            return true;
      }

      private function AtualizarDadosRodada($conexao, $objRodada, $tabela) {
            $dadosRodadaAnterior = $objRodada::RetornaPontuacaoRodadaAnterior($conexao, $objRodada, $tabela);
            $pontosRodadaAnterior = $dadosRodadaAnterior['0'];	
            $naMoscaRodadaAnterior = $dadosRodadaAnterior['1'];				
            $posicaoRodadaAnterior = $dadosRodadaAnterior['2'];	
            
            $pontuacaoInicial = $objRodada::RetornaPontuacaoInicial($conexao, $objRodada);

            $objRodada->TotalPontos = $pontosRodadaAnterior + $objRodada->PontosRodada + $pontuacaoInicial;
            $objRodada->TotalNaMosca = $naMoscaRodadaAnterior + $objRodada->NaMosca; 
            $objRodada->PosicaoRodadaAnterior = $posicaoRodadaAnterior;   
            
            return $objRodada;
      }

      private function RetornaPontuacaoRodadaAnterior($conexao, $objRodada, $tabela){
            $pontosRodadaAnterior = 0;	
            $naMoscaRodadaAnterior = 0;				
            $posicaoRodadaAnterior = 0;

            $rodadaAnterior = $objRodada->Rodada - 1;	

            if ($rodadaAnterior == "0" OR $rodadaAnterior == "19") {
                  return array($objRodada->TotalPontos, $objRodada->TotalNaMosca, $objRodada->PosicaoRodada);
            }     

            $sql = "
                  SELECT 
                        totalPontos AS pontos, 
                        totalNaMosca AS naMosca,
                        posicaoRodada
                  FROM 
                        ".$tabela." 
                  WHERE 
                        rodada = '".$rodadaAnterior."' AND 
                        campeonatoId = '".$objRodada->CampeonatoId."' AND 
                        participanteId = '".$objRodada->ParticipanteId."' AND 
                        turno = '".$objRodada->Turno."' ";                         

            $rs=$conexao->query($sql);		
            $reg = mysqli_fetch_array($rs);

            $pontosRodadaAnterior = $reg['pontos'];	
            $naMoscaRodadaAnterior = $reg['naMosca'];				
            $posicaoRodadaAnterior = $reg['posicaoRodada'];

            return array($pontosRodadaAnterior, $naMoscaRodadaAnterior, $posicaoRodadaAnterior);
      }

      private function RetornaPontuacaoInicial($conexao, $rodadaParticipante){
            $sql = "
                  SELECT 
                        pontos 
                  FROM 
                        pontuacao_inicial 
                  WHERE 
                        participanteId = '".$rodadaParticipante->ParticipanteId."' AND 
                        campeonatoId = '".$rodadaParticipante->CampeonatoId."' AND 
                        turno like '".$rodadaParticipante->Turno."' AND 
                        rodada like '".$rodadaParticipante->Rodada."' ";
            
            $rs =$conexao->query($sql);
      
            $pontuacaoInicial = 0;
            while ($reg=mysqli_fetch_array($rs)) {
                  $pontuacaoInicial = $pontuacaoInicial + $reg['pontos'];
            }
            
            return $pontuacaoInicial;
      }
}

class Jogo {
      public $Id;
      public $campeonatoId;
      public $turno;
      public $rodada;
      public $data;
      public $mandanteId;
      public $visitanteId;
      public $estadio;
      public $nomeMandante;
      public $nomeVisitante;
      public $mandanteAbreviado;
      public $visitanteAbreviado;

      public function AtualizarJogo($conexao, $jogo){
            $sql = "
                  UPDATE 
                        jogos 
                  SET 
                        placarMandante = '$jogo->PlacarMandante', 
                        placarVisitante = '$jogo->PlacarVisitante' 
                  WHERE 
                        id = '" . $jogo->Id . "' ";					
                
            return $conexao->query($sql);
      }
      
      public function RetornaDadosJogo($conexao, $jogoId){
            $sqlJogo = "
                  SELECT 
                        nomeMandante,
                        nomeVisitante,
                        data,
                        mandanteId,
                        visitanteId,
                        estadio,
                        mandanteAbreviado,
                        visitanteAbreviado
                  FROM 
                        jogos
                  WHERE 
                        id = '".$jogoId."' ";					

            $rsJogo=$conexao->query($sqlJogo);
            $regJogo=mysqli_fetch_array($rsJogo);

            $jogo = new Jogo();

            $jogo->NomeMandante = $regJogo['nomeMandante'];
            $jogo->NomeVisitante = $regJogo['nomeVisitante'];
            $jogo->MandanteId = $regJogo['mandanteId'];
            $jogo->VisitanteId = $regJogo['visitanteId'];
            $jogo->Estadio = $regJogo['estadio'];
            $jogo->Data = $regJogo['data'];
            $jogo->MandanteAbreviado = $regJogo['mandanteAbreviado'];
            $jogo->VisitanteAbreviado = $regJogo['visitanteAbreviado'];

            return $jogo;
      }
}

class Palpite {
      public $Id;
      public $JogoId;
      public $ParticipanteId;
      public $PalpiteMandante;
      public $PalpiteVisitante;
      public $Rodada;
      public $Turno;
      public $CampeonatoId;
      public $DataJogo;
      public $nomeMandante;
      public $NomeVisitante;
      public $FotoMandante;
      public $FotoVisitante;
      public $MandanteAbreviado;
      public $VisitanteAbreviado;
      public $Estadio;
      public $DataPalpite;
      public $Padrao;
      public $PontosRodada;
      public $NaMosca;
      public $PlacarMandante;
      public $PlacarVisitante;

      public function GravarPalpite($conexao, $palpite){              
            $sql = "
                  SELECT 
                        id 
                  FROM 
                        palpites 
                  WHERE 
                        participanteId = '".$palpite->ParticipanteId."' AND 
                        jogoId = '".$palpite->JogoId."' ";
	
            $rs=$conexao->query($sql);
            $reg = mysqli_fetch_array($rs);
            
            $palpiteId = $reg['id'];   
            
            if (!ValidaRodada($conexao, $palpite->JogoId)){
                  return false;
            }
            else{
                  if (empty($palpiteId)) {
                        return $palpite->InserirPalpite($conexao, $palpite);
                  }
                  else {
                        $palpite->Id = $palpiteId;
                        return $palpite->AlterarPalpite($conexao, $palpite);
                  }
            }
      }

      public function InserirPalpite($conexao, $palpite){
            $jogo = new Jogo();            

            $dadosJogo = $jogo->RetornaDadosJogo($conexao, $palpite->JogoId);          
                              
            $sql = "
                  INSERT INTO palpites (
                        jogoId,
                        campeonatoId,
                        participanteId,
                        palpiteMandante,
                        palpiteVisitante,
                        rodada,
                        turno,
                        dataPalpite,
                        dataJogo,
                        nomeMandante,
                        nomeVisitante,
                        fotoMandante,
                        fotoVisitante,
                        mandanteAbreviado,
                        visitanteAbreviado,
                        estadio,
                        padrao ) 
                  VALUES(
                        '$palpite->JogoId',
                        '$palpite->CampeonatoId',
                        '$palpite->ParticipanteId',
                        '$palpite->PalpiteMandante',
                        '$palpite->PalpiteVisitante',
                        '$palpite->Rodada',
                        '$palpite->Turno',
                        '$palpite->DataPalpite',
                        '$dadosJogo->Data',
                        '$dadosJogo->NomeMandante',
                        '$dadosJogo->NomeVisitante',
                        '$dadosJogo->MandanteId.png',
                        '$dadosJogo->VisitanteId.png',
                        '$dadosJogo->MandanteAbreviado',
                        '$dadosJogo->VisitanteAbreviado',
                        '$dadosJogo->Estadio',
                        '$palpite->Padrao' )";
                          
            return $conexao->query($sql);
      }

      public function AlterarPalpite($conexao, $palpite) {
            $sql="	
                  UPDATE 
                        palpites 
                  SET 
                        palpiteMandante='$palpite->PalpiteMandante',
                        palpiteVisitante='$palpite->PalpiteVisitante',
                        rodada='$palpite->Rodada',
                        dataPalpite='$palpite->DataPalpite' 
                  WHERE 
                        id = '".$palpite->Id."' ";	
     
           return $conexao->query($sql);
      }

      public function AtualizaPontosPalpites($conexao, $palpite){
            $sql = "
                  UPDATE 
                        palpites 
                  SET 
                        pontosRodada = '$palpite->PontosRodada', 
                        naMosca = '$palpite->NaMosca',
                        placarMandante = '$palpite->PlacarMandante',
                        placarVisitante = '$palpite->PlacarVisitante'
                  WHERE 
                        id = '".$palpite->Id."' ";	
                        
            return $conexao->query($sql);
      }
      
      public function GravarPalpitePadrao($conexao, $campeonatoId, $rodada) {
            $sqlJogod = "
                  SELECT 
                        id,
                        campeonatoId,
                        rodada,
                        turno
                  FROM 
                        jogos 
                  WHERE 
                        rodada = '".$rodada."' AND 
                  campeonatoId = '".$campeonatoId."' ";	                 
      
            $rsJogos=$conexao->query($sqlJogod);	
      
            while ($regJogos=mysqli_fetch_array($rsJogos)) {
                  $jogoId = $regJogos['id'];
                  $campeonatoId = $regJogos['campeonatoId'];
                  $rodada = $regJogos['rodada'];
                  $turno = $regJogos['turno'];
      
                  $sqlParticipantesCampeonato = "
                        SELECT 
                              participanteId 
                        FROM 
                              participantes_campeonato 
                        WHERE 
                              campeonatoId = '".$campeonatoId."' AND 
                              participanteId not in (SELECT participanteId FROM palpites WHERE campeonatoId = '".$campeonatoId."' AND 
                              jogoId = '".$jogoId."') ";
      
                  $rsParticipantesCampeonato=$conexao->query($sqlParticipantesCampeonato);	
      
                  while ($regParticipantesCampeonato=mysqli_fetch_array($rsParticipantesCampeonato)) {	
                        date_default_timezone_set("America/Sao_Paulo");

                        $participante = new Participante();
                        $participante->Id = $regParticipantesCampeonato['participanteId'];                        

                        $participante = $participante->RetornaPalpitePadrao($conexao, $participante);

                        $palpitePadrao = new Palpite();

                        $palpitePadrao->JogoId = $jogoId;
                        $palpitePadrao->CampeonatoId = $campeonatoId;
                        $palpitePadrao->ParticipanteId = $participante->Id;
                        $palpitePadrao->PalpiteMandante = $participante->PadraoMandante;
                        $palpitePadrao->PalpiteVisitante = $participante->PadraoVisitante;
                        $palpitePadrao->Rodada = $rodada;
                        $palpitePadrao->Turno = $turno;
                        $palpitePadrao->DataPalpite = date('Y-m-d H:i:s');
                        $palpitePadrao->Padrao = "1";  
                        
                        $retorno = $palpitePadrao->InserirPalpite($conexao, $palpitePadrao);
      
                        if (!$retorno) {
                              return false;		
                        }

                        unset($participante);
                        unset($palpitePadrao);
                  }
            }
      
            return true;
      }      

      public function PodeGravarPalpitePadrao($conexao, $campeonatoId, $rodada, $participanteId) {
            $sql = "SELECT 
                              rodada 
                        FROM 
                              pontuacao_inicial 
                        WHERE 
                              campeonatoId = '".$campeonatoId."' AND 
                              participanteId = '".$participanteId."' ";
            
            $rs=$conexao->query($sql);		
            $reg = mysqli_fetch_array($rs);
      
            $rodadaPontuacaoInicial = $reg['rodada'];
      
            if ($rodada > $rodadaPontuacaoInicial) return true;
            else return false;
      }      
      
      public function AtualizarPalpites($conexao, $jogo) {   
            $sql = "
                  SELECT 
                        id, 
                        palpiteMandante, 
                        palpiteVisitante,
                        padrao
                  FROM 
                        palpites 
                  WHERE 
                        jogoId = '".$jogo->Id."' ";                       
            
            $rs=$conexao->query($sql);
            
            $retorno = false;
            while ($reg=mysqli_fetch_array($rs)) 
            {
                  $palpite = new Palpite();
                  $palpite->Id = $reg['id'];
                  $palpite->PlacarMandante = $jogo->PlacarMandante;
                  $palpite->PlacarVisitante = $jogo->PlacarVisitante;

                  $palpiteId = $reg['id'];
                  $palpiteMandante = $reg['palpiteMandante'];
                  $palpiteVisitante = $reg['palpiteVisitante'];	
                  $padrao = $reg['padrao'];	

                  $acertoVitoriaMandante = 10;
                  $acertoVitoriaVisitante = 10;
                  $acertoEmpate = 15;
                  $acertoPlacar = 5;

                  // palpite padrão vale 70% do valor do palpite
                  if ($padrao) {
                        $acertoVitoriaMandante = 8;
                        $acertoVitoriaVisitante = 11;
                        $acertoEmpate = 11;
                        $acertoPlacar = 3.5;
                  }

                  // calcula pontuação da rodada
                  $qtPontos = 0;
                  $naMosca = 0;	
                              
                  if ($palpiteMandante == "" || $palpiteVisitante == "") {	
                        $palpite->PontosRodada = '0';
                        $palpite->NaMosca = '0';	
                  }
                  else {				
                        // acerto do empate
                        if ($palpiteMandante == $palpiteVisitante && $palpite->PlacarMandante == $palpite->PlacarVisitante) {
                              $qtPontos = $qtPontos + $acertoEmpate;	
                              $naMosca = $naMosca + 1;
                        }
                              
                        // acerto do vencedor
                        else if ($palpiteMandante > $palpiteVisitante && $palpite->PlacarMandante > $palpite->PlacarVisitante) {
                              $qtPontos = $qtPontos + $acertoVitoriaMandante;	
                              $naMosca = $naMosca + 1;	
                        }

                        // acerto do perdedor
                        else if ($palpiteMandante < $palpiteVisitante && $palpite->PlacarMandante < $palpite->PlacarVisitante) {
                              $qtPontos = $qtPontos + $acertoVitoriaVisitante;	
                              $naMosca = $naMosca + 1;	
                        }

                        // acerto do placar mandante
                        if ($palpiteMandante == $palpite->PlacarMandante) {
                              $qtPontos = $qtPontos + $acertoPlacar;
                              $naMosca = $naMosca + 1;
                        }
                        
                        // acerto do placar visitante
                        if ($palpiteVisitante == $palpite->PlacarVisitante) {
                              $qtPontos = $qtPontos + $acertoPlacar;
                              $naMosca = $naMosca + 1;
                        }

                        $acertoNaMosca = 0;
                        if ($naMosca == 3) {
                              $acertoNaMosca = 1;	
                        }
                        
                        $palpite->PontosRodada = $qtPontos;
                        $palpite->NaMosca = $acertoNaMosca;	
                  }
                  
                  if (!Palpite::AtualizaPontosPalpites($conexao, $palpite)){
                        return false;
                  }	
            }
            
            return true;
      }

      public function GravarPadraoParaPalpitesIguais($conexao, $campeonatoId, $rodada){
            $sqlParticipante = "
                  SELECT
                        participanteId
                  FROM
                        participantes_campeonato
                  WHERE
                        campeonatoId = ".$campeonatoId."
                  ORDER BY
                        participanteId ";                      

            $rsParticipante=$conexao->query($sqlParticipante);

            while ($regParticipante=mysqli_fetch_array($rsParticipante)) 
            {
                  $participanteId = $regParticipante['participanteId'];  
            
                  $sqlPalpites = "
                        SELECT 
                              id
                        FROM 	
                              palpites 
                        WHERE
                              rodada = '".$rodada."' AND 
                              participanteid = '".$participanteId."' AND
                              campeonatoId = '".$campeonatoId."'
                        GROUP BY
                              palpitemandante, palpitevisitante ";  

                  $rs=$conexao->query($sqlPalpites);
                  $registros = $rs->num_rows;
                  
                  $padrao = 0;
                  if ($registros == 1) {  
                        $padrao = 1;
                  }      

                  $sql = "
                        UPDATE 
                              palpites 
                        SET 
                              padrao = '$padrao'
                        WHERE 
                              rodada = '".$rodada."' AND
                              campeonatoId = '".$campeonatoId."' AND
                              participanteId = '".$participanteId."'";	
                              
                  if (!$conexao->query($sql)) {
                        return false;
                  }
            }

            return true;
      }
}

class Campeonato {
  
      public $Id;
      public $Descricao;
      public $Ano;  
      public $Serie;
      public $Ativo;

      public function GravarCampeonato($campeonato, $conexao){
            $sql = "
                  INSERT INTO campeonatos (
                        descricao, 
                        ano, 
                        serie, 
                        ativo) 
                  VALUES (
                        '$campeonato->Descricao', 
                        '$campeonato->Ano',
                        '$campeonato->Serie', 
                        '$campeonato->Ativo') ";

            $conexao->query($sql);

            return $conexao->insert_id; 
      } 

      public function ExcluirCampeonato($id, $conexao){
            $sql = "
                  DELETE FROM 
                        campeonatos 
                  WHERE 
                        id = '" . $id . "' ";	
		
		return $conexao->query($sql);	
      }     

      public function AlterarCampeonato($campeonato, $conexao){
            $sql = "
                  UPDATE 
                        campeonatos 
                  SET 
                        descricao = '$campeonato->Descricao', 
                        ano = '$campeonato->Ano', 
                        serie = '$campeonato->Serie',
                        ativo = '$campeonato->Ativo'	
                  WHERE 
                        id = '".$campeonato->Id."' ";

            return $conexao->query($sql);
      }

      public function CarregarDadosCampeonato($conexao, $campeonato) {
            $sql = "
                  SELECT 
                        * 
                  FROM 
                        campeonatos 
                  WHERE 
                        id = '" . $campeonato->Id . "' ";
            
            $rs=$conexao->query($sql);
            $reg=mysqli_fetch_array($rs);
            
            $campeonato->Descricao = $reg['descricao'];
            $campeonato->Ano = $reg['ano'];
            $campeonato->Serie = $reg['serie'];
            $campeonato->Ativo = $reg['ativo'];
            
            return $campeonato;
      }  

      public function ApagarTimesDoCampeonato($conexao, $campeonatoId){
        	$sql = "
                  DELETE FROM 
                        times_campeonato 
                  WHERE 
                        campeonatoId = '".$campeonatoId."' ";

	      $conexao->query($sql);    
      }

      public function InserirTimesNoCampeonato($conexao, $campeonatoId, $times){
            $quantidadeTimes = count($times);
            
            foreach ($times as $time) {
                  $sql = "
                        INSERT INTO times_campeonato (
                              campeonatoId, 
                              timeId) 
                        VALUES (
                              '$campeonatoId', 
                              '$time') ";
                  
                  if (!$conexao->query($sql)) {
                        return false;
                  }
            }

            return true;
      }	
      
      public function RetornaCampeonatoAtivo($conexao) {
            $sql = "
                  SELECT 
				id 
			FROM 
				campeonatos 
			WHERE 
                        ativo = 1
                  LIMIT 1";

            $rs=$conexao->query($sql);
            $reg=mysqli_fetch_array($rs);

	      return $reg["id"];		
      }
}

class Time {
      public $Id;
      public $Nome;
      public $NomeAbreviado;
	public $Cidade;
	public $Uf;
	public $Estadio;	
      public $Foto;
      public $NomeMinusculo;
      
      public function GravarTime($time, $conexao){
            $sql = "
                  INSERT INTO times (
                        nome, 
                        cidade, 
                        uf, 
                        estadio, 
                        nomeMinusculo, 
                        nomeAbreviado) 
                  VALUES (
                        '$time->Nome', 
                        '$time->Cidade', 
                        '$time->Uf', 
                        '$time->Estadio', 
                        '$time->NomeMinusculo', 
                        '$time->NomeAbreviado') ";

            $conexao->query($sql);

            return $conexao->insert_id; 
      } 

      public function ExcluirTime($id, $conexao){
            $sql = "
                  DELETE FROM 
                        times 
                  WHERE 
                        id = '" . $id . "' ";	
		
		return $conexao->query($sql);	
      }     

      public function AlterarTime($time, $conexao){
            $sql = "
                  UPDATE 
                        times 
                  SET 
                        nome = '$time->Nome',
                        nomeMinusculo = '$time->NomeMinusculo',
                        cidade = '$time->Cidade',
                        uf = '$time->Uf',
                        nomeAbreviado = '$time->NomeAbreviado',
                        estadio = '$time->Estadio'
                  WHERE 
                        id = '". $time->Id."' ";

            return $conexao->query($sql);
      }

      public function CarregarDadosTime($conexao, $time) {		
            $sql = "
                  SELECT 
                        * 
                  FROM 
                        times 
                  WHERE 
                        id = '". $time->Id."' ";
            
            $rs=$conexao->query($sql);
            $reg=mysqli_fetch_array($rs);
            
            $time->Foto = $reg['foto'];
            $time->Nome = $reg['nome'];
            $time->NomeAbreviado = $reg['nomeAbreviado'];
            $time->Cidade = $reg['cidade'];
            $time->Estado = $reg['uf'];
            $time->Estadio = $reg['estadio'];
            $time->NomeMinusculo = $reg['nomeMinusculo'];

            return $time;
      }

}

?>