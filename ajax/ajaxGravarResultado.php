﻿<?php
	SESSION_START();
	require_once('../classes/ConexaoBancoDeDados.php');
	require_once('../classes/Campeonato.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 
	
	$campeonatoId = $_SESSION['campeonatoId'];
	$participanteId = $_POST['id_usuario'];	
	$rodada = $_POST['txtRodada'];
	
	if ($participanteId == "") {
		$participanteId = "%";
	}
		
	if ($rodada == 0) {
		print "Selecione uma rodada!";
	}
	else {
		$sql = "SELECT 
					id, 
					mandanteId,
					nomeMandante,
					mandanteAbreviado,
					placarMandante, 
					visitanteId,
					nomeVisitante,
					visitanteAbreviado,
					placarVisitante,
					data
				FROM 
					jogos
				WHERE 
					rodada = ".$rodada." AND
					campeonatoId = ".$campeonatoId." 
				ORDER BY 
					data";
		
		$rs=$conexao->query($sql);
		$total_registros = $rs->num_rows;
	?>
		<!-- tabela dos palpites -->
		<form name="palpites" method="post" action="../paginas/gravarResultado1.php" style="font-size: 16px;">	
			<div id="caixa">						
				<?php

				$count = 0;
				while($reg=mysqli_fetch_array($rs))
				{						
					$jogoId = $reg['id'];
					$mandanteId = $reg['mandanteId'];
					$nomeMandante = $reg['nomeMandante'];
					$mandanteAbreviado = $reg['mandanteAbreviado'];
					$placarMandante = $reg['placarMandante'];
					$visitanteId = $reg['visitanteId'];
					$nomeVisitante = $reg['nomeVisitante'];	
					$visitanteAbreviado = $reg['visitanteAbreviado'];					
					$placarVisitante = $reg['placarVisitante'];
					$fotoMandante = $mandanteId.'.png';
					$fotoVisitante = $visitanteId.'.png';
					$data = $reg['data'];				
					$jogo = " x ";
					$count = $count + 1;	
					$dataJogo = date('d/m H:i', strtotime($data));		?>

					<div class="caixa" style="color: red;max-width: 880px;"><?PHP print $dataJogo; ?></div>	
					
					<div class="jogo">			
						<div class="jogo-item"><?PHP print $mandanteAbreviado; ?></div>	

						<div class="jogo-item"><?PHP print $nomeMandante; ?></div>					
						
						<div class="jogo-item"><img class="distintivo" src="imagens/times/<?php print $fotoMandante; ?>"/></div>
												
						<div class="jogo-item">
                                		<input style="padding: 0 5px 0 0;" type="number" name="txtmandante<?php print $count; ?>" value="<?php print $placarMandante; ?>" 
								class="form-control texto-palpite" maxlength="1" max=9 min="0" >
                            		</div>	
						
						<div class="jogo-item"><?php print $jogo; ?></div>
												
						<div class="jogo-item" >
							<input style="padding: 0 0px 0 0;" type="number" name="txtvisitante<?php print $count; ?>" value="<?php print $placarVisitante; ?>"
								class="form-control texto-palpite" maxlength="1" max=9 min="0" >
						</div>						
						
						<div class="jogo-item" ><img class="distintivo" src="imagens/times/<?php print $fotoVisitante; ?>"/></div>

						<div class="jogo-item"><?PHP print $visitanteAbreviado; ?></div>	
						
						<div class="jogo-item"><?PHP print $nomeVisitante; ?></div>				
						
					</div>
													
					<input type="hidden" name="txtjogo<?php print $count; ?>" value="<?php print $jogoId; ?>" />
					<input type="hidden" name="txtjogoData<?php print $count; ?>" value="<?php print $data; ?>" />	
					<input type="hidden" name="rodada" value="<?php print $rodada; ?>" />		

					<hr>											
				<?PHP
				} ?>				
										
				<div class="btn-palpite">
					<button type="submit" class="btn btn-block btn-danger btn-lg">Gravar resultado</button>
				</div>	
				
				<input type="hidden" name="total_registros" value="<?php print $total_registros; ?>" />
			</div>
		</form>
		<?php 
	} 
		?>