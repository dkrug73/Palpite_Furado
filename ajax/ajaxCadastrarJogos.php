﻿
<?php
	include "../conexao/dbConexao.php";
	include "../utils/funcoes.php";	
	
	$campeonatoId = $_POST['campeonato'];
	$rodada = $_POST['rodada'];
		
	if ($rodada == "")
	{
		$rodada = "%";
	}
		
	$sql = "SELECT 
					id,
					nomeMandante,
					nomeVisitante,
					estadio,
					DATE_FORMAT(data,'%d/%m/%Y  %H:%i') AS dataJogo, 
					CONCAT(rodada,'ª rodada') AS rodada,
					placarMandante,
					placarVisitante,
					rodada as rod
				FROM 
					jogos 
				WHERE
					campeonatoId = '".$campeonatoId."' AND
					rodada like '".$rodada."'
				ORDER BY 
					rod ASC, data ";
	
	$rs=$conexao->query($sql);
	$total_registros=$rs->num_rows;
	?>

	<div class="box-body">	
		<h3 class="box-title">Times</h3>
		<div class="box-tools">
			<div class="input-group" style="width: 150px;">	</div>
		</div>
		
		<div class="box-body table-responsive no-padding" style="min-height: 430px;">
			<table class="table table-striped">
				<tr>
					<th>ID</th>
					<th>Rodada</th>
					<th>Data/hora</th>
					<th>Mandante</th>
					<th>Placar</th>
					<th>Visitante</th>	
					<th>Estádio</th>
				</tr>

				<?PHP
				// Exibe os registros na tabela
				while($reg=mysqli_fetch_array($rs))
				{			
					$id = $reg["id"];
					$rodada = $reg["rodada"];
					$mandante = $reg["nomeMandante"];
					$visitante = $reg["nomeVisitante"];
					$placarMandante = $reg["placarMandante"];
					$placarVisitante = $reg["placarVisitante"];
					$dataJogo = $reg["dataJogo"];
					$estadio = $reg["estadio"];
					
					if ($placarMandante != "" ) $jogo = $placarMandante." x ".$placarVisitante;
					else $jogo = " x "; ?>							
				
				<tr onclick="location.href = '../cadastrarJogo.php?&id=<?PHP print $id; ?>&titulo=Alteração de registro'; " style='cursor: pointer;'> 
					<td><?PHP print $id; ?></td>
					<td><?PHP print $rodada; ?></td>
					<td><?PHP print $dataJogo; ?></td>
					<td><?PHP print $mandante; ?></td>								
					<td><?PHP print $jogo; ?></td>
					<td><?PHP print $visitante; ?></td>	
					<td><?PHP print $estadio; ?></td>
				</tr>
				<?php
				} ?>
			</table>
		</div>
	</div>
	
	