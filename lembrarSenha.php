<?php 
	SESSION_START();
	include "conexao/dbConexao.php";
	include "utils/funcoes.php";

	LembrarSenha($conexao);

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

    $mensagem = "";
	$tipoAviso = "";

	if(isset($_GET['msg'])){
		$mensagem = $_GET['msg'];
	}
	if (isset($_GET['tipoAviso'])) {
		$tipoAviso = $_GET['tipoAviso'];
	} 	
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Contato</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/geral.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<link rel="icon" type="image/png" href="imagens/favicon.png">
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">
		
		<!-- CABEÇALHO -->
		<?php 
		include("componentes/cabecalho.php"); ?>

		<!-- MENU -->
		<?php 
		include("componentes/menu.php"); ?>	

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

            <section class="content-header">
				<h1>Esqueceu sua senha?<small>Digite seu e-mail no campo abaixo e depois clique no botão "Enviar senha". 
                            Sua senha será enviada para o e-mail informado.</small></h1>				
			</section>

            <section class="content">				
				<div class="box box-primary">

					<?php
					if($tipoAviso == "erro"){ ?>
						<div class="alert alert-danger alert-dismissable" style="margin: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-ban"></i>Erro!</h4>
							<?php print $mensagem; ?>
						</div>
						<?php
					} else if ($tipoAviso == "alerta"){ ?>
						<div class="alert alert-warning alert-dismissable" style="margin: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-warning"></i> Aviso!</h4>
							<?php print $mensagem; ?>
						</div>					
					<?php 
					} 
					else if($tipoAviso == "sucesso"){ ?>
						<div class="alert alert-success alert-dismissable" style="margin: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-check"></i>Sucesso!</h4>
							<?php print $mensagem; ?>
						</div>
					<?php 
					} ?>

					<form role="form" name="email" class="form-horizontal" method="post" action="paginas/lembrarSenha1.php" 
						enctype="multipart/form-data">
						
                        <div class="box-body">
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label" style="width: 200px;">Email</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="email" name="email" 
                                        placeholder="Informe seu email" required>
								</div>
							</div>                      
                        </div>
					
						<!-- /.box-body -->
						<div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary" name="acao" value="inc" 
                                style="margin-right: 15px;">Enviar senha</button>
					    </div>	
						<!-- /.box-footer -->
					</form>
				</div>
			</section>
		</div>

		<!-- RODAPE -->		
		<?php 
		include "componentes/rodape.php"; ?>

		<!-- ENTRAR -->
		<div class="modal fade" id="myModal4" tabindex="-1" role="dialog">			
			<?php 
			include("login/entrar.php"); ?>	
		</div>

		<!-- NOVO CADASTRO -->
		<div class="modal fade" id="myModal5" tabindex="-1" role="dialog">
			<?php 
			include("login/novo_cadastro.php"); ?>
		</div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
</body>

</html>