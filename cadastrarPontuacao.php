<?php 
	SESSION_START();
	include "conexao/dbConexao.php";
	include "utils/funcoes.php";

	LembrarSenha($conexao);

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$participanteId = '0';	
	if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}

	$mensagem = "";
	$tipoAviso = "";
	if(isset($_GET['msg'])){
		$mensagem = $_GET['msg'];
	}
	if (isset($_GET['tipoAviso'])) {
		$tipoAviso = $_GET['tipoAviso'];
	} 

	// inicializa valores
	$id = "";
	$descricao = "";
	$descricaoCampeonato = "";
	$bonus = "";
	$empate = "";
	$vencedor = "";
	$placar = "";

	if(isset($_GET['id'])){
		$id = $_GET['id'];
		
		if ($id != "") {
			$sql = "SELECT * FROM pontuacao WHERE id = '" . $id . "' ";
			
			$rs=$conexao->query($sql);
			$reg=mysqli_fetch_array($rs);
			
			$id = $reg['id'];
			$descricao = $reg['descricao'];
			$bonus = $reg['bonus'];
			$empate = $reg['empate'];
			$vencedor = $reg['vencedor'];			
			$placar = $reg['placar'];
		} 
	}		
	
	$sql="SELECT * FROM pontuacao ORDER BY id";
	
	$rs=$conexao->query($sql);	
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Cadastrar Pontuação</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/geral.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
	<link rel="icon" type="image/png" href="imagens/favicon.png">

	<script language="javascript" src="dist/js/utils.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">
		
		<!-- CABEÇALHO -->
		<?php include("componentes/cabecalho.php"); ?>

		<!-- MENU -->
		<?php include("componentes/menu.php"); ?>	

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Cadastro de Campeonato<small>Incluir/alterar</small></h1>				
			</section>

			<!-- Main content -->
			<section class="content">				
				<div class="box box-primary">

					<?php
					if($tipoAviso == "erro"){ ?>
						<div class="alert alert-danger alert-dismissable" style="margin: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-ban"></i>Erro!</h4>
							<?php print $mensagem; ?>
						</div>
						<?php	
					} 
					else if ($tipoAviso == "alerta"){ ?>
						<div class="alert alert-warning alert-dismissable" style="margin: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-warning"></i> Aviso!</h4>
							<?php print $mensagem; ?>
						</div>						
					<?php 				
					} 
					else if ($tipoAviso == "sucesso"){ ?>
						<div class="alert alert-success alert-dismissable" style="margin: 10px;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-check"></i>Sucesso!</h4>
							<?php print $mensagem; ?>
						</div>
					<?php 
					} ?>

					<form role="form" name="cadastro" class="form-horizontal" method="post" action="paginas/cadastrarPontuacao1.php" 
						enctype="multipart/form-data" onsubmit="return valida_form();">

						<div class="box-body">
							<div class="form-group">
								<label for="inputid" class="col-sm-2 control-label">ID</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="id" name="id" placeholder="ID" 
										value="<?php print $id; ?>" readonly='yes' required>
								</div>
							</div>	

							<div class="form-group">
								<label for="inputNome" class="col-sm-2 control-label">Descrição</label>
								<div class="col-sm-10">
									<input type="descricao" class="form-control" id="descricao" name="descricao" placeholder="Descricao" value="<?php print $descricao; ?>" required>
								</div>
							</div>

							<div class="form-group">
								<label for="inputBomus" class="col-sm-2 control-label">Bônus</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="bonus" name="bonus" placeholder="Bônus" 
										value="<?php print $bonus; ?>" style="width: 250px;" required>
									<p class="help-block">Para acertos EM CHEIO do placar</p>
								</div>
							</div>													
						
							<div class="form-group">
								<label for="empate" class="col-sm-2 control-label">Empate</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Empate" name="empate" 
										value="<?php print $empate; ?>" maxlength="4" id="empate" style="width: 250px;" required>
									<p class="help-block">Para acertos do EMPATE</p>
								</div>
							</div>

							<div class="form-group">                
								<label for="vencedor" class="col-sm-2 control-label">Vencedor</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Vencedor" name="vencedor" 
										value="<?php print $vencedor; ?>" maxlength="4" id="vencedor" style="width: 250px;"	required>
									<p class="help-block">Para acertos do VENCEDOR</p>
								</div>
							</div>	

							<div class="form-group">   
								<label for="placar" class="col-sm-2 control-label" >Placar</label>
								<div class="col-sm-10" >
									<input type="text" class="form-control" placeholder="Placar" name="placar" 
										value="<?php print $placar; ?>" maxlength="4" id="placar" style="width: 250px;" required>
									<p class="help-block" >Para acertos EM CHEIO de um dos placares</p>
								</div>								
							</div>

						</div>
						<!-- /.box-body -->
						<div class="box-footer">
						<button type="submit" id="submit" class="btn btn-primary" name="acao" value="inc" 
							style="width: 80px; margin-right: 15px;">Salvar</button>
						<button type="submit" class="btn btn-danger" name="acao" value="exc" style="width: 80px;">Excluir</button>
					</div>	
						<!-- /.box-footer -->
					</form>

					<div class="box-body">	
						<h3 class="box-title">Pontuação</h3>
						<div class="box-tools">
							<div class="input-group" style="width: 150px;">	</div>
						</div>
						
						<div class="box-body table-responsive no-padding">
							<table class="table table-striped">
								<tr>
									<th>ID</th>
									<th>Descrição</th>
									<th>Bônus</th>
									<th>Empate</th>
									<th>Vencedor</th>
									<th>Placar</th>
								</tr>
								
								<?PHP
								while($reg=mysqli_fetch_array($rs)) 
								{
									$id = $reg["id"];
									$descricao = $reg["descricao"];
									$bonus = $reg["bonus"];
									$empate = $reg["empate"];
									$vencedor = $reg["vencedor"];
									$placar = $reg["placar"];									
									?>									
														
									<tr onclick="location.href = 'cadastrarPontuacao.php?acao=alt&id=<?PHP print $id; ?>&titulo=Alteração de registro'; " style='cursor: pointer;'> 									
										<td><?PHP print $id; ?></td>
										<td><?PHP print $descricao; ?></td>
										<td><?PHP print $bonus; ?></td>
										<td><?PHP print $empate; ?></td>
										<td><?PHP print $vencedor; ?></td>
										<td><?PHP print $placar; ?></td>
									</tr>							
									<?PHP 
								} ?>
							</table>
						</div>
					</div>
				</div>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<!-- RODAPE -->		
		<?php include "componentes/rodape.php"; ?>

		<!-- ENTRAR -->
		<div class="modal fade" id="myModal4" tabindex="-1" role="dialog">			
			<?php include("login/entrar.php"); ?>	
		</div>

		<!-- NOVO CADASTRO -->
		<div class="modal fade" id="myModal5" tabindex="-1" role="dialog">
			<?php include("login/novo_cadastro.php"); ?>
		</div>
	</div>
	<!-- ./wrapper -->
	
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<script src="plugins/fastclick/fastclick.js"></script>
	<script src="dist/js/app.min.js"></script>
	<script src="dist/js/demo.js"></script>
	<script src="dist/js/dimensoes.Imagem.Antes.Upload.js"></script>
	<script src="dist/js/antesUpload.js"></script>
</body>

</html>