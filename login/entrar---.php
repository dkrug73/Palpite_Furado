<?php
    /*
    if(isset($_POST['email'])) {
        SESSION_START();
        require_once('../classes/Participante.php');
	    require_once('../classes/ConexaoBancoDeDados.php');

        $conexaoBancoDeDados = new ConexaoBancoDeDados();
        $conexao = $conexaoBancoDeDados->ConectarMySql(); 

        $participante = new Participante($conexao);
        




        $pagina = "..".$_SESSION['pagina'];        

        $email = "";
	    $senha = "";
	
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $senha = $_POST['senha'];
        }

        $lembrete = (isset($_POST['lembrete'])) ? $_POST['lembrete'] : ''; 

        global $_SG;
        $cS = ($_SG['caseSensitive']) ? 'BINARY' : '';
        // Usa a função addslashes para escapar as aspas
        $nusuario = addslashes($email);
        $nsenha = addslashes($senha);
            
        if (!empty($email) && !empty($senha)):
            $sql = "SELECT * FROM participantes WHERE (".$cS." email = '".$nusuario."' AND ".$cS." senha = '".$nsenha."') LIMIT 1";
            $query=$conexao->query($sql);            

            $resultado = mysqli_fetch_array($query);          
                
            if(!empty($resultado)):
                $_SESSION['participanteId'] = $resultado['id'];
                $_SESSION['participanteNome'] = $resultado['nome'];
                $_SESSION['email'] = $resultado['email'];
                $_SESSION['senha'] = $resultado['senha'];
                $_SESSION['padraoMandante'] = $resultado['padraoMandante'];
                $_SESSION['padraoVisitante'] = $resultado['padraoVisitante'];
                $_SESSION['receberemail'] = $resultado['receberemail'];
                $_SESSION['foto'] = $resultado['foto'];
                $_SESSION['nivel'] = $resultado['nivel'];
                
                $_SESSION['acao'] = "alt";            
                $_SESSION['logado'] = TRUE;                
                
                if($lembrete == "SIM") {
                    $cookie_name = "CookieLembrete";
                    $cookie_value = 'SIM';
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias

                    $cookie_name = "CookieEmail";
                    $cookie_value = $email;
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias

                    $cookie_name = "CookieSenha";
                    $cookie_value = $senha;
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias

                    $cookie_name = "CookieSenha";
                    $cookie_value = $senha;
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 * 30), "/"); // 30 dias
                }
                else {			
                    $cookie_name = "CookieLembrete";
                    $cookie_value = 'SIM';
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias

                    $cookie_name = "CookieEmail";
                    $cookie_value = $email;
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias

                    $cookie_name = "CookieSenha";
                    $cookie_value = $senha;
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias

                    $cookie_name = "CookieSenha";
                    $cookie_value = $senha;
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30 ), "/"); // 30 dias
                }
        
                echo "<script>window.location = '../palpites.php'</script>";
            else: 
                print "<script type='text/javascript'> alert('Email ou Senha inválido!'); </script>";
                
               echo "<script>window.location =  '$pagina' </script>";
        
            endif;    
        endif;
            
        mysqli_close($conexao);
    }
    */

    $erro = null;
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
?>		

<div class="modal-dialog" role="document">
    <div class="modal-content modal-info">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><img src="../imagens/logo.png"></a>
            </div>
            
            <div class="login-box-body">
                <p class="login-box-msg">Faça o login para entrar no site.</p>

                <form action="../paginas/validarAcesso.php" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Email ou Usuário" id="email" name="email" required>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Senha" id="senha" name="senha">
                        <span class="glyphicon glyphicon-lock form-control-feedback" ></span>
                    </div>
                <?php
                if ($erro == 1){
                                        echo '<font color="FF0000">Usuário ou senha inválidos.</font>';
                                    } ?>         
                
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label style="margin-left: 20px;">
                                    <input type="checkbox" name="lembrete" value="SIM" > Lembrar senha
                                </label>
                            </div>
                        </div>
                        
                        <div class="col-xs-4">
                            <button type="submit" id="entrar" nome="entrar" class="btn btn-primary btn-block btn-flat">Entrar</button>
                        </div>
                        
                    </div>
                </form>       
                <a href="#" onclick="this.href='paginas/lembrarSenha1.php?email='+document.getElementById('email').value">Eu esqueci minha senha</a>
                <br>  
                <a href="cadastro.php" class="text-center">Cadastrar novo participante</a>
            </div>
        </div>
    </div>
</div>