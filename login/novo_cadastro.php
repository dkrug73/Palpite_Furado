<script language="javascript" src="dist/js/utils.js"></script>



<div class="modal-dialog" role="document">
    <div class="modal-content modal-info">
        <div class="login-box">
            <div class="login-logo">
                <a href="index.php"><img src="../imagens/logo.png"></a>
            </div>

            <div class="register-box-body">
                <p class="login-box-msg">Cadastrar novo participante</p>

                <form action="paginas/cadastro1.php" method="post" >
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome completo" required>
                        <span class="glyphicon glyphicon-user form-control-feedback" ></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control"  required placeholder="Informe sua senha." 
                            name="senha" onchange="form.contraSenha.pattern = this.value;" >
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" required placeholder="Confirmar senha" class="form-control"
                            name="contraSenha" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'As senhas informadas não conferem' : '')">
                        <span class="glyphicon glyphicon-log-in form-control-feedback" required></span>
                    </div>

                    <div class="form-group">
								
								<div class="row">
									<div class="col-xs-5" style="width: 175px;">
										<input type="text" class="form-control" id="mandante" name="mandante" placeholder="Padrão Mandante" required>
									</div>
									<div class="col-xs-5" style="width: 175px; float: right;">
										<input type="text" class="form-control" id="visitante" name="visitante" placeholder="Padrão Visitante" required>
									</div>
								</div>
							</div>

                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck" aria-required>
                                <label style="margin-left: 20px;">
                                    <input type="checkbox" id="ck_permissao" required> Eu li e concordo com o <a href="../regulamento.php">Regulamento</a>.
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" name="acao" id="acao" value="inc" >Gravar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>