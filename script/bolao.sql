
DROP TABLE IF EXISTS `campeonatos`;

CREATE TABLE `campeonatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(60) NOT NULL,
  `ano` varchar(4) NOT NULL,
  `serie` varchar(2) NOT NULL,
  `ativo` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

CREATE TABLE `pontuacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  `bonus` int(4) NOT NULL,
  `placar` int(4) NOT NULL,
  `empate` int(4) NOT NULL,
  `vencedor` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `jogos`;

CREATE TABLE `jogos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campeonatoId` int(11) NOT NULL,
  `turno` int(1) NOT NULL,
  `rodada` int(2) NOT NULL,
  `data` datetime NOT NULL,
  `mandanteId` int(11) NOT NULL,
  `nomeMandante` varchar(60) NOT NULL,
  `visitanteId` int(11) NOT NULL,
  `nomeVisitante` varchar(60) NOT NULL,
  `placarMandante` int(2) DEFAULT NULL,
  `placarVisitante` int(2) DEFAULT NULL,
  `mandanteAbreviado` varchar(3) NOT NULL,
  `visitanteAbreviado` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=989 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `palpites`;
CREATE TABLE `palpites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campeonatoId` int(11) NOT NULL,
  `jogoId` int(11) NOT NULL,
  `participanteId` int(11) NOT NULL,
  `palpiteMandante` int(2) DEFAULT NULL,
  `palpiteVisitante` int(2) DEFAULT NULL,
  `padrao` char(1) NOT NULL,
  `pontosRodada` int(3) DEFAULT NULL,
  `naMosca` int(1) DEFAULT NULL,
  `rodada` int(2) DEFAULT NULL,
  `turno` int(2) DEFAULT NULL,
  `dataPalpite` datetime NOT NULL,
  `dataJogo` datetime NOT NULL,
  `nomeMandante` varchar(60) NOT NULL,
  `nomeVisitante` varchar(60) NOT NULL,
  `fotoMandante` varchar(60) NOT NULL,
  `fotoVisitante` varchar(60) NOT NULL,
  `placarMandante` int(2) DEFAULT NULL,
  `placarVisitante` int(2) DEFAULT NULL,
  `mandanteAbreviado` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visitanteAbreviado` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9861 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `participantes`;
CREATE TABLE `participantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `nomeUsuario` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `desativar` char(1) DEFAULT NULL,
  `foto` varchar(60) DEFAULT NULL,
  `receberemail` char(1) DEFAULT NULL,
  `nivel` int(1) NOT NULL DEFAULT '0',
  `padraoMandante` int(1) DEFAULT NULL,
  `padraoVisitante` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `participantes_campeonato`;

CREATE TABLE `participantes_campeonato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campeonatoId` int(11) NOT NULL,
  `participanteId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pontuacao_inicial`;
CREATE TABLE `pontuacao_inicial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campeonatoId` int(11) NOT NULL,
  `participanteId` int(11) NOT NULL,
  `rodada` int(2) NOT NULL,
  `turno` int(1) NOT NULL,
  `pontos` int(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rodada`;

CREATE TABLE `rodada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campeonatoId` int(11) NOT NULL,
  `turno` int(2) NOT NULL,
  `rodada` int(2) NOT NULL,
  `participanteId` int(11) NOT NULL,
  `totalPontos` int(11) NOT NULL,
  `naMosca` int(2) NOT NULL,
  `posicaoRodada` int(3),
  `nomeParticipante` varchar(60) NOT NULL,
  `fotoParticipante` varchar(60) DEFAULT NULL,
  `pontosRodada` int(11) NOT NULL,
  `posicaoRodadaAnterior` int(3),
  `totalNaMosca` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2403 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `rodadaGeral`;

CREATE TABLE `rodadaGeral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campeonatoId` int(11) NOT NULL,
  `turno` int(2) NOT NULL,
  `rodada` int(2) NOT NULL,
  `participanteId` int(11) NOT NULL,
  `totalPontos` int(11) NOT NULL,
  `naMosca` int(2) NOT NULL,
  `posicaoRodada` int(3),
  `nomeParticipante` varchar(60) NOT NULL,
  `fotoParticipante` varchar(60) DEFAULT NULL,
  `pontosRodada` int(11) NOT NULL,
  `posicaoRodadaAnterior` int(3),
  `totalNaMosca` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2403 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `estados`;
CREATE TABLE `estados` (
  `id` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uf` varchar(2) NOT NULL DEFAULT '',
  `nome` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

LOCK TABLES `estados` WRITE;
INSERT INTO `estados` VALUES (21,'RO','Rondonia'),(20,'RN','Rio Grande do Norte'),(19,'RJ','Rio de Janeiro'),(18,'PR','Parana'),(17,'PI','Piaui'),(16,'PE','Pernambuco'),(15,'PB','Paraiba'),(14,'PA','Para'),(13,'MT','Mato Grosso'),(12,'MS','Mato Grosso do Sul'),(11,'MG','Minas Gerais'),(10,'MA','Maranhao'),(09,'GO','Goias'),(08,'ES','Espirito Santo'),(07,'DF','Distrito Federal'),(06,'CE','Ceara'),(05,'BA','Bahia'),(04,'AP','Amapa'),(03,'AM','Amazonas'),(02,'AL','Alagoas'),(01,'AC','Acre'),(22,'RR','Roraima'),(23,'RS','Rio Grande do Sul'),(24,'SC','Santa Catarina'),(25,'SE','Sergipe'),(26,'SP','Sao Paulo'),(27,'TO','Tocantins');


DROP TABLE IF EXISTS `times`;
CREATE TABLE `times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `cidade` varchar(60) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `estadio` varchar(40) NOT NULL,
  `foto` varchar(60) NOT NULL,
  `nomeMinusculo` varchar(60) NOT NULL,
  `nomeAbreviado` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

LOCK TABLES `times` WRITE;
INSERT INTO `times` VALUES 
(1,'Grêmio','Porto Alegre','RS','Arena do Grêmio','1.jpg'),
(2,'Internacional','Porto Alegre','RS','Beira-Rio','2.jpg'),
(3,'Atlético-GO','Goiânia','GO','Rei Pelé','3.jpg'),
(4,'Atlético - MG','Belo Horizonte','MG','Independência','4.jpg'),
(5,'São Paulo','São Paulo','SP','Pacaembú','5.jpg'),
(6,'Vasco','Rio de Janeiro','RJ','São Januário','6.jpg'),
(7,'Bahia','Salvador','BA','Pituaçu','7.jpg'),
(8,'Botafogo','Rio de Janeiro','RJ','João Havelange','8.jpg'),
(9,'Atlético - PR','Curitiba','PR','Arena da Baixada','9.jpg'),
(10,'Corinthians','São Paulo','SP','Pacaembú','10.jpg'),
(11,'Coritiba','Curitiba','PR','Couto Pereira','11.jpg'),
(12,'Cruzeiro','Belo Horizonte','MG','Mineirão','12.jpg'),
(13,'Vitória','Salvador','BA','Barradão','13.jpg'),
(14,'Flamengo','Rio de Janeiro','RJ','Maracanã','14.jpg'),
(15,'Fluminense','Rio de Janeiro','RJ','Maracanã','15.jpg'),
(16,'Sport','Recife','PE','Ilha do Retiro','16.jpg'),
(17,'Náutico','Recife','PE','Aflitos','17.jpg'),
(18,'Palmeiras','São Paulo','SP','Allianz Parque','18.jpg'),
(19,'Ponte Preta','Campinas','SP','Moisés Lucarelli','19.jpg'),
(20,'Santos','Santos','SP','Vila Belmiro','20.jpg'),
(21,'Chapecoense','Chapecó','SC','Arena Condá','21.jpg');
UNLOCK TABLES;



DROP TABLE IF EXISTS `times_campeonato`;
CREATE TABLE `times_campeonato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campeonatoId` int(11) NOT NULL,
  `timeId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `login` varchar(30) DEFAULT NULL,
  `senha` varchar(8) DEFAULT NULL,
  `acesso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;


LOCK TABLES `usuarios` WRITE;
INSERT INTO `usuarios` VALUES (1,'Administrador do sistema','dkrug73@gmail.com','dk1065$#',1);
UNLOCK TABLES;
