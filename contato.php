<?php 
	SESSION_START();
	require_once('classes/Participante.php');
	require_once('classes/ConexaoBancoDeDados.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql();  

	$participante = new Participante($conexao);

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$erro = null;
	if (isset($_GET['erro'])){
		$erro = $_GET['erro'];
	}	   

	if(isset ($_SESSION['participanteId']) == true) {
		$participante->Id = $_SESSION['participanteId'];
		$participante->Nome = $_SESSION['participanteNome'];
		$participante->Email = $_SESSION['email'];
	}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Contato</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/participante.js"></script> 
	<script src="componentes/js/utils.js"></script>	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">	
	<link rel="icon" type="image/png" href="imagens/favicon.png">	
	
	<script>	
		$(document).ready(function(){
			
			var participanteId = '<?php echo ($participante->Id); ?>';			
			
			if (participanteId != ""){
				var participanteNome = titleize('<?php echo ($participante->Nome) ?>');
				document.getElementById('nome').value = participanteNome;
				document.getElementById('email').value = '<?php echo ($participante->Email) ?>';
				$('#assunto').focus();
			}

			$('#btn-enviar').click(function() {
				var enviar = true;
				document.getElementById("erro-nome").innerHTML = "";
				document.getElementById("erro-email").innerHTML = "";
				document.getElementById("erro-assunto").innerHTML = "";
				document.getElementById("erro-mensagem").innerHTML = "";

				if (document.getElementById("mensagem").value == "" ) {
					document.getElementById("erro-mensagem").innerHTML = "Preencha o campo Mensagem";
					$('#mensagem').focus();
					enviar = false;
				}

				if (document.getElementById("assunto").value == "") {
					document.getElementById("erro-assunto").innerHTML = "Preencha o campo Assunto";
					$('#assunto').focus();
					enviar = false;
				}

				if (document.getElementById("email").value == "") {
					document.getElementById("erro-email").innerHTML = "Preencha o campo email";
					$('#email').focus();
					enviar = false;
				}

				if (document.getElementById("nome").value == "") {
					document.getElementById("erro-nome").innerHTML = "Preencha o campo Nome";
					$('#nome').focus();
					enviar = false;
				}

				if (enviar){
					$.ajax({
						url: 'paginas/contato1.php',
						method: 'post',
						data: $('#formulario-contato').serialize(),
						success: function(data){
							$('html, body').animate({scrollTop:0}, 'slow');
							var classe = '#salvar-erro';
							if (data == 0){
								classe = '#salvar-sucesso';
							}

							document.getElementById("assunto").value = null;
							document.getElementById("mensagem").value = null;
							$('#assunto').focus();

							ExibirAviso(classe);
						},

						beforeSend: function(){
							var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Enviando...';
							$('#btn-enviar').html(loadingText);    
							$('#btn-enviar').attr('disabled', 'disabled');
						},

						complete: function(){
							$('#btn-enviar').html('Enviar email');
							$('#btn-enviar').removeAttr('disabled');
						}
					});

					return false;
				}

				return false;
			});   

			function ExibirAviso(classe){     
				$(classe).show(); 
				setTimeout(function() {$(classe).fadeOut('slow');}, 5000);
			}         
		});		
	</script>

</head>

<body class="hold-transition skin-blue sidebar-mini">

	<?php include("componentes/menu.php"); ?>	
	
	<div class = "container">

		<div id="salvar-sucesso" style="display:none;">
			<div class="alert alert-success text-center mt-3" role="alert" id="alerta">
				Email enviado com sucesso!
			</div>           
		</div>

		<div id="salvar-erro" style="display:none;">
			<div class="alert alert-danger text-center" role="alert" id="alerta">
				Erro ao enviar email
			</div> 
		</div>

		<section class="titulo-pagina">
			<h3 class="tituloPagina">Contato</h3>				
		</section>
	
		<section>
			<form name="formulario-contato" id="formulario-contato" method="" action="" enctype="">

				<div class="form-group row">
                    <label for="nome" class="col-lg-2 col-md-2 col-form-label">Nome</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="nome" name="nome" value="" maxlength="100">
                        <span id="erro-nome" style="color: red;font-size: 14px;">  </span>
                	</div>
                </div>

				<div class="form-group row">
                    <label for="email" class="col-lg-2 col-md-2 col-form-label">Email</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="email" name="email" value="" maxlength="100">
                        <span id="erro-email" style="color: red;font-size: 14px;">  </span>
                	</div>
                </div>
				
				<div class="form-group row">
                    <label for="assunto" class="col-lg-2 col-md-2 col-form-label">Assunto</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="assunto" name="assunto" value="" maxlength="100">
                        <span id="erro-assunto" style="color: red;font-size: 14px;">  </span>
                	</div>
                </div>

				<div class="form-group row">
                    <label for="mensagem" class="col-lg-2 col-md-2 col-form-label">Mensagem</label>

                    <div class="col-lg-10 col-md-10">
                        <textarea type="text" class="form-control" id="mensagem" name="mensagem" value="" 
							style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        <span id="erro-mensagem" style="color: red;font-size: 14px;">  </span>
                	</div>
                </div>

				<div class="form-group row">
                    <label for="assunto" class="col-lg-2 col-md-2 col-form-label"></label>

                    <div class="col-lg-10 col-md-10">
						<button type="submit" id="btn-enviar" class="btn btn-primary" style="width: 150px; margin-top: 10px" >Enviar email</button>
                	</div>
                </div>		
						
			</form>
		</section>

	</div>

	<!-- MODAL ENTRAR -->		
	<?php include("modal/entrar.php"); ?>

	<?php include("componentes/rodape.php"); ?>

    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>