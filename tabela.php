<?php 
	SESSION_START();
	include "conexao/dbConexao.php";
	include "utils/funcoes.php";

	LembrarSenha($conexao); 
   
	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];
	
	$participanteId = '0';	
	if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}
	$campeonatoId = $_SESSION['campeonatoId']; 

	
	if(isset($_POST['txtTime'])){
		$_SESSION['time'] = $_POST['txtTime'];
	}	
	
	$time = "ponte preta";
	if(isset ($_SESSION['time']) == true) {
		$time = $_SESSION['time'];
	}
 ?>  

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Tabela</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="dist/css/geral.css">
	<link rel="icon" type="image/png" href="imagens/favicon.png">

	<style>.skin-blue .sidebar-menu > a, .skin-blue .sidebar-menu > li.active > a {color: yellow;}</style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<!-- CABEÇALHO -->
		<?php include("componentes/cabecalho.php"); ?>

		<!-- MENU -->
		<?php include("componentes/menu.php"); ?>	

		<!-- CONTEÚDO DA PÁGINA -->
		<div class="content-wrapper">
			<section class="content">
				<div class="row">

					<!-- BOX CAMPEONATO -->
					<div class="col-md-4" style="min-width: 300px;">
						<div class="box box-solid box-primary">
							<div class="box-header">
								<h3 class="box-title">Tabela do campeonato</h3>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="tbnet-gadget">
									<div id="tbnet-classificacao">Carregando...	</div>			
									<a id="tbnet-link" href="http://www.tabeladobrasileirao.net/" 
										target="_blank" 
										class="tbnet-link" 
										title="Campeonato Brasileiro">Campeonato Brasileiro</a>
									<script 
										async src="http://gadgetsparablog.com/ws/tabeladobrasileirao/script?funcao=classificacao&campeonato=serie-a" 
										type="text/javascript">
									</script>
								</div>
							</div>
						</div>
					</div>
					<!-- BOX CAMPEONATO -->			

					<!-- BOX PRÓXIMOS JOGOS -->
					<div class="col-md-4" style="min-width: 430px;">
						<div class="box box-solid box-info">
							<div class="box-header">
								<h3 class="box-title">Próximos jogos</h3>
								<div class="box-tools pull-right tabela" style="top: 3px;">
									<div class="has-feedback">
									<form name="tabela" action="tabela.php" method="POST">	
										<select  name="txtTime" id="txtTime" class="form-control" onChange="tabela.submit(this.value);">											
											<?php 					
											$sql="	SELECT 
														times.nomeMinusculo as ID,
														times.nome as Nome
													FROM times_campeonato INNER JOIN times 
														ON timeId = times.id 
													WHERE
														campeonatoId = '". $campeonatoId."' 
													ORDER BY 
														Nome ";
												
											$rs=$conexao->query($sql);
											
											while($reg=mysqli_fetch_array($rs))
											{									
												if ($time == $reg['ID']) {
													$itens = $itens."<option value='".$reg['ID'].
														"'selected='selected'"."'>".$reg['Nome']."</option><br />";									
												}
												else {
													$itens = $itens."<option value='".$reg['ID']."'>".
													$reg['Nome']."</option><br />";	
												}	
											}						
											print $itens;														
											?>         
										</select>
										
										</form>
									</div>
								</div>
							</div>
							
							<div class="box-body">
								<div class="tbnet-gadget"><div id="tbnet-proxjogos">Carregando...</div>
									<a id="tbnet-link" 
										href="http://www.tabeladobrasileirao.net/" 
										target="_blank" class="tbnet-link" title="Brasileirão">Brasileirão</a>
									<script 
									async src="http://gadgetsparablog.com/ws/tabeladobrasileirao/script?funcao=proxjogos&time=<?php print $time; ?>" 
									type="text/javascript"></script>
								</div>			
							</div>
						</div>
					</div>
					<!-- BOX PRÓXIMOS JOGOS -->
				</div>
			</section>
		</div>
		<!-- FINAL DO CONTEÚDO -->

		<!-- RODAPE -->		
		<?php include "componentes/rodape.php"; ?>

		<!-- ENTRAR -->
		<div class="modal fade" id="myModal4" tabindex="-1" role="dialog">			
			<?php include("login/entrar.php"); ?>	
		</div>

		<!-- NOVO CADASTRO -->
		<div class="modal fade" id="myModal5" tabindex="-1" role="dialog">
			<?php include("login/novo_cadastro.php"); ?>
		</div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
</body>

</html>