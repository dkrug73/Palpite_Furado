<?PHP 
	SESSION_START();
	require_once('classes/ConexaoBancoDeDados.php');
	require_once('classes/Campeonato.php');

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$erro = '0';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }	

	$campeonato = new Campeonato($conexao);
	
	$_SESSION['campeonatoId'] = $campeonato->RetornaCampeonatoAtivo();
	
	$participanteId = "0";
    if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}
	


	// carrega a grid de pontos da rodada
	$sqlGrid = "SELECT
				id, 
				nome, 
				email, 
				CASE desativar 
					WHEN 0 THEN 'Ativo' 
					WHEN 1 THEN 'Desativado' 
					END as desativar
			FROM 
				participantes			
			ORDER BY 
				nome";				
				
	$rsGrid=$conexao->query($sqlGrid);
	
	// Recupera parâmetros passados pela página
	$id = "";
	$nome = "";
	$desativar = 0;

	if(isset($_GET['id'])){
		$id = $_GET['id'];
		
		if ($id != "") {
			$sql = "SELECT * FROM participantes WHERE id = '" . $id . "' ";
			
			$rs=$conexao->query($sql);
			$reg=mysqli_fetch_array($rs);
			
			$nome = $reg['nome'];
			$desativar = $reg['desativar'];
		} 
	}	
?>

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Desativar Participante</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" type="image/png" href="imagens/favicon.png">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/utils.js"></script>
	<script src="componentes/js/desativarParticipante.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">
		
		<!-- MENU -->
		<?php include("componentes/menu.php"); ?>

		<div class = "container">

			<div id="salvar-sucesso" style="display:none;">
				<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
					Sucesso desativar participante!
				</div>           
			</div>

			<div id="salvar-erro" style="display:none;">
				<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
					Erro ao desativar participante.
				</div> 
			</div>

			<section class="titulo-pagina">
				<h3 class="tituloPagina">Desativar participante</h3>				
			</section>
		
			<section>	
				<form name="formulario-cadastro" id="formulario-cadastro" method="" action="" 
						enctype="">

					<div class="form-group row">
						<label for="id" class="col-lg-2 col-md-2 col-form-label">ID</label>

						<div class="col-lg-10 col-md-10">
							<input type="text" class="form-control" id="id" name="id" value="" maxlength="100" readonly="yes" requiered>
							<span id="erro-id" style="color: red;font-size: 14px;">  </span>
					</div>
					</div>

					<div class="form-group row">
						<label for="nome-participante" class="col-lg-2 col-md-2 col-form-label">Nome do participante</label>

						<div class="col-lg-10 col-md-10">
							<input type="text" class="form-control" id="nomeParticipante" name="nomeParticipante" value="" maxlength="100" required readonly="yes">
							<span id="erro-participante" style="color: red;font-size: 14px;">  </span>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-2 col-md-2 col-form-label"></div>

						<div class="col-lg-5 col-md-12">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="desativar" name="desativar">
								<label class="custom-control-label" for="desativar">Ativo</label>
							</div>
						</div>
					</div>  			

					<div class="form-group row">
						<div class="col-lg-2 col-md-2 col-form-label"></div>
						
						<div class="col-lg-10 col-md-10">
							<button type="submit" class="btn btn-primary mr-2" id="btn_salvar">Salvar</button>

							<button type="reset" class="btn btn-default mr-2" id="btn_cancelar">Cancelar</button>
						</div>
					</div>
				</form>

				<div id="resultado-tabela"></div>

			</section>
		
		</div>

		<!-- MODAL ENTRAR -->		
		<?php include("modal/entrar.php"); ?>

		<!-- RODAPE -->		
		<?php include("componentes/rodape.php"); ?>
	</div>
	<!-- ./wrapper -->
	
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>