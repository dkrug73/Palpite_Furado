<?php 
	SESSION_START();

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$erro = '0';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }	
	
	$participanteId = "0";
    if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cadastro de Campeonato</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/campeonato.js"></script> 	
	<script src="componentes/js/utils.js"></script>
	<link rel="icon" type="image/png" href="imagens/favicon.png">	
</head>

<body>

	<?php include("componentes/menu.php"); ?>	

	<div class = "container">

		<div id="salvar-sucesso" style="display:none;">
			<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
				Campeonato salva com sucesso!
			</div>           
		</div>

		<div id="salvar-erro" style="display:none;">
			<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
				Erro ao salvar campeonato.
			</div> 
		</div>

		<div id="excluir-sucesso" style="display:none;">
			<div class="alert alert-success text-center mt-2" role="alert" id="alerta">
				Campeonato excluído com sucesso!
			</div>           
		</div>

		<div id="excluir-erro" style="display:none;">
			<div class="alert alert-danger text-center mt-2" role="alert" id="alerta">
				Erro ao excluir campeonato.
			</div> 
		</div>

		<section class="titulo-pagina">
            <h3 class="tituloPagina">Cadastro de campeonato</h3>				
        </section>

		<section>	
			<form name="formulario-cadastro" id="formulario-cadastro" method="" action="" enctype="">

				<div class="form-group row">
					<label for="id" class="col-lg-2 col-md-2 col-form-label">ID</label>

					<div class="col-lg-10 col-md-10">
						<input type="text" class="form-control" id="id" name="id" value="" maxlength="100" readonly='yes'>
						<span id="erro-id" style="color: red;font-size: 14px;">  </span>
					</div>
				</div>

				<div class="form-group row">
                    <label for="descricao" class="col-lg-2 col-md-2 col-form-label">Descrição</label>

                    <div class="col-lg-10 col-md-10">
                        <input type="text" class="form-control" id="descricao" name="descricao" value="" maxlength="100">
                        <span id="erro-descricao" style="color: red;font-size: 14px;">  </span>
                    </div>
                </div>

				<div class="form-group row">
                    <label for="ano" class="col-lg-2 col-md-2 col-form-label">Ano</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="number" class="form-control" id="ano" name="ano" value="" maxlength="4" >
                        <span id="erro-ano" style="color: red;font-size: 14px;">  </span>
                 	</div>
                </div>				

				<div class="form-group row">
                    <label for="serie" class="col-lg-2 col-md-2 col-form-label">Série</label>

                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" id="serie" name="serie" value="" maxlength="1" style="font-variant-caps: all-petite-caps;">
                        <span id="erro-serie" style="color: red;font-size: 14px;">  </span>
                 	</div>
                </div>

				<div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>

                    <div class="col-lg-5 col-md-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ativo" name="ativo">
                            <label class="custom-control-label" for="ativo">Campeonato ativo</label>
                        </div>
                    </div>
                </div>  	

				<div class="form-group row">
                    <div class="col-lg-2 col-md-2 col-form-label"></div>
                    
                    <div class="col-lg-10 col-md-10">
                        <button type="button" class="btn btn-primary mr-2" id="btn-salvar">Salvar</button>

                        <button type="reset" class="btn btn-default mr-2" id="btn-cancelar">Cancelar</button>

                        <button type="button" class="btn btn-danger mr-2" id="btn-excluir">Excluir</button>

                    </div>
                </div>
			</form>

            <div id="resultado-tabela"></div>

		</section>
	</div>

	<!-- MODAL ENTRAR -->		
	<?php include("modal/entrar.php"); ?>
	
	<?php include("componentes/rodape.php"); ?>

	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>