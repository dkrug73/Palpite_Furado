<?php 
	SESSION_START();
	require_once('classes/Participante.php');
	require_once('classes/ConexaoBancoDeDados.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql();  

	$participante = new Participante($conexao);

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];

	$erro = null;
	if (isset($_GET['erro'])){
		$erro = $_GET['erro'];
	}	

	if(isset ($_SESSION['participanteId']) == true) {
		$participante->Id = $_SESSION['participanteId'];

		$participante->CarregarDadosParticipante();
	}	
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Regulamento</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
	<script src="componentes/js/participante.js"></script> 
	<script src="componentes/js/utils.js"></script>	
	
	<link rel="icon" type="image/png" href="imagens/favicon.png">	
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<?php include("componentes/menu.php"); ?>	

		<div class = "container">
			<section class="titulo-pagina">
				<h3 class="tituloPagina">Regulamento</h3>				
       		</section>
            		
			<div class="box-body"> 

				<h3>1. Como participar</h3>				
				<ul>							
					<P><LI>Para participar, basta preencher os dados do cadastro para obter login e senha para participação. 
						Caso você já tenha participado das edições anteriores não haverá a necessidade de um outro cadastro. </li></P>

					<P><LI>Após concluir o cadastro basta acessar a página Palpites e sugerir resultados para os jogos da rodada atual 
						e/ou posteriores e clicar no botão Gravar. É importante conferir a mensagem de retorno informando que os palpites 
						foram gravados com sucesso.</li></P>

					<P><LI>Caso haja falha ao gravar os resultados, estes deverão ser enviados para o email  
						<a href="mailto:dkrug73@gmail.com?subject=Palpites da rodada">dkrug73@gmail.com </a> dentro do tempo hábil 
						para informar os palpites.</li></P>

					<P><LI>Ao participar do bolão fica entendido que o participante leu e aceitou o regulamento em vigor. </li></P>								
				</ul> 					

				<h3>2. Condições de Participação</h3>				
				<ul>							
					<P><LI>Será aceito somente 01 (um) cadastro por participante e, por conseguinte, terá apenas um login e uma senha para participar 
						durante todo campeonato. Esse controle de inscrição única será feito através do email informado pelo participante no cadastro.</li></P>
						
					<P><LI>Serão considerados válidos somente os palpites dos participantes que o fizerem exclusivamente durante o horário permitido, ou seja, 
						em até <strong>2 (duas) horas antes do início da rodada</strong>, sendo que até este momento os participantes poderão modificar seus palpites.</li></P>

					<P><LI>Transcorrido o tempo limite para gravar os palpites da rodada (2 horas antes do início do primeiro jogo da rodada) 
					<strong>TODOS os jogos da rodada em andamento serão bloqueados para inclusão ou alteração de palpites</strong>. Não serão aceitos palpites 
						fora do horário programado em hipótese alguma. Caso sejam enviados por email, serão consideradas a data e hora do envio.</li></P>

					<P><LI>Caso haja falha no horário cadastrado dos jogos ou qualquer fator extracampo e algum participante informar ou alterar indevidamente 
						os resultados dos jogos, terão seus palpites ANULADOS e a pontuação ficará zerada para a rodada, sem possibilidade de reconsideração 
						ou retorno ao palpite anterior.</li></P>

					<P><LI>Para postar os palpites é obrigatório efetuar o login. Se o participante não estiver conseguindo acessar a página, 
						me envie um email informando o problema, juntamente com os palpites da rodada.</li></P>

					<P><LI>Os resultados das apurações dos palpites serão publicados sempre em até 12 (doze) horas após do término da rodada, 
						conforme o calendário oficial do respectivo campeonato. A apuração dos resultados se dará com a verificação dos palpites indicados 
						pelos participantes em confronto com os resultados oficiais dos jogos.</li></P>

					<P><LI>A utilização do Palpite Padrão tem por objetivo gravar o mesmo resultado para os jogos da rodada em caso de esquecimento 
						ou impossibilidade de o participante informar seus palpites. <strong>Este artifício deverá ser utilizado somente em último caso e 
						esporadicamente</strong>. Ficando caracterizada a constante utilização do Palpite Padrão o participante será removido do bolão 
						sem aviso prévio.</li></P>

					<P><LI>Informar o mesmo resultado para todos os jogos da rodada poderá ser considerado ou subentendido como uso do Palpite Padrão, 
						ficando assim o participante sujeito a exclusão do bolão.</li></P>								
				</ul>
					
				<h3>3. Pontuação</h3>				
				<ul>
					<P><LI> A pontuação dos participantes será feita em função de seus prognósticos para o resultado e o placar das partidas.</li></P>
					
					<P><LI>Se o palpite do participante corresponder ao resultado e placar exatos do jogo (das duas equipes), ele terá direito a 
						seguinte pontuação, de acordo com a tabela abaixo:</li></P>

					<div class="tabelas">	
						<div class="box3">
							<div class="box-header">
								<h3 class="box-title">Pontuação de cada jogo</h3>
							</div>
							<div class="box-body no-padding">
								<table class="table table-striped">											
									<tr><td>Vitória mandante</td> <td>10 pontos</td></tr>
									<tr><td>Vitória visitante</td> <td>15 pontos</td></tr>
									<tr><td>Empate</td> <td>15 pontos</td></tr>    
									<tr><td>Placar</td> <td>5 pontos</td></tr>  
								</table>
							</div>
						</div>
					</div>

					<p><li>A pontuação total do jogo será a soma dos acertos, de acordo com a tabela. Por exemplo:</li></p>

					<div class="tabelas">	
						<div  class="box5">
							<div class="box-header">
								<h3 class="box-title">Exemplos de pontuação</h3>
							</div>
							<div class="box-body no-padding">
								<table class="table table-striped">	
									<tr>
										<th>Condição</th>
										<th>Vitória mandante</th>
										<th>Vitória visitante</th>
										<th>Empate</th>
										<th>Placar mandante</th>
										<th>Placar visitante</th>
										<th style="width: 115px;">Total</th>
									</tr>

									<tr>
										<td>Acertar somente vitória mandante</td> 
										<td>10</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td style="font-weight: bolder;">10 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente vitória visitante</td> 
										<td>0</td>
										<td>15</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td style="font-weight: bolder;">15 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente empate</td> 
										<td>0</td>
										<td>0</td>
										<td>15</td>
										<td>0</td>
										<td>0</td>
										<td style="font-weight: bolder;">15 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente placar mandante</td> 
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>5</td>
										<td>0</td>
										<td style="font-weight: bolder;">5 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente placar visitante</td> 
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>5</td>
										<td style="font-weight: bolder;">5 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente vitória mandante e um placar</td> 
										<td>10</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>5</td>
										<td style="font-weight: bolder;">15 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente vitória visitante e um placar</td> 
										<td>0</td>
										<td>15</td>
										<td>0</td>
										<td>5</td>
										<td>0</td>
										<td style="font-weight: bolder;">20 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente empate e um placar</td> 
										<td>0</td>
										<td>0</td>
										<td>15</td>
										<td>5</td>
										<td>0</td>
										<td style="font-weight: bolder;">20 pontos</td>
									</tr>

									<tr>
										<td>Acertar na mosca vitória mandante</td> 
										<td>10</td>
										<td>0</td>
										<td>0</td>
										<td>5</td>
										<td>5</td>
										<td style="font-weight: bolder;">20 pontos</td>
									</tr>
									
									<tr>
										<td>Acertar na mosca vitória visitante</td> 
										<td>0</td>
										<td>15</td>
										<td>0</td>
										<td>5</td>
										<td>5</td>
										<td style="font-weight: bolder;">25 pontos</td>
									</tr>

									<tr>
										<td>Acertar na mosca vitória visitante</td> 
										<td>0</td>
										<td>0</td>
										<td>15</td>
										<td>5</td>
										<td>5</td>
										<td style="font-weight: bolder;">25 pontos</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<br>
					<p><li>Se for palpite padrão, valerá apenas 70% do valor da pontuação e o total geral da rodada será arredondado para um múltiplo de 5. Por exemplo:</li></p>

					<div class="tabelas">	
						<div class="box5">
							<div class="box-header">
								<h3 class="box-title">Exemplos de pontuação para palpite padrão</h3>
							</div>
							<div class="box-body no-padding">
								<table class="table table-striped">	
									<tr>
										<th>Condição</th>
										<th>Vitória mandante</th>
										<th>Vitória visitante</th>
										<th>Empate</th>
										<th>Placar mandante</th>
										<th>Placar visitante</th>
										<th style="width: 115px;">Total</th>
									</tr>

									<tr>
										<td>Acertar somente vitória mandante</td> 
										<td>7</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td style="font-weight: bolder;">7 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente vitória visitante</td> 
										<td>0</td>
										<td>11</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td style="font-weight: bolder;">11 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente empate</td> 
										<td>0</td>
										<td>0</td>
										<td>11</td>
										<td>0</td>
										<td>0</td>
										<td style="font-weight: bolder;">11 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente placar mandante</td> 
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>4</td>
										<td>0</td>
										<td style="font-weight: bolder;">4 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente placar visitante</td> 
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>4</td>
										<td style="font-weight: bolder;">4 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente vitória mandante e um placar</td> 
										<td>7</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>4</td>
										<td style="font-weight: bolder;">11 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente vitória visitante e um placar</td> 
										<td>0</td>
										<td>11</td>
										<td>0</td>
										<td>4</td>
										<td>0</td>
										<td style="font-weight: bolder;">15 pontos</td>
									</tr>

									<tr>
										<td>Acertar somente empate e um placar</td> 
										<td>0</td>
										<td>0</td>
										<td>11</td>
										<td>4</td>
										<td>0</td>
										<td style="font-weight: bolder;">15 pontos</td>
									</tr>

									<tr>
										<td>Acertar na mosca vitória mandante</td> 
										<td>7</td>
										<td>0</td>
										<td>0</td>
										<td>4</td>
										<td>4</td>
										<td style="font-weight: bolder;">15 pontos</td>
									</tr>
									
									<tr>
										<td>Acertar na mosca vitória visitante</td> 
										<td>0</td>
										<td>11</td>
										<td>0</td>
										<td>4</td>
										<td>4</td>
										<td style="font-weight: bolder;">19 pontos</td>
									</tr>

									<tr>
										<td>Acertar na mosca vitória visitante</td> 
										<td>0</td>
										<td>0</td>
										<td>11</td>
										<td>4</td>
										<td>4</td>
										<td style="font-weight: bolder;">19 pontos</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					
					<P><LI> Ao final de cada rodada, será divulgado o ranking dos participantes.</li></P>
					<P><LI> A verificação do resultado do presente Projeto é de inteira responsabilidade dos participantes.</li></P>								
					<P><LI> Se alguém entrar no bolão no decorrer do campeonato, iniciará com a mesma pontuação do último colocado.</li></P>
					<P><LI> Para o segundo turno, a pontuação será zerada. No entanto, terá uma segunda tabela com a classificação geral 
						(soma do primeiro e segundo turno). </li></P>
				</ul>
							
				<h3>4. Disposições Gerais</h3>
				<ul>
					<P><LI> Se alguém esquecer de postar os palpites, os pontos serão computados de acordo com o palpite padrão informado.
						Se não houver palpite padrão, os pontos do usuário referente a esta rodada serão zerados.</li></P>
					
					<P><LI> O participante será desclassificado e seu cadastro removido, automaticamente, em caso de fraude comprovada ou pelo não cumprimento 
						de quaisquer das condições estabelecidas neste regulamento, sendo terminantemente proibida a utilização de sistemas, softwares e outras ferramentas 
						ou métodos automáticos, repetitivos ou programados, que criem condições de cadastramento, navegação ou participação, consideradas pela 
						Comissão como práticas irregulares, desleais ou que atentem contra os objetivos desta promoção. 
						A Comissão poderá, a seu critério, suspender ou excluir a participante no caso de suspeita ou indícios de que ela tenha se valido de tais artifícios.</li></P>
						
					<P><LI> Em momento algum poderá o Realizador ser responsabilizado por inscrições perdidas, atrasadas, enviadas erroneamente, incompletas, 
						incorretas, inválidas ou imprecisas. O Realizador não será responsável por problemas, falhas ou funcionamento técnico, de qualquer tipo, 
						em redes de computadores, servidores ou provedores, equipamentos de computadores, hardware ou software, ou erro, interrupção, defeito, atraso ou falha 
						em operações ou transmissões para o correto processamento de inscrições, incluindo, mas não se limitando, a transmissão imprecisa de inscrições ou falha 
						do Realizador em recebê-las, em razão de problemas técnicos, congestionamento na internet ou no site ligado ao Projeto, vírus, falha de programação (bugs) 
						ou violação por terceiros (hackers).</li></P>
						
					<P><LI> O presente Regulamento poderá ser alterado e/ou o Projeto suspenso ou cancelado, sem aviso prévio, por motivo de força maior ou por qualquer outro ou motivo 
						que esteja fora do controle do Realizadoroa e que comprometa a realização do Projeto de forma a impedir ou modificar substancialmente a sua condução como 
						originalmente planejado.</li></P>
						
					<P><LI> Para esclarecer eventuais dúvidas sobre o presente Projeto, os participantes poderão entrar em contato pelo e-mail 
						<a href="mailto:dkrug73@gmail.com?subject=Dúvidas sobre o bolão">dkrug73@gmail.com</a>.</P>

					<P><LI> Quaisquer dúvidas, divergências ou situações não previstas neste Regulamento serão julgadas e decididas de forma soberana e 
						irrecorrível pela Comissão do Bolão.</li></P>
						
					<P><LI> A participação neste Projeto implica a aceitação total e irrestrita de todos os itens deste Regulamento.</li></P>			
				</ul> 
			</div>
          		</div>
			</section>
		</div>

		<!-- RODAPE -->			
		<?php include("componentes/rodape.php"); ?>

		<!-- MODAL ENTRAR -->		
		<?php include("modal/entrar.php"); ?>

	</div>
	<!-- ./wrapper -->

	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>