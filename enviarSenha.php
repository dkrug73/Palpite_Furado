<?php
    SESSION_START();
	require_once('classes/ConexaoBancoDeDados.php');
    require_once('classes/Participante.php');
    require_once('dist/class/PHPMailerAutoload.php');

	$conexaoBancoDeDados = new ConexaoBancoDeDados();
    $conexao = $conexaoBancoDeDados->ConectarMySql(); 

    $email = $_POST["email"];

    $senha = geraSenha(6, false, true);

    $sqlEnviar = "SELECT id FROM participantes WHERE email = '$email' ";

    $rs=$conexao->query($sqlEnviar);

    $resultado = false;

    if ($rs->num_rows == 1){
        $sql = "UPDATE participantes SET senha = MD5('$senha') WHERE email = '$email' ";

        $resultado = $conexao->query($sql);

        ini_set('display_errors', 1);

        error_reporting(E_ALL);

        $from = "palpitefurado@palpitefurado.com";

        $to = $email;

        $subject = "Alteração de senha";

        $message = "Sua nova senha é: $senha";

        $headers = "De:". $from;

        mail($to, $subject, $message, $headers);
    }

    mysqli_close($conexao);

    echo $resultado;


function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
{
    /**
    * @param integer $tamanho Tamanho da senha a ser gerada
    * @param boolean $maiusculas Se terá letras maiúsculas
    * @param boolean $numeros Se terá números
    * @param boolean $simbolos Se terá símbolos
    *
    * @return string A senha gerada
    */
    $lmin = 'abcdefghijklmnopqrstuvwxyz';
    $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $num = '1234567890';
    $simb = '!@#$%*-';
    $retorno = '';
    $caracteres = '';
    $caracteres .= $lmin;
    
    if ($maiusculas) $caracteres .= $lmai;
    
    if ($numeros) $caracteres .= $num;
    
    if ($simbolos) $caracteres .= $simb;
    
    $len = strlen($caracteres);
    for ($n = 1; $n <= $tamanho; $n++) {
        $rand = mt_rand(1, $len);
        $retorno .= $caracteres[$rand-1];
    }

    return $retorno;
}