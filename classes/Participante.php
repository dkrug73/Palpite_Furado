<?php

class Participante{
    public $Id;
    public $Nome;
    public $NomeUsuario;
    public $Administrador;
    public $Email;
    public $Senha;
    public $ReceberEmail;
    public $PadraoMandante;
    public $PadraoVisitante;
    public $Foto;
    public $Nivel;
    public $AceitouTermos;
    public $Desativar;
    private $Conexao;
    
    public function setId ($parametroId){
        $this->Id = $parametroId;
    }

    public function getId(){
        return $this->Id;
    }   
    
    public function setNome ($parametroNome){
        $this->Nome = $parametroNome;
    }

    public function getNome(){
        return $this->Nome;
    }   

    public function setNomeUsuario ($parametroNomeUsuario){
        $this->NomeUsuario = $parametroNomeUsuario;
    }

    public function getNomeUsuario(){
        return $this->NomeUsuario;
    }   

    public function setAdministrador ($parametroAdministrador){
        $this->Administrador = $parametroAdministrador;
    }

    public function getAdministrador(){
        return $this->Administrador;
    }   

    public function setEmail ($parametroEmail){
        $this->Email = $parametroEmail;
    }

    public function getEmail(){
        return $this->Email;
    }   

    public function setSenha ($parametroSenha){
        $this->Senha = $parametroSenha;
    }

    public function getSenha(){
        return $this->Senha;
    }   

    public function setReceberEmail ($parametroReceberEmail){
        $this->ReceberEmail = $parametroReceberEmail;
    }

    public function getReceberEmail(){
        return $this->ReceberEmail;
    }   

    public function setPadraoMandante ($parametroPadraoMandante){
        $this->PadraoMandante = $parametroPadraoMandante;
    }

    public function getPadraoMandante(){
        return $this->PadraoMandante;
    }   

    public function setPadraoVisitante ($parametroPadraoVisitante){
        $this->PadraoVisitante = $parametroPadraoVisitante;
    }

    public function getPadraoVisitante(){
        return $this->PadraoVisitante;
    }   

    public function setFoto ($parametroFoto){
        $this->Foto = $parametroFoto;
    }

    public function getFoto(){
        return $this->Foto;
    }   

    public function setNivel ($parametroNivel){
        $this->Nivel = $parametroNivel;
    }

    public function getNivel(){
        return $this->Nivel;
    }   

    public function setAceitouTermos ($parametroAceitouTermos){
        $this->AceitouTermos = $parametroAceitouTermos;
    }

    public function getAceitouTermos(){
        return $this->AceitouTermos;
    }  

    public function setDesativar ($parametroDesativar){
        $this->Desativar = $parametroDesativar;
    }

    public function getDesativar(){
        return $this->Desativar;
    }  
    
    public function setConexao ($parametroConexao){
        $this->Conexao = $parametroConexao;
    }

    function __construct($conexao){
        $this->Conexao = $conexao;
    }   

    public function GravarParticipante(){ 
        $resultado = null;       

        if(empty($this->Id)){   
            $validarParticipante = $this->ValidarNomeUsuarioEmailCadastro(); 

            if ($validarParticipante > 0) return $validarParticipante;

            $sql = "
                INSERT INTO participantes (
                      nome,
                      nomeUsuario,
                      email,
                      senha,
                      receberemail,
                      padraoMandante,
                      padraoVisitante,
                      aceitouTermos,
                      foto) 
                VALUES (
                      '$this->Nome',
                      '$this->NomeUsuario', 
                      '$this->Email', 
                      '$this->Senha', 
                      '$this->ReceberEmail', 
                      '$this->PadraoMandante', 
                      '$this->PadraoVisitante',
                      '1',
                      '$this->Foto') ";

            $resultado = $this->Conexao->query($sql);
            $this->Id = $this->Conexao->insert_id;  
        }
        else{
            $sql = "
                UPDATE 
                    participantes 
                SET 
                    nome ='$this->Nome', 
                    nomeUsuario = '$this->NomeUsuario', 
                    email = '$this->Email', ";                        
            if (isset($this->Foto)){
                $sql = $sql."foto = '$this->Foto', ";
            } 
            
            $sql = $sql."senha = '$this->Senha', ";
            $sql = $sql." padraoMandante = '$this->PadraoMandante', ";     
            $sql = $sql." padraoVisitante = '$this->PadraoVisitante' ";     
            
            $sql = $sql."WHERE
                    id = '$this->Id' "; 
                    
           $resultado = mysqli_query($this->Conexao, $sql);         

        }

        if ($resultado) {
            $this->AtualizaVariaveisSessao();
            return 0;
        }
        else return 5;
    }
    
    public function ValidarNomeUsuarioEmailCadastro() {
        $sql = "
            SELECT 
                nomeUsuario,
                email 
            FROM 
                participantes 
            WHERE 
                nomeUsuario = '$this->NomeUsuario' OR
                email = '$this->Email' 
            LIMIT 1 ";           

        $rs=$this->Conexao->query($sql);
        $reg = mysqli_fetch_array($rs); 

        if ($rs->num_rows == 0) return 0;

        $nomeUsuario = $reg['nomeUsuario']; 
        $email = $reg['email']; 

        if (strtoupper($nomeUsuario) == strtoupper($this->NomeUsuario)) return 3;
        elseif (strtoupper($email) == strtoupper($this->Email)) return 4;
        else return 0;
    }

    public function ObterDadosParticipante($email, $senha){
        $sql = "
            SELECT 
                * 
            FROM 
                participantes 
            WHERE 
                (email = '$email' OR
                nomeUsuario = '$email') AND
                senha = '$senha' ";

        $rs=$this->Conexao->query($sql);

        $reg = mysqli_fetch_array($rs); 

        $this->Id = $reg['id']; 
        $this->Nome = $reg['nome']; 
        $this->NomeUsuario = $reg['nomeUsuario']; 
        $this->Email = $reg['email']; 
        $this->Senha = $reg['senha']; 
        $this->PadraoMandante = $reg['padraoMandante']; 
        $this->PadraoVisitante = $reg['padraoVisitante'];	
        $this->ReceberEmail = $reg['receberEmail']; 
        $this->Foto = $reg['foto']; 
        $this->Nivel = $reg['nivel']; 
    }
    
    public function ValidarParticipante($campeonatoId, $rodadaPalpite){
        $sql = "
            SELECT 
                id 
            FROM 
                participantes_campeonato  
            WHERE 
                campeonatoId = '$campeonatoId' AND 
                participanteId = '$this->Id' ";

        $rs=$this->Conexao->query($sql);
        $reg = mysqli_fetch_array($rs);

        if ($reg['id'] == "" && $this->Id != "") {
            $sql_insere = "
                INSERT INTO participantes_campeonato (participanteId, campeonatoId) VALUES ('$this->Id','$campeonatoId') ";	

            $this->Conexao->query($sql_insere);

            $this->CarregarDadosParticipante();

            $turno = $this->RetornaTurnoDaRodada($campeonatoId, $rodadaPalpite);
            
            $rodada = new Rodada($this->Conexao);
            $rodada->ParticipanteId = $this->Id;
            $rodada->NomeParticipante = $this->Nome;
            $rodada->FotoParticipante = $this->Foto;
            $rodada->CampeonatoId = $campeonatoId;
            $rodada->Turno = $turno;
            $rodada->PontosRodada = '0';
            $rodada->NaMosca = '0';                  
            $rodada->TotalNaMosca = '0';
            $rodada->PosicaoRodada = '0';

            if ($rodadaPalpite == "1" or $rodadaPalpite == "20") {
                $rodada->Rodada = $rodadaPalpite;
                $rodada->TotalPontos = '0';
            }
            else {
                $dados = $rodada->RetornaDadosUltimoColocado($rodada, $campeonatoId, ($rodadaPalpite - 1));
            }
            
            $rodada->Id = $rodada->IncluirRodada($rodada);
        }
    }

    public function CarregarDadosParticipante() {
        $sql="
            SELECT 
                * 
            FROM 
                participantes 
            WHERE 
                id='$this->Id' ";	
    
        $rs=$this->Conexao->query($sql);
        $reg=mysqli_fetch_array($rs);
    
        $this->Nome = $reg['nome'];
        $this->NomeUsuario = $reg['nomeUsuario'];
        $this->Email = $reg['email'];
        $this->Senha = $reg['senha'];
        $this->Foto = $reg['foto'];
        $this->ReceberEmail = $reg['receberemail'];
        $this->PadraoMandante = $reg['padraoMandante'];
        $this->PadraoVisitante = $reg['padraoVisitante'];
        $this->AceitouTermos = $reg['aceitouTermos'];
    }

    public function RetornaTurnoDaRodada($campeonatoId, $rodada) {
        $sql = "
            SELECT DISTINCT 
                turno 
            FROM 
                jogos 
            WHERE 
                campeonatoId = '$campeonatoId' AND 
                rodada = '$rodada' ";	
    
        $rs=$this->Conexao->query($sql);	
        $reg=mysqli_fetch_array($rs);
        
        return $reg['turno'];
    }

    public function DesativarCadastro(){
        $sql = "
            UPDATE 
                participantes 
            SET 
                desativar = $this->Desativar 
            WHERE 
                id = $this->Id ";	
        
        return mysqli_query($this->Conexao, $sql);   
    }

    public function AtualizaVariaveisSessao() {
            
            unset($_SESSION['participanteId']);
            unset($_SESSION['participanteNome']);
            unset($_SESSION['nomeUsuario']);
            unset($_SESSION['email']);
            unset($_SESSION['senha']);
            unset($_SESSION['padraoMandante']);
            unset($_SESSION['padraoVisitante']);
            unset($_SESSION['receberemail']);
            unset($_SESSION['foto']);
            unset($_SESSION['nivel']);
            unset($_SESSION['logado']);  

        $_SESSION['participanteId'] = $this->Id;
        $_SESSION['participanteNome'] = $this->Nome;
        $_SESSION['nomeUsuario'] = $this->NomeUsuario;
        $_SESSION['email'] = $this->Email;
        $_SESSION['senha'] = $this->Senha;
        $_SESSION['padraoMandante'] = $this->PadraoMandante;
        $_SESSION['padraoVisitante'] = $this->PadraoVisitante;
        $_SESSION['receberemail'] = $this->ReceberEmail;
        $_SESSION['foto'] = $this->Foto;
        $_SESSION['nivel'] = 0;
        $_SESSION['logado'] = true;      
    }

    public function FecharConexao(){
        mysqli_close($this->Conexao);
        $this->Conexao = null;   
    }
}
?>