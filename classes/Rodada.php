<?php

class Rodada {
    public $Id;
    public $ParticipanteId;
    public $NomeParticipante;
    public $FotoParticipante;
    public $CampeonatoId;
    public $Turno;
    public $Rodada;
    public $PontosRodada;
    public $NaMosca;
    public $TotalPontos;
    public $TotalNaMosca;
    public $PosicaoRodada;
    public $PosicaoRodadaAnterior;
    private $Conexao;
    
    public function setId ($parametroId){
        $this->Id = $parametroId;
    }

    public function getId(){
        return $this->Id;
    }  
    
    public function setParticipanteId ($parametroParticipanteId){
        $this->ParticipanteId = $parametroParticipanteId;
    }

    public function getParticipanteId(){
        return $this->ParticipanteId;
    }  

    public function setNomeParticipante ($parametroNomeParticipante){
        $this->NomeParticipante = $parametroNomeParticipante;
    }

    public function getNomeParticipante(){
        return $this->NomeParticipante;
    }  

    public function setFotoParticipante ($parametroFotoParticipante){
        $this->FotoParticipante = $parametroFotoParticipante;
    }

    public function getFotoParticipante(){
        return $this->FotoParticipante;
    }  

    public function setCampeonatoId ($parametroCampeonatoId){
        $this->CampeonatoId = $parametroCampeonatoId;
    }

    public function getCampeonatoId(){
        return $this->CampeonatoId;
    }  

    public function setTurno ($parametroTurno){
        $this->Turno = $parametroTurno;
    }

    public function getTurno(){
        return $this->Turno;
    }  

    public function setRodada ($parametroRodada){
        $this->Rodada = $parametroRodada;
    }

    public function getRodada(){
        return $this->Rodada;
    }  

    public function setPontosRodada ($parametroPontosRodada){
        $this->PontosRodada = $parametroPontosRodada;
    }

    public function getPontosRodada(){
        return $this->PontosRodada;
    }  
    
    public function setNaMosca ($parametroNaMosca){
        $this->NaMosca = $parametroNaMosca;
    }

    public function getNaMosca(){
        return $this->NaMosca;
    }  

    public function setTotalPontos ($parametroTotalPontos){
        $this->TotalPontos = $parametroTotalPontos;
    }

    public function getTotalPontos(){
        return $this->TotalPontos;
    } 

    public function setTotalNaMosca ($parametroTotalNaMosca){
        $this->TotalNaMosca = $parametroTotalNaMosca;
    }

    public function getTotalNaMosca(){
        return $this->TotalNaMosca;
    } 

    public function setPosicaoRodada ($parametroPosicaoRodada){
        $this->PosicaoRodada = $parametroPosicaoRodada;
    }

    public function getPosicaoRodada(){
        return $this->PosicaoRodada;
    } 

    public function setPosicaoRodadaAnterior ($parametroPosicaoRodadaAnterior){
        $this->PosicaoRodadaAnterior = $parametroPosicaoRodadaAnterior;
    }

    public function getPosicaoRodadaAnterior(){
        return $this->PosicaoRodadaAnterior;
    } 

    public function setConexao ($parametroConexao){
        $this->Conexao = $parametroConexao;
    }

    function __construct($conexao){
        $this->Conexao = $conexao;
    }   

      public function IncluirRodada($rodada){
            $sql = "
            INSERT INTO rodada (
                        participanteId, 
                        nomeParticipante, 
                        fotoParticipante, 
                        campeonatoId, 
                        turno,
                        rodada, 
                        pontosRodada, 
                        naMosca, 
                        totalPontos, 
                        totalNaMosca, 
                        posicaoRodada)
            VALUES (
                        '$rodada->ParticipanteId',
                        '$rodada->NomeParticipante',
                        '$rodada->FotoParticipante',
                        '$rodada->CampeonatoId',
                        '$rodada->Turno',
                        '$rodada->Rodada',
                        '$rodada->PontosRodada',
                        '$rodada->NaMosca', 
                        '$rodada->TotalPontos',
                        '$rodada->TotalNaMosca', 
                        '$rodada->PosicaoRodada') ";

            $this->Conexao->query($sql);

            return $this->Conexao->insert_id; 
      } 

      public function GravarPontuacaoRodada($objRodada, $tabela) {            
            $sql = "
                  SELECT 
                        id 
                  FROM 
                        $tabela 
                  WHERE 
                        participanteId = $objRodada->ParticipanteId AND 
                        rodada = $objRodada->Rodada AND 
                        turno = $objRodada->Turno AND 
                        campeonatoId = $objRodada->CampeonatoId ";               
            
            $rs=$this->Conexao->query($sql);

            $reg=mysqli_fetch_array($rs);
            
            $rodadaId = $reg['id'];

            if (empty($rodadaId)) {
                  return $objRodada->InserirRodada($objRodada, $tabela);
            }
            else {
                  $objRodada->Id = $rodadaId;
                  return $objRodada->AlterarRodada($objRodada, $tabela);
            }
      }

      private function InserirRodada($objRodada, $tabela){
            $sql = "
                  INSERT INTO $tabela (
                        participanteid,
                        nomeparticipante,
                        fotoparticipante,
                        campeonatoid,
                        turno,
                        rodada,
                        pontosrodada,
                        namosca,
                        posicaorodada,
                        posicaorodadaanterior,
                        totalpontos,
                        totalnamosca)
                  VALUES (
                        '$objRodada->ParticipanteId',
                        '$objRodada->NomeParticipante',
                        '$objRodada->FotoParticipante',
                        '$objRodada->CampeonatoId',
                        '$objRodada->Turno',
                        '$objRodada->Rodada',
                        '$objRodada->PontosRodada',
                        '$objRodada->NaMosca',
                        '$objRodada->PosicaoRodada',
                        '$objRodada->PosicaoRodadaAnterior',
                        '$objRodada->TotalPontos',
                        '$objRodada->TotalNaMosca') "; 

            return $this->Conexao->query($sql);	  
      }

      private function AlterarRodada($objRodada, $tabela){
            $sql = "
                  UPDATE $tabela SET
                        turno = '$objRodada->Turno',
                        rodada = '$objRodada->Rodada',
                        pontosRodada = '$objRodada->PontosRodada',
                        naMosca = '$objRodada->NaMosca',
                        posicaoRodada = '$objRodada->PosicaoRodada',
                        posicaoRodadaAnterior = '$objRodada->PosicaoRodadaAnterior',
                        totalPontos = '$objRodada->TotalPontos',
                        totalNaMosca = '$objRodada->TotalNaMosca' 								
                  WHERE 
                        id = $objRodada->Id "; 
                        
            return $this->Conexao->query($sql);	                
      }

      public function GravaPontuacaoPosicaoRodadaParticipante() { 
            $maximoPalpitesPontos = $this->RetornaRodadaMaximoPontos();
            
            for ($contadorRodada = $this->Rodada; $contadorRodada <= $maximoPalpitesPontos; $contadorRodada++) {    
                  if (!$this->AtualizarTabelaRodadaGeral($contadorRodada)) return false;
            }	

            return true;
      }

      public function AtualizarTabelaRodada($rodada) {
            $sqlApagar = "DELETE FROM rodada WHERE rodada = $rodada and campeonatoId = $this->CampeonatoId ";                   
            if(!$this->Conexao->query($sqlApagar)) return false;

            $posicaoRodada = 0;			
            $rodadaInicial = 1;
            
            $sql = "
                  SELECT
                        participanteId,
                        participantes.nome as nomeParticipante,
                        participantes.foto,
                        SUM(pontosRodada) AS pontos, 
                        SUM(naMosca) AS naMosca,
                        rodada,
                        turno,
                        palpites.padrao,
                        (
                              SELECT 
                                    SUM(pp.pontosRodada) 
                              FROM 
                                    palpites pp 
                              WHERE 
                                    pp.participanteId = palpites.participanteId AND 
                                    pp.campeonatoId = palpites.campeonatoId AND 
                                    pp.turno = palpites.turno AND 
                                    pp.rodada <= palpites.rodada
                        ) as totalPontos,
                        (
                              SELECT 
                                    SUM(pp.naMosca) 
                              FROM 
                                    palpites pp 
                              WHERE 
                                    pp.participanteId = palpites.participanteId AND 
                                    pp.campeonatoId = palpites.campeonatoId AND 
                                    pp.turno = palpites.turno AND 
                                    pp.rodada <= palpites.rodada
                        ) as totalNaMosca						
                  FROM 
                        palpites INNER JOIN 
                        participantes ON participanteId = participantes.id
                  WHERE 
                        campeonatoId = $this->CampeonatoId AND 
                        rodada = $rodada AND
                        participanteId IN (SELECT id FROM participantes WHERE desativar is null OR desativar = 0)
                  GROUP BY 
                        participanteId 
                  ORDER BY 
                        rodada DESC,
                        totalPontos DESC,
                        totalNaMosca DESC,
                        nomeParticipante ASC ";           
                                          
            $rs=$this->Conexao->query($sql);	

            $turno = "";

            while ($reg=mysqli_fetch_array($rs)) {
                  $posicaoRodada += 1;
                  $rodadaParticipante = new Rodada($this->Conexao);

                  $padrao = $reg['padrao'];
                  $turno = $reg['turno'];

                  $rodadaParticipante->ParticipanteId = $reg['participanteId'];
                  $rodadaParticipante->NomeParticipante = $reg['nomeParticipante'];
                  $rodadaParticipante->FotoParticipante = $reg['foto'];
                  $rodadaParticipante->Turno = $reg['turno'];                  
                  $rodadaParticipante->PontosRodada = $reg['pontos'];
                  $rodadaParticipante->NaMosca = $reg['naMosca'];
                  $rodadaParticipante->CampeonatoId = $this->CampeonatoId;
                  $rodadaParticipante->Rodada = $rodada;
                  $rodadaParticipante->PosicaoRodada = $posicaoRodada; 
                  $rodadaParticipante->TotalPontos = $reg['totalPontos'];
                  $rodadaParticipante->TotalNaMosca = $reg['totalNaMosca'];

                  $dadosRodadaAnterior = $rodadaParticipante->RetornaPontuacaoRodadaAnterior($rodadaParticipante, "rodada");
                  $pontosRodadaAnterior = $dadosRodadaAnterior['0'];	
                  $naMoscaRodadaAnterior = $dadosRodadaAnterior['1'];				
                  $posicaoRodadaAnterior = $dadosRodadaAnterior['2'];	  

                  $rodadaParticipante->TotalPontos = $pontosRodadaAnterior + $rodadaParticipante->PontosRodada;
                  $rodadaParticipante->TotalNaMosca = $naMoscaRodadaAnterior + $rodadaParticipante->NaMosca; 
                  $rodadaParticipante->PosicaoRodadaAnterior = $posicaoRodadaAnterior;                     

                  $dadosRodadaAnterior = $rodadaParticipante->RetornaPontuacaoRodadaAnterior($rodadaParticipante, "rodada");
                  $rodadaParticipante->PosicaoRodadaAnterior = $dadosRodadaAnterior['2'];	
                  
                  if (!$rodadaParticipante->InserirRodada($rodadaParticipante, "rodada")) return false;    
                        
                  unset($rodadaParticipante);                        
            }	

            $this->RetornaPontuacaoInicial($rodada, $turno, $posicaoRodada+1);

            return true;
      }      

      public function AtualizarTabelaRodadaGeral($rodada) {
            $sqlApagar = "DELETE FROM rodadaGeral WHERE rodada = $rodada and campeonatoId = $this->CampeonatoId ";                   
            if(!$this->Conexao->query($sqlApagar)) return false;

            $rodadaInicial = 1;

            $sql = "
                  SELECT
                        participanteId,
                        nomeParticipante,
                        fotoParticipante,
                        turno,
                        naMosca,
                        pontosRodada,
                        naMosca,
                        totalNaMosca						
                  FROM 
                        rodada
                  WHERE 
                        campeonatoId = $this->CampeonatoId AND 
                        rodada = $rodada AND
                        participanteId IN (SELECT id FROM participantes WHERE desativar is null OR desativar = 0)
                  ORDER BY 
                        pontosRodada desc, 
                        naMosca desc, 
                        nomeParticipante asc";

            $rs=$this->Conexao->query($sql);	
            
            $somarPosicao = true;
            $posicaoRodada = 1;
            $countPosicao = 0;
            $posicao = 0;
            $pontosAnterior = 0;
            $naMoscaAnterior = 0;

            while ($reg=mysqli_fetch_array($rs)) {
                  $rodadaParticipante = new Rodada($this->Conexao);

                  $rodadaParticipante->ParticipanteId = $reg['participanteId'];
                  $rodadaParticipante->NomeParticipante = $reg['nomeParticipante'];
                  $rodadaParticipante->FotoParticipante = $reg['fotoParticipante'];
                  $rodadaParticipante->Turno = $reg['turno'];   
                  
                  $rodadaParticipante->CampeonatoId = $this->CampeonatoId;  
                  $rodadaParticipante->Rodada = $rodada;  
                  $rodadaParticipante->NaMosca = $reg['naMosca'];  
                  $rodadaParticipante->PontosRodada = $reg['pontosRodada']; 
                  $rodadaParticipante->TotalNaMosca = $reg['totalNaMosca'];; 

                  if ($rodadaParticipante->PontosRodada == $pontosAnterior AND $rodadaParticipante->NaMosca == $naMoscaAnterior){
                        $countPosicao++;
                        $somarPosicao = false;
                  }
                  else{
                        $somarPosicao = true;
                        $posicao++;                        
                  }  

                  if ($somarPosicao){
                        $posicao = $posicao + $countPosicao;
                        $countPosicao = 0;
                  }
                  
                  $rodadaParticipante->PosicaoRodada = $posicaoRodada;
                  $posicaoRodada++;

                  $pontos = 0;
                  switch ($posicao) {                  
                        case 1:
                              $pontos = 10;
                              break;

                        case 2:
                              $pontos = 8;
                              break;

                        case 3:
                              $pontos = 6;                       
                              break;

                        case 4:
                              $pontos = 5;
                              break;

                        case 5:
                              $pontos = 4;
                              break;

                        case 6:
                              $pontos = 2;
                              break;

                        case 7:
                              $pontos = 1;
                              break;
                  }                  

                  $pontosAnterior = $reg['pontosRodada']; 
                  $naMoscaAnterior = $reg['naMosca'];

                  $rodadaParticipante->PontosRodada = $pontos;

                  $dadosRodadaAnterior = $rodadaParticipante::RetornaPontuacaoRodadaAnterior($rodadaParticipante, "rodadaGeral");
                  $pontosRodadaAnterior = $dadosRodadaAnterior['0'];	
                  $naMoscaRodadaAnterior = $dadosRodadaAnterior['1'];				
                  $posicaoRodadaAnterior = $dadosRodadaAnterior['2'];                 

                  if(empty($posicaoRodadaAnterior)) {
                        $posicaoRodadaAnterior = $rodadaParticipante->PosicaoRodada;
                  }

                  $rodadaParticipante->TotalPontos = $pontosRodadaAnterior + $pontos;
                  $rodadaParticipante->PosicaoRodadaAnterior = $posicaoRodadaAnterior;
                        
                  if (!$rodadaParticipante->InserirRodada($rodadaParticipante, "rodadaGeral")) return false;            
                  
                  unset($rodadaParticipante);
            }

            if(!$this->AtualizarPosicoesTabelaGeral($rodada)) return false;
            else return true;
      }

      public function RetornaRodadaMaximoPontos(){
            $sql = "
                  SELECT 
                        MAX(rodada) AS rodada 
                  FROM 
                        palpites 
                  WHERE 
                        campeonatoId = $this->CampeonatoId AND 
                        (SELECT turno FROM jogos WHERE id = jogoId ) = turno AND 
                        pontosRodada > 0 
                  ORDER BY 
                        rodada DESC limit 1";	

            $rs =$this->Conexao->query($sql);

            $reg=mysqli_fetch_array($rs);

            if (empty($reg['rodada'])) return 1;
            else return $reg['rodada'];
      }

      private function ApagarRodada($conexao, $campeonatoId, $rodada, $tabela){
            $sql = "
                  DELETE FROM 
                        $tabela 
                  WHERE 
                        campeonatoId = $campeonatoId AND 
                        rodada = $rodada ";

            $this->Conexao->query($sql);
      }     

      private function AtualizarPosicoesTabelaGeral($rodada) {
            $sql = "
                  SELECT
                        id
                  FROM
                        rodadaGeral
                  WHERE
                        rodada = $rodada
                  ORDER BY
                        rodada, 
                        totalPontos DESC, 
                        totalNaMosca DESC ";     

            $rs=$this->Conexao->query($sql);

            $count = 0;
            while ($reg=mysqli_fetch_array($rs)) {
                  $count += 1;                  
                  $id = $reg['id'];	

                  $sqlRodada = "UPDATE rodadaGeral SET posicaoRodada = $count WHERE id = $id ";

                  if (!$this->Conexao->query($sqlRodada)) {
                        return false;
                  }  
            }

            return true;
      }
      
      public function RetornaPontuacaoRodadaAnterior($objRodada, $tabela){
           $pontosRodadaAnterior = 0;	
            $naMoscaRodadaAnterior = 0;				
            $posicaoRodadaAnterior = 0;

            $rodadaAnterior = $objRodada->Rodada - 1;	

            if ($rodadaAnterior == "0" OR $rodadaAnterior == "19") {
                  return array(0, 0, 0);
            }  
            else{
                  $sql = "
                        SELECT 
                              totalPontos AS pontos, 
                              totalNaMosca AS naMosca,
                              posicaoRodada
                        FROM 
                              $tabela 
                        WHERE 
                              rodada = '$rodadaAnterior' AND 
                              campeonatoId = '$objRodada->CampeonatoId' AND 
                              participanteId = '$objRodada->ParticipanteId' AND 
                              turno = '$objRodada->Turno' "; 

                  $rs=$this->Conexao->query($sql);		
                  $reg = mysqli_fetch_array($rs);

                  $pontosRodadaAnterior = $reg['pontos'];	
                  $naMoscaRodadaAnterior = $reg['naMosca'];				
                  $posicaoRodadaAnterior = $reg['posicaoRodada'];

                  return array($pontosRodadaAnterior, $naMoscaRodadaAnterior, $posicaoRodadaAnterior);
            }
      }

      public function RetornaPontuacaoInicial($rodada, $turno, $posicao){
            $sql = "
                  SELECT 
                        participanteId, 
                        pontos, 
                        participantes.nome, 
                        participantes.foto 
                  FROM 
                        pontuacao_inicial INNER JOIN 
                        participantes ON pontuacao_inicial.participanteId = participantes.id 
                  WHERE 
                        campeonatoId = $this->CampeonatoId AND 
                        turno = $turno AND 
                        rodada = $rodada  ";
            
            $rs =$this->Conexao->query($sql);

            while ($reg=mysqli_fetch_array($rs)) {
                  $rodadaParticipante = new Rodada($this->Conexao);

                  $rodadaParticipante->ParticipanteId = $reg['participanteId'];
                  $rodadaParticipante->NomeParticipante = $reg['nome'];
                  $rodadaParticipante->FotoParticipante = $reg['foto'];
                  $rodadaParticipante->Turno = $turno;                  
                  $rodadaParticipante->PontosRodada = 0;
                  $rodadaParticipante->NaMosca = 0;
                  $rodadaParticipante->CampeonatoId = $this->CampeonatoId;
                  $rodadaParticipante->Rodada = $rodada;
                  $rodadaParticipante->PosicaoRodada = $posicao; 
                  $rodadaParticipante->TotalPontos = $reg['pontos'];
                  $rodadaParticipante->TotalNaMosca = 0;
                  $rodadaParticipante->PosicaoRodadaAnterior = $posicao;	
            
                  if (!$rodadaParticipante->InserirRodada($rodadaParticipante, "rodada")) return false;    
                        
                  unset($rodadaParticipante); 
            } 
            
            return true;
      }

      public function RetornaDadosUltimoColocado($rodada, $campeonatoId, $rodadaPalpite) {
            $sql = "
                  SELECT 
                        max(rodada) AS rodada, 
                        min(totalPontos) AS pontos, 
                        max(posicaoRodada) AS posicao 
                  FROM 
                        rodada 
                  where 
                        campeonatoId = '$campeonatoId' and 
                        rodada = '$rodadaPalpite' ";
            
            $rs =$this->Conexao->query($sql);
            $reg=mysqli_fetch_array($rs);

            $rodada->Rodada = $reg['rodada'];
            $rodada->TotalPontos = $reg['pontos'];
            $rodada->PosicaoRodada = $reg['posicao'];

            return $rodada;
      }

      public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;   
      }
}

?>