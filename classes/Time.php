<?php
class Time {
      public $Id;
      public $Nome;
      public $NomeAbreviado;
	public $Cidade;
	public $Uf;
	public $Estadio;	
      public $Foto;
      public $NomeMinusculo;
      private $Conexao;

      public function setId ($parametroId){
            $this->Id = $parametroId;
      }

      public function getId(){
            return $this->Id;
      }   

      public function setNome ($parametroNome){
            $this->Nome = $parametroNome;
      }

      public function getNome(){
            return $this->Nome;
      }   

      public function setNomeAbreviado ($parametroNomeAbreviado){
            $this->NomeAbreviado = $parametroNomeAbreviado;
      }

      public function getNomeAbreviado(){
            return $this->NomeAbreviado;
      }   

      public function setCidade ($parametroCidade){
            $this->Cidade = $parametroCidade;
      }

      public function getCidade(){
            return $this->Cidade;
      }   

      public function setUf ($parametroUf){
            $this->Uf = $parametroUf;
      }

      public function getUf(){
            return $this->Uf;
      } 

      public function setEstadio ($parametroEstadio){
            $this->Estadio = $parametroEstadio;
      }

      public function getEstadio(){
            return $this->Estadio;
      } 

      public function setFoto ($parametroFoto){
            $this->Foto = $parametroFoto;
      }

      public function getFoto(){
            return $this->Foto;
      } 

      public function setNomeMinusculo ($parametroNomeMinusculo){
            $this->NomeMinusculo = $parametroNomeMinusculo;
      }

      public function getNomeMinusculo(){
            return $this->NomeMinusculo;
      } 

      public function setConexao ($parametroConexao){
            $this->Conexao = $parametroConexao;
      }

      function __construct($conexao){
            $this->Conexao = $conexao;
      }  
      
      public function GravarTime(){
            $sql = "";
            $retorno = false;
    
            if (empty($this->Id)){
                  $sql = "
                        INSERT INTO times (
                              nome, 
                              cidade,
                              estadio, 
                              nomeAbreviado) 
                        VALUES (
                              '$this->Nome', 
                              '$this->Cidade',
                              '$this->Estadio', 
                              '$this->NomeAbreviado') ";

                  if ($this->Conexao->query($sql)) $this->Id = $this->Conexao->insert_id;  
            }
            else{
                  $sql = "
                        UPDATE 
                              times 
                        SET 
                              nome = '$this->Nome',
                              cidade = '$this->Cidade',
                              nomeAbreviado = '$this->NomeAbreviado',
                              estadio = '$this->Estadio'
                        WHERE 
                        id = '". $this->Id."' ";

                  mysqli_query($this->Conexao, $sql);    
            }

            if (!empty($this->Id)){
                  $nome_imagem = "sem_imagem.png";

                  $foto = $this->Foto;
      
                  // Se a foto estiver sido selecionada
                  if (!empty($foto["name"]) AND !empty($this->Foto))
                  {                  	
                        // Pega extensão da imagem
                        preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

                        $nome_imagem = $this->Id. "." .$ext[1];
            
                        // Caminho de onde ficará a imagem
                        $caminho_imagem = "../imagens/times/" . $nome_imagem;
                        
                        // Faz o upload da imagem para seu respectivo caminho
                        move_uploaded_file($foto["tmp_name"], $caminho_imagem);

                        $sql = "UPDATE times SET foto = '$nome_imagem' WHERE id = $this->Id ";

                        return mysqli_query($this->Conexao, $sql);     
                  }                  
            }
      } 

      public function CarregarDadosTime($id) {		
            $sql = "
                  SELECT 
                        * 
                  FROM 
                        times 
                  WHERE 
                        id = $id ";
            
            $rs=$this->Conexao->query($sql);
            $reg=mysqli_fetch_array($rs);
            
            $this->Foto = $reg['foto'];
            $this->Nome = $reg['nome'];
            $this->NomeAbreviado = $reg['nomeAbreviado'];
            $this->Cidade = $reg['cidade'];
            $this->Estado = $reg['uf'];
            $this->Estadio = $reg['estadio'];
            $this->NomeMinusculo = $reg['nomeMinusculo'];
      }

      public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;   
      }
}

?>