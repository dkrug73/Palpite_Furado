
<?php

class Campeonato {
  
    public $Id;
    public $Descricao;
    public $Ano;  
    public $Serie;
    public $Ativo;
    private $Conexao;

    public function setId ($parametroId){
        $this->Id = $parametroId;
    }

    public function getId(){
        return $this->Id;
    }   

    public function setDescricao ($parametroDescricao){
        $this->Descricao = $parametroDescricao;
    }

    public function getDescricao(){
        return $this->Descricao;
    }   

    public function setAno ($parametroAno){
        $this->Ano = $parametroAno;
    }

    public function getAno(){
        return $this->Ano;
    }   

    public function setSerie ($parametroSerie){
        $this->Serie = $parametroSerie;
    }

    public function getSerie(){
        return $this->Serie;
    }   

    public function setAtivo ($parametroAtivo){
        $this->Ativo = $parametroAtivo;
    }

    public function getAtivo(){
        return $this->Ativo;
    } 

    public function setConexao ($parametroConexao){
        $this->Conexao = $parametroConexao;
    }

    function __construct($conexao){
        $this->Conexao = $conexao;
    }   

    public function GravarCampeonato(){
        $sql = "";
        $retorno = false;

        if (empty($this->Id)){
            $sql = "
                INSERT INTO campeonatos (
                        descricao, 
                        ano, 
                        serie, 
                        ativo) 
                VALUES (
                        '$this->Descricao', 
                        '$this->Ano',
                        '$this->Serie', 
                        '$this->Ativo') ";
        }
        else{
            $sql = "
              UPDATE 
                    campeonatos 
              SET 
                    descricao = '$this->Descricao', 
                    ano = '$this->Ano', 
                    serie = '$this->Serie',
                    ativo = '$this->Ativo'	
              WHERE 
                    id = $this->Id ";
        }        

        return $this->Conexao->query($sql);
  } 

  public function CarregarDadosCampeonato($conexao, $campeonato) {
        $sql = "
              SELECT 
                    * 
              FROM 
                    campeonatos 
              WHERE 
                    id = '" . $campeonato->Id . "' ";
        
        $rs=$conexao->query($sql);
        $reg=mysqli_fetch_array($rs);
        
        $campeonato->Descricao = $reg['descricao'];
        $campeonato->Ano = $reg['ano'];
        $campeonato->Serie = $reg['serie'];
        $campeonato->Ativo = $reg['ativo'];
        
        return $campeonato;
  }  

  public function ApagarTimesDoCampeonato($conexao, $campeonatoId){
        $sql = "
              DELETE FROM 
                    times_campeonato 
              WHERE 
                    campeonatoId = '".$campeonatoId."' ";

      $conexao->query($sql);    
  }

  public function InserirTimesNoCampeonato($conexao, $campeonatoId, $times){
        $quantidadeTimes = count($times);
        
        foreach ($times as $time) {
              $sql = "
                    INSERT INTO times_campeonato (
                          campeonatoId, 
                          timeId) 
                    VALUES (
                          '$campeonatoId', 
                          '$time') ";
              
              if (!$conexao->query($sql)) {
                    return false;
              }
        }

        return true;
  }	
  
    public function RetornaCampeonatoAtivo() {
        $sql = "
            SELECT 
                id 
            FROM 
                campeonatos 
            WHERE 
                ativo = 1
            LIMIT 1";

        $rs=$this->Conexao->query($sql);
        $reg=mysqli_fetch_array($rs);

        return $reg["id"];		
    }

    public function RetornaUltimaRodadaPontosCampeonato() {
        $rodada = "1";
        
        $sql = "
            SELECT DISTINCT 
                rodada 
            FROM 
                rodada 
            WHERE 
                campeonatoId = '".$this->Id."' 
            ORDER BY 
                rodada DESC LIMIT 1 ";	
        
        $rs=$this->Conexao->query($sql);	
        $reg=mysqli_fetch_array($rs);
    
        if ($reg['rodada'] != "") $rodada = $reg['rodada'];
    
        return $rodada;
    }

    public function RetornaTurnoDaRodada($rodada) {
        $sql = "
            SELECT DISTINCT 
                turno 
            FROM 
                jogos 
            WHERE 
                campeonatoId = '$this->Id' AND 
                rodada = '$rodada' ";	
    
        $rs=$this->Conexao->query($sql);	
        $reg=mysqli_fetch_array($rs);
        $registros = $rs->num_rows;	

        $turno = 1;
        if ($registros > 0){
            $turno = $reg['turno'];
        }
        
        return $turno;
    }

    public function RetornaRodadaAtual(){	
        date_default_timezone_set("America/Sao_Paulo");
        $sql = "
            SELECT 
                DATEDIFF( NOW(), data) AS ultimaRodada,
                data, 
                rodada 
            FROM 
                jogos 
            WHERE 
                campeonatoId = '$this->Id' 
            ORDER BY  
                ultimaRodada DESC ";
            
        $rs=$this->Conexao->query($sql);
    
        $rodada = 1;
        while($row=mysqli_fetch_array($rs))
        {	
            $data_rodada=($row['ultimaRodada']);
            $rodada = $row['rodada'];
    
            if($data_rodada < 1)
            {
                return $row['rodada'];	
            }		
        }
        
        return $rodada;
    }

    public function RetornaDataHoraInicioRodada($rodada){
        $sql = "SELECT 
                    DATE_FORMAT( data , '%d/%c/%Y - %H:%i:%s' ) AS data 
                FROM 
                    jogos 
                WHERE 
                    campeonatoId = '".$this->Id."' AND	
                    rodada = '".$rodada."' 
                ORDER BY 
                    data ASC LIMIT 1";
        
        $rs=$this->Conexao->query($sql);
        $reg = mysqli_fetch_array($rs);
        
        return $reg['data'];			
    }

    public function RetornaUltimaRodadaPontos($campeonatoId, $turno, $tabela) {
        $sql = "
            SELECT DISTINCT 
                rodada 
            FROM 
                ".$tabela." 
            WHERE 
                campeonatoId = '".$campeonatoId."' AND 
                turno = '".$turno."' 
            ORDER BY 
                rodada DESC LIMIT 1 ";
        
        $rs=$this->Conexao->query($sql);	
        $reg=mysqli_fetch_array($rs);
    
        return $reg['rodada'];
    }

    public function RetornaUltimaRodadaPalpites($campeonatoId) {
        $sql = "
            SELECT DISTINCT 
                rodada 
            FROM 
                palpites 
            WHERE 
                campeonatoId = $campeonatoId AND 
                pontosRodada is not null 
            ORDER BY 
                rodada DESC LIMIT 1 ";

        $rs=$this->Conexao->query($sql);	
        $reg=mysqli_fetch_array($rs);

        
        $registros = $rs->num_rows;

        if ($registros == 0) {  
               return 0;
        }      
    
        return $reg['rodada'];
    }

    public function RetornaSQLTurnoGeral($tabela, $rodada, $turno){
        $sql = "
			SELECT 
				id, 
				campeonatoId, 
				turno, 
				rodada,
				participanteId, 
				totalPontos, 
				naMosca, 
				posicaoRodada, 
				nomeParticipante, 
				fotoParticipante, 
				pontosRodada, 
				posicaoRodadaAnterior, 
				totalNaMosca,							
				(posicaoRodadaAnterior - posicaoRodada) AS variacao
			FROM
				".$tabela."
			WHERE 
				campeonatoId = '".$this->Id."' AND 
				rodada = '".$rodada."' AND 
				turno = '".$turno."' 
			ORDER BY 
				totalPontos DESC, 
				totalNaMosca DESC, 
				nomeParticipante ASC";
							
        return $this->Conexao->query($sql); 
    }

    public function RetornaSQLTurnoRodada($rodada) {
        $sql = "
			SELECT 
				id, 
				campeonatoId, 
				turno, 
				rodada,
				participanteId, 
				pontosRodada,
				naMosca, 
				nomeParticipante, 
				fotoParticipante
			FROM
				rodada
			WHERE 
				campeonatoId = ".$this->Id." AND 
				rodada = ".$rodada."
			ORDER BY 
				pontosRodada DESC, 
				naMosca DESC, 
				nomeParticipante ASC";
							
		return $this->Conexao->query($sql); 
    }

    public function ParticipanteDesativar($participanteId) {
        $sql = "
            SELECT 
                desativar 
            FROM 
                participantes 
            WHERE 
                id = ".$participanteId."";
    
        $rs=$this->Conexao->query($sql);
        $reg=mysqli_fetch_array($rs);
    
        return $reg["desativar"];		
    }
    

    public function FecharConexao(){
        mysqli_close($this->Conexao);
        $this->Conexao = null;   
    }
}

?>