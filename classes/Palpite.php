<?php
class Palpite {
    public $Id;
    public $JogoId;
    public $ParticipanteId;
    public $PalpiteMandante;
    public $PalpiteVisitante;
    public $Rodada;
    public $Turno;
    public $CampeonatoId;
    public $DataJogo;
    public $nomeMandante;
    public $NomeVisitante;
    public $FotoMandante;
    public $FotoVisitante;
    public $MandanteAbreviado;
    public $VisitanteAbreviado;
    public $Estadio;
    public $DataPalpite;
    public $Padrao;
    public $PontosRodada;
    public $NaMosca;
    public $PlacarMandante;
    public $PlacarVisitante;
    private $Conexao;

    public function setId ($parametroId){
        $this->Id = $parametroId;
    }

    public function getId(){
        return $this->Id;
    } 

    public function setJogoId ($parametroJogoId){
        $this->JogoId = $parametroJogoId;
    }

    public function getJogoId(){
        return $this->JogoId;
    } 

    public function setParticipanteId ($parametroParticipanteId){
        $this->ParticipanteId = $parametroParticipanteId;
    }

    public function getParticipanteId(){
        return $this->ParticipanteId;
    } 

    public function setPalpiteMandante ($parametroPalpiteMandante){
        $this->PalpiteMandante = $parametroPalpiteMandante;
    }

    public function getPalpiteMandante(){
        return $this->PalpiteMandante;
    } 

    public function setPalpiteVisitante ($parametroPalpiteVisitante){
        $this->PalpiteVisitante = $parametroPalpiteVisitante;
    }

    public function getPalpiteVisitante(){
        return $this->PalpiteVisitante;
    } 

    public function setRodada ($parametroRodada){
        $this->Rodada = $parametroRodada;
    }

    public function getRodada(){
        return $this->Rodada;
    } 

    public function setTurno ($parametroTurno){
        $this->Turno = $parametroTurno;
    }

    public function getTurno(){
        return $this->Turno;
    } 

    public function setCampeonatoId ($parametroCampeonatoId){
        $this->CampeonatoId = $parametroCampeonatoId;
    }

    public function getCampeonatoId(){
        return $this->CampeonatoId;
    } 

    public function setDataJogo ($parametroDataJogo){
        $this->DataJogo = $parametroDataJogo;
    }

    public function getDataJogo(){
        return $this->DataJogo;
    } 

    public function setnomeMandante ($parametronomeMandante){
        $this->nomeMandante = $parametronomeMandante;
    }

    public function getnomeMandante(){
        return $this->nomeMandante;
    } 

    public function setNomeVisitante ($parametroNomeVisitante){
        $this->NomeVisitante = $parametroNomeVisitante;
    }

    public function getNomeVisitante(){
        return $this->NomeVisitante;
    } 

    public function setFotoMandante ($parametroFotoMandante){
        $this->FotoMandante = $parametroFotoMandante;
    }

    public function getFotoMandante(){
        return $this->FotoMandante;
    } 

    public function setFotoVisitante ($parametroFotoVisitante){
        $this->FotoVisitante = $parametroFotoVisitante;
    }

    public function getFotoVisitante(){
        return $this->FotoVisitante;
    } 

    public function setMandanteAbreviado ($parametroMandanteAbreviado){
        $this->MandanteAbreviado = $parametroMandanteAbreviado;
    }

    public function getMandanteAbreviado(){
        return $this->MandanteAbreviado;
    } 
    
    public function setVisitanteAbreviado ($parametroVisitanteAbreviado){
        $this->VisitanteAbreviado = $parametroVisitanteAbreviado;
    }

    public function getVisitanteAbreviado(){
        return $this->VisitanteAbreviado;
    } 

    public function setEstadio ($parametroEstadio){
        $this->Estadio = $parametroEstadio;
    }

    public function getEstadio(){
        return $this->Estadio;
    } 

    public function setDataPalpite ($parametroDataPalpite){
        $this->DataPalpite = $parametroDataPalpite;
    }

    public function getDataPalpite(){
        return $this->DataPalpite;
    } 

    public function setPadrao ($parametroPadrao){
        $this->Padrao = $parametroPadrao;
    }

    public function getPadrao(){
        return $this->Padrao;
    } 

    public function setPontosRodada ($parametroPontosRodada){
        $this->PontosRodada = $parametroPontosRodada;
    }

    public function getPontosRodada(){
        return $this->PontosRodada;
    } 

    public function setNaMosca ($parametroNaMosca){
        $this->NaMosca = $parametroNaMosca;
    }

    public function getNaMosca(){
        return $this->NaMosca;
    } 

    public function setPlacarMandante ($parametroPlacarMandante){
        $this->PlacarMandante = $parametroPlacarMandante;
    }

    public function getPlacarMandante(){
        return $this->PlacarMandante;
    } 

    public function setPlacarVisitante ($parametroPlacarVisitante){
        $this->PlacarVisitante = $parametroPlacarVisitante;
    }

    public function getPlacarVisitante(){
        return $this->PlacarVisitante;
    } 

    public function setConexao ($parametroConexao){
        $this->Conexao = $parametroConexao;
    }

    function __construct($conexao){
        $this->Conexao = $conexao;
    }   

    public function GravarPalpite(){ 
        if (!$this->ValidaRodadaJogo()){
            return false;
        }

        $sql = "
            SELECT 
                id 
            FROM 
                palpites 
            WHERE 
                participanteId = '$this->ParticipanteId' AND 
                jogoId = '$this->JogoId' ";

        $rs=$this->Conexao->query($sql);
        
        $reg = mysqli_fetch_array($rs);
        
        $this->Id = $reg['id'];    
        
        if (empty($this->Id)) {
            return $this->InserirPalpite();
        }
        else {
            return $this->AlterarPalpite();
        }
    }

    public function InserirPalpite(){
        $jogo = new Jogo($this->Conexao);        
        $jogo->Id = $this->JogoId;   

        $jogo = $jogo->RetornaDadosJogo($jogo);          
                        
        $sql = "
            INSERT INTO palpites (
                jogoId,
                campeonatoId,
                participanteId,
                palpiteMandante,
                palpiteVisitante,
                rodada,
                turno,
                dataJogo,
                nomeMandante,
                nomeVisitante,
                fotoMandante,
                fotoVisitante,
                mandanteAbreviado,
                visitanteAbreviado,
                estadio,
                padrao ) 
            VALUES(
                '$this->JogoId',
                '$this->CampeonatoId',
                '$this->ParticipanteId',
                '$this->PalpiteMandante',
                '$this->PalpiteVisitante',
                '$this->Rodada',
                '$this->Turno',
                '$jogo->Data',
                '$jogo->NomeMandante',
                '$jogo->NomeVisitante',
                '$jogo->MandanteId.png',
                '$jogo->VisitanteId.png',
                '$jogo->MandanteAbreviado',
                '$jogo->VisitanteAbreviado',
                '$jogo->Estadio',
                '$this->Padrao' )";
                        
        return $this->Conexao->query($sql);
    }

    public function AlterarPalpite() {
        $sql="	
            UPDATE 
                palpites 
            SET 
                palpiteMandante='$this->PalpiteMandante',
                palpiteVisitante='$this->PalpiteVisitante',
                rodada='$this->Rodada',
                dataPalpite = now()
            WHERE 
                id = '$this->Id' ";	

        return $this->Conexao->query($sql);
    }

    public function AtualizaPontosPalpites($palpite){
        $sql = "
            UPDATE 
                palpites 
            SET 
                pontosRodada = '$palpite->PontosRodada', 
                naMosca = '$palpite->NaMosca',
                placarMandante = '$palpite->PlacarMandante',
                placarVisitante = '$palpite->PlacarVisitante'
            WHERE 
                id = $palpite->Id ";	
                    
        return $this->Conexao->query($sql);
    }

    public function ApagarPontosJogo(){
        $sql = "
            UPDATE 
                palpites 
            SET 
                pontosRodada = null,
                naMosca = null, 
                placarMandante = null, 
                placarVisitante = null 
            WHERE jogoId = $this->JogoId ";

        return $this->Conexao->query($sql);
    }
    
    public function GravarPalpitePadrao() {
        $sql = "
            SELECT 
                id, 
                turno
            FROM 
                jogos 
            WHERE 
                rodada = $this->Rodada AND 
                campeonatoId = $this->CampeonatoId AND 
                placarMandante IS null ";
                
        $rs = $this->Conexao->query($sql);

        $this->Padrao = "1";  
                            
        while ($reg = mysqli_fetch_array($rs)) {  
            $jogoId = $reg['id'];
            $turno = $reg['turno'];
            $this->JogoId = $jogoId;
            $this->Turno = $turno;         

            $sqlParticipante = "                
                SELECT 
                    participantes_campeonato.participanteId, 
                    participantes.padraoMandante, 
                    participantes.padraoVisitante 
                FROM 
                    participantes_campeonato INNER JOIN 
                    participantes ON 
                        participantes_campeonato.participanteId = participantes.id 
                WHERE 
                    participantes_campeonato.campeonatoId = $this->CampeonatoId AND 
                    participantes_campeonato.participanteId NOT IN (
                        SELECT 
                            participanteId 
                        FROM 
                            palpites
                        WHERE 
                            campeonatoId = $this->CampeonatoId AND 
                            jogoId = $jogoId) ";
                
            $rsParticipante=$this->Conexao->query($sqlParticipante);	
        
            while ($regParticipante=mysqli_fetch_array($rsParticipante)) {
                $participanteId = $regParticipante['participanteId']; 
                $padraoMandante = $regParticipante['padraoMandante']; 
                $padraoVisitante = $regParticipante['padraoVisitante']; 

                $this->ParticipanteId = $participanteId;
                $this->PalpiteMandante = $padraoMandante;
                $this->PalpiteVisitante = $padraoVisitante;
                            
                $this->InserirPalpite();
            }
        }
    
        return true;
    }      

    public function PodeGravarPalpitePadrao($campeonatoId, $rodada, $participanteId) {
            $sql = "SELECT 
                            rodada 
                        FROM 
                            pontuacao_inicial 
                        WHERE 
                            campeonatoId = $campeonatoId AND 
                            participanteId = $participanteId ";
            
            $rs=$this->Conexao->query($sql);		
            $reg = mysqli_fetch_array($rs);
    
            $rodadaPontuacaoInicial = $reg['rodada'];
    
            if ($rodada > $rodadaPontuacaoInicial) return true;
            else return false;
    }      
    
    public function AtualizarPalpites($jogo) {   
        $sql = "
            SELECT 
                    id, 
                    palpiteMandante, 
                    palpiteVisitante,
                    padrao
            FROM 
                    palpites 
            WHERE 
                    jogoId = $jogo->Id ";                       
            
        $rs=$this->Conexao->query($sql);
            
        while ($reg=mysqli_fetch_array($rs)) 
        {
            $palpite = new Palpite($this->Conexao);
            $palpite->Id = $reg['id'];
            $palpite->CampeonatoId = $this->CampeonatoId;
            $palpite->Rodada = $this->Rodada;
            $palpite->PlacarMandante = $jogo->PlacarMandante;
            $palpite->PlacarVisitante = $jogo->PlacarVisitante;

            $palpiteId = $reg['id'];
            $palpiteMandante = $reg['palpiteMandante'];
            $palpiteVisitante = $reg['palpiteVisitante'];	
            $padrao = $reg['padrao'];	

            $acertoVitoriaMandante = 10;
            $acertoVitoriaVisitante = 15;
            $acertoEmpate = 15;
            $acertoPlacar = 5;

            // calcula pontuação da rodada
            $qtPontos = 0;
            $naMosca = 0;	
                        
            if ($palpiteMandante == "" || $palpiteVisitante == "") {	
                $palpite->PontosRodada = '0';
                $palpite->NaMosca = '0';	
            }
            else {				
                // acerto do empate
                if ($palpiteMandante == $palpiteVisitante && $palpite->PlacarMandante == $palpite->PlacarVisitante) {
                    $qtPontos = $qtPontos + $acertoEmpate;	
                    $naMosca = $naMosca + 1;
                }
                    
                // acerto do vencedor
                else if ($palpiteMandante > $palpiteVisitante && $palpite->PlacarMandante > $palpite->PlacarVisitante) {
                    $qtPontos = $qtPontos + $acertoVitoriaMandante;	
                    $naMosca = $naMosca + 1;	
                }

                // acerto do perdedor
                else if ($palpiteMandante < $palpiteVisitante && $palpite->PlacarMandante < $palpite->PlacarVisitante) {
                    $qtPontos = $qtPontos + $acertoVitoriaVisitante;	
                    $naMosca = $naMosca + 1;	
                }

                // acerto do placar mandante
                if ($palpiteMandante == $palpite->PlacarMandante) {
                    $qtPontos = $qtPontos + $acertoPlacar;
                    $naMosca = $naMosca + 1;
                }
                
                // acerto do placar visitante
                if ($palpiteVisitante == $palpite->PlacarVisitante) {
                    $qtPontos = $qtPontos + $acertoPlacar;
                    $naMosca = $naMosca + 1;
                }

                $acertoNaMosca = 0;
                if ($naMosca == 3) {
                    $acertoNaMosca = 1;	
                }
                
                $palpite->PontosRodada = $qtPontos;
                $palpite->NaMosca = $acertoNaMosca;	
            }
                
            if (!$this->AtualizaPontosPalpites($palpite)){
                unset($palpite);
                    return false;
            }           
            
            unset($palpite);
        }        

        return true;
    }

    public function GravarPadraoParaPalpitesIguais(){       
        $sql = "
            SELECT 
                id
            FROM 	
                palpites 
            WHERE
                rodada = '$this->Rodada' AND 
                participanteId = '$this->ParticipanteId' AND
                campeonatoId = '$this->CampeonatoId'
            GROUP BY
                palpitemandante, 
                palpitevisitante ";  

        $rs=$this->Conexao->query($sql);
        $registros = $rs->num_rows;
        
        $padrao = 0;
        if ($registros == 1) {  
                $padrao = 1;
        }      

        $sql = "
            UPDATE 
                palpites 
            SET 
                padrao = '$padrao'
            WHERE 
                rodada = '$this->Rodada' AND 
                participanteId = '$this->ParticipanteId' AND
                campeonatoId = '$this->CampeonatoId' AND
                placarMandante IS null";	
                        
        if (!$this->Conexao->query($sql)) {
            return false;
        }           

        return true;
    }

    public function CarregarPalpites(){
        $sql="
            SELECT 
                palpiteMandante, 
                palpiteVisitante, 
                pontosRodada,
                naMosca 
            FROM 
                palpites 
            WHERE 
                jogoId = $this->JogoId AND 
                participanteId = $this->ParticipanteId ";

        $rs=$this->Conexao->query($sql);
        $registros = $rs->num_rows;		
        $reg = mysqli_fetch_array($rs);

        if ($registros > 0){
            $this->PalpiteMandante = $reg['palpiteMandante'];
            $this->PalpiteVisitante = $reg['palpiteVisitante'];
            $this->PontosRodada = $reg['pontosRodada'];
            $this->NaMosca = $reg['naMosca'];	
        }       
    }

    // função para validar a data e hora do jogo  e bloquear a rodada-->formato datatime (AAAA-mm-dd h:m:s)
    public function ValidaRodadaJogo() {		
        date_default_timezone_set("America/Sao_Paulo");
        
        $dataInicioRodada = $this->RetornaDataInicioRodada();
                        
        $data_atual = time(); // formato timestamp
        
        // irá bloquear os palpites 3 minutos antes do jogo
        $data_jogo = strtotime($dataInicioRodada) - 300; // Gera o timestamp de $data_mysql
                
        if($data_jogo >= $data_atual) {	
            return true;
        }
        else {
            return false;
        }
    }

    private function RetornaDataInicioRodada(){
        $sql = "
            SELECT 
                data 
            FROM 
                jogos 
            WHERE 
                id = '$this->JogoId' 
            ORDER BY 
                data ASC LIMIT 1";
        
        $rs=$this->Conexao->query($sql);
        $reg = mysqli_fetch_array($rs);
        
        return $reg['data'];			
    }

    public function FecharConexao() {
        mysqli_close($this->Conexao);
        $this->Conexao = null;   
    }
}
?>
