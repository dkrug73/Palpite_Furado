<?php
    class Jogo {
        public $Id;
        public $CampeonatoId;
        public $Turno;
        public $Rodada;
        public $Data;
        public $MandanteId;
        public $VisitanteId;
        public $Estadio;
        public $NomeMandante;
        public $NomeVisitante;
        public $MandanteAbreviado;
        public $VisitanteAbreviado;
        private $Conexao;

        public function setId ($parametroId){
            $this->Id = $parametroId;
        }

        public function getId(){
            return $this->Id;
        } 

        public function setCampeonatoId ($parametroCampeonatoId){
            $this->CampeonatoId = $parametroCampeonatoId;
        }

        public function getCampeonatoId(){
            return $this->CampeonatoId;
        } 

        public function setTurno ($parametroTurno){
            $this->Turno = $parametroTurno;
        }

        public function getTurno(){
            return $this->Turno;
        } 

        public function setRodada ($parametroRodada){
            $this->Rodada = $parametroRodada;
        }

        public function getRodada(){
            return $this->Rodada;
        } 

        public function setData ($parametroData){
            $this->Data = $parametroData;
        }

        public function getData(){
            return $this->Data;
        } 

        public function setMandanteId ($parametroMandanteId){
            $this->MandanteId = $parametroMandanteId;
        }

        public function getMandanteId(){
            return $this->MandanteId;
        } 

        public function setVisitanteId ($parametroVisitanteId){
            $this->VisitanteId = $parametroVisitanteId;
        }

        public function getVisitanteId(){
            return $this->VisitanteId;
        } 

        public function setEstadio ($parametroEstadio){
            $this->Estadio = $parametroEstadio;
        }

        public function getEstadio(){
            return $this->Estadio;
        } 

        public function setNomeMandante ($parametroNomeMandante){
            $this->NomeMandante = $parametroNomeMandante;
        }

        public function getNomeMandante(){
            return $this->NomeMandante;
        } 

        public function setNomeVisitante ($parametroNomeVisitante){
            $this->NomeVisitante = $parametroNomeVisitante;
        }

        public function getNomeVisitante(){
            return $this->NomeVisitante;
        } 

        public function setMandanteAbreviado ($parametroMandanteAbreviado){
            $this->MandanteAbreviado = $parametroMandanteAbreviado;
        }

        public function getMandanteAbreviado(){
            return $this->MandanteAbreviado;
        } 

        public function setVisitanteAbreviado ($parametroVisitanteAbreviado){
            $this->VisitanteAbreviado = $parametroVisitanteAbreviado;
        }

        public function getVisitanteAbreviado(){
            return $this->VisitanteAbreviado;
        } 

        public function setConexao ($parametroConexao){
            $this->Conexao = $parametroConexao;
        }

        function __construct($conexao){
            $this->Conexao = $conexao;
        }   

        public function GravarJogo(){
            $resultado = null;       

            if(empty($this->Id)){   
                $sql = "
                INSERT INTO jogos (
                    campeonatoId,
                    turno,
                    rodada,
                    data,
                    mandanteId,
                    visitanteId,
                    estadio,
                    nomeMandante,
                    nomeVisitante,
                    mandanteAbreviado,
                    visitanteAbreviado) 
                VALUES(
                    '$this->CampeonatoId',
                    '$this->Turno',
                    '$this->Rodada',
                    '$this->Data',
                    '$this->MandanteId',
                    '$this->VisitanteId',
                    '$this->Estadio',
                    '$this->NomeMandante',
                    '$this->NomeVisitante',
                    '$this->MandanteAbreviado',
                    '$this->VisitanteAbreviado') ";
            }
            else{
                $sql = "
                    UPDATE 
                        jogos 
                    SET 
                        turno = '$this->Turno', 
                        rodada = '$this->Rodada', 
                        data = '$this->Data', 
                        mandanteId = '$this->MandanteId', 
                        visitanteId = '$this->VisitanteId', 
                        nomeMandante = '$this->NomeMandante', 
                        nomeVisitante = '$this->NomeVisitante', 
                        mandanteAbreviado = '$this->MandanteAbreviado', 
                        visitanteAbreviado = '$this->VisitanteAbreviado', 
                        estadio = '$this->Estadio' 
                    WHERE 
                        id = $this->Id ";
            }

            return $this->Conexao->query($sql);
        }

        public function AtualizarJogo(){
            $sql = "
                UPDATE 
                    jogos 
                SET ";
                
                if ($this->PlacarMandante == ""){
                    $sql = $sql. "
                        placarMandante = null, 
                        placarVisitante = null ";
                }
                else{
                    $sql = $sql. "
                        placarMandante = '$this->PlacarMandante', 
                        placarVisitante = '$this->PlacarVisitante' ";
                }
                     
                $sql =  $sql."
                WHERE 
                    id = $this->Id ";	
                
            return $this->Conexao->query($sql);
      }
        
        public function RetornaDadosJogo($jogo){
            $sql = "
                SELECT 
                    nomeMandante,
                    nomeVisitante,
                    data,
                    mandanteId,
                    visitanteId,
                    estadio,
                    mandanteAbreviado,
                    visitanteAbreviado
                FROM 
                    jogos
                WHERE 
                    id = '$jogo->Id' ";					

            $rs=$this->Conexao->query($sql);
            $reg=mysqli_fetch_array($rs);

            $jogo->NomeMandante = $reg['nomeMandante'];
            $jogo->NomeVisitante = $reg['nomeVisitante'];
            $jogo->MandanteId = $reg['mandanteId'];
            $jogo->VisitanteId = $reg['visitanteId'];
            $jogo->Estadio = $reg['estadio'];
            $jogo->Data = $reg['data'];
            $jogo->MandanteAbreviado = $reg['mandanteAbreviado'];
            $jogo->VisitanteAbreviado = $reg['visitanteAbreviado'];

            return $jogo;
        }

        public function RetornaUltimaRodada($campeonatoId){
            $sql = "
            SELECT 
                MAX(rodada) AS rodada
            FROM 
                jogos
            WHERE 
                campeonatoId = $campeonatoId ";

            $rs=$this->Conexao->query($sql);
            $reg=mysqli_fetch_array($rs);

            return $reg['rodada'];
        }

        public function FecharConexao(){
            mysqli_close($this->Conexao);
            $this->Conexao = null;   
        }
    }
?>
