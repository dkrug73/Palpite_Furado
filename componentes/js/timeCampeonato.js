$(document).ready(function(){  

    CarregarTabela("carregarTimesCampeonato.php");
    
    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboCampeonato.php',
        success: function(data){
            $('#campeonatoId').html(data);
        }
    });      
    
    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboTimesCampeonato.php',
        success: function(data){
            $('#timeId').html(data);
        }
    });
      
    $('#btn-salvar').click(function(){ 
        $.ajax({
            url: 'paginas/cadastrarTimesCampeonato1.php',
            method: 'post',
            data: $('#formulario-cadastro').serialize(),
            success: function(data){
                CarregarTabela("carregarTimesCampeonato.php");

                $('#formulario-cadastro').each (function(){
                    this.reset();
                });
                
                var classe = '#salvar-erro';
                if (data == 1){
                    classe = '#salvar-sucesso';
                }

                ExibirAviso(classe);             
            }
        });  

        return false;
    });
  
    $('#resultado-tabela').on('click', 'tr', function() { 
        document.getElementById('id').value = $(this).find("td:nth-child(1)").text();
        document.getElementById('timeId').value = $(this).find("td:nth-child(2)").attr('id');
    }); 

    $('#btn-excluir').click(function(){
        var timeId = document.getElementById('id').value;

        if(timeId != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: timeId, pagina: 'times_campeonato' },
                success: function(data){                        
                    CarregarTabela("carregarTimesCampeonato.php");

                    $('#formulario-cadastro').each (function(){
                        this.reset();
                    });

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    });   
        
});