$(document).ready(function(){  

    CarregarTabela("carregarPontuacaoInicial.php");	

    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboCampeonato.php',
        success: function(data){
            $('#campeonatoId').html(data);
        }
    });      
    
    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboParticipante.php',
        data: { campeonatoId: null },
        success: function(data){
            $('#participanteId').html(data);
        }
    });
   
    $('#btn-salvar').click(function(){ 
        var salvar = true;
        document.getElementById("erro-participante").innerHTML = "";
        document.getElementById("erro-rodada").innerHTML = "";
        document.getElementById("erro-turno").innerHTML = "";
        document.getElementById("erro-pontos").innerHTML = "";
        
        if (document.getElementById("participanteId").value == 0) {
            document.getElementById("erro-participante").innerHTML = "Selecione um participante.";
            salvar = false;
        }

        if (document.getElementById("rodada").value == "") {
            document.getElementById("erro-rodada").innerHTML = "O campo Rodada é obrigatório.";
            salvar = false;
        }

        if (document.getElementById("turno").value == "") {
            document.getElementById("erro-turno").innerHTML = "O campo Turno é obrigatório.";
            salvar = false;
        }


        if (document.getElementById("pontos").value == "") {
            document.getElementById("erro-pontos").innerHTML = "O campo Pontos é obrigatório.";
            salvar = false;
        }

        if(salvar){  
            $.ajax({
                type: 'post',
                url: 'paginas/pontuacaoInicial1.php',
                data: $("#formulario-cadastro").serialize(),
                success: function(data){
                    CarregarTabela("carregarPontuacaoInicial.php");	
                    LimparCamposForm('#formulario-cadastro');
                    
                    var classe = '#salvar-erro';
                    if (data == 1){
                        classe = '#salvar-sucesso';
                    }

                    ExibirAviso(classe);             
                }
            });  
        }   
        
        return false;
    });
  
    $('#resultado-tabela').on('click', 'tr', function() {            
        var id= $(this).find("td:nth-child(1)").text();
        var participanteId = $(this).find("td:nth-child(2)").attr('id');
        var pontos = $(this).find("td:nth-child(3)").text();
        var rodada = $(this).find("td:nth-child(4)").text();
        var turno = $(this).find("td:nth-child(5)").text();

        document.getElementById('id').value = id;
        document.getElementById('participanteId').value = participanteId;
        document.getElementById('rodada').value = rodada;
        document.getElementById('turno').value = turno;
        document.getElementById('pontos').value = pontos;

        $('#rodada').focus();
    }); 

    $('#btn-excluir').click(function(){
        var pontuacao = document.getElementById('id').value;

        if(pontuacao != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: pontuacao, pagina: 'pontuacao_inicial' },
                success: function(data){                        
                    CarregarTabela("carregarPontuacaoInicial.php");	
                    
                    $('#formulario-cadastro').each (function(){
                        this.reset();
                    });

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    });   
        
});