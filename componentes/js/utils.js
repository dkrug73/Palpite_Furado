function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#foto-time').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function CarregarTabela(pagina){
    $.ajax({
        url: 'paginas/tabelas/' + pagina,
        method: 'post',
        success: function(data){
            $('#resultado-tabela').html(data);
        }
    });
};

function CarregarTabelaFormulas(formulaId){
    $.ajax({
        url: 'paginas/tabelas/carregarTabelaFormulaJogos.php',
        method: 'post',
        data: { id: formulaId },
        success: function(data){
            $('#resultado-tabela').html(data);
        }
    });
};

function LimparCamposForm(form){
    $(form).each (function(){
        this.reset();
    });
};

function LimparCampos(campos){
    campos.forEach(function(item) {
        document.getElementById(item).value = null;
    });
}

function ExibirAviso(classe){            
    $(classe).show(); 
     setTimeout(function() {$(classe).fadeOut('slow');}, 3000);
}   

$(document).ready(function(){
    $(".somenteNumero").bind("keyup blur focus", function(e) {
        e.preventDefault();
        var expre = /[^\d]/g;
        $(this).val($(this).val().replace(expre,''));
    });
});

function titleize(text) {
    var words = text.toLowerCase().split(" ");
    for (var a = 0; a < words.length; a++) {
        var w = words[a];
        words[a] = w[0].toUpperCase() + w.slice(1);
    }
    return words.join(" ");
}

function AbrirPalpites (participanteId){
    sessionStorage.setItem('participanteId', participanteId );
    window.location = "palpitesAlheios.php";
}