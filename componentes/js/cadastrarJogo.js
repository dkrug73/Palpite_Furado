$(document).ready(function(){	
			
    CarregarTabela();			

    $('#resultado-tabela').on('click', 'tr', function() { 
        var id = $(this).find("td:nth-child(1)").text();
        var rodada = $(this).find("td:nth-child(2)").text();
        var turno = $(this).find("td:nth-child(3)").text();
        var data = $(this).find("td:nth-child(4)").text();
        var mandanteId = $(this).find("td:nth-child(5)").attr('id');
        var visitanteId = $(this).find("td:nth-child(6)").attr('id');
        var estadio = $(this).find("td:nth-child(7)").text();

        document.getElementById('id').value = id;
        document.getElementById('rodada').value = rodada;
        document.getElementById('turno').value = turno;
        document.getElementById('mandanteId').value = mandanteId;
        document.getElementById('visitanteId').value = visitanteId;
        document.getElementById('dataJogo').value = data;
        document.getElementById('estadio').value = estadio;  
        
        document.getElementById('dataJogo').focus();
    }); 

    
    $('#btn_salvar').click(function() {
        var salvar = true;
        document.getElementById("erro-rodada").innerHTML = "";
        document.getElementById("erro-turno").innerHTML = "";
        document.getElementById("erro-dataJogo").innerHTML = "";
        document.getElementById("erro-mandanteId").innerHTML = "";

        if (document.getElementById("rodada").value == "") {
            document.getElementById("erro-rodada").innerHTML = "Obrigatório.";
            salvar = false;
        }

        if (document.getElementById("turno").value == "") {
            document.getElementById("erro-turno").innerHTML = "Obrigatório.";
            salvar = false;
        }

        if (document.getElementById("dataJogo").value == "") {
            document.getElementById("erro-dataJogo").innerHTML = "O campo Data é obrigatório.";
            salvar = false;
        }

        if (document.getElementById("mandanteId").value == document.getElementById("visitanteId").value) {
            document.getElementById("erro-mandanteId").innerHTML = "Selecione um time válido.";
            salvar = false;
        }

        if (salvar){
            $.ajax ({
                method: "post",
                url: "paginas/cadastrarJogo1.php",
                data: { 
                    id: document.getElementById("id").value,
                    rodada: document.getElementById('rodada').value,
                    turno: document.getElementById('turno').value,
                    dataJogo: document.getElementById('dataJogo').value,
                    mandanteId: document.getElementById('mandanteId').value,
                    visitanteId: document.getElementById('visitanteId').value
                },
                success: function(data) {
                    var classe = '#salvar-erro';

                    if (data == 1){
                        classe = '#salvar-sucesso';
                        LimparCamposForm('#formulario-cadastro');                        
                    }

                    CarregarTabela(); 

                    $(classe).show(); 

                    setTimeout(function() {$(classe).fadeOut('slow');}, 2000);			
                }
            });				
        }

        return false;
    });

    $('#btn_excluir').click(function(){       
        var jogoId = document.getElementById('id').value;
        if(jogoId != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: jogoId, pagina: 'jogos' },
                success: function(data){                        
                    CarregarTabela(); 
                    LimparCamposForm('#formulario-cadastro');

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    $(classe).show(); 

                    setTimeout(function() {$(classe).fadeOut('slow');}, 2000);		                    
                }
            });
        }
    });  
});

function CarregarTabela(){
    $.ajax({
        url: 'paginas/tabelas/carregarJogosRodada.php',
        method: 'post', 
        success: function(data){
            $('#resultado-tabela').html(data);
        }
    });
};