$(document).ready(function(){  

    CarregarTabela("carregarCampeonatos.php");	
      
    $('#btn-salvar').click(function(){ 
        var salvar = true;
        document.getElementById("erro-descricao").innerHTML = "";
        document.getElementById("erro-ano").innerHTML = "";
        document.getElementById("erro-serie").innerHTML = "";
        
        if (document.getElementById("descricao").value == 0) {
            document.getElementById("erro-descricao").innerHTML = "O campo Descrição é obrigatório.";
            salvar = false;
        }

        if (document.getElementById("ano").value == "") {
            document.getElementById("erro-ano").innerHTML = "O campo Ano é obrigatório.";
            salvar = false;
        }

        if (document.getElementById("serie").value == "") {
            document.getElementById("erro-serie").innerHTML = "O campo Série é obrigatório.";
            salvar = false;
        }

        if(salvar){  
            $.ajax({
                type: 'post',
                url: 'paginas/cadastrarCampeonato1.php',
                data: $("#formulario-cadastro").serialize(),
                success: function(data){
                    CarregarTabela("carregarCampeonatos.php");	
                    LimparCamposForm('#formulario-cadastro');
                    
                    var classe = '#salvar-erro';
                    if (data == 1){
                        classe = '#salvar-sucesso';
                    }

                    ExibirAviso(classe);             
                }
            });  
        }   
        
        return false;
    });
  
    $('#resultado-tabela').on('click', 'tr', function() { 
        document.getElementById('id').value = $(this).find("td:nth-child(1)").text();
        document.getElementById('descricao').value = $(this).find("td:nth-child(2)").text();
        document.getElementById('ano').value = $(this).find("td:nth-child(3)").text();
        document.getElementById('serie').value = $(this).find("td:nth-child(4)").text();
        document.getElementById('ativo').checked = $(this).find("td:nth-child(5)").text() == "sim";

        $('#descricao').focus();
    }); 

    $('#btn-excluir').click(function(){
        var pontuacao = document.getElementById('id').value;

        if(pontuacao != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: pontuacao, pagina: 'campeonatos' },
                success: function(data){                        
                    CarregarTabela("carregarCampeonatos.php");	
                    
                    $('#formulario-cadastro').each (function(){
                        this.reset();
                    });

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    });   
        
});