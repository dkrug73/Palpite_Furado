$(document).ready(function(){

    $('#btn_salvar').click(function() {
        var salvar = true;
        document.getElementById("erro-usuario").innerHTML = "";
        document.getElementById("erro-email").innerHTML = "";
        document.getElementById("erro-nome").innerHTML = "";
        document.getElementById("erro-senha").innerHTML = "";
        document.getElementById("erro-contraSenha").innerHTML = "";
        document.getElementById("erro-mandante").innerHTML = "";
        document.getElementById("erro-visitante").innerHTML = "";
        document.getElementById("erro-regulamento").innerHTML = "";

        if (!document.getElementById("regulamento").checked) {
            document.getElementById("erro-regulamento").innerHTML = "Para participar do bolão é necessário ler e aceitar o regulamento";
            $('#regulamento').focus();
            salvar = false;
        }

        if (document.getElementById("visitante").value == "") {
            document.getElementById("erro-visitante").innerHTML = "Visit.";
            $('#visitante').focus();
            salvar = false;
        }

        if (document.getElementById("mandante").value == "") {
            document.getElementById("erro-mandante").innerHTML = "Mandan.";
            $('#mandante').focus();
            salvar = false;
        }

        if ((
            document.getElementById("senha").value != "" && 
            document.getElementById("contraSenha").value != ""
            ) && 
            document.getElementById("senha").value != 
                document.getElementById("contraSenha").value){
            document.getElementById("erro-senha").innerHTML = "As senhas informadas são diferentes";
            $('#senha').focus();
            salvar = false;
        }

        if (document.getElementById("contraSenha").value == "" ) {
            document.getElementById("erro-contraSenha").innerHTML = "Preencha o campo Contra senha";
            $('#contraSenha').focus();
            salvar = false;
        }

        if (document.getElementById("senha").value == "") {
            document.getElementById("erro-senha").innerHTML = "Preencha o campo Senha";
            $('#senha').focus();
            salvar = false;
        }

        if (document.getElementById("email").value == "") {
            document.getElementById("erro-email").innerHTML = "Preencha o campo Email";
            $('#email').focus();
            salvar = false;
        }

        if (document.getElementById("nome-usuario").value == "") {
            document.getElementById("erro-usuario").innerHTML = "Preencha o campo Nome";
            $('#nome-usuario').focus();
            salvar = false;
        }

        if (document.getElementById("nome").value == "") {
            document.getElementById("erro-nome").innerHTML = "Preencha o campo Nome";
            $('#nome').focus();
            salvar = false;
        }

        if (document.getElementById("mandante").value == "1" && document.getElementById("visitante").value == "0") {
            document.getElementById("erro-padrao").innerHTML = "O palpite padrão deverá ser diferente de 1x0.";
            $('#mandante').focus();
            salvar = false;
        }

        if (salvar){
            $.ajax({
                url: 'paginas/cadastro1.php',
                method: 'post',
                data: new FormData($('#formulario-cadastro')[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function(retorno){
                    if (retorno == 0){
                        window.location = 'palpites.php';
                    }
                    else if (retorno == 3){
                        document.getElementById("erro-usuario").innerHTML = "Nome Usuário já cadastrado";
                    }
                    else if (retorno == 4){
                        document.getElementById("erro-email").innerHTML = "Email já cadastrado";
                    }
                },
            });

            return false;
        }

        return false;
    });   

    /*
    $('#btn-excluir').click(function(){
        var jogador = document.getElementById('jogador-id').value;

        if(jogador != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: jogador, pagina: 'jogadores' },
                success: function(data){                        
                    CarregarTabela('carregarTabelaJogadores.php'); 
                    LimparCamposForm('#formulario');

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    }); 
    */ 
});