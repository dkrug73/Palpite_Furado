$(document).ready(function(){	
			
    CarregarTabela("carregarParticipantes.php");			

    $('#resultado-tabela').on('click', 'tr', function() { 
        document.getElementById('id').value = $(this).find("td:nth-child(1)").text();
        document.getElementById('nomeParticipante').value = $(this).find("td:nth-child(2)").text();
        document.getElementById('desativar').checked = $(this).find("td:nth-child(5)").text() == "Ativo";
    }); 

    $('#btn_salvar').click(function() {
        var salvar = true;
        document.getElementById("erro-id").innerHTML = "";

        if (document.getElementById("id").value == "") {
            document.getElementById("erro-id").innerHTML = "Carregue o ID do participante a ser alterado.";
            salvar = false;
        }

        if (salvar){
            $.ajax ({
                method: "post",
                url: "paginas/desativarParticipante1.php",
                data: $('#formulario-cadastro').serialize(),
                success: function(data) {
                    var classe = '#salvar-erro';

                    if (data == 0){
                        classe = '#salvar-sucesso';
                        LimparCamposForm('#formulario-cadastro');
                        CarregarTabela("carregarParticipantes.php"); 
                    }

                    $(classe).show(); 

                    setTimeout(function() {$(classe).fadeOut('slow');}, 2000);			
                }
            });				
        }

        return false;
    });
});