$(document).ready(function(){  

    CarregarTabela("carregarParticipantesCampeonato.php");
    
    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboCampeonato.php',
        success: function(data){
            $('#campeonatoId').html(data);
        }
    });      
    
    $.ajax({
        type: 'post',
        url: 'paginas/combos/carregarComboParticipantesCampeonato.php',
        success: function(data){
            $('#participanteId').html(data);
        }
    });
      
    $('#btn-salvar').click(function(){ 
        $.ajax({
            url: 'paginas/cadastrarParticipantesCampeonato1.php',
            method: 'post',
            data: $('#formulario-cadastro').serialize(),
            success: function(data){
                CarregarTabela("carregarParticipantesCampeonato.php");

                $('#formulario-cadastro').each (function(){
                    this.reset();
                });
                
                var classe = '#salvar-erro';
                if (data == 1){
                    classe = '#salvar-sucesso';
                }

                ExibirAviso(classe);             
            }
        });  

        return false;
    });
  
    $('#resultado-tabela').on('click', 'tr', function() { 
        document.getElementById('id').value = $(this).find("td:nth-child(1)").text();
        document.getElementById('participanteId').value = $(this).find("td:nth-child(2)").attr('id');
    }); 

    $('#btn-excluir').click(function(){
        var id = document.getElementById('id').value;

        if(id != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: id, pagina: 'participantes_campeonato' },
                success: function(data){                        
                    CarregarTabela("carregarParticipantesCampeonato.php");

                    $('#formulario-cadastro').each (function(){
                        this.reset();
                    });

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    });   
        
});