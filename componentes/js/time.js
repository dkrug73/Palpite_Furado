$(document).ready(function(){  

    CarregarTabela("carregarTimes.php");	
      
    $('#btn-salvar').click(function(){ 
        var salvar = true;
        document.getElementById("erro-nome").innerHTML = "";
        document.getElementById("erro-nome-abreviado").innerHTML = "";
        document.getElementById("erro-cidade").innerHTML = "";
        document.getElementById("erro-estadio").innerHTML = "";
        
        if (document.getElementById("nome").value == 0) {
            document.getElementById("erro-nome").innerHTML = "O campo Nome do time é obrigatório.";
            salvar = false;
        }

        if (document.getElementById("nome-abreviado").value == "") {
            document.getElementById("erro-nome-abreviado").innerHTML = "O campo Nome abreviado é obrigatório.";
            salvar = false;
        }

        if (document.getElementById("cidade").value == "") {
            document.getElementById("erro-cidade").innerHTML = "O campo Cidade é obrigatório.";
            salvar = false;
        }

        if (document.getElementById("estadio").value == "") {
            document.getElementById("erro-estadio").innerHTML = "O campo Estádio é obrigatório.";
            salvar = false;
        }

        if(salvar){  
            $.ajax({
                url: 'paginas/cadastrarTime1.php',
                method: 'post',
                data: new FormData($('#formulario-cadastro')[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    CarregarTabela("carregarTimes.php");	
                    LimparCamposForm('#formulario-cadastro');
                    document.getElementById('foto-time').src = "../imagens/times/sem_imagem.png";
                    
                    var classe = '#salvar-erro';
                    if (data == 1){
                        classe = '#salvar-sucesso';
                    }

                    ExibirAviso(classe);             
                }
            });  
        }   
        
        return false;
    });
  
    $('#resultado-tabela').on('click', 'tr', function() { 
        document.getElementById('id').value = $(this).find("td:nth-child(1)").text();
        document.getElementById('nome').value = $(this).find("td:nth-child(2)").text();
        document.getElementById('nome-abreviado').value = $(this).find("td:nth-child(3)").text();
        document.getElementById('cidade').value = $(this).find("td:nth-child(4)").text();
        document.getElementById('estadio').value = $(this).find("td:nth-child(5)").text();

        var foto = $(this).find("td:nth-child(6) img").attr('src');

        if(foto == '') foto = 'imagens/times/sem_imagem.png';

        document.getElementById('foto-time').src = foto;  

        $('#nome').focus();
    }); 

    $('#btn-excluir').click(function(){
        var pontuacao = document.getElementById('id').value;

        if(pontuacao != ''){
            $.ajax({
                url: 'paginas/excluirCadastro.php',      
                method: 'post',  
                data: { id: pontuacao, pagina: 'times' },
                success: function(data){                        
                    CarregarTabela("carregarTimes.php");	
                    
                    $('#formulario-cadastro').each (function(){
                        this.reset();
                    });

                    document.getElementById('foto-time').src = "../imagens/times/sem_imagem.png";

                    var classe = '#excluir-erro';
                    if (data == 1){
                        classe = '#excluir-sucesso';
                    }

                    ExibirAviso(classe);                        
                }
            });
        }
    });   
        
});