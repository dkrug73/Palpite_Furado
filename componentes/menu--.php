<?php
    $pagina = $_SERVER['REQUEST_URI'];

    $administrador = false;

    if (isset($_SESSION['nivel'])){
        if ($_SESSION['nivel'] === '1') $administrador = true;
    }
?>



<!-- MENU -->
<nav class="navbar fixed-top navbar-expand-lg bg-info fundo text-white navbar-dark sticky-top custom">
    <div class="container">
        <div class="navbar-header">    
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>

            <a class="navbar-brand" href="index.php"> <img src="imagens/logo.png" id="logo"> </a>                                  
        </div>


        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <!--
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Início</a>
                </li>
                -->
                <li class="nav-item">
                    <a class="nav-link <?php ($pagina == "/index.php") ? active : '' ?>" href="index.php">Ranking</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link <?php ($pagina == "/palpites.php") ? active : '' ?>" href="palpites.php">Palpites</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link <?php ($pagina == "/palpitesAlheios.php") ? active : '' ?>" href="palpitesAlheios.php">Palpites Alheios</a>
                </li>


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false">Diversos</a>
                    
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="cadastro.php">Cadastro</a>
                        <a class="dropdown-item" href="regulamento.php">Regulamento</a>
                        <a class="dropdown-item" href="contato.php">Contato</a>
                    </div>
                </li>

               

                <li class="nav-item dropdown" <?= $administrador ? '' : 'style="display: none"' ?>>
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Manutenção</a>
                    
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="gravarResultado.php">Gravar resultado</a></li>
                        <li><a class="dropdown-item" href="desativarParticipante.php">Desativar participante</a></li>
                        <li><a class="dropdown-item" href="pontuacaoInicial.php">Pontuação inicial</a></li>
                        <li><a class="dropdown-item" href="recalcularTabela.php">Recalcular tabela</a></li>
                        <li><a class="dropdown-item" href="contato.php">Enviar email</a></li>
                    </ul>
                </li>  

                <li class="nav-item dropdown" <?= $administrador ? '' : 'style="display: none"' ?>>
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Cadastros</a>
                    
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="cadastrarCampeonato.php">Campeonatos</a></li>
                        <li><a class="dropdown-item" href="cadastrarJogo.php">Jogos</a></li>
                        <li><a class="dropdown-item" href="cadastrarTime.php">Time</a></li>
                        <li><a class="dropdown-item" href="cadastrarTimesCampeonato.php">Times/campeonato</a></li>
                        <li><a class="dropdown-item" href="cadastrarParticipantesCampeonato.php">Participantes/campeonato</a></li>
                    </ul>
                </li>  

                <li class="nav-item dropdown" <?= $administrador ? '' : 'style="display: none"' ?>>
                    <li class="nav-item">
                        <a class="nav-link <?php ($pagina == "/botao/index.php") ? active : '' ?>" href="botao/index.php">Botão</a>
                    </li>
                </li>  
            </ul>

            <ul class="form-inline my-2 my-lg-0"> <?php
            if (!isset($_SESSION['participanteId']))
            { ?>

                <div class="dropdown-divider"></div>
                <button type="button" class="btn btn-link text-white" data-toggle="modal" data-target="#exampleModal">
                    Entrar
                </button> 

                <button type="button" class="btn btn-link text-white" data-toggle="modal" data-target="#exampleModalSenha">
                    Esqueci minha senha
                </button>  <?php
            }
            else
            {
                echo '<li class="nav-item"><a class="nav-link" href="paginas/sair.php" style="color: lightgoldenrodyellow;">Sair</a></li>';
            }  ?>  

        </ul>
        </div>
        
        
    </div>
</nav> <!-- FIM MENU -->


