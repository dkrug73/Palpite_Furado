<?php 	
	$participanteId = '';
	$participanteNome = '';

	if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
		$participanteNome = $_SESSION['participanteNome'];
	}   
    
    if(isset($_SESSION['campeonatoId']) == false) {
        $sql = "SELECT id FROM campeonatos WHERE ativo = 1 ";	
        $rs=$conexao->query($sql);
        $reg = mysqli_fetch_array($rs);
        
        $_SESSION['campeonatoId'] = $reg['id'];
    }
?>

<header class="main-header">
    <a href="index.php" class="logo">
        <span class="logo-mini"><img src="imagens/favicon.png"></span>
        <span class="logo-lg">PALPITE<b> FURADO</b></span>
    </a>
    
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php 
                if (!empty($participanteId)) { ?>
                    <li><a href="#" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php print $participanteNome; ?></a></li>
                <?php }
                else { ?>
                    <li><a href="#" data-toggle="modal" data-target="#myModal4"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Entrar</a></li> 
                    <!--<li><a href="#" data-toggle="modal" data-target="#myModal5"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Cadastrar</a></li> -->
                    <?php
                } ?>                
                
                <li><a href="login/sair.php" data-toggle="modal" data-target="login/sair.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Sair</a></li>               
            </ul>
        </div>
    </nav>
</header>



