<div id="salvar-sucesso" style="display:none;">
    <div class="alert alert-success text-center" role="alert" id="alerta">
        Sucesso ao salvar palpites!
    </div>           
</div>

<div id="salvar-erro" style="display:none;">
    <div class="alert alert-danger text-center" role="alert" id="alerta">
        Erro ao salvar palpites!
    </div> 
</div>

<div id="excluir-sucesso" style="display:none;">
    <div class="alert alert-success text-center" role="alert" id="alerta">
        Sucesso ao excluir cadastro!
    </div>           
</div>

<div id="excluir-erro" style="display:none;">
    <div class="alert alert-danger text-center" role="alert" id="alerta">
        Erro ao excluir cadastro!
    </div> 
</div>