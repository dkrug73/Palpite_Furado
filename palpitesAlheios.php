<?php 
	SESSION_START();
	require_once('classes/Participante.php');
	require_once('classes/Campeonato.php');
	require_once('classes/ConexaoBancoDeDados.php');

	$_SESSION["pagina"] = $_SERVER['REQUEST_URI'];
	
	$conexaoBancoDeDados = new ConexaoBancoDeDados();
	$conexao = $conexaoBancoDeDados->ConectarMySql(); 

	$participante = new Participante($conexao);
   
	$participanteId = '0';	
	if(isset ($_SESSION['participanteId']) == true) {
		$participanteId = $_SESSION['participanteId'];
	}
	
	$erro = '';
    if (isset($_GET['erro'])){
        $erro = $_GET['erro'];
    }
    
	$campeonato = new Campeonato($conexao);
	
    if (!isset($_SESSION['campeonatoId'])){
		$_SESSION['campeonatoId'] = $campeonato->RetornaCampeonatoAtivo();
	}

	$campeonato->Id = $_SESSION['campeonatoId'];	
 ?>  

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Palpite Furado | Alheios</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/css/estilo.css">
	<link rel="stylesheet" href="bootstrap/fontawesomee-5.6.3-web\css/all.css">
	<link rel="stylesheet" href="bootstrap/css/css/palpites-alheios.css">	
	<script src="bootstrap/jQuery/jquery-3.3.1.min.js"></script> 
    <link rel="icon" type="image/png" href="imagens/favicon.png">

	<script type="text/JavaScript">   		
        window.onload = function(){

            var e = document.getElementById("txtParticipante");
			var itemSelecionado = e.options[e.selectedIndex].value;
			
			var participanteId = JSON.parse(sessionStorage.getItem('participanteId'));			

			if (participanteId != null){
				itemSelecionado = participanteId;
				sessionStorage.setItem('participanteId', null );
			}
			
			document.getElementById('txtParticipante').value = itemSelecionado;

			getValor(itemSelecionado, "<?php echo $campeonato->Id ?>");
        }
		
		function getValor(valor, campeonatoId){	
			setTimeout(		
				function(){	
					next = valor;
					$("#recebeValor").load(
						"paginas/tabelas/carregarTabelaPalpitesAlheios.php",{
							txtParticipante:valor,
							campeonatoId:campeonatoId
						}
					)
				}
			);				
		}
		
	</script>
</head>

<body>

	<!-- Site wrapper -->
	<div class="wrapper">

        <!-- MENU -->
		<?php include("componentes/menu.php"); ?>	


		<div class="container"> 

			<div class="form-group pt-4" style="text-align: -moz-center; text-align: -webkit-center;">
				<?php include("utils/selectParticipante.php") ?>
			</div>

			<form id="formulario-palpites" name="formulario-palpites">
				<div name="recebeValor" id="recebeValor"></div>	
			</form>	
		</div>



		</section>
        </div> <!-- /.content-wrapper -->

		<!-- MODAL ENTRAR -->		
		<?php include("modal/entrar.php"); ?>
		
		<!-- RODAPE -->		
		<?php include("componentes/rodape.php"); ?>

	</div>
	<!-- ./wrapper -->

	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>